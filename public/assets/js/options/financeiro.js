jQuery(document).ready(function($) {

    $('.moneyx').maskMoney({
        thousands: '.',
        decimal: ',',
        allowZero: true,
        affixesStay: true
    });

    // Busca Cliente/Fornecedor (Filtro)
    $("#clie_forn").autocomplete({
      source: baseUrl+"index.php/financeiro/autoCompleteClienteFornecedor",
      minLength: 2,
      select: function(event, ui) {
        $("#clie_forn_id").val(ui.item.id);
      }
    });

    // Modal Receita
    $("#clie_receita").autocomplete({
      source: baseUrl+"index.php/financeiro/autoCompleteCliente",
      minLength: 1,
      select: function(event, ui) {

        $("#clie_receita_id").val(ui.item.id);

      }
    });

    // Modal Despesa
    $("#clie_forn_despesa").autocomplete({
      source: baseUrl+"index.php/financeiro/autoCompleteClienteFornecedor",
      minLength: 1,
      select: function(event, ui) {

        $("#clie_forn_despesa_id").val(ui.item.id);
      }
    });

    $('#pago').click(function(event) {
      var flag = $(this).is(':checked');
      if (flag == true) {
        $('#divPagamento').show();
      } else {
        $('#divPagamento').hide();
      }
    });


    $('#recebido').click(function(event) {
      var flag = $(this).is(':checked');
      if (flag == true) {
        $('#divRecebimento').show();
      } else {
        $('#divRecebimento').hide();
      }
    });


    $('#pagoEditar').click(function(event) {
      var flag = $(this).is(':checked');
      if (flag == true) {
        $('#divPagamentoEditar').show();
      } else {
        $('#divPagamentoEditar').hide();
      }
    });

    $("#formaPgtoEditar").change(function() {


      var flag = null;
      var flag = document.getElementById('formaPgtoEditar').value;

      if ((flag == 'Cartão de Crédito') || (flag == 'Cartão de Débito')) {
        $('#divDadosCartao').show();
      } else {
        $('#divDadosCartao').hide();
      }

    });


    $("#formReceita").validate({
      rules: {
        descricao: {
          required: true
        },
        clie_receita: {
          required: true
        },
        valor: {
          required: true
        },
        vencimento: {
          required: true
        }

      },
      messages: {
        descricao: {
          required: 'Campo Requerido.'
        },
        clie_receita: {
          required: 'Campo Requerido.'
        },
        valor: {
          required: 'Campo Requerido.'
        },
        vencimento: {
          required: 'Campo Requerido.'
        }
      }

    });

    $("#formDespesa").validate({
      rules: {
        descricao: {
          required: true
        },
        fornecedor: {
          required: true
        },
        valor: {
          required: true
        },
        vencimento: {
          required: true
        }
      },
      messages: {
        descricao: {
          required: 'Campo Requerido.'
        },
        fornecedor: {
          required: 'Campo Requerido.'
        },
        valor: {
          required: 'Campo Requerido.'
        },
        vencimento: {
          required: 'Campo Requerido.'
        }
      }
    });
    $(document).on('click', '.excluir', function(event) {

      $("#idExcluir").val($(this).attr('idLancamento'));

    });


    $(document).on('click', '.editar', function(event) {
      $("#idEditar").val($(this).attr('idLancamento'));
      $("#idVenda").val($(this).attr('venda'));

    //   console.log($(this).attr('valor'));
    //   $("#valorEditar").val($(this).attr('valor'));
      $("#valorEditar").val($(this).attr('valor')).trigger('mask.maskMoney');

      $("#descricaoEditar").val($(this).attr('descricao'));
      $("#fornecedorEditar").val($(this).attr('cliente'));
      $("#vencimentoEditar").val($(this).attr('vencimento'));
      $("#pagamentoEditar").val($(this).attr('pagamento'));
      $("#formaPgtoEditar").val($(this).attr('formaPgto'));
      $("#tipoEditar").val($(this).attr('tipo'));

      $("#Id_cartao").val($(this).attr('autorizacao_nsu'));
      $("#bandeira_cart").val($(this).attr('bandeira_cart'));


      $("#urlAtualEditar").val($(location).attr('href'));
      var baixado = $(this).attr('baixado');
      if (baixado == 1) {
        $("#pagoEditar").attr('checked', true);
        $("#divPagamentoEditar").show();
      } else {
        $("#pagoEditar").attr('checked', false);
        $("#divPagamentoEditar").hide();
      }

      var cartao = $(this).attr('formaPgto');
      if ((cartao == 'Cartão de Crédito') || (cartao == 'Cartão de Débito')) {
        $('#divDadosCartao').show();
      } else {
        $('#divDadosCartao').hide();
      }

    });

    $(document).on('click', '#btnExcluir', function(event) {

      var id = $("#idExcluir").val();

      $.ajax({

        type: "POST",

        url: baseUrl+"index.php/financeiro/excluirLancamento",

        data: "id=" + id,

        dataType: 'json',

        success: function(data)

        {

          if (data.result == true) {

            $("#btnCancelExcluir").trigger('click');

            $("#divLancamentos").html('<div class="progress progress-striped active"><div class="bar" style="width: 100%;"></div></div>');

            window.location.href = "";



          } else {

            $("#btnCancelExcluir").trigger('click');

            alert('Ocorreu um erro ao tentar excluir produto.');

          }

        }

      });

      return false;

    });
  });


  $(document).ready(function() {
    $(".datepicker").datepicker({
      regional : 'pt-BR',
      dateFormat: "dd/mm/yy"
    });

    var qtdFormaPagamento = 1;

    $('#qtdFormaPagamento').val(qtdFormaPagamento);

    $('#novaFormaPagamento').on('click', function(event) {

      qtdFormaPagamento++;

      $('#qtdFormaPagamento').val(qtdFormaPagamento);

      $('<div class="col-xs-12" style="margin-left: 0" id="formasPagamento">' +
        '<div class="col-xs-3" style="margin-left: 0">' +
        '<label for="valor">Valor*</label>' +
        '<input class="form-control moneyx"  type="text" name="valor' + qtdFormaPagamento + '" id="valorEditar' + qtdFormaPagamento + '" />' +
        '</div>' +
        '<div class="col-xs-4" >' +
        '<label for="vencimento">Data Vencimento*</label>' +
        '<input class="form-control datepicker" type="text" name="vencimento' + qtdFormaPagamento + '" id="vencimentoEditar' + qtdFormaPagamento + '"  />' +
        '</div>' +
        '<div class="col-xs-4">' +
        '<label for="formaPgto">Forma Pgto</label>' +
        '<select name="formaPgto' + qtdFormaPagamento + '" id="formaPgtoEditar' + qtdFormaPagamento + '"  class="form-control">' +
        '<option value="Dinheiro">Dinheiro</option>' +
        '<option value="Cheque">Cheque</option>' +
        '<option value="Cartão de Crédito">Cartão de Crédito</option>' +
        '<option value="Cartão de Débito">Cartão de Débito</option>' +
        '<option value="Crédito Loja">Crédito Loja</option>' +
        '<option value="Vale Alimentação">Vale Alimentação</option>' +
        '<option value="Vale Refeição">Vale Refeição</option>' +
        '<option value="Vale Presente">Vale Presente</option>' +
        '<option value="Vale Combustível">Vale Combustível</option>' +
        '<option value="Duplicata Mercantil">Duplicata Mercantil</option>' +
        '<option value="Boleto Bancário">Boleto Bancário</option>' +
        '<option value="Transferência Bancária">Transferência Bancária</option>' +
        '<option value="Sem pagamento">Sem pagamento</option>' +
        '<option value="Outros">Outros</option>' +
        '</select>' +
        '</div>' +
        '</div>' +
        '</div>').prependTo('#formasPagamentoNova');
    });
    /* DESPESA */
    var qtdFormaPagamentoDes = 1;

    $('#qtdFormaPagamentoDes').val(qtdFormaPagamentoDes);

    $('#novaFormaPagamentoDes').on('click', function(event) {
      qtdFormaPagamentoDes++;
      $('#qtdFormaPagamentoDes').val(qtdFormaPagamentoDes);

      $('<div class="col-xs-12" style="margin-left: 0" id="formasPagamentoDes">' +
        '<div class="col-xs-3" style="margin-left: 0">' +
        '<label for="valor">Valor*</label>' +
        '<input class="form-control moneyx"  type="text" name="valor' + qtdFormaPagamentoDes + '" id="valorEditar' + qtdFormaPagamentoDes + '" />' +
        '</div>' +
        '<div class="col-xs-4" >' +
        '<label for="vencimento">Data Vencimento*</label>' +
        '<input class="form-control datepicker" type="text" name="vencimento' + qtdFormaPagamentoDes + '" id="vencimentoEditar' + qtdFormaPagamentoDes + '"  />' +
        '</div>' +
        '<div class="col-xs-4">' +
        '<label for="formaPgto">Forma Pgto</label>' +
        '<select name="formaPgto' + qtdFormaPagamentoDes + '" id="formaPgtoEditar' + qtdFormaPagamentoDes + '"  class="form-control">' +
        '<option value="Dinheiro">Dinheiro</option>' +
        '<option value="Cheque">Cheque</option>' +
        '<option value="Cartão de Crédito">Cartão de Crédito</option>' +
        '<option value="Cartão de Débito">Cartão de Débito</option>' +
        '<option value="Crédito Loja">Crédito Loja</option>' +
        '<option value="Vale Alimentação">Vale Alimentação</option>' +
        '<option value="Vale Refeição">Vale Refeição</option>' +
        '<option value="Vale Presente">Vale Presente</option>' +
        '<option value="Vale Combustível">Vale Combustível</option>' +
        '<option value="Duplicata Mercantil">Duplicata Mercantil</option>' +
        '<option value="Boleto Bancário">Boleto Bancário</option>' +
        '<option value="Sem pagamento">Sem pagamento</option>' +
        '<option value="Transferência Bancária">Transferência Bancária</option>' +
        '<option value="Outros">Outros</option>' +
        '</select>' +
        '</div>' +
        '</div>' +
        '</div>').prependTo('#formasPagamentoNovaDes');
    });
  });
