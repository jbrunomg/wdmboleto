$(document).ready(function() {
    var table = $('#tableFinanceiro').DataTable({
        "processing": true,
        "serverSide": true,
        "ajax": {
            "url": base_url + "financeiro/listarDados", // URL para buscar os dados
            "type": "POST",
            "data": function(d) {
                // Adicionando os parâmetros esperados pela DataTables à requisição
                d.draw = d.draw || 1; // 'draw' é um número de identificação de requisição
                d.start = d.start || 0; // Início dos registros para a página atual
                d.length = d.length || 10; // Quantidade de registros por página
                if($('#clie_forn_id').val() != '' && $('#clie_forn').val() != ''){
                    d.cliente = $('#clie_forn_id').val(); // Valor do campo Cliente / Fornecedor
                }else{
                    d.cliente = ''; // Valor do campo Cliente / Fornecedor
                }
                d.periodo = $('#periodo').val(); // Valor do campo Período
                d.situacao = $('#situacao').val(); // Valor do campo Situação
                d.tipo = $('#tipo').val(); // Valor do campo Tipo
                // Outros parâmetros opcionais podem ser adicionados aqui conforme necessário
                return d;
            }
        },
        "columns": [
            { "data": "idFinanceiro" },
            { "data": "financeiro_tipo" },
            { "data": "financeiro_forn_clie_usua" },
            { "data": "vencimento" },
            { "data": "status" },
            { "data": "financeiro_valor" },
            { "data": "icon" }
        ],
        "columnDefs": [
            {
                "orderable": false, // Define todas as colunas como não ordenáveis
                "targets": '_all' // Aplica a definição a todas as colunas
            }
        ],
        "responsive": true,
        "lengthChange": false, // Não permitir a alteração da quantidade de registros por página
        "autoWidth": false,
        "searching": false, // Desativa a pesquisa padrão do DataTables
        language: {
            emptyTable: "Nenhum registro encontrado",
            info: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            infoFiltered: "(Filtrados de _MAX_ registros)",
            infoEmpty: "Mostrando 0 até 0 de 0 registros",
            search: "Pesquisar ",
            paginate: {
              next: "Próximo",
              previous: "Anterior",
              first: "Primeiro",
              last: "Último"
            },
            zeroRecords: "Nenhum registro encontrado" // Tradução personalizada aqui
          },
    });

    // Ação do botão Filtrar
    $('#btnFiltrar').click(function() {
        // Recarrega os dados da tabela com base nos filtros
        table.ajax.reload();
    });
});
