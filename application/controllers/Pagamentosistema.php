<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pagamentosistema extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('pagamentosistema_model');
    }
    


    public function index()
    {

        $resultado = $this->pagamentosistema_model->listar();

        // echo '<pre>';
        // var_dump($resultado);die();

        $dadosView['dados'] = $resultado;
        $dadosView['meio']  = 'pagamentosistema/pagamento';
        $this->load->view('tema/tema',$dadosView);
    }
 

    public function visualizar()
    {
        $id = $this->uri->segment(3);

        $resultado = $this->pagamentosistema_model->getById($id);
               
        //$this->data['emitente'] = $this->mapos_model->getEmitente();

        $dadosView['results'] = $resultado;
        $dadosView['meio']  = 'pagamentosistema/visualizarPagamento';
        $this->load->view('tema/tema',$dadosView);
       
    }
}

