<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Arquivo extends CI_Controller {

	public function __construct()
	{
		parent::__construct();		
		$this->load->model('Arquivo_model'); 
		
	}

	public function index(){        
        $resultado = $this->Arquivo_model->listar();        

        $dadosView['dados'] = $resultado;
        $dadosView['meio']  = 'arquivo/listar';
        $this->load->view('tema/tema',$dadosView);
    }

    public function adicionar()
	{		

		$dadosView['permissoes']   = $this->Arquivo_model->todasPermissoes();		
		$dadosView['meio']         = 'arquivo/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
	

		$arquivo = $this->do_upload();

    	$file = $arquivo['file_name'];
    	$path = $arquivo['full_path'];
    	$url = base_url().'public/assets/arquivos/'.date('d-m-Y').'/'.$file;
    	$tamanho = $arquivo['file_size'];
    	$tipo = $arquivo['file_ext'];

    	$data = $this->input->post('data');

    	if($data == null){
    		$data = date('Y-m-d');
    	}
    	else{
    		$data = explode('/',$data);
    		$data = $data[2].'-'.$data[1].'-'.$data[0];
    	}

        $data = array(
            'documento' => $this->input->post('documento'),
            'descricao' => $this->input->post('descricao'),
            'file' => $file,
            'path' => $path,
            'url' => $url,
            'cadastro' => $data,
            'tamanho' => $tamanho,
            'tipo' => $tipo
        );

       // var_dump($data);die();
        $resultado = $this->Arquivo_model->add($data);

        if ($resultado == TRUE) {
        	$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
            redirect(base_url() . 'arquivo/adicionar/');
        } else {
        	$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
        }

	}


	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Arquivo_model->getById($id);
		$dadosView['permissoes'] = $this->Arquivo_model->todasPermissoes();
		$dadosView['meio']       = 'arquivo/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('idDocumentos');


		$data = $this->input->post('data');

    	if($data == null){
    		$data = date('Y-m-d');
    	}
    	else{
    		$data = explode('/',$data);
    		$data = $data[2].'-'.$data[1].'-'.$data[0];
    	}

		$dados = array(
            'documento' => $this->input->post('documento'),
            'descricao' => $this->input->post('descricao'),          
            'cadastro'  => $data,
            'documento_visivel'   => 1

        );


		$resultado = $this->Arquivo_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Arquivo', 'refresh');
		
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'documento_visivel' => 0
						
					);

		$resultado = $this->Arquivo_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}

	public function do_upload(){

     	
    	$date = date('d-m-Y');

		$config['upload_path'] = './public/assets/arquivos/'.$date;
	    $config['allowed_types'] = 'txt|jpg|jpeg|gif|png|pdf|PDF|JPG|JPEG|GIF|PNG|xls|xlsx|xlsm|ods';
	    //$config['max_size']     = 0;
	    //$config['max_width']  = '3000';
	    //$config['max_height']  = '2000';
	    $config['encrypt_name'] = true;


		if (!is_dir('./public/assets/arquivos/'.$date)) {

			mkdir('./public/assets/arquivos/' . $date, 0777, TRUE);

		}

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
            v($error);
			$this->session->set_flashdata('error','Erro ao fazer upload do arquivo, verifique se a extensão do arquivo é permitida.');
            redirect(base_url() . 'arquivos/adicionar/');
		}
		else
		{
			//$data = array('upload_data' => $this->upload->data());
			return $this->upload->data();
		}
	}


	public function download($id = null){
    	
       	if($id == null || !is_numeric($id)){
    		$this->session->set_flashdata('error','Erro! O arquivo não pode ser localizado.');
            redirect(base_url() . 'arquivos/');
    	}

    	$file = $this->Arquivo_model->getById($id);

    	$this->load->library('zip');

    	$path = $file->path;

		$this->zip->read_file($path); 

		$this->zip->download('file'.date('d-m-Y-H.i.s').'.zip');
    }



	public function driveArgox()
	{
		$this->download('dowloads','Argox_7.4.2_M-4.exe');
	}

	public function impressaoExe()
	{
		$this->download('dowloads','GeradorIngresso.exe');
	}

	public function odbc()
	{
		$this->download('dowloads','mysql-connector-odbc-5.1.12-win32.msi');
	}

	public function downloaddd($diretorio,$arquivo){
        
        $arquivoPath = './public/uploads/'.$diretorio."/".$arquivo;
 
        // forçamos o download no browser 
        // passando como parâmetro o path original do arquivo
        force_download($arquivoPath,null);
    }

}

/* End of file Arquivos.php */
/* Location: ./application/controllers/Arquivos.php */