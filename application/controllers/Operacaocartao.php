<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class OperacaoCartao extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Operacaocartao_model');
	}

	public function index()
	{
		$usuario      = $this->session->userdata('usuario_id');
		$usuaPadrao   = $this->session->userdata('usuario_padrao');
		$resultado          = $this->Operacaocartao_model->listarOperacao($usuario, $usuaPadrao);
		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'operacaocartao/listar';
		$this->load->view('tema/tema', $dadosView);
	}

	public function vendaCartao()
	{
		$dadosView['meio']  = 'operacaocartao/vendaCartao';
		$this->load->view('tema/tema', $dadosView);
	}

	public function selecionarCartao()
	{
		$dadosCartaoUsados = $this->Operacaocartao_model->dadosCartaoUsados($this->input->post('usuario'));

		$resultadodadosCartao = array();

		foreach ($dadosCartaoUsados as $k) {
			$resultado = $this->Operacaocartao_model->dadosCartaoBandeira($k->taxa_cartao_bandeira);
			foreach ($resultado as $j) {
				array_push($resultadodadosCartao, $j);
			}
		}
		foreach ($resultadodadosCartao as $fo) {
			echo "<option value='" . $fo->categoria_cart_id . "'>" . $fo->categoria_cart_nome_credenciadora . ' - ' . $fo->categoria_cart_descricao . "</option>";
		}
	}


	public function consultaParcela()
	{
		// $usuario = $this->session->userdata('usuario_id');
		$dados = $this->Operacaocartao_model->consultaParcela($_POST['parcela'], $_POST['cartao'], $_POST['usuario']);
		echo json_encode($dados[0]->taxa);
	}

	public function consultaParcelaBanco()
	{
		// $usuario = $this->session->userdata('usuario_id');
		$dados = $this->Operacaocartao_model->consultaParcelaBanco($_POST['parcela'], $_POST['cartao'], $_POST['usuario']);
		echo json_encode($dados[0]->taxa);
	}

	public function consultaParcelaCartao()
	{
		// $usuario = $this->session->userdata('usuario_id');
		$dados = $this->Operacaocartao_model->consultaParcelaCartao($_POST['parcela'], $_POST['cartao'], $_POST['usuario']);
		echo json_encode($dados[0]->taxa);
	}

	public function consultaCartao()
	{
		$dados = $this->Operacaocartao_model->consultaCartao($_POST['cartao']);
		echo json_encode($dados[0]->categoria_cart_nome_credenciadora);
	}

	public function consultaCartaoDescricao()
	{
		$dados = $this->Operacaocartao_model->consultaCartaoDescricao($_POST['cartao']);
		echo json_encode($dados[0]->categoria_cart_id);
	}

	public function consultaBandeira()
	{
		$dados = $this->Operacaocartao_model->consultaCartao($_POST['cartao']);
		echo json_encode($dados[0]->categoria_cart_descricao);
	}

	public function salvaSimulacao()
	{
		$dados1 = $_POST;
		$dados2 = array(
			'id_usuario'  => $this->session->userdata('usuario_id')
		);

		$dados  = array_merge($dados1, $dados2);

		$resultado = $this->Operacaocartao_model->salvaSimulacao($dados);

		if ($resultado) {
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	public function salvaVendaCartao()
	{
		//var_dump($_POST['dataOperacao']);die();
		$dados = array(
			'usuario_id'  => $this->session->userdata('usuario_id'),
			'operacao_total_boleto'  => $_POST['total_boletos'],
			'operacao_total_transacao'  => $_POST['total_transacao'],
			'operacao_taxa'        => $_POST['taxaCartao'],
			'operacao_taxa_banco'  => $_POST['taxa'],
			'operacao_status'  => 'cartao',
			'operacao_data_cadastro'  => $_POST['dataOperacao'], //date("Y/d/m", strtotime($_POST['dataOperacao']))	  		
		);

		$idOperacao = $this->Operacaocartao_model->salvaVendaCartao($dados);

		foreach ($_POST['boletos'] as $b) {

			$dadosItens = array(
				'operacao_id'  => $idOperacao,
				'itens_operacao_numero_boleto'  => $b['codigo_boleto'],
				'itens_operacao_valor_boleto'  => $b['valor'],
				'itens_operacao_bandeira_cartao'  => $b['cartao_bandeira'],
				'itens_operacao_numero_parcela'  => $b['parcela'],
				'itens_operacao_valor_transacao'  => $b['valor_tr'],
				'itens_operacao_data_cadastro'  => $_POST['dataOperacao'], //date("Y/d/m", strtotime($_POST['dataOperacao']))	  		
			);

			if ($this->Operacaocartao_model->addItensOperacao($dadosItens) == false) {
				echo json_encode(false);
			}
		}

		foreach ($_POST['autorizacoes'] as $a) {

			$dadosItensAutorizacao = array(
				'operacao_id'  => $idOperacao,
				'autorizacao_operacao_numero_nsu'  => $a['autorizacao'],
				'autorizacao_operacao_data_cadastro'  => $_POST['dataOperacao'], //date("Y/d/m", strtotime($_POST['dataOperacao']))	  		
			);

			if ($this->Operacaocartao_model->addItensAutorizacaoOperacao($dadosItensAutorizacao) == false) {
				echo json_encode(false);
			}
		}

		echo json_encode($idOperacao);
	}

	public function vendaCartaoConfirmacao($id)
	{

		$dadosCartaoUsados = $this->Operacaocartao_model->dadosCartaoUsados($this->session->userdata('usuario_id'));
		$resultadodadosCartao = array();

		foreach ($dadosCartaoUsados as $k) {
			$resultado = $this->Operacaocartao_model->dadosCartaoBandeira($k->taxa_cartao_bandeira);
			foreach ($resultado as $j) {
				array_push($resultadodadosCartao, $j);
			}
		}
		$dadosView['cartao'] = $resultadodadosCartao;
		$dadosView['meio']  = 'operacaocartao/vendaCartaoConfirmacao';
		$this->load->view('tema/tema', $dadosView);
	}


	public function imprimir()
	{
		$this->load->model('Relatorios_model');

		$id = $this->uri->segment(3);

		$resultado = $this->Operacaocartao_model->listarOperacaoImp($id);
		$autorizacao = $this->Operacaocartao_model->buscarAutorizacoes($id);
		$emitente  = $this->Relatorios_model->agenciaEmitente();

		$data['emitente'] = $emitente;
		$data['dados'] = $resultado;


		$data['autorizacao'] = $autorizacao;
		$data['titulo'] = 'Comprovante';
		$gerar['conteudo'] = $this->load->view('relatorios/operacaocartao/comprovante_operacao', $data, true);
		$html   = $this->load->view('relatorios/impressao', $gerar, true);
		gerarPdf($html, 'Comprovante_Operacao');
	}


	public function buscarItensOperacao()
	{
		$dados = $this->Operacaocartao_model->buscarItensOperacao($_POST['id']);
		echo json_encode($dados);
	}

	public function buscarOperacao()
	{
		$dados = $this->Operacaocartao_model->buscarOperacao($_POST['id']);
		echo json_encode($dados);
	}

	public function buscarAutorizacoes()
	{
		$dados = $this->Operacaocartao_model->buscarAutorizacoes($_POST['id']);
		echo json_encode($dados);
	}

	public function addItensOperacao()
	{

		$idItens = $this->Operacaocartao_model->addItensOperacaoID($_POST);
		if ($idItens) {
			echo json_encode($idItens);
		} else {
			echo json_encode(false);
		}
	}

	public function eddItensOperacao()
	{

		// if(isset($_POST['itens_operacao_tipo'])){

		$idItens = $this->Operacaocartao_model->eddItensOperacao($_POST, $_POST['itens_operacao_id']);
		if ($idItens) {
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}

		// }else{

		// 	$validador =  $this->BoletoValidator($_POST['itens_operacao_numero_boleto']);
		// 	if($validador){
		// 		$idItens = $this->Operacaocartao_model->eddItensOperacao($_POST, $_POST['itens_operacao_id']);
		// 		if ($idItens) {			
		// 			echo json_encode(true);
		// 		} else {
		// 			echo json_encode(false);
		// 		}
		// 	}else {
		// 		echo json_encode(false);
		// 	}

		// }

	}

	public function addAutorizacoesOperacao()
	{

		$idItens = $this->Operacaocartao_model->addAutorizacoesOperacaoID($_POST);
		if ($idItens) {
			echo json_encode($idItens);
		} else {
			echo json_encode(false);
		}
	}

	public function eddAutorizacoesOperacao()
	{

		$idItens = $this->Operacaocartao_model->eddAutorizacoesOperacao($_POST, $_POST['autorizacao_operacao_id']);
		if ($idItens) {
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	public function atualizarOperacao()
	{

		$ressultado = $this->Operacaocartao_model->atualizarOperacao($_POST, $_POST['operacao_id']);
		if ($ressultado) {
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	public function finalizar_venda()
	{

		$ressultado = $this->Operacaocartao_model->atualizarOperacao($_POST, $_POST['operacao_id']);
		if ($ressultado) {
			$taxa = $this->Operacaocartao_model->consultaParcela($_POST['tipo'], $_POST['cartao_bandeira'], $_POST['cliente_id'])[0]->taxa;
			$valorTotal = $_POST['operacao_total_boleto'];
			$taxaBanco = $this->Operacaocartao_model->consultaParcelaBanco($_POST['tipo'], $_POST['cartao_bandeira'], $_POST['cliente_id'])[0]->taxa;

			$valorDescontoBanco = ($valorTotal * $taxaBanco) / 100;
			$valorDesconto = ($valorTotal * $taxa) / 100;

			$valorLiquido = round($valorDesconto - $valorDescontoBanco, 2);
			$_POST['luc_ope'] = $valorLiquido;
			$this->Operacaocartao_model->addOperacaoFinanceiro($_POST);
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	public function get_cliente()
	{
		$dados = $this->Operacaocartao_model->get_cliente(limpaCPF_CNPJ($_POST['rg_cpf']));
		if ($dados) {
			echo json_encode($dados);
		} else {
			echo json_encode(false);
		}
	}

	public function adicionarCliente()
	{
		$idCliente = $this->Operacaocartao_model->adicionarCliente($_POST);
		if ($idCliente) {
			echo json_encode($idCliente);
		} else {
			echo json_encode(false);
		}
	}

	public function BoletoValidator($codigo)
	{
		$somenteNumeros = preg_replace('/[^0-9]/', '', $codigo);
		$this->load->library('BoletoValidator');
		$convenio = BoletoValidator::convenio($somenteNumeros);
		$boleto = BoletoValidator::boleto($somenteNumeros);

		if ($convenio or $boleto) {
			return true;
		}

		return false;
	}


	public function excluir()
	{
		$id = $this->input->post('id');
		$resultado = $this->Operacaocartao_model->excluir($id);

		if ($resultado) {
			echo json_encode(array('status' => true));
		} else {
			echo json_encode(array('status' => false));
		}
	}

	public function vendaCartaoCSV()
	{
		$dadosView['meio']  				= 'operacaocartao/vendaCartaoCSV';
		$dadosView['lancamentos_importar']	= $this->Operacaocartao_model->lancamentosImportar();
		$this->load->view('tema/tema', $dadosView);
	}

	public function vendaCartaoCSV2()
	{
		$dadosView['meio']  				= 'operacaocartao/vendaCartaoCSV2';
		$dadosView['lancamentos_importar']	= $this->Operacaocartao_model->lancamentosImportar();
		$this->load->view('tema/tema', $dadosView);
	}

	public function uploadCsv()
	{
		// die(json_encode($_FILES));
		$DGUB = array();

		$dadosCsv = array();
		$dadosCsvList = [];
		$data = date('Y-m-d H:i:s');
		$cliente_id = $this->input->post('cliente_id');

		if(!$this->Operacaocartao_model->verificarTaxaCliente($cliente_id)) {
			die(json_encode(array('status' => 'false', 'message' => 'Cliente sem taxa cadastrada')));
		}

		$qtdRegistros = 0;
		$qtdAdicionados = 0;

		$banco = '';
		$csv = $_FILES['csvFile']['tmp_name'];

		if ($handle = fopen($csv, 'r')) {
			$primeiroItem = fgetcsv($handle, 1000, ';')[0];
			$primeiroItem = preg_replace('/\p{C}+/u', '', $primeiroItem);
			if (trim($primeiroItem) == 'STONECODE') { /* estrutura nova Stone */
			// if (trim($primeiroItem) == 'HORA DA VENDA') { -- estrutura antiga Stone
					$banco = 'Stone';
			} else if (trim($primeiroItem) == 'Nome Cliente') {
				$banco = 'PagBank';
			}
			fclose($handle);
		} else {
			die(json_encode(array('status' => 'false', 'message' => 'Erro ao abrir o arquivo')));
		}

		if ($banco == 'PagBank') {
			if (($handle = fopen($csv, 'r')) !== false) {
				fgetcsv($handle, 1000, ';');

				while (($line = fgetcsv($handle, 1000, ';')) !== false) {
					if($line[11] == 'Cancelada') {
						continue;
					}
					$dataOriginal = $line[3];
					$dataFormatada = $this->formatDateTime($dataOriginal);

					$dadosCsv = array(
						'operacao_csv_banco'            => !empty($banco) ? $banco : null,
						'operacao_csv_numero_cartao'    => !empty($line[12]) ? $line[12] : null,
						'operacao_csv_id_transacao'     => !empty($line[2]) ? $line[2] : null,
						'operacao_csv_data_transacao'   => !empty($dataFormatada) ? $dataFormatada : null,
						'operacao_csv_tipo_operacao'    => !empty($line[6]) ? $line[6] : null,
						'operacao_csv_bandeira'         => !empty($line[5]) 
						? ($line[5] == 'AmericanExpress' 
							? preg_replace('/([a-z])([A-Z])/', '$1 $2', $line[5]) 
							: ($line[5] == 'Amex' 
								? 'American Express' 
								: $line[5])) 
						: null,
						'operacao_csv_id_maquinha'      => !empty($line[15]) ? $line[15] : null,
						'operacao_csv_valor_bruto'		=> !empty($line[8]) ? number_format(bcdiv(str_replace(['.', ','], ['', '.'], $line[8]), 1, 6), 2, '.', '') : null,
						'operacao_csv_valor_liquido' 	=> !empty($line[10]) ? number_format(bcdiv(str_replace(['.', ','], ['', '.'], $line[10]), 1, 6), 2, '.', '') : null,
						'operacao_csv_valor_taxa'       => !empty($line[9]) ? $line[9] : null,
						'operacao_csv_status'           => !empty($line[11]) ? $line[11] : null,
						'operacao_csv_codigo_nsu'       => !empty($line[13]) ? $line[13] : null,
						'operacao_csv_codigo_venda'     => !empty($line[16]) ? $line[16] : null,
						'operacao_csv_nome_cliente'     => !empty($line[0]) ? $line[0] : null,
						'operacao_csv_email_cliente'    => !empty($line[1]) ? $line[1] : null,
						'operacao_csv_id_cliente'       => !empty($cliente_id) ? $cliente_id : null,
						'operacao_csv_id_usuario'       => !empty($this->session->userdata('usuario_id')) ? $this->session->userdata('usuario_id') : null,
						'data_criacao'                  => !empty($data) ? $data : null,
					);

					if (isset($line[7])) {
						preg_match('/\d+/', $line[7], $matches);

						if (isset($matches[0])) {
							$dadosCsv['operacao_csv_parcelas'] = (int)$matches[0];
						} else {
							$dadosCsv['operacao_csv_parcelas'] = 0;
						}
					} else {
						$dadosCsv['operacao_csv_parcelas'] = 0;
					}

					// Verificar se o registro já existe na tabela antes de adicionar
					if (!$this->Operacaocartao_model->existeRegistroUnico($dadosCsv)) {
						$resultado = $this->Operacaocartao_model->addOperacaoCSV($dadosCsv);

						if ($resultado) {
							$qtdAdicionados++;
						}
					}
					$qtdRegistros++;
				}
				fclose($handle);
				$dadosCsvList = $this->Operacaocartao_model->lancamentosImportar();
				die(json_encode(array('status' => 'true', 'dados' => $dadosCsvList, 'qtdRegistros' => $qtdRegistros, 'qtdAdicionados' => $qtdAdicionados)));
			}
		} else if ($banco == 'Stone') {
			if (($handle = fopen($csv, 'r')) !== false) {
				fgetcsv($handle, 1000, ';');

				while (($line = fgetcsv($handle, 1000, ';')) !== false) {
					if($line[13] == 'Cancelada') { /* estrutura nova Stone */
					// if($line[11] == 'Cancelada') { -- estrutura antiga Stone
						continue;
					}

					$dataOriginal = $line[1];
					// $dataOriginal = $line[0]; -- estrutura antiga Stone
					$dataUltimoStatus = $line[14]; /* estrutura nova Stone */

					$dataFormatada = $this->formatDateTime($dataOriginal);
					$dataUltimoStatusFormatada = $this->formatDateTime($dataUltimoStatus); /* estrutura nova Stone */

					$dadosCsv = array( /* estrutura nova Stone */
						'operacao_csv_banco'              	=> !empty($banco) ? $banco : null,
						'operacao_csv_data_transacao'     	=> !empty($dataFormatada) ? $dataFormatada : null,
						'operacao_csv_stone_codigo'       	=> !empty($line[0]) ? $line[0] : null,
						'operacao_csv_bandeira'           	=> !empty($line[2]) 
						? ($line[2] == 'AmericanExpress' 
							? preg_replace('/([a-z])([A-Z])/', '$1 $2', $line[2]) 
							: ($line[2] == 'Amex' 
								? 'American Express' 
								: $line[2])) 
						: null,
						'operacao_csv_stone_id'           	=> !empty($line[4]) ? $line[4] : null,
						'operacao_csv_valor_bruto'		=> !empty($line[6]) ? number_format(bcdiv(str_replace(['.', ','], ['', '.'], $line[6]), 1, 6), 2, '.', '') : null,
						'operacao_csv_valor_liquido' 	=> !empty($line[7]) ? number_format(bcdiv(str_replace(['.', ','], ['', '.'], $line[7]), 1, 6), 2, '.', '') : null,
						'operacao_csv_desconto_mdr'       	=> !empty($line[8]) ? $line[8] : null,
						'operacao_csv_desconto_antecipacao' => !empty($line[9]) ? $line[9] : null,
						'operacao_csv_numero_cartao'      	=> !empty($line[10]) ? $line[10] : null,
						'operacao_csv_meio_captura'       	=> !empty($line[11]) ? $line[11] : null,
						'operacao_csv_serial_numero'      	=> !empty($line[12]) ? $line[12] : null,
						'operacao_csv_ultimo_status'      	=> !empty($line[13]) ? $line[13] : null,
						'operacao_csv_data_ultimo_status' 	=> !empty($dataUltimoStatusFormatada) ? $dataUltimoStatusFormatada : null,
						'operacao_csv_id_cliente'        	=> !empty($cliente_id) ? $cliente_id : null,
						'operacao_csv_id_usuario'        	=> !empty($this->session->userdata('usuario_id')) ? $this->session->userdata('usuario_id') : null,
						'data_criacao'                   	=> !empty($data) ? $data : null,
					);					
					
					// $dadosCsv = array( -- estrutura antiga Stone
					// 	'operacao_csv_banco'              	=> !empty($banco) ? $banco : null,
					// 	'operacao_csv_data_transacao'     	=> !empty($dataFormatada) ? $dataFormatada : null,
					// 	'operacao_csv_bandeira'           	=> !empty($line[2]) 
					// 	? ($line[2] == 'AmericanExpress' 
					// 		? preg_replace('/([a-z])([A-Z])/', '$1 $2', $line[2]) 
					// 		: ($line[2] == 'Amex' 
					// 			? 'American Express' 
					// 			: $line[2])) 
					// 	: null,
					// 	'operacao_csv_meio_captura'       	=> !empty($line[3]) ? $line[3] : null,
					// 	'operacao_csv_stone_id'           	=> !empty($line[4]) ? $line[4] : null,
					// 	'operacao_csv_valor_bruto'        	=> !empty($line[5]) ? floatval(str_replace(['.', ','], ['', '.'], $line[5])) : null,
					// 	'operacao_csv_valor_liquido'      	=> !empty($line[6]) ? floatval(str_replace(['.', ','], ['', '.'], $line[6])) : null,
					// 	'operacao_csv_desconto_antecipacao' => !empty($line[7]) ? $line[7] : null,
					// 	'operacao_csv_desconto_mdr'       	=> !empty($line[8]) ? $line[8] : null,
					// 	'operacao_csv_numero_cartao'      	=> !empty($line[9]) ? $line[9] : null,
					// 	'operacao_csv_serial_numero'      	=> !empty($line[10]) ? $line[10] : null,
					// 	'operacao_csv_ultimo_status'      	=> !empty($line[11]) ? $line[11] : null,
					// 	'operacao_csv_data_ultimo_status' 	=> !empty($line[12]) ? $line[12] : null,
					// 	'operacao_csv_stone_codigo'       	=> !empty($line[13]) ? $line[13] : null,
					// 	'operacao_csv_id_cliente'        	=> !empty($cliente_id) ? $cliente_id : null,
					// 	'operacao_csv_id_usuario'        	=> !empty($this->session->userdata('usuario_id')) ? $this->session->userdata('usuario_id') : null,
					// 	'data_criacao'                   	=> !empty($data) ? $data : null,
					// );					

					$formaPagamento = trim($line[3]); /* estrutura nova Stone */
					// $formaPagamento = trim($line[1]); -- estrutura antiga Stone
					$partes = explode(' ', $formaPagamento);

					$tipoOperacao = '';
					$parcelas = 0;

					if (isset($partes[0])) {
						$tipoOperacao = $partes[0];
						$dadosCsv['operacao_csv_tipo_operacao'] = $tipoOperacao;

						if (isset($partes[1])) {
							$parcelas = (int) rtrim($partes[1], 'x');
							$dadosCsv['operacao_csv_parcelas'] = $parcelas;
						} else {
							$dadosCsv['operacao_csv_parcelas'] = 0;
						}
					}

					if (!$this->Operacaocartao_model->existeRegistroUnico($dadosCsv)) {
						$resultado = $this->Operacaocartao_model->addOperacaoCSV($dadosCsv);

						if ($resultado) {
							$qtdAdicionados++;
						}
					}
					$qtdRegistros++;

					// $DGUB[] = $dadosCsv;
				}
				fclose($handle);
				// die(json_encode($DGUB));
				$dadosCsvList = $this->Operacaocartao_model->lancamentosImportar();
				die(json_encode(array('status' => 'true', 'dados' => $dadosCsvList, 'qtdRegistros' => $qtdRegistros, 'qtdAdicionados' => $qtdAdicionados)));
			}
		}
	}

	public function uploadCsv2()
	{
		// die(json_encode($_FILES));
		if (empty($_FILES['csvFile']) || $_FILES['csvFile']['error'] > 0 || $_FILES['csvFile']['size'] == 0 || $_FILES['csvFile']['type'] != 'text/csv') {
			die(json_encode(array('status' => 'false', 'message' => 'Selecione um arquivo CSV válido')));
		}

		try {
			$qtdRegistros	= 0;
			$qtdAdicionados = 0;
			$qtdCancelados 	= 0;
			$qtdExistente	= 0;

			$CSV 			= $_FILES['csvFile'];
			$dadosCsv 		= array();
			$dadosCsvList 	= [];

			if ($handle = fopen($CSV['tmp_name'], 'r')) {
				$primeiraLinha = fgetcsv($handle, 1000, ';')[0];
				$primeiraLinha = preg_replace('/\p{C}+/u', '', $primeiraLinha);

				if (trim($primeiraLinha) == 'STONECODE') {
					$banco = 'Stone';
				} else if (trim($primeiraLinha) == 'Nome Cliente') {
					$banco = 'PagBank';
				}

				fclose($handle);

				if ($banco == 'PagBank') {
					if (($handle = fopen($CSV['tmp_name'], 'r')) !== false) {
						fgetcsv($handle, 1000, ';');

						while (($line = fgetcsv($handle, 1000, ';')) !== false) {
							$qtdRegistros++;
							if ($line[11] == 'Cancelada') {
								$qtdCancelados++;
								continue;
							}

							$cliente_id = $this->Operacaocartao_model->getClienteMaquinha($line[15]);

							$dataOriginal 	= $line[3];
							$dataFormatada 	= $this->formatDateTime($dataOriginal);

							$dadosCsv = array(
								'operacao_csv_banco'            => !empty($banco) ? $banco : null,
								'operacao_csv_numero_cartao'    => !empty($line[12]) ? $line[12] : null,
								'operacao_csv_id_transacao'     => !empty($line[2]) ? $line[2] : null,
								'operacao_csv_data_transacao'   => !empty($dataFormatada) ? $dataFormatada : null,
								'operacao_csv_tipo_operacao'    => !empty($line[6]) ? $line[6] : null,
								'operacao_csv_bandeira'         => !empty($line[5]) 
								? ($line[5] == 'AmericanExpress' 
									? preg_replace('/([a-z])([A-Z])/', '$1 $2', $line[5]) 
									: ($line[5] == 'Amex' 
										? 'American Express' 
										: $line[5])) 
								: null,
								'operacao_csv_id_maquinha'      => !empty($line[15]) ? $line[15] : null,
								'operacao_csv_valor_bruto'		=> !empty($line[8]) ? number_format(bcdiv(str_replace(['.', ','], ['', '.'], $line[8]), 1, 6), 2, '.', '') : null,
								'operacao_csv_valor_liquido' 	=> !empty($line[10]) ? number_format(bcdiv(str_replace(['.', ','], ['', '.'], $line[10]), 1, 6), 2, '.', '') : null,
								'operacao_csv_valor_taxa'       => !empty($line[9]) ? $line[9] : null,
								'operacao_csv_status'           => !empty($line[11]) ? $line[11] : null,
								'operacao_csv_codigo_nsu'       => !empty($line[13]) ? $line[13] : null,
								'operacao_csv_codigo_venda'     => !empty($line[16]) ? $line[16] : null,
								'operacao_csv_nome_cliente'     => !empty($line[0]) ? $line[0] : null,
								'operacao_csv_email_cliente'    => !empty($line[1]) ? $line[1] : null,
								'operacao_csv_id_usuario'       => !empty($this->session->userdata('usuario_id')) ? $this->session->userdata('usuario_id') : null,
								'operacao_csv_id_cliente'       => !empty($cliente_id) ? $cliente_id : null,
								'data_criacao'                  => date('Y-m-d H:i:s'),
								'usuario_nome' 					=> $cliente_id == null ? null : $this->Operacaocartao_model->getClienteNome($cliente_id)
							);

							if (isset($line[7])) {
								preg_match('/\d+/', $line[7], $matches);

								if (isset($matches[0])) {
									$dadosCsv['operacao_csv_parcelas'] = (int)$matches[0];
								} else {
									$dadosCsv['operacao_csv_parcelas'] = 0;
								}
							} else {
								$dadosCsv['operacao_csv_parcelas'] = 0;
							}

							if (!$this->Operacaocartao_model->existeRegistroUnico($dadosCsv)) {
								$dadosCsvList[] = $dadosCsv;
								$qtdAdicionados++;
							} else {
								$qtdExistente++;
							}
						}
					}
				} elseif ($banco == 'Stone') {
					if (($handle = fopen($CSV['tmp_name'], 'r')) !== false) {
						fgetcsv($handle, 1000, ';');

						while (($line = fgetcsv($handle, 1000, ';')) !== false) {
							$qtdRegistros++;
							if ($line[13] == 'Cancelada') {
								$qtdCancelados++;
								continue;
							}

							$cliente_id = $this->Operacaocartao_model->getClienteMaquinha($line[12]);

							$dataOriginal = $line[1];
							$dataUltimoStatus = $line[14];

							$dataFormatada = $this->formatDateTime($dataOriginal);
							$dataUltimoStatusFormatada = $this->formatDateTime($dataUltimoStatus);

							$dadosCsv = array(
								'operacao_csv_banco'              	=> !empty($banco) ? $banco : null,
								'operacao_csv_data_transacao'     	=> !empty($dataFormatada) ? $dataFormatada : null,
								'operacao_csv_stone_codigo'       	=> !empty($line[0]) ? $line[0] : null,
								'operacao_csv_bandeira'           	=> !empty($line[2])
									? ($line[2] == 'AmericanExpress'
										? preg_replace('/([a-z])([A-Z])/', '$1 $2', $line[2])
										: ($line[2] == 'Amex'
											? 'American Express'
											: $line[2]))
									: null,
								'operacao_csv_stone_id'           	=> !empty($line[4]) ? $line[4] : null,
								'operacao_csv_valor_bruto'			=> !empty($line[6]) ? number_format(bcdiv(str_replace(['.', ','], ['', '.'], $line[6]), 1, 6), 2, '.', '') : null,
								'operacao_csv_valor_liquido' 		=> !empty($line[7]) ? number_format(bcdiv(str_replace(['.', ','], ['', '.'], $line[7]), 1, 6), 2, '.', '') : null,
								'operacao_csv_desconto_mdr'       	=> !empty($line[8]) ? $line[8] : null,
								'operacao_csv_desconto_antecipacao' => !empty($line[9]) ? $line[9] : null,
								'operacao_csv_numero_cartao'      	=> !empty($line[10]) ? $line[10] : null,
								'operacao_csv_meio_captura'       	=> !empty($line[11]) ? $line[11] : null,
								'operacao_csv_serial_numero'      	=> !empty($line[12]) ? $line[12] : null,
								'operacao_csv_ultimo_status'      	=> !empty($line[13]) ? $line[13] : null,
								'operacao_csv_data_ultimo_status' 	=> !empty($dataUltimoStatusFormatada) ? $dataUltimoStatusFormatada : null,
								'operacao_csv_id_usuario'        	=> !empty($this->session->userdata('usuario_id')) ? $this->session->userdata('usuario_id') : null,
								'operacao_csv_id_cliente'        	=> !empty($cliente_id) ? $cliente_id : null,
								'data_criacao'                   	=> date('Y-m-d H:i:s'),
								'usuario_nome' 						=> $cliente_id == null ? null : $this->Operacaocartao_model->getClienteNome($cliente_id)
							);

							$formaPagamento = trim($line[3]);
							$partes = explode(' ', $formaPagamento);

							$tipoOperacao = '';
							$parcelas = 0;

							if (isset($partes[0])) {
								$tipoOperacao = $partes[0];
								$dadosCsv['operacao_csv_tipo_operacao'] = $tipoOperacao;

								if (isset($partes[1])) {
									$parcelas = (int) rtrim($partes[1], 'x');
									$dadosCsv['operacao_csv_parcelas'] = $parcelas;
								} else {
									$dadosCsv['operacao_csv_parcelas'] = 0;
								}
							}

							if (!$this->Operacaocartao_model->existeRegistroUnico($dadosCsv)) {
								$dadosCsvList[] = $dadosCsv;
								$qtdAdicionados++;
							} else {
								$qtdExistente++;
							}
						}
					}
				}

				// $dadosCsvList = $this->Operacaocartao_model->lancamentosImportar();
				die(json_encode(array(
					'status' 				=> 'true',
					'qtdRegistros' 			=> $qtdRegistros,
					'qtdAdicionados' 		=> $qtdAdicionados,
					'qtdExistente'			=> $qtdExistente,
					'qtdCancelados' 		=> $qtdCancelados,
					'dados' 				=> $dadosCsvList
				)));
			} else {
				die(json_encode(array('status' => 'false', 'message' => 'Erro ao abrir o arquivo')));
			}
		} catch (Exception $e) {
			fclose($handle);
			die(json_encode(
				array(
					'status' 	=> 'false',
					'message' 	=> 'Houve um erro ao processar o arquivo, tente novamente!'
				)
			));
		}
	}

	public function addUploadCsv2()
	{
		$dados = json_decode($this->input->post('dados'), true);
		if (empty($dados)) {
			die(json_encode(array('status' => 'false', 'message' => 'Nenhum registro para importar!')));
		}

		try {
			$this->db->trans_start();

			foreach($dados as $dado) {
				unset($dado['usuario_nome']);
				$resultado = $this->Operacaocartao_model->addOperacaoCSV($dado);

				if (!$resultado) {
					$this->db->trans_rollback();
					die(json_encode(array('status' => 'false', 'message' => 'Houve um erro ao processar o arquivo, tente novamente!')));
				}
			}
			
			$this->db->trans_complete();
			
			if ($this->db->trans_status() === false) {
				$this->db->trans_rollback();
				die(json_encode(array('status' => 'false', 'message' => 'Erro ao salvar os dados, transação não concluída!')));
			} else {
				die(json_encode(array('status' => 'true', 'message' => 'Dados importados com sucesso!')));
			}
		} catch (Exception $e) {
			die(json_encode(array('status' => 'false', 'message' => 'Houve um erro inesperado ao processar o arquivo, tente novamente!')));
		}
	}

	public function importarCsvUnico()
	{
		$taxa = $this->Operacaocartao_model->consultaParcelaBanco($_POST['parcela'], $_POST['cartao'], $_POST['usuario']);
	}

	public function cancelarOperacoes()
	{
		$dados = json_decode($this->input->post('dados'), true);

		try {
			foreach ($dados as $d) {
				$this->Operacaocartao_model->cancelarOperacao($d['id']);
			}
			die(json_encode(array('status' => 'true')));
		} catch (Exception $e) {
			die(json_encode(array('status' => 'false')));
		}
	}

	public function importarOperacoes()
	{
		$dados = json_decode($this->input->post('dados'), true);
		// die(json_encode($dados));
		$dadosOperacao = array();
		try {
			foreach ($dados as $d) {
				// if ($d['tipo'] == 'Crédito' or $d['tipo'] == 'Débito') {
					$categoria_cartao = $this->Operacaocartao_model->dadosCartaoUsadosCsv($d['cliente'], !empty($d['bandeira']) ? $d['bandeira'] : null, !empty($d['tipo']) ? $d['tipo'] : null);
					$taxa             = empty($categoria_cartao) ? 0 : $this->Operacaocartao_model->consultaParcelaCsv($d['tipo'],$d['parcelas'], $categoria_cartao, $d['cliente'])[0]->taxa;
					$taxaBanco        = empty($categoria_cartao) ? 0 : $this->Operacaocartao_model->consultaParcelaBancoCsv($d['tipo'],$d['parcelas'], $categoria_cartao, $d['cliente'])[0]->taxa;
					
					// die(json_encode($d));

					$valor_bruto = $d['valor_bruto'];
					// $valor_liquido = $d['valor_liquido'];
					$valor = ($valor_bruto * $taxa) / 100;

					// $valor = ceil($valor_bruto - $valor * 100) / 100;
					$valor = round($valor_bruto - $valor, 2);

					// $data = 
					$dadosOperacao = array(
						'usuario_id'                => $d['usuario'],
						'cliente_id'                => $d['cliente'],
						'operacao_total_boleto'     => $valor_bruto,
						'operacao_total_transacao'  => $valor,
						'operacao_taxa'             => $taxa,
						'operacao_taxa_banco'       => $taxaBanco,
						'operacao_status' 			=> ($d['tipo'] == 'Crédito' || $d['tipo'] == 'Débito') ? 'cartao' : $d['tipo'],
						'operacao_data_cadastro'    => $d['data_operacao']
					);
					
					$dados_salvar = array(
						'usuario_id'                => $d['usuario'],
						'cliente_id'                => $d['cliente'],
						'operacao_total_boleto'     => $valor_bruto,
						'operacao_total_transacao'  => $valor,
						'operacao_taxa'             => $taxa,
						'operacao_taxa_banco'       => $taxaBanco,
						'operacao_status' 			=> ($d['tipo'] == 'Crédito' || $d['tipo'] == 'Débito') ? 'cartao' : $d['tipo'],
						'operacao_data_cadastro'    => $d['data_operacao'],
						'operacao_faturado' 	   	=> 1
					);

					// die(json_encode($dadosOperacao));
					$idOperacao = $this->Operacaocartao_model->salvaVendaCartao($dados_salvar);
					// die(json_encode($d));
					$bandeira_cartao = $this->Operacaocartao_model->dadosCartaoBandeira($categoria_cartao);					
					// die(json_encode($bandeira_cartao));
					$dadosItens = array(
						'operacao_id'  => $idOperacao,
						'itens_operacao_numero_boleto'  	=> 'CARTÃO DE SIMULAÇÃO',
						'itens_operacao_valor_boleto'  		=> $valor_bruto,
						'itens_operacao_bandeira_cartao'  	=> $d['tipo'] != 'Pix' ? $bandeira_cartao[0]->categoria_cart_nome_credenciadora . ' - ' . $bandeira_cartao[0]->categoria_cart_descricao : $d['tipo'],
						'itens_operacao_numero_parcela' 	=> $d['tipo'] == 'Débito' ? 'debito' : ((strlen($d['parcelas']) == 1) ? '0' . $d['parcelas'] : $d['parcelas']),
						'itens_operacao_valor_transacao'  	=> $valor,
						'itens_operacao_data_cadastro'  	=> $d['data_operacao']
					);

					// die(json_encode($dadosItens));

					if ($this->Operacaocartao_model->addItensOperacao($dadosItens) == false) {
						die(json_encode(array('status' => 'false')));
					}

					$dadosItensAutorizacao = array(
						'operacao_id'  => $idOperacao,
						'autorizacao_operacao_numero_nsu'  	=> $d['codigo'],
						'autorizacao_operacao_data_cadastro'  => $d['data_operacao']
					);

					if ($this->Operacaocartao_model->addItensAutorizacaoOperacao($dadosItensAutorizacao) == false) {
						die(json_encode(array('status' => 'false')));
					}

					$dadosOperacaoCsv = array(
						'operacao_csv_id' => $d['id'],
						'operacao_csv_id_operacao' => $idOperacao
					);
					// Importa a operação
					$this->Operacaocartao_model->importarOperacao($dadosOperacaoCsv);
					// die(json_encode(array('status' => 'true')));
					$dadosOperacao['operacao_id'] 		= $idOperacao;
					$dadosOperacao['dataOperacao'] 		= $d['data_operacao'];
					$dadosOperacao['cliente_nome'] 		= $this->Operacaocartao_model->getClienteNome($d['cliente']) . ' | USUA';
					$dadosOperacao['autorizacoes'] 		= $d['codigo'];
					$dadosOperacao['cartao_bandeira'] 	= $bandeira_cartao[0]->categoria_cart_id; //$d['tipo'] == 'Crédito' || $d['tipo'] == 'Débito' ? $bandeira_cartao : $d['tipo'];


					// Definindo as variáveis
					$k = $taxa;
					$a = $valor_bruto;
					$b = $taxaBanco;

					// Calculando c
					$c = $a * ($b / 100);

					// Calculando d
					$d = $a - $c;

					// Calculando e
					$e = $a * ($k / 100);

					// Calculando f
					$f = $a - $e;

					// Calculando g
					$g = $d - $f;

					$dadosOperacao['luc_ope']    = $g;
				
					
					// die(json_encode($dadosOperacao));
					$this->Operacaocartao_model->addOperacaoFinanceiro($dadosOperacao, true);
				// }
			}
			die(json_encode(array('status' => 'true')));
		} catch (Exception $e) {
			die(json_encode(array('status' => 'false')));
		}
	}

	public function visualizarOperacoes()
	{
		// die(json_encode($this->input->post()));
		$tipo = $this->input->post('tipo');
		$data = $this->input->post('data');
		$dados = $this->Operacaocartao_model->visualizarOperacoes($data, $tipo);
		// die(json_encode($dados));

		if ($dados) {
			die(json_encode(array('status' => 'true', 'dados' => $dados)));
		} else {
			die(json_encode(array('status' => 'false')));
		}
	}

	public function excluirOperacoes() 
	{
		$json = file_get_contents('php://input');    
		$dados = json_decode($json, true);

		$resultado = $this->Operacaocartao_model->excluirOperacoes($dados['ids']);
		die(json_encode(array('status' => $resultado)));
	}

	public function autoCompleteCliente()
	{
		$termo = strtolower($this->input->get('term'));
		$this->Operacaocartao_model->buscarClientes($termo);
		// echo json_encode($resultado);
	}


	//		FUNÇÕES PRIVATE UTIL.		//
	private function formatDateTime($dateStr) {
		$formatInput = (strlen($dateStr) > 16) ? 'd/m/Y H:i:s' : 'd/m/Y H:i';
		$dateTime = DateTime::createFromFormat($formatInput, $dateStr);
		
		if ($dateTime === false) {
			return '0000-00-00 00:00:00';
		} else {
			$formatOutput = (strlen($dateStr) > 16) ? 'Y-m-d H:i:s' : 'Y-m-d H:i';
			return $dateTime->format($formatOutput);
		}
	}
	//		/FUNÇÕES PRIVATE UTIL.		//
}
