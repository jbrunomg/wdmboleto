<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Simulador extends CI_Controller {
    
    function __construct() {

        parent::__construct(); 
        $this->load->model('simulador_model');
        
       
    }

    public function index(){

        // $this->load->library('form_validation');    
        // $this->data['custom_error'] = '';        

        // $this->data['menuSimuladorCart'] = 'Simulador';
        $this->data['permissoes']   =  $this->simulador_model->todasPermissoes();
		$this->data['meio']         = 'simulador/simular';
        $this->load->view('tema/tema',$this->data);
	}


    public function simular() {

        $this->load->library('form_validation');    
        $this->data['custom_error'] = '';

        $valorProd  = $this->input->post('vlprod');  // str_replace (',', '.', str_replace ('.', '', $this->input->post('vlprod')));
        $valorEntr  = $this->input->post('vlentr');  // str_replace (',', '.', str_replace ('.', '', $this->input->post('vlentr'))); 
        $qtdParcela = 18;

        // var_dump( $valorProd.' - '.$valorEntr );die();

        $valorSimular = ($valorProd - (($valorEntr) ? $valorEntr : 0) );

       // var_dump($valorSimular);die();

        $dados = array(
                'valorCompra'    => $this->input->post('vlprod'),
                'valorEntrada'   => (($this->input->post('vlentr')) ? $this->input->post('vlentr') : '0.00'),
                'valorSimular'   => $valorSimular,
                'qtdParcela'     => 18,      
            );

         $this->data['simulado'] = $dados;

        // var_dump( $this->data['simulado']['valorCompra']);die();

        $this->data['resultado']  = $this->simulador_model->getSimular($valorSimular);   

        //var_dump($dadosView['resultado']);die();     

        $this->data['permissoes']   =  $this->simulador_model->todasPermissoes();
        $this->data['meio']         = 'simulador/simular';
        $this->load->view('tema/tema',$this->data);
    } 


    public function configurar(){      

        $this->data['taxas']        =  $this->simulador_model->pegarTaxas();
        $this->data['permissoes']   =  $this->simulador_model->todasPermissoes();
        $this->data['meio']         = 'simulador/configuracaoCartao';

        $this->load->view('tema/tema',$this->data);

    }

    public function editarTaxasExe()
    {   

        $data = $_POST; 

        // var_dump($data);die();

        $valores = array(); 

        foreach ($data as $chave => $dados) {

            $valores = array(
                    'percentual_parcela' => $dados
                    );

            $resultado = $this->simulador_model->editarTaxasExe($valores,$chave);

        }


        if ($resultado) {           
            $this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
        }else{
            $this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
        }

        redirect('simulador/configurar', 'refresh');
      
        
    }


	
    
}

