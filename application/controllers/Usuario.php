<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Usuario_model');
	}

	public function index()
	{

		$resultado = $this->Usuario_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'usuario/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function cidadesPorId()
	{
		$uf = $this->input->post('estado');
        
        $cidades = $this->Usuario_model->tadasCidadesIdEstado($uf);
        
        foreach ($cidades as $cidade) {
           echo "<option value='".$cidade->id."'>".$cidade->nome."</option>";
        }
	}

	public function adicionar()
	{
		$dadosView['estados']    = $this->Usuario_model->todosEstados();
		$dadosView['permissoes'] = $this->Usuario_model->tadasPermissoes();
		$dadosView['meio']       = 'usuario/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	

		$dados = array(
		  'usuario_nome'         => $this->input->post('nome'),
		  'usuario_rg'           => $this->input->post('rg'),
		  'usuario_cpf'          => $this->input->post('cpf'),
		  'usuario_rua'          => $this->input->post('endereco'),
		  'usuario_numero'       => $this->input->post('numero'),
		  'usuario_cep'          => $this->input->post('cep'),
		  'usuario_bairro'       => $this->input->post('bairro'),
		  'usuario_complemento'  => $this->input->post('complemento'),
		  'usuario_cidade'       => $this->input->post('cidade'),
		  'usuario_estado'       => $this->input->post('estado'),
		  'usuario_email'        => $this->input->post('email'),
		  'usuario_senha'        => sha1(md5($this->input->post('senha'))),
		  'usuario_telefone'     => $this->input->post('telefone'),
		  //'usuario_celular'      => $this->input->post(),
		  'usuario_data_cadastro'=> date('Y-m-d'),
		  'usuario_permissoes_id'=> $this->input->post('permissao'),
		  'usuario_sexo'         => $this->input->post('sexo'),
		  //'usuario_imagem'       => $this->input->post(), falta fazer upload de imagem
		  'usuario_situacao'     => $this->input->post('situacao'),
		  'usuario_atualizacao'  => date('Y-m-d H:i:s'),
		  'usuario_visivel'      => 1,
		  'usuario_login'        => $this->input->post('login')
		);

		$resultado = $this->Usuario_model->inserir($dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Usuario', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Usuario_model->pegarPorId($id);
		$dadosView['cidades']    = $this->Usuario_model->tadasCidadesIdEstado($dadosView['dados'][0]->usuario_estado);
		$dadosView['estados']    = $this->Usuario_model->todosEstados();
		$dadosView['permissoes'] = $this->Usuario_model->tadasPermissoes();
		$dadosView['meio']       = 'usuario/editar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('usuario_id');

		$dados = array(
		  'usuario_nome'         => $this->input->post('nome'),
		  'usuario_rg'           => $this->input->post('rg'),
		  'usuario_cpf'          => $this->input->post('cpf'),
		  'usuario_rua'          => $this->input->post('endereco'),
		  'usuario_numero'       => $this->input->post('numero'),
		  'usuario_cep'          => $this->input->post('cep'),
		  'usuario_bairro'       => $this->input->post('bairro'),
		  'usuario_complemento'  => $this->input->post('complemento'),
		  'usuario_cidade'       => $this->input->post('cidade'),
		  'usuario_estado'       => $this->input->post('estado'),
		  'usuario_email'        => $this->input->post('email'),		  
		  'usuario_telefone'     => $this->input->post('telefone'),
		  //'usuario_celular'      => $this->input->post(),
		  'usuario_data_cadastro'=> date('Y-m-d'),
		  'usuario_permissoes_id'=> $this->input->post('permissao'),
		  'usuario_sexo'         => $this->input->post('sexo'),
		  //'usuario_imagem'       => $this->input->post(), falta fazer upload de imagem
		  'usuario_situacao'     => $this->input->post('situacao'),
		  'usuario_atualizacao'  => date('Y-m-d H:i:s'),
		  'usuario_visivel'      => 1,
		  'usuario_login'        => $this->input->post('login')
		);

		if($this->input->post('senha') != ''){
			$senha = array('usuario_senha' => sha1(md5($this->input->post('senha'))));

			$dados = array_merge($dados, $senha);
		}

		$resultado = $this->Usuario_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Usuario', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'usuario_visivel' => 0,
						'usuario_atualizacao' => date('Y-m-d H:i:s')
					);

		$resultado = $this->Usuario_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Usuario_model->pegarPorId($id);
		$dadosView['cidades']    = $this->Usuario_model->tadasCidadesIdEstado($dadosView['dados'][0]->usuario_estado);
		$dadosView['estados']    = $this->Usuario_model->todosEstados();
		$dadosView['permissoes'] = $this->Usuario_model->tadasPermissoes();
        $dadosView['maquinas'] = $this->Usuario_model->listarMaquinasDoUsuario($id);
		$dadosView['meio']       = 'usuario/visualizar';
		$this->load->view('tema/tema',$dadosView);
	}
    public function excluirMaquina()
    {
        $id = $this->input->post('id');

        try {
            $this->Usuario_model->excluirMaquina($id);
            echo json_encode(array('status' => true));
        } catch (\Error $e) {
            echo json_encode(array('status' => false));
        }
    }
}

/* End of file Usuario.php */
/* Location: ./application/controllers/Usuario.php */