<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operacao extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Operacao_model');
	}

	public function index()
	{	
		$usuario      = $this->session->userdata('usuario_id');
		$usuaPadrao   = $this->session->userdata('usuario_padrao');
		$resultado          = $this->Operacao_model->listarOperacao($usuario,$usuaPadrao);
		$dadosView['dados'] = $resultado;		
		$dadosView['meio']  = 'operacao/listar';
		$this->load->view('tema/tema',$dadosView);
	}

    public function vendaBoleto()
	{
		$dadosCartaoUsados = $this->Operacao_model->dadosCartaoUsados($this->session->userdata('usuario_id'));
		$resultadodadosCartao = array();

		foreach ($dadosCartaoUsados as $k) {
			$resultado = $this->Operacao_model->dadosCartaoBandeira($k->taxa_cartao_bandeira);
			foreach ($resultado as $j) {
				array_push($resultadodadosCartao, $j);
 			}		
		}

		$dadosView['cartao'] = $resultadodadosCartao;
		$dadosView['meio']  = 'operacao/vendaBoleto';
		$this->load->view('tema/tema',$dadosView);
	}

	public function consultaParcela()
	{
		$usuario = $this->session->userdata('usuario_id');
		$dados = $this->Operacao_model->consultaParcela($_POST['parcela'], $_POST['cartao'], $usuario);
		echo json_encode($dados[0]->taxa);
	}

	public function consultaCartao()
	{
		$dados = $this->Operacao_model->consultaCartao($_POST['cartao']);
		echo json_encode($dados[0]->categoria_cart_nome_credenciadora);
	}

	public function consultaCartaoDescricao()
	{
		$dados = $this->Operacao_model->consultaCartaoDescricao($_POST['cartao']);
		echo json_encode($dados[0]->categoria_cart_id);
	}

	public function consultaBandeira()
	{
		$dados = $this->Operacao_model->consultaCartao($_POST['cartao']);
		echo json_encode($dados[0]->categoria_cart_descricao);
	}

	public function salvaSimulacao()
	{
		$dados1 = $_POST; 	
		$dados2 = array( 	
			'id_usuario'  => $this->session->userdata('usuario_id') 	  		
		);

		$dados  = array_merge($dados1,$dados2);
		
		$resultado = $this->Operacao_model->salvaSimulacao($dados);

		if ($resultado) {			
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	public function salvaVendaBoleto()
	{
		$dados = array( 	
			'usuario_id'  => $this->session->userdata('usuario_id'),
			'operacao_total_boleto'  => $_POST['total_boletos'],  
			'operacao_total_transacao'  => $_POST['total_transacao'],  
			'operacao_data_cadastro'  => date("Y/m/d")	  		
		);

		$idOperacao = $this->Operacao_model->salvaVendaBoleto($dados);

		foreach ($_POST['boletos'] as $b) {
			
			$dadosItens = array( 	
				'operacao_id'  => $idOperacao,
				'itens_operacao_numero_boleto'  => $b['codigo_boleto'], 
				'itens_operacao_valor_boleto'  => $b['valor'],  
				'itens_operacao_bandeira_cartao'  => $b['cartao_bandeira'],
				'itens_operacao_numero_parcela'  => $b['parcela'],
				'itens_operacao_valor_transacao'  => $b['valor_tr'],
				'itens_operacao_data_cadastro'  => date("Y/m/d")	  		
			);

			if ($this->Operacao_model->addItensOperacao($dadosItens) == false){ echo json_encode(false); }
		}

		foreach ($_POST['autorizacoes'] as $a) {
			
			$dadosItensAutorizacao = array( 	
				'operacao_id'  => $idOperacao,
				'autorizacao_operacao_numero_nsu'  => $a['autorizacao'], 
				'autorizacao_operacao_data_cadastro'  => date("Y/m/d")	  		
			);

			if ($this->Operacao_model->addItensAutorizacaoOperacao($dadosItensAutorizacao) == false){ echo json_encode(false); }
		}

		echo json_encode($idOperacao);
	}

	public function vendaBoletoConfirmacao($id)
	{

		$dadosCartaoUsados = $this->Operacao_model->dadosCartaoUsados($this->session->userdata('usuario_id'));
		$resultadodadosCartao = array();

		foreach ($dadosCartaoUsados as $k) {
			$resultado = $this->Operacao_model->dadosCartaoBandeira($k->taxa_cartao_bandeira);
			foreach ($resultado as $j) {
				array_push($resultadodadosCartao, $j);
 			}		
		}
		$dadosView['cartao'] = $resultadodadosCartao;
		$dadosView['meio']  = 'operacao/vendaBoletoConfirmacao';
		$this->load->view('tema/tema',$dadosView);
	}


	public function imprimir()
	{	
		$this->load->model('Relatorios_model');

		$id = $this->uri->segment(3);		

		$resultado = $this->Operacao_model->listarOperacaoImp($id);
		$autorizacao = $this->Operacao_model->buscarAutorizacoes($id);
		$emitente  = $this->Relatorios_model->agenciaEmitente();

		$data['emitente'] = $emitente;
		$data['dados'] = $resultado;

		
		$data['autorizacao'] = $autorizacao;
		$data['titulo'] = 'Comprovante';
		$gerar['conteudo'] = $this->load->view('relatorios/operacao/comprovante_operacao',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		gerarPdf($html,'Comprovante_Operacao');
		
	}


	public function buscarItensOperacao()
	{
		$dados = $this->Operacao_model->buscarItensOperacao($_POST['id']);
		echo json_encode($dados);
	}

	public function buscarOperacao()
	{
		$dados = $this->Operacao_model->buscarOperacao($_POST['id']);
		echo json_encode($dados);
	}

	public function buscarAutorizacoes()
	{
		$dados = $this->Operacao_model->buscarAutorizacoes($_POST['id']);
		echo json_encode($dados);
	}

	public function addItensOperacao(){
		
		$idItens = $this->Operacao_model->addItensOperacaoID($_POST);
		if ($idItens) {			
			echo json_encode($idItens);
		} else {
			echo json_encode(false);
		}
	}

	public function eddItensOperacao(){

		if(isset($_POST['itens_operacao_tipo'])){
			
			$idItens = $this->Operacao_model->eddItensOperacao($_POST, $_POST['itens_operacao_id']);
			if ($idItens) {			
				echo json_encode(true);
			} else {
				echo json_encode(false);
			}

		}else{

			$validador =  $this->BoletoValidator($_POST['itens_operacao_numero_boleto']);
			if($validador){
				$idItens = $this->Operacao_model->eddItensOperacao($_POST, $_POST['itens_operacao_id']);
				if ($idItens) {			
					echo json_encode(true);
				} else {
					echo json_encode(false);
				}
			}else {
				echo json_encode(false);
			}

		}

	}

	public function addAutorizacoesOperacao(){
		
		$idItens = $this->Operacao_model->addAutorizacoesOperacaoID($_POST);
		if ($idItens) {			
			echo json_encode($idItens);
		} else {
			echo json_encode(false);
		}
	}

	public function eddAutorizacoesOperacao(){
		
		$idItens = $this->Operacao_model->eddAutorizacoesOperacao($_POST, $_POST['autorizacao_operacao_id']);
		if ($idItens) {			
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	public function atualizarOperacao(){
		
		$ressultado = $this->Operacao_model->atualizarOperacao($_POST, $_POST['operacao_id']);
		if ($ressultado) {			
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	public function finalizar_venda(){
		
		$ressultado = $this->Operacao_model->atualizarOperacao($_POST, $_POST['operacao_id']);
		if ($ressultado) {			
			echo json_encode(true);
		} else {
			echo json_encode(false);
		}
	}

	public function get_cliente()
	{
		$dados = $this->Operacao_model->get_cliente(limpaCPF_CNPJ($_POST['rg_cpf']));
		if ($dados) {			
			echo json_encode($dados);
		} else {
			echo json_encode(false);
		}
	}

	public function adicionarCliente()
	{	
		$idCliente = $this->Operacao_model->adicionarCliente($_POST);
		if ($idCliente) {			
			echo json_encode($idCliente);
		} else {
			echo json_encode(false);
		}
	}

	public function BoletoValidator($codigo)
	{	
		$somenteNumeros = preg_replace('/[^0-9]/', '', $codigo);
		$this->load->library('BoletoValidator');
		$convenio = BoletoValidator::convenio($somenteNumeros);
		$boleto = BoletoValidator::boleto($somenteNumeros);

		if($convenio or $boleto){
			return true;
		}

		return false;

	}
	
	
}
