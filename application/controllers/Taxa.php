<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taxa extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Taxa_model');
	}

	public function index()
	{

	    $resultado = $this->Taxa_model->listar();
	    $dadosView['bandeira']   = $this->Taxa_model->todasBandeiras();

		//var_dump($resultado);die();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'taxaassociado/listar';
		$this->load->view('tema/tema',$dadosView);
	}



	public function adicionar()
	{		
		$dadosView['permissoes'] = $this->Taxa_model->todasPermissoes();
		$dadosView['bandeira']   = $this->Taxa_model->todasBandeiras();
		$dadosView['usuario']    = $this->Taxa_model->todosAssociados();

		$dadosView['meio']       = 'taxaassociado/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
     
		$dados = array(		  
		  'taxa_cartao_bandeira'   => implode(',',$this->input->post('taxa_cartao_bandeira')),
		  'taxa_usuario_id'        => $this->input->post('taxa_usuario_id'),	  
		  'taxa_data_cadastro'     => date('Y-m-d'),		    	  
		  'taxa_visivel'   => 1
		  
		);


  		// echo '<pre>';
		// var_dump($dados);die();

	    if (is_numeric($id = $this->Taxa_model->inserir($dados)) ) { 

	        $this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');

	        $dadosView['dados']      = $this->Taxa_model->pegarPorId($id);

	        $dadosView['taxaBand']   = explode(',',$dadosView['dados'][0]->taxa_cartao_bandeira);

	        $dadosView['bandeira']   = $this->Taxa_model->todasBandeiras();
			$dadosView['permissoes'] = $this->Taxa_model->todasPermissoes();
		    $dadosView['meio']       = 'taxaassociado/editar';


			$this->load->view('tema/tema',$dadosView);

	    } else {
	      
	       $this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');

	       redirect('Taxa', 'refresh');
	    }


	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']        = $this->Taxa_model->pegarPorId($id);
		$dadosView['bandeira']     = $this->Taxa_model->todasBandeiras();
	    $dadosView['taxaBand']     = explode(',',$dadosView['dados'][0]->taxa_cartao_bandeira);		
		$dadosView['permissoes']   = $this->Taxa_model->todasPermissoes();
		$dadosView['meio']         = 'taxaassociado/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('taxa_id');

		$dados = $this->input->post();

		// var_dump($dados);die();

		$resultado = $this->Taxa_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Taxa', 'refresh');

	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'taxa_visivel' => 0
						
					);

		$resultado = $this->Taxa_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Taxa_model->pegarPorId($id);	
		$dadosView['permissoes']  = $this->Taxa_model->todasPermissoes();	

		$dadosView['meio']       = 'taxaassociado/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}


	public function selecionarAssociado(){
      
      $associado   = $this->input->post('associado');
      
      $bandeira    = $this->Taxa_model->pegarAssociado($associado); // Pegar todas as bandeira vinculado ao associado.
      
      $bandeira    = explode(',', $bandeira[0]->taxa_cartao_bandeira);

      //var_dump($bandeira);die();
      

      $usuario   = $this->Taxa_model->pegarBandeiraLivre($bandeira); // pegar bandeiras que não estão vinculado ao associados

      //echo  "<option value=''>Selecionar uma bandeira</option>";
      foreach ($usuario as $valor) {
          echo "<option value='".$valor->categoria_cart_id."'>".$valor->categoria_cart_nome_credenciadora.' - '.$valor->categoria_cart_descricao."</option>";

    }

  }

}

/* End of file Taxa.php */
/* Location: ./application/controllers/Taxa.php */