<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cartao extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cartao_model');
	}

	public function index()
	{

		$resultado = $this->Cartao_model->listar();

		$dadosView['dados'] = $resultado;
		$dadosView['meio']  = 'cartao/listar';
		$this->load->view('tema/tema',$dadosView);
	}



	public function adicionar()
	{		
		$dadosView['permissoes'] = $this->Cartao_model->todasPermissoes();
		$dadosView['meio']       = 'cartao/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
	
		/*
            01 - Visa / Visa Electron
            02 - Mastercard / Maestro
            03 - American Express
            04 - Sorocred

            05 - Diners Club
            06 - Elo
            07 - Hipercard
            08 - Aura
            09 - Cabal
            99 - Outros  */      

            $bandeira = array(                    
                'Visa / Visa Electron',
                'Mastercard / Maestro', 
                'American Express',
                'Hipercard',
				'Elo',
                'Hiper',
                'Outros'         
                    
            );

        foreach ($bandeira as $b) {    

		$dados = array(

		  'categoria_cart_cnpj_credenciadora'   => $this->input->post('cnpj_credenciadora'),
		  'categoria_cart_nome_credenciadora'   => $this->input->post('nome_credenciadora'),
		  'categoria_cart_descricao'            => $b,  
		  'categoria_cart_data_cadastro'        => date('Y-m-d'),		    	  
		  'categoria_cart_visivel'   => 1
		  
		);

  	    // echo '<pre>';
		// var_dump($dados);die();

		$resultado = $this->Cartao_model->inserir($dados);

	    };

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Cartao', 'refresh');
	}

	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Cartao_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->Cartao_model->todasPermissoes();
		$dadosView['meio']       = 'cartao/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('categoria_cart_id');


		$dados = array(

		  'categoria_cart_cnpj_credenciadora'   => $this->input->post('cnpj_credenciadora'),
		  'categoria_cart_nome_credenciadora'   => $this->input->post('nome_credenciadora'),
		  'categoria_cart_descricao'            => $this->input->post('cart_descricao'),  
		  'categoria_cart_data_cadastro'        => date('Y-m-d'),		    	  
		  'categoria_cart_visivel'   => 1
		  
		);


		$resultado = $this->Cartao_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Cartao', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');


		// var_dump($id);die();

		$dados = array(
						'categoria_cart_visivel' => 0
						
					);

		$resultado = $this->Cartao_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Cartao_model->pegarPorId($id);	
		$dadosView['permissoes']  = $this->Cartao_model->todasPermissoes();	

		$dadosView['meio']       = 'cartao/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}



	public function selecionarBandeiras()
	{
		$termo  = $this->input->get('term');
        $ret['results'] = $this->Cartao_model->selecionarBandeiras($termo['term']);
		echo json_encode($ret);
	}

}

/* End of file Cartao.php */
/* Location: ./application/controllers/Cartao.php */