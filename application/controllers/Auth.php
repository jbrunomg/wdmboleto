<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Auth_model');
	}

	public function index()
	{
		$this->load->view('login');
	}

	public function processarLogin()
	{

		$this->form_validation->set_rules('login', 'Login', 'trim|required|valid_email');       
        $this->form_validation->set_rules('senha', 'Senha', 'trim|required');
        $this->form_validation->set_error_delimiters('<div class="text-danger">', '</div>');

        if ($this->form_validation->run() == FALSE)
        {
            $this->load->view('login');
        }
        else
        {	        	

        	$dados = array(
        					'usuario_email'   => $this->input->post('login'),
        					'usuario_senha'   => sha1(md5($this->input->post('senha'))),
        					'usuario_visivel' => 1,
        					'usuario_situacao'=> 1,
        					'permissao_visivel'  => 1
        				  );        	

        	$resultado = $this->Auth_model->processarLogin($dados); 
        	
        	if($resultado){

	        	$session = array(
						        'usuario_id'            => $resultado[0]->usuario_id,
						        'usuario_email'         => $resultado[0]->usuario_email,
						        'usuario_nome'          => $resultado[0]->usuario_nome,
								'usuario_sexo'          => $resultado[0]->usuario_sexo,
								'usuario_cpf'           => $resultado[0]->usuario_cpf,
								'usuario_celular'       => $resultado[0]->usuario_celular,
						        'permissao_nome'        => $resultado[0]->permissao_nome,					       
						        'usuario_permissoes_id' => $resultado[0]->usuario_permissoes_id,
						        'usuario_imagem'        => $resultado[0]->usuario_imagem,	
						        'usuario_padrao'        => $resultado[0]->usuario_padrao, 	// Quem ver tudo do sistema 				      
						        'permissao'             => $resultado[0]->permissao_permissoes					       
							);

							

				$this->session->set_userdata($session);

				redirect('Dashboard','refresh');

        	}else{

        		$this->session->set_flashdata('erro', 'Login/Senha Inválidos');
        		$this->load->view('login');
        	}
        }
		
	}

	public function sair()
	{
		$this->session->sess_destroy();
		$this->load->view('login');
	}

	public function emitente()
	{
		$emitente = $this->input->post();

		if($emitente){

			unset($emitente['id']);

			$dados = array();

			foreach ($emitente as $key => $value) {
				$dados[$key] = $value;
			}

			$subirImg = $this->fazerUpload();

			if($subirImg){
				$dados['url_logo'] = $subirImg;
			}

			$resultado = $this->Auth_model->editarEmitente($dados,$this->input->post('id'));

			if($resultado){
	    		$this->session->set_flashdata('success','Registro adicionado com sucesso!');
	    	}else{
	    		$this->session->set_flashdata('erro','Erro ao adicionar o registro!');
	    	}

		}

		$dadosView['estados']  = $this->Auth_model->todosEstados();
		$dadosView['dados']    = $this->Auth_model->pegarEmitente();
		$dadosView['cidades']  = $this->Auth_model->todasCidadesIdEstado($dadosView['dados'][0]->uf);
		$dadosView['meio']     = 'emitente/emitente';
		$this->load->view('tema/tema',$dadosView);
	}

	public function fazerUpload()
	{
		if($_FILES['arquivo'] && $_FILES['arquivo']['size'] > 0){
			$ext = pathinfo($_FILES['arquivo']['name'], PATHINFO_EXTENSION);

			$nome = explode('.'.$ext, $_FILES['arquivo']['name']);

			$config['upload_path']          = 'public/assets/images';
			$config['allowed_types']        = 'gif|jpg|png|jpeg';
			$config['file_name']       		= $nome[0].time().'.'.$ext;
			$config['max_size']             = 1000000;

			if (!is_dir('public/assets/images')) {
	            mkdir('public/assets/images', 0777, TRUE);
	        }

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('arquivo'))
			{
				return false;
			}
			
			return base_url()."public/assets/images/".$config['file_name'];

		}else{
			return false;
		}

    }
}

/* End of file Auth.php */
/* Location: ./application/controllers/Auth.php */