<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoriafinanceiro extends CI_Controller {

    public function __construct()
    {   
        parent::__construct();
        $this->load->model('Categoriafinanceiro_model');
    }

    public function index()
    {
       // $dadosView['dados'] = $this->Categoriafinanceiro_model->listar();

        $resultado = $this->Categoriafinanceiro_model->listar();

		$dadosView['dados'] = $resultado;

        $dadosView['meio'] = 'categoriafinanceiro/listar';
        $this->load->view('tema/tema',$dadosView);    
    }

    public function adicionar()
	{				
		$dadosView['permissoes'] = $this->Categoriafinanceiro_model->todasPermissoes();
		$dadosView['meio']       = 'categoriafinanceiro/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
		$dados = array(              
              'categoria_fina_descricao'     => $this->input->post('categoria_fina_descricao'),
              'categoria_fina_data_cadastro' => date('Y-m-d'), 
              'categoria_fina_visivel'       => 1                  
        );


		// var_dump($dados);die();

		$resultado = $this->Categoriafinanceiro_model->inserir($dados);

		if ($resultado) {
			$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');	
		}else{		
			$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
		}

		redirect('Categoriafinanceiro', 'refresh');
	}


	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Categoriafinanceiro_model->pegarPorId($id);
		$dadosView['permissoes'] = $this->Categoriafinanceiro_model->todasPermissoes();
		$dadosView['meio']       = 'categoriafinanceiro/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('categoria_fina_id');

		$dados = array(

		  'categoria_fina_descricao'     => $this->input->post('categoria_fina_descricao'),
			  
		);


		$resultado = $this->Categoriafinanceiro_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Categoriafinanceiro', 'refresh');
	}


    public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'categoria_fina_visivel' => 0
						
					);

		$resultado = $this->Categoriafinanceiro_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}



	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']       = $this->Categoriafinanceiro_model->pegarPorId($id);	
		$dadosView['permissoes']  = $this->Categoriafinanceiro_model->todasPermissoes();	

		$dadosView['meio']       = 'categoriafinanceiro/visualizar';

		$this->load->view('tema/tema',$dadosView);


	}





}
