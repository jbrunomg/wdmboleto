<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Maquinas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Maquinas_model');
    }

    public function index()
    {
        $dadosView['dados'] =  $this->Maquinas_model->listarMaquinas();       
        $dadosView['meio']  =  'maquina/listar';

        $this->load->view('tema/tema',$dadosView);
    }
    
    public function adicionar()
    {
        $dadosView['dados'] = $this->Maquinas_model->pegarAdministradorasCartao();       
        $dadosView['meio']  = 'maquina/adicionar';

        $this->load->view('tema/tema',$dadosView);
    }

    public function adicionarExe()
    {
        $dados = $this->input->post();
        $dados['maquina_data_cadastro'] = date('Y-m-d');

        $resultado = $this->Maquinas_model->inserir($dados);

        if ($resultado) {           
            $this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
        } else {
            $this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
        }

        redirect('Maquinas', 'refresh');

    }
    public function verificandoNumeroDaMaquina()
    {
        $numero = $this->input->post('numero');
        $id = $this->uri->segment(3) ?: false;
        $validation = $this->Maquinas_model->buscarMaquinaPorNumero($numero);

        $response = [];
        if($id) {
            if($validation) {
                $response['success'] = $validation[0]->maquina_id === $id;
            } else {
                $response['success'] = true;
            }
        } else {
            $response['success'] = count($validation) === 0;
        }
        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function visualizar()
    {
        $id = $this->uri->segment(3);
        $dadosView = [
            'dados' => $this->Maquinas_model->pegarPorId($id),
            'clientes' => $this->Maquinas_model->buscarTodosOsClientes(),
            'maquinasClientes' => $this->Maquinas_model->buscarClientesDaMaquina($id),
            'meio' => 'maquina/visualizar'
        ];
        $dadosView['dados'][0]->administradora_cartao = $this->Maquinas_model->buscarAdministradoraCartaoPorId($dadosView['dados'][0]->maquina_categoria_cartao_id);
        $this->load->view('tema/tema',$dadosView);
    }

    public function editar()
    {
        $id = $this->uri->segment(3);

        $dadosView['dados']  = $this->Maquinas_model->pegarPorId($id);
        $dadosView['cartao'] = $this->Maquinas_model->pegarAdministradorasCartao();
        $dadosView['meio']   = 'maquina/editar';

        $this->load->view('tema/tema',$dadosView);
    }

    public function editarExe()
    {
        $id = $this->uri->segment(3);
        $dados = $this->input->post();

        $dados['maquina_data_atualizacao'] = date('Y-m-d');

        $dataView = [];
        try {
            $this->Maquinas_model->editar($id,$dados);
            $dataView['atualizar'] = true;
            redirect('maquinas','auto',$dataView);
        } catch (\Error $e) {
            $dataView['atualizar'] = $e;
            redirect('maquinas','auto',$dataView);
        }
    }

    public function excluir()
    {
        $id = $this->input->post('id');
        $dados = [
            'maquina_visivel' => 0
        ];
        try {
            $this->Maquinas_model->excluir($id,$dados);
            echo json_encode(array('status' => true));
        } catch (\Error $e) {
            echo json_encode(array('status' => false));
        }
    }
    public function listarClientes()
    {

        $id = $this->uri->segment(3);
        $dadosView['dados'] = $this->Maquinas_model->buscarClientesDaMaquina($id);
        $dadosView['maquina'] = $this->Maquinas_model->buscarDadosDaMaquinaPorId($id);

        // var_dump( $dadosView['maquina']);die();
       
        $dadosView['meio']  ='maquina/maquinas_clientes/listar';

        $this->load->view('tema/tema',$dadosView);
    }
    public function listarClientesEmVisualizarMaquinas()
    {
        $id = $this->uri->segment(3);
        $dadosView['dados'] = $this->Maquinas_model->buscarClientesDaMaquina($id);
        $this->load->view('maquina/maquinas_clientes/listar',$dadosView);
    }
    public function adicionarCliente()
    {
        $dadosView['dados'] = $this->Maquinas_model->buscarTodosOsClientes();
        $dadosView['meio'] = 'maquina/maquinas_clientes/adicionar';

        $this->load->view('tema/tema' ,$dadosView);
    }
    public function adicionarClienteExe()
    {
        $dados = $this->input->post();
        $dados['maquinas_clientes_data_cadastro'] = date('Y-m-d',strtotime($dados['maquinas_clientes_data_cadastro']));

        $redicetUrl = 'maquinas/listarClientes/' . $dados['maquinas_clientes_maquina_id'];

        $test = $this->Maquinas_model->adicionarMaquinaAoCliente($dados);
        redirect($redicetUrl,'refresh');
    }
    public function visualizarCliente()
    {
        $cliente_id = $this->uri->segment(3);
        $maquina_id = $this->uri->segment(4);

        $dadosView['dados']   = $this->Maquinas_model->buscarClienteDaMaquinaPorId($cliente_id, $maquina_id);
        // $dadosView['dados'] = $dadosView['dados'][0];

        $dadosView['maquina'] = $this->Maquinas_model->buscarDadosDaMaquinaPorId($maquina_id);

        // $dadosView['maquina'] = $dadosView['maquina'][0];

        // $dadosView['maquina']->cartao = $this->Maquinas_model->buscarAdministradoraCartaoPorId($maquina_id);
        // $dadosView['maquina']->cartao = $dadosView['maquina']->cartao[0];

       //  var_dump( $dadosView['maquina']);die();

        $dadosView['meio'] ='maquina/maquinas_clientes/visualizar';
        $this->load->view('tema/tema',$dadosView);
    }
    public function editarCliente()
    {
        $id = $this->uri->segment(3);
        $dadosView['dados'] = $this->Maquinas_model->buscarDadosDaMaquinasClientesPorId($id);
        $dadosView['dados'] = $dadosView['dados'][0];
        $dadosView['cliente'] = $this->Maquinas_model->buscarTodosOsClientes();

        $dadosView['meio'] ='maquina/maquinas_clientes/editar';
        $this->load->view('tema/tema',$dadosView);
    }
    public function editarClienteExe()
    {
        $id = $this->uri->segment(3);
        $dados = $this->input->post();

        $dados['maquinas_clientes_data_cadastro'] = date('Y-m-d',strtotime($dados['maquinas_clientes_data_cadastro']));

        $this->Maquinas_model->editarMaquinasClientes($id, $dados);

        $redicetUrl = 'maquinas/listarClientes/' . $dados['maquinas_clientes_maquina_id'];
        redirect($redicetUrl, 'refresh');
    }
    public function excluirCliente()
    {
        $id = $this->input->post('id');

        try {
            $this->Maquinas_model->excluirCliente($id);
            echo json_encode(array('status' => true));
        } catch (\Error $e) {
            echo json_encode(array('status' => false));
        }
    }

}