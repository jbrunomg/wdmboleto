<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cliente extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Cliente_model');
	}

	public function index()
	{
		$resultado = $this->Cliente_model->listar();
		$dadosView['dados'] = $resultado;		
		$dadosView['meio']  = 'cliente/listar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionar()
	{
		$dadosView['estados']    = $this->Cliente_model->todosEstados();		
		$dadosView['permissoes'] = $this->Cliente_model->todasPermissoes();
		$dadosView['meio']       = 'cliente/adicionar';
		$this->load->view('tema/tema',$dadosView);
	}

	public function adicionarExe()
	{	
		$Cnpj =  str_replace("-","",str_replace(".","",$this->input->post('cliente_cpfCnpj')));
		
		$resultadoCnpj = $this->Cliente_model->pegarPorCnpj($Cnpj);

		if ($resultadoCnpj) {	

			$this->session->set_flashdata('erro', "O Cliente: ".'\n' .$resultadoCnpj[0]->cliente_nome.'\n'. " já está cadastrado em nosso sistema!");

		} else {

			$dados1 = $this->input->post(); 	

			$dados2 = array( 	
				'cliente_cpfCnpj'       => $Cnpj, 	  
				'cliente_dtNascimento'  => date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('cliente_dtNascimento')))),	
			    'cliente_data_cadastro' => date('Y-m-d') // Data do 1º dia de cadastro				
			);
			
			$dados  = array_merge($dados1,$dados2);

			$resultado = $this->Cliente_model->inserir($dados);

			if ($resultado) {			
				$this->session->set_flashdata('sucesso', 'Registro inserido com sucesso!');
			} else {
				$this->session->set_flashdata('erro', 'Tivemos problema para inserir o registro!');
			}			
		}	

		redirect('Cliente', 'refresh');
	}


	public function editar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Cliente_model->pegarPorId($id);
		$dadosView['cidades']    = $this->Cliente_model->todasCidadesIdEstado($dadosView['dados'][0]->cliente_estado);
		$dadosView['estados']    = $this->Cliente_model->todosEstados();		
		$dadosView['permissoes'] = $this->Cliente_model->todasPermissoes();
		$dadosView['meio']       = 'cliente/editar';


		$this->load->view('tema/tema',$dadosView);
	}

	public function editarExe()
	{	

		$id = $this->input->post('cliente_id');

		$dados1 = $this->input->post();

		$dados2 = array(  	   
		    'cliente_dtNascimento' =>  date('Y-m-d', strtotime(str_replace('/','-',$this->input->post('cliente_dtNascimento'))))		
		);
			
		$dados  = array_merge($dados1,$dados2);

		// var_dump($dados);die();

		$resultado = $this->Cliente_model->editar($id,$dados);

		if ($resultado) {			
			$this->session->set_flashdata('sucesso', 'Registro alterado com sucesso!');
		}else{
			$this->session->set_flashdata('erro', 'Tivemos problema para alterado o registro!');
		}

		redirect('Cliente', 'refresh');
	}

	public function excluir()
	{
		$id = $this->input->post('id');

		$dados = array(
						'cliente_visivel' => 0
						
					);

		$resultado = $this->Cliente_model->excluir($id,$dados);

		if ($resultado) {			
			echo json_encode(array('status' => true));
		}else{
			echo json_encode(array('status' => false));
		}
	}
	
	public function visualizar()
	{
		$id = $this->uri->segment(3);

		$dadosView['dados']      = $this->Cliente_model->pegarPorId($id);
		$dadosView['cidades']    = $this->Cliente_model->todasCidadesIdEstado($dadosView['dados'][0]->cliente_estado);
		$dadosView['estados']    = $this->Cliente_model->todosEstados();		
		$dadosView['permissoes'] = $this->Cliente_model->todasPermissoes();		

		$dadosView['meio']       = 'cliente/visualizar';

		$this->load->view('tema/tema',$dadosView);

	}

	public function cidadesPorId()
	{
		$uf = $this->input->post('cliente_estado'); 	

        $cidades = $this->Cliente_model->todasCidadesIdEstado($uf);
        
        echo  "<option value=''>Selecionar uma cidade</option>";
        foreach ($cidades as $cidade) {
           echo "<option value='".$cidade->id."'>".$cidade->nome."</option>";
        }
	}

	public function selecionarCidade(){
		
		$estado  = $this->input->post('cliente_estado');
		$cidades = $this->Cliente_model->selecionarCidades($estado);

		echo  "<option value=''>Selecionar uma cidade</option>";
		foreach ($cidades as $cidade) {
			echo "<option value='".$cidade->id_cidade."'>".$cidade->nome."</option>";
		}

	}

	public function selecionarClientes()
  	{   
    	$termo  = $this->input->get('term');
        $ret['results'] = $this->Cliente_model->selecionarClientes($termo['term']);
    	echo json_encode($ret);
  	}



	public function wscnpj()
    {
        $caracteres = array(".","/","-");
        $cpfCnpj  = $this->input->post('documento');        
        $cpfCnpj = str_replace($caracteres, "", $cpfCnpj);       
        $json = file_get_contents('https://receitaws.com.br/v1/cnpj/'.$cpfCnpj);
        
        echo $json;
    }

    public function wscep()
    {
        $cep =  explode("-",$this->input->post('cep'));
        $cep = $cep[0].$cep[1];
        $xml = file_get_contents('http://webservices.profissionaisdaweb.com.br/cep/'.$cep.'.xml');
        $xml = simplexml_load_string($xml);
        echo json_encode($xml);
    }


	public function Mask($mask,$str){

	    $str = str_replace(" ","",$str);

	    for($i=0;$i<strlen($str);$i++){
	        $mask[strpos($mask,"#")] = $str[$i];
	    }

	    return $mask;
	}

	
}

/* End of file Cliente.php */
/* Location: ./application/controllers/Cliente.php */