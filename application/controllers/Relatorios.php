<?php
ini_set('memory_limit', '1024M'); // para geração arquivo remessa
ini_set('max_execution_time', 0); // para geração arquivo remessa


defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('Relatorios_model');
	}

	public function aniversariantes()
	{
		$data['titulo'] = 'Aniversáriantes';
		$data['meio'] = 'relatorios/aniversariantes/aniversariantes';
		$this->load->view('tema/tema',$data);
	}

	public function aniversariantesRapido()
	{	
		$mes = $this->uri->segment(3);		

		$resultado = $this->Relatorios_model->aniversariantesRapido($mes);
		$emitente  = $this->Relatorios_model->agenciaEmitente();

		$data['emitente'] = $emitente[0];
		$data['dados'] = $resultado;
		$data['titulo'] = 'Aniversáriantes';
		$gerar['conteudo'] = $this->load->view('relatorios/aniversariantes/impressao_aniversariantes',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		gerarPdf($html,'Aniversariantes');
	}

	public function aniversariantesPersonalizado()
	{
		$dataInicial = date('Y-m-d',strtotime(str_replace('/', '-',$this->input->post('dataInicio'))));
		$dataFinal   = date('Y-m-d',strtotime(str_replace('/', '-',$this->input->post('dataFim'))));

		$resultado = $this->Relatorios_model->aniversariantesPersonalizado($dataInicial,$dataFinal);

		$data['dados'] = $resultado;
		$data['titulo'] = 'Aniversáriantes';
		$gerar['conteudo'] = $this->load->view('relatorios/aniversariantes/impressao_aniversariantes',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		gerarPdf($html);

	}

	public function estudanteAtivoInativo()
	{
		$data['titulo'] = 'Ativo / Inativo';
		$data['meio'] = 'relatorios/estudanteativoinativo/estudanteativoinativo';
		$this->load->view('tema/tema',$data);
	}

	public function estudanteAtivoInativoRapido()
	{	
		$tipo = $this->uri->segment(3);// "Ativa/Inativo"		

		$resultado = $this->Relatorios_model->estudanteAtivoInativo($tipo);
		$emitente  = $this->Relatorios_model->agenciaEmitente();

		$data['emitente'] = $emitente[0];
		$data['dados'] = $resultado;
		$data['titulo'] = 'Estudante ATIVO/INATIVO';
		$gerar['conteudo'] = $this->load->view('relatorios/estudanteativoinativo/impressao_estudanteativoinativo',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		//gerarPdf($html,'ATIVO/INATIVO');
		
		header("Content-type: application/vnd.ms-excel");
        header("Content-type: application/force-download");
        header("Content-Disposition: attachment; filename=estudante{$tipo}" .date("d-m-Y_H-i") . ".xls");
        header("Pragma: no-cache");
		echo $html;
		
	}

	public function financeiroNotaFiscal()
	{
		$data['titulo'] = 'Financeiro';
		$data['meio'] = 'relatorios/financeiro/financeironotafiscal';
		$this->load->view('tema/tema',$data);
	}

	public function notasFiscaisRapido()
	{	

		$ano = date('Y'); // Ano atual

		$mes = $this->uri->segment(3);		

		$resultado = $this->Relatorios_model->notasFiscaisRapido($mes,$ano);

		$empreinativa = $this->Relatorios_model->empresainativa($mes,$ano);
		$emprenova = $this->Relatorios_model->empresanova($mes,$ano);
		$emprereativada = $this->Relatorios_model->empresareativada($mes,$ano);

		$emitente  = $this->Relatorios_model->agenciaEmitente();

		$data['emitente'] = $emitente[0];		
		$data['dados'] = $resultado;

		$data['empreinativa'] = $empreinativa;
		$data['emprenova']    = $emprenova;
		$data['emprereativada'] = $emprereativada;		
		

		$data['titulo'] = 'CONTROLE DE NOTAS FISCAIS '.$mes.'/'.$ano;
		
		// $data['meio'] = 'relatorios/financeiro/impressao_notas_fiscais';
		// $this->load->view('tema/tema',$data);

		$gerar['conteudo'] = $this->load->view('relatorios/financeiro/impressao_notas_fiscais',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		gerarPdf($html,'Controle_Nota_fiscais_'.$mes.'_'.$ano);
	}


	public function imprimirdemonstrativo()
	{	
		$mes = date('m');
		$ano = date('Y'); // Ano atual
		$lastmes = date('m', strtotime('-1 months'));

		$id = $this->uri->segment(3);

		$enviar = $this->uri->segment(4);		


		$dadosView['contrato'] = $this->Relatorios_model->imprimirdemonstrativo($id);

		$dadosView['contrato_anteriror'] = $this->Relatorios_model->checarDemonstrativoId($lastmes,$ano,$id);

		$contrato = explode(',',  $dadosView['contrato'][0]->grupo);		
		$contrato_anteriror = explode(',',  $dadosView['contrato_anteriror'][0]->demonstrativoempr_cod_estudante);

		$_array = $contrato_anteriror;
		$arrays = $contrato;
		$result = array_diff( $arrays , $_array );

		$estudantes = array();	

		foreach ($dadosView['contrato'] as $estudante) {

				$data_term  = $estudante->contrestempr_data_term;	
				$data_renI  = $estudante->contrestempr_data_renI;
				$data_renII = $estudante->contrestempr_data_renII;
				$data_renIII = $estudante->contrestempr_data_renIII;
				$data_rescisao = $estudante->contrestempr_data_rescisao;


			if(in_array($estudante->palualcodig, $result)){
			 

				$valor = $estudante->sempemvalor;	
				$dataInicio = $estudante->contrestempr_data_ini;

				if (($estudante->contrestempr_data_rescisao <> '0000-00-00') and ($estudante->contrestempr_data_rescisao <> '1970-01-01')) {
					$datafim = date_create($estudante->contrestempr_data_rescisao);
					$Rescindido = true;
				} else {
					if ($id == '325') {
						$datafim = date('Y').'-'.$mes.'-'.'19';
					} else {
						$datafim = date('Y').'-'.$mes.'-'.'30';
					}
					$Rescindido = false;
				}				

				$contrestempr_data_ini = $dataInicio;
				$contrestempr_data_fim = $datafim;

				$valorPagar = $this->calcularValor($valor,$dataInicio,$datafim ,true,$Rescindido);
				$valorTotalPagar = $valorTotalPagar + $valorPagar;
				$dataTermino = $this->terminoContrato($data_term,$data_renI,$data_renII,$data_renIII,$data_rescisao);

			} else {

				$valor = $estudante->sempemvalor;		
				if ($id == '325') {	
					$dataInicio =  date('Y').'-'.$lastmes.'-'.'20';
		    	} else {
		    		$dataInicio =  date('Y').'-'.$mes.'-'.'01';	
		    	}

				if (($estudante->contrestempr_data_rescisao <> '0000-00-00') and ($estudante->contrestempr_data_rescisao <> '1970-01-01')) {
					$datafim = $estudante->contrestempr_data_rescisao;
					$Rescindido = true;
				} else {
					if ($id == '325') {
						$datafim = date('Y').'-'.$mes.'-'.'19';
					} else {
						$datafim = date('Y').'-'.$mes.'-'.'30';
					}
					$Rescindido = false;
				}

				$contrestempr_data_ini = $dataInicio;
				$contrestempr_data_fim = $datafim;

				$valorPagar = $this->calcularValor($valor,$dataInicio,$datafim ,false,$Rescindido);
				$valorTotalPagar = $valorTotalPagar + $valorPagar;
				$dataTermino = $this->terminoContrato($data_term,$data_renI,$data_renII,$data_renIII,$data_rescisao);							
			}

			$estudante->dataInicio  =  $contrestempr_data_ini;
			$estudante->datafim     =  $contrestempr_data_fim;
			$estudante->valorTotal  =  $valorPagar;
			$estudante->SomaTotal   =  $valorTotalPagar;
			$estudante->termino     =  $dataTermino;
			$estudantes[$estudante->palualcodig] = $estudante;

		}

		// Checar existencia de contrato caso não tenha criar (mes/ano/empresa_id)
		$estorno = $this->Relatorios_model->estornoDemonstrativoId($lastmes,$ano,$id);

		$dadosView['estorno'] = $estorno;

		$dadosView['estudante'] = $estudantes;

		$emitente  = $this->Relatorios_model->agenciaEmitente();

		$dadosView['emitente'] = $emitente[0];
		
		$dadosView['titulo'] = 'DEMONSTRATIVO DE ESTUDANTES EM ESTÁGIO';
		$gerar['conteudo'] = $this->load->view('relatorios/demonstrativo/impressao_demonstrativo',$dadosView,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		if ($enviar == 'true') {
		$emails = $dadosView['contrato'][0]->sempememailgestor;
		$assunto = 'Demonstrativo - Nudep';
		$this->enviarEmailDemonstrativo($emails,$html,$assunto);	
		} else {
		gerarPdf($html,'Demonstrativo');
	    }
	}


	public function calcularValor($valor,$dataInicio,$datafim,$devedor,$Rescindido)
	{	
		
		$datetime1 = date_create($dataInicio);
		$datetime2 = date_create($datafim);
		$interval  = date_diff($datetime1,$datetime2);

		$diasGeral = $interval->format('%a');
		$meses = $interval->format('%m');
 	

		if ($meses <> '0') {
			// Definimos o intervalo de 1 ano
			$interval = new DateInterval('P1M');
			 
			// Resgatamos datas de cada ano entre data de início e fim
			$period = new DatePeriod($datetime1, $interval, $datetime2); 

			$arr = array();
			foreach($period as $date) {				
				$dataArray = $date->format("m");			
				$arr[] = $dataArray;
			}

			$fruta = array_shift($arr); // Remover 1º array
			$fruta = array_pop($arr); // Remover 1º ultimo	

			if ( in_array('02',$arr)) {
				$dias = ((($diasGeral)+2) - (30 * $meses));	
			} else {
				$dias = ((($diasGeral)+1) - (30 * $meses));
			}
										
		} else {
			$dias = $interval->format('%d')+1; 
		}
	

		if($devedor){
			if ($Rescindido) {				
				return  number_format(((round($valor/30,2)) * $dias), 2, ".", "");
			}	
			
			   return number_format(((round($valor/30,2) * $dias) + ($valor * $meses) ), 2, ".", "");
			
		}else{
			if ($Rescindido) {
				return  number_format(((round($valor/30,2)) * $dias), 2, ".", "");
			}
			   return number_format($valor, 2, ".", "");

		}
	}

	public function terminoContrato($data_term,$data_renI,$data_renII,$data_renIII,$data_rescisao)
	{
		if(($data_rescisao <> '0000-00-00') and ($data_rescisao <> '1970-01-01')) {
			return $data_rescisao;

		} else if(($data_renIII <> '0000-00-00') and ($data_renIII <> '1970-01-01')) {
			return $data_renIII;

		} else if(($data_renII <> '0000-00-00') and ($data_renII <> '1970-01-01')) {
			return $data_renII;

		} else if(($data_renI <> '0000-00-00') and ($data_renI <> '1970-01-01')) {
			return $data_renI; 

		} else if(($data_term <> '0000-00-00') and ($data_term <> '1970-01-01')) {	
			return $data_term;

	    }
    }

    public function enviarEmailDemonstrativo($emails,$conteudo,$assunto)
	{	

		$this->load->library('email'); // inserido no AutoLoad
		$this->email->from('emailsite@wdmtecnologia.com.br', 'Informativos');
		$this->email->subject($assunto);

		$this->email->to($emails); 
		
		$this->email->message($conteudo);

		//v($this->email->send());
		
		if($this->email->send())		
        {
            $this->session->set_flashdata('success','Email enviado com sucesso!');
            redirect('Empresa/visualizardemonstrativo/'.$this->uri->segment(3), 'refresh');
            return true;            
        }
        else
        {
            $this->session->set_flashdata('error',$this->email->print_debugger());
            return false;
        }
	}



	public function imprimirpagamento()
	{	
		$id = $this->uri->segment(3);


		$data['results'] = $this->Relatorios_model->imprimirpagamento($id);
		
		$emitente  = $this->Relatorios_model->agenciaEmitente();

		$data['emitente'] = $emitente[0];
		// $data['dados'] = $resultado;
		$data['titulo'] = 'PAGAMENTO DO SISTEMA';
		$gerar['conteudo'] = $this->load->view('relatorios/pagamento/impressao_pagamento',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		gerarPdf($html,'Pagamento');
	}


	public function empresa()
	{	
		$data['empresa'] = $this->Relatorios_model->listarempresa();
		$data['titulo'] = 'Empresas';
		$data['meio'] = 'relatorios/empresa/empresa';
		$this->load->view('tema/tema',$data);
	}

	public function empresademonstrativomesano()
	{
		$mesAno  = $this->input->post('mes_ano');

		$dados = explode("-", $mesAno);
		// echo $dados[0]; 
		// echo $dados[1]; 

		$dados = array(
	  		  
		  'demonstrativoempr_ano'   => $dados[0],
		  'demonstrativoempr_mes'   => $dados[1],

		  'demonstrativoempr_empresa_id'  => $this->input->post('empresa'),	    	  
		  'demonstrativoempr_visivel'   => 1
		  
		);

		$resultado = $this->Relatorios_model->patrimonioRapido();
		// var_dump($dados);die();

	}

	public function instituicao()
	{	
		$data['instituicao'] = $this->Relatorios_model->listarinstituicao();
		$data['titulo'] = 'Instituição';
		$data['meio']   = 'relatorios/instituicao/instituicao';
		$this->load->view('tema/tema',$data);
	}

	public function instituicaoRapido()
	{	

		$resultado = $this->Relatorios_model->instituicaoRapido();
		$emitente = $this->Relatorios_model->agenciaEmitente($this->session->userdata('usuario_igreja_id'));
	
		$data['emitente'] = $emitente[0];
		$data['dados'] = $resultado;
		$data['titulo'] = 'Instituição com alunos - Ativos';
		$gerar['conteudo'] = $this->load->view('relatorios/instituicao/impressao_Instituicao',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		gerarPdf($html,'Instituição');
	}


	public function patrimonio()
	{
		$data['titulo'] = 'Patrimônios';
		$data['meio']   = 'relatorios/patrimonio/patrimonios';
		$this->load->view('tema/tema',$data);
	}

	public function patrimonioRapido()
	{	

		$resultado = $this->Relatorios_model->patrimonioRapido();
		$emitente = $this->Relatorios_model->agenciaEmitente($this->session->userdata('usuario_igreja_id'));
	
		$data['emitente'] = $emitente[0];
		$data['dados'] = $resultado;
		$data['titulo'] = 'Patrimônios';
		$gerar['conteudo'] = $this->load->view('relatorios/patrimonio/impressao_patrimonios',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		gerarPdf($html,'Patrimonios');
	}

	public function patrimonioPersonalizado()
	{
		$dataInicial = date('Y-m-d',strtotime(str_replace('/', '-',$this->input->post('dataInicio'))));
		$dataFinal   = date('Y-m-d',strtotime(str_replace('/', '-',$this->input->post('dataFim'))));

		$where = array('patrimonio_visivel' => 1);

		if($this->input->post('conservacao')){
			$tipo  = array('patrimonio_estado_conservacao'    => $this->input->post('conservacao'));
			$where = array_merge($where,$tipo);
		}
		
		$resultado = $this->Relatorios_model->patrimonioPersonalizado($dataInicial,$dataFinal,$where);
		$emitente = $this->Relatorios_model->agenciaEmitente($this->session->userdata('usuario_igreja_id'));
	
		$data['emitente'] = $emitente[0];
		$data['dados'] = $resultado;
		$data['titulo'] = 'Patrimônios';
		$gerar['conteudo'] = $this->load->view('relatorios/patrimonio/impressao_patrimonios',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		gerarPdf($html,'Patrimonios');

	}

	public function membros()
	{
		$data['titulo'] = 'Membros';
		$data['meio'] = 'relatorios/membros/membros';
		$this->load->view('tema/tema',$data);
	}

	public function membrosRapidoSimples()
	{	
		$resultado = $this->Relatorios_model->membrosRapidoSimples();
		$emitente  = $this->Relatorios_model->agenciaEmitente($this->session->userdata('usuario_igreja_id'));
	
		$data['emitente'] = $emitente[0];
		$data['dados']    = $resultado;
		$data['titulo']   = 'Membros';
		$gerar['conteudo'] = $this->load->view('relatorios/membros/impressao_membros_simples',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		gerarPdf($html,'Membros');
	}

	
	public function demonstrativoview()
	{
		$data['titulo'] = 'Demonstrativo';
		$data['meio'] = 'relatorios/financeiros/demonstrativo';
		$this->load->view('tema/tema',$data);
	}

	public function demonstrativo()
	{	
		$tipo = $this->uri->segment(3);// "Recrutamento/financeiro/administrador/ambos"
		$ano  = $this->uri->segment(4);

		//var_dump($ano);die();

		$financeiro = $resultado = $this->Relatorios_model->demonstrativofinanceiro($ano);
		foreach ($financeiro as $f) {
			$data['financeiro'][(integer)$f->demonstrativofin_mes] = $f;
		}

		$administrador = $resultado = $this->Relatorios_model->demonstrativoadministrador($ano);
		foreach ($administrador as $f) {
			$data['administrador'][(integer)$f->demonstrativoadm_mes] = $f;
		}	

		$recrutamento = $resultado = $this->Relatorios_model->demonstrativorecrutamento($ano);
		foreach ($recrutamento as $f) {
			$data['recrutamento'][(integer)$f->demonstrativorecrut_mes] = $f;
		}	
		
		$data['tipo'] = $tipo;
		$gerar['conteudo']  = $this->load->view('relatorios/financeiros/impressao_demonstrativo',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		
		header("Content-type: application/vnd.ms-excel");
        header("Content-type: application/force-download");
        header("Content-Disposition: attachment; filename=demonstrativo" .date("d-m-Y_H-i") . ".xls");
        header("Pragma: no-cache");
		echo $html;
	}

	public function extenso($valor = 0, $maiusculas = false) {
	    if(!$maiusculas){
	        $singular = ["centavo", "real", "mil", "milhão", "bilhão", "trilhão", "quatrilhão"];
	        $plural = ["centavos", "reais", "mil", "milhões", "bilhões", "trilhões", "quatrilhões"];
	        $u = ["", "um", "dois", "três", "quatro", "cinco", "seis",  "sete", "oito", "nove"];
	    }else{
	        $singular = ["CENTAVO", "REAL", "MIL", "MILHÃO", "BILHÃO", "TRILHÃO", "QUADRILHÃO"];
	        $plural = ["CENTAVOS", "REAIS", "MIL", "MILHÕES", "BILHÕES", "TRILHÕES", "QUADRILHÕES"];
	        $u = ["", "um", "dois", "TRÊS", "quatro", "cinco", "seis",  "sete", "oito", "nove"];
	    }

	    $c = ["", "cem", "duzentos", "trezentos", "quatrocentos", "quinhentos", "seiscentos", "setecentos", "oitocentos", "novecentos"];
	    $d = ["", "dez", "vinte", "trinta", "quarenta", "cinquenta", "sessenta", "setenta", "oitenta", "noventa"];
	    $d10 = ["dez", "onze", "doze", "treze", "quatorze", "quinze", "dezesseis", "dezesete", "dezoito", "dezenove"];

	    $z = 0;
	    $rt = "";

	    $valor = number_format($valor, 2, ".", ".");
	    $inteiro = explode(".", $valor);
	    for($i=0;$i<count($inteiro);$i++)
	    for($ii=strlen($inteiro[$i]);$ii<3;$ii++)
	    $inteiro[$i] = "0".$inteiro[$i];

	    $fim = count($inteiro) - ($inteiro[count($inteiro)-1] > 0 ? 1 : 2);
	    for ($i=0;$i<count($inteiro);$i++) {
	        $valor = $inteiro[$i];
	        $rc = (($valor > 100) && ($valor < 200)) ? "cento" : $c[$valor[0]];
	        $rd = ($valor[1] < 2) ? "" : $d[$valor[1]];
	        $ru = ($valor > 0) ? (($valor[1] == 1) ? $d10[$valor[2]] : $u[$valor[2]]) : "";

	        $r = $rc.(($rc && ($rd || $ru)) ? " e " : "").$rd.(($rd &&
	        $ru) ? " e " : "").$ru;
	        $t = count($inteiro)-1-$i;
	        $r .= $r ? " ".($valor > 1 ? $plural[$t] : $singular[$t]) : "";
	        if ($valor == "000")$z++; elseif ($z > 0) $z--;
	        if (($t==1) && ($z>0) && ($inteiro[0] > 0)) $r .= (($z>1) ? " de " : "").$plural[$t];
	        if ($r) $rt = $rt . ((($i > 0) && ($i <= $fim) && ($inteiro[0] > 0) && ($z < 1)) ? ( ($i < $fim) ? ", " : " e ") : " ") . $r;
	    }

	    if(!$maiusculas){
	        $return = $rt ? $rt : "zero";
	    } else {
	        if ($rt) $rt = preg_replace(" /E/ "," e ",ucwords($rt));
	            $return = ($rt) ? ($rt) : "Zero" ;
	    }

	    if(!$maiusculas){
	        return preg_replace(" /E/ "," e ",ucwords($return));
	    }else{
	        return strtoupper($return);
	    }
	}

	public function transacoesBoleto()
	{
		$data['titulo'] = 'Transações';
		$data['meio'] = 'relatorios/operacao/transacoes';
		$this->load->view('tema/tema',$data);
	}

	public function transacoesCartao()
	{
		$data['titulo'] = 'Transações';
		$data['meio'] = 'relatorios/operacaocartao/transacoes';
		$this->load->view('tema/tema',$data);
	}

	public function transacoesPersonalizada()
    {

		$mes = $this->uri->segment(3);
		$ano = $this->input->post('anoInicio');
		$dadosView['dados'] = $this->Relatorios_model->transacoes($mes, $ano);
        $this->load->library('PHPExcel');
        $arquivo = './public/assets/arquivos/planilhas/transacoes.xlsx';
        $planilha = $this->phpexcel;

        $styleArray = array(
            'font' => array(
                'bold' => false,
				'size'  => 15,
                'color' => array('rgb' => 'FFFFFF')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '606060')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)

        );  

		$styleArray2 = array(
            'font' => array(
                'bold' => false,
				'size'  => 14
			),
			'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
        ); 

        $planilha->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $planilha->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $planilha->getActiveSheet()->getColumnDimension('C')->setWidth(40);
        $planilha->getActiveSheet()->getColumnDimension('D')->setWidth(40);
        $planilha->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('G')->setWidth(40);
		$planilha->getActiveSheet()->getColumnDimension('H')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('J')->setWidth(45);

        $planilha->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'ID')
                    ->setCellValue('B1', 'Data Transação')
                    ->setCellValue('C1', 'Assossiado')
                    ->setCellValue('D1', 'Bandeira')
                    ->setCellValue('E1', 'Qtd Parcelas')
					->setCellValue('F1', 'Autorização')
					->setCellValue('G1', 'Nome do Cliente')
					->setCellValue('H1', 'CPF do Cliente')
					->setCellValue('I1', 'Valor do Boleto')
					->setCellValue('J1', 'Valor Total da Transação');

		$contador = 1;
        foreach ($dadosView['dados'] as $p){ 

            $contador++;
            $planilha->setActiveSheetIndex(0)
                ->setCellValue('A'.$contador, $p->operacao_id)
                ->setCellValue('B'.$contador, date('d/m/Y', strtotime(str_replace('-','/',$p->operacao_data_cadastro))))
                ->setCellValue('C'.$contador, $p->usuario_nome)
                ->setCellValue('D'.$contador, $p->itens_operacao_bandeira_cartao)
				->setCellValue('E'.$contador, $p->itens_operacao_numero_parcela)
                ->setCellValue('F'.$contador, $p->autorizacao_operacao_numero_nsu)
				->setCellValue('G'.$contador, $p->cliente_nome)
				->setCellValue('H'.$contador, $this->formatCnpjCpf($p->cliente_cpfCnpj))
				->setCellValue('I'.$contador, 'R$ '.number_format($p->operacao_total_boleto, 2, ',', '.'))
				->setCellValue('J'.$contador, 'R$ '.number_format($p->operacao_total_transacao, 2, ',', '.'));
			$planilha->getActiveSheet()->getStyle('A'.$contador.':J'.$contador.'')->applyFromArray($styleArray2);
        }

        $planilha->getActiveSheet()->setTitle('Relatorio de Transações');
        $planilha->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
        $objgravar = PHPExcel_IOFactory::createWriter($planilha, 'Excel2007');
        $objgravar->save($arquivo);

        ######## FIM EXCEL ##########

        $dados = file_get_contents($arquivo); // Lê o conteúdo do arquivo
        $nome = 'Relatorio de Transações.xlsx';

        force_download($nome, $dados);


    }

	public function transacoesPersonalizadaPeriodo()
    {  
		$dadosView['dados'] = $this->Relatorios_model->transacoesPerido($this->input->post('anoInicio'));
        $this->load->library('PHPExcel');
        $arquivo = './public/assets/arquivos/planilhas/transacoes.xlsx';
        $planilha = $this->phpexcel;

        $styleArray = array(
            'font' => array(
                'bold' => false,
				'size'  => 15,
                'color' => array('rgb' => 'FFFFFF')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '606060')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)

        );  

		$styleArray2 = array(
            'font' => array(
                'bold' => false,
				'size'  => 14
			),
			'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
        ); 

        $planilha->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $planilha->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $planilha->getActiveSheet()->getColumnDimension('C')->setWidth(40);
        $planilha->getActiveSheet()->getColumnDimension('D')->setWidth(40);
        $planilha->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('G')->setWidth(40);
		$planilha->getActiveSheet()->getColumnDimension('H')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('J')->setWidth(45);

        $planilha->setActiveSheetIndex(0)
                    ->setCellValue('A1', 'ID')
                    ->setCellValue('B1', 'Data Transação')
                    ->setCellValue('C1', 'Assossiado')
                    ->setCellValue('D1', 'Bandeira')
                    ->setCellValue('E1', 'Qtd Parcelas')
					->setCellValue('F1', 'Autorização')
					->setCellValue('G1', 'Nome do Cliente')
					->setCellValue('H1', 'CPF do Cliente')
					->setCellValue('I1', 'Valor do Boleto')
					->setCellValue('J1', 'Valor Total da Transação');

		$contador = 1;
        foreach ($dadosView['dados'] as $p){ 

            $contador++;
            $planilha->setActiveSheetIndex(0)
                ->setCellValue('A'.$contador, $p->operacao_id)
                ->setCellValue('B'.$contador, date('d/m/Y', strtotime(str_replace('-','/',$p->operacao_data_cadastro))))
                ->setCellValue('C'.$contador, $p->usuario_nome)
                ->setCellValue('D'.$contador, $p->itens_operacao_bandeira_cartao)
				->setCellValue('E'.$contador, $p->itens_operacao_numero_parcela)
                ->setCellValue('F'.$contador, $p->autorizacao_operacao_numero_nsu)
				->setCellValue('G'.$contador, $p->cliente_nome)
				->setCellValue('H'.$contador, $this->formatCnpjCpf($p->cliente_cpfCnpj))
				->setCellValue('I'.$contador, 'R$ '.number_format($p->operacao_total_boleto, 2, ',', '.'))
				->setCellValue('J'.$contador, 'R$ '.number_format($p->operacao_total_transacao, 2, ',', '.'));
			$planilha->getActiveSheet()->getStyle('A'.$contador.':J'.$contador.'')->applyFromArray($styleArray2);
        }

        $planilha->getActiveSheet()->setTitle('Relatorio de Transações');
        $planilha->getActiveSheet()->getStyle('A1:J1')->applyFromArray($styleArray);
        $objgravar = PHPExcel_IOFactory::createWriter($planilha, 'Excel2007');
        $objgravar->save($arquivo);

        ######## FIM EXCEL ##########

        $dados = file_get_contents($arquivo); // Lê o conteúdo do arquivo
        $nome = 'Relatorio de Transações Periodo.xlsx';

        force_download($nome, $dados);


    }



    public function transacoesCartPersonalizada()
    {
		$mes = $this->uri->segment(3);
		$ano = date('Y');

		$dadosView['dados'] = $this->Relatorios_model->transacoesCartao($mes, $ano);
		$this->load->library('PHPExcel');
        $arquivo = './public/assets/arquivos/planilhas/transacoes.xlsx';
        $planilha = $this->phpexcel;

		$styleArray = array(
            'font' => array(
                'bold' => false,
				'size'  => 15,
                'color' => array('rgb' => 'FFFFFF')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '606060')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)

        );  

		$styleArray2 = array(
            'font' => array(
                'bold' => false,
				'size'  => 14
			),
			'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
        ); 

        $planilha->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $planilha->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $planilha->getActiveSheet()->getColumnDimension('C')->setWidth(40);
        $planilha->getActiveSheet()->getColumnDimension('D')->setWidth(40);
        $planilha->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('G')->setWidth(40);
		$planilha->getActiveSheet()->getColumnDimension('H')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('J')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('K')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('M')->setWidth(25);

        $planilha->setActiveSheetIndex(0)                    
                    ->setCellValue('A1', 'Data Transação')
                    ->setCellValue('B1', 'Lojista')
                    ->setCellValue('C1', 'Bandeira')
                    ->setCellValue('D1', 'Bruto')
                    ->setCellValue('E1', 'Autorização')
					->setCellValue('F1', 'Parcelas')
					->setCellValue('G1', 'Dec. da Ope')
					->setCellValue('H1', 'Desc. em Real')
					->setCellValue('I1', 'Liquido')
					->setCellValue('J1', 'Desc. Loja')
					->setCellValue('K1', 'Rep. Loja')
					->setCellValue('L1', 'Luc. Ope')
					->setCellValue('M1', 'Status');

		$contador = 1;
        foreach ($dadosView['dados'] as $p){ 

			$bruto    = $p->operacao_total_boleto - $p->operacao_total_transacao;
			$taxa     = $p->operacao_taxa_banco;
			$descReal = $p->operacao_total_boleto * ($p->operacao_taxa_banco/ 100) ;
			$liquido  = $p->operacao_total_boleto - $descReal;
			$descLoja = $p->operacao_total_boleto * ($p->operacao_taxa/ 100);
			$repLoja  = $p->operacao_total_boleto - $descLoja;
			$lucOpe   = $liquido - $repLoja;
			$status   = ($p->financeiro_baixado == 1) ? 'Pago' : 'Aberto'; 

            $contador++;
            $planilha->setActiveSheetIndex(0)                
                ->setCellValue('A'.$contador, date('d/m/Y', strtotime(str_replace('-','/',$p->operacao_data_cadastro))))
                ->setCellValue('B'.$contador, $p->usuario_nome)
                ->setCellValue('C'.$contador, $p->itens_operacao_bandeira_cartao)
                ->setCellValue('D'.$contador, 'R$ '.number_format($p->operacao_total_boleto, 2, ',', '.'))				
				->setCellValue('E'.$contador, $p->autorizacao_operacao_numero_nsu)	
				->setCellValue('F'.$contador, $p->itens_operacao_numero_parcela)
				->setCellValue('G'.$contador, empty($taxa) ? '' :  $taxa.'%')           

				
				->setCellValue('H'.$contador, 'R$ '.number_format($descReal, 2, ',', '.'))
				->setCellValue('I'.$contador, 'R$ '.number_format($liquido, 2, ',', '.'))
				->setCellValue('J'.$contador, 'R$ '.number_format($descLoja, 2, ',', '.'))				
				->setCellValue('K'.$contador, 'R$ '.number_format($repLoja, 2, ',', '.'))
				->setCellValue('L'.$contador, 'R$ '.number_format($lucOpe, 2, ',', '.'))
				->setCellValue('M'.$contador, $status)
			;
			$planilha->getActiveSheet()->getStyle('A'.$contador.':M'.$contador.'')->applyFromArray($styleArray2);
        }

        $planilha->getActiveSheet()->setTitle('Relatorio de Transações');
        $planilha->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray);
        $objgravar = PHPExcel_IOFactory::createWriter($planilha, 'Excel2007');
        $objgravar->save($arquivo);

        ######## FIM EXCEL ##########

        $dados = file_get_contents($arquivo); // Lê o conteúdo do arquivo
        $nome = 'Relatorio de Transações.xlsx';

        force_download($nome, $dados);


    }

	public function transacoesCartPersonalizadaPeriodo()
    {
		$usuario = $this->input->post('usuario_id');
		$inicio = empty($this->input->post('anoInicioPeriodoCliente')) ? '' : date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('anoInicioPeriodoCliente'))));
		$fim = empty($this->input->post('anoFimPeriodoCliente')) ? '' : date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('anoFimPeriodoCliente'))));
		$dadosView['dados'] = $this->Relatorios_model->transacoesPeridoCart($inicio, $fim, $usuario);
        $this->load->library('PHPExcel');
        $arquivo = './public/assets/arquivos/planilhas/transacoes.xlsx';
        $planilha = $this->phpexcel;

		$styleArray = array(
            'font' => array(
                'bold' => false,
				'size'  => 15,
                'color' => array('rgb' => 'FFFFFF')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			),
			'fill' => array(
				'type' => PHPExcel_Style_Fill::FILL_SOLID,
				'color' => array('rgb' => '606060')
			),
			'borders' => array(
				'allborders' => array(
					'style' => PHPExcel_Style_Border::BORDER_THIN,
					'color' => array('rgb' => '000000')
				)
			)

        );  

		$styleArray2 = array(
            'font' => array(
                'bold' => false,
				'size'  => 14
			),
			'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
			)
        ); 

        $planilha->getActiveSheet()->getColumnDimension('A')->setWidth(10);
        $planilha->getActiveSheet()->getColumnDimension('B')->setWidth(25);
        $planilha->getActiveSheet()->getColumnDimension('C')->setWidth(40);
        $planilha->getActiveSheet()->getColumnDimension('D')->setWidth(40);
        $planilha->getActiveSheet()->getColumnDimension('E')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('F')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('G')->setWidth(40);
		$planilha->getActiveSheet()->getColumnDimension('H')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('I')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('J')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('K')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('L')->setWidth(25);
		$planilha->getActiveSheet()->getColumnDimension('M')->setWidth(25);

        $planilha->setActiveSheetIndex(0)                    
                    ->setCellValue('A1', 'Data Transação')
                    ->setCellValue('B1', 'Lojista')
                    ->setCellValue('C1', 'Bandeira')
                    ->setCellValue('D1', 'Bruto')
                    ->setCellValue('E1', 'Autorização')
					->setCellValue('F1', 'Parcelas')
					->setCellValue('G1', 'Dec. da Ope')
					->setCellValue('H1', 'Desc. em Real')
					->setCellValue('I1', 'Liquido')
					->setCellValue('J1', 'Desc. Loja')
					->setCellValue('K1', 'Rep. Loja')
					->setCellValue('L1', 'Luc. Ope')
					->setCellValue('M1', 'Status');


					/* 
					->setCellValue('A1', 'ID')
                    ->setCellValue('B1', 'Data Transação')
                    ->setCellValue('C1', 'Lojista')
                    ->setCellValue('D1', 'Bandeira')
                    ->setCellValue('E1', 'Qtd Parcelas')
					->setCellValue('F1', 'Autorização')
					->setCellValue('G1', 'Valor do Cartão')
					->setCellValue('H1', 'Valor Total da Transação')
					->setCellValue('I1', 'Lucro Bruto')
					->setCellValue('J1', 'Taxa Mix')
					->setCellValue('K1', 'Lucro Líquido');
					*/

		$contador = 1;
        foreach ($dadosView['dados'] as $p){ 

			$bruto    = $p->operacao_total_boleto - $p->operacao_total_transacao;
			$taxa     = $p->operacao_taxa_banco;
			$descReal = $p->operacao_total_boleto * ($p->operacao_taxa_banco/ 100) ;
			$liquido  = $p->operacao_total_boleto - $descReal;
			$descLoja = $p->operacao_total_boleto * ($p->operacao_taxa/ 100);
			$repLoja  = $p->operacao_total_boleto - $descLoja;
			$lucOpe   = $liquido - $repLoja;
			$status   = ($p->financeiro_baixado == 1) ? 'Pago' : 'Aberto'; 

            $contador++;
            $planilha->setActiveSheetIndex(0)                
                ->setCellValue('A'.$contador, date('d/m/Y', strtotime(str_replace('-','/',$p->operacao_data_cadastro))))
                ->setCellValue('B'.$contador, $p->usuario_nome)
                ->setCellValue('C'.$contador, $p->itens_operacao_bandeira_cartao)
                ->setCellValue('D'.$contador, 'R$ '.number_format($p->operacao_total_boleto, 2, ',', '.'))				
				->setCellValue('E'.$contador, $p->autorizacao_operacao_numero_nsu)	
				->setCellValue('F'.$contador, $p->itens_operacao_numero_parcela)
				->setCellValue('G'.$contador, empty($taxa) ? '' :  $taxa.'%')           

				
				->setCellValue('H'.$contador, 'R$ '.number_format($descReal, 2, ',', '.'))
				->setCellValue('I'.$contador, 'R$ '.number_format($liquido, 2, ',', '.'))
				->setCellValue('J'.$contador, 'R$ '.number_format($descLoja, 2, ',', '.'))				
				->setCellValue('K'.$contador, 'R$ '.number_format($repLoja, 2, ',', '.'))
				->setCellValue('L'.$contador, 'R$ '.number_format($lucOpe, 2, ',', '.'))
				->setCellValue('M'.$contador, $status)
			;
			$planilha->getActiveSheet()->getStyle('A'.$contador.':M'.$contador.'')->applyFromArray($styleArray2);
        }

        $planilha->getActiveSheet()->setTitle('Relatorio de Transações');
        $planilha->getActiveSheet()->getStyle('A1:M1')->applyFromArray($styleArray);
        $objgravar = PHPExcel_IOFactory::createWriter($planilha, 'Excel2007');
        $objgravar->save($arquivo);

        ######## FIM EXCEL ##########

        $dados = file_get_contents($arquivo); // Lê o conteúdo do arquivo
        $nome = 'Relatorio de Transações Cliente.xlsx';

        force_download($nome, $dados);


    }

	public function transacoesCartPdfPeriodo()
    {
		$usuario = $this->input->post('usuario_idPdf');
		$inicio = empty($this->input->post('diaInicioPdf')) ? '' : date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('diaInicioPdf'))));
		$fim = empty($this->input->post('diaFimPdf')) ? '' : date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('diaFimPdf'))));
		$data['dados'] = $this->Relatorios_model->transacoesPeridoCart($inicio, $fim, $usuario);
		$data['titulo'] = 'Transações de Cartão por Período';
		$data['tipo'] = $this->input->post('tipo');
		$gerar['conteudo'] = $this->load->view('relatorios/operacaocartao/impressao_transacoes_cartao',$data,true);
		$html   = $this->load->view('relatorios/impressao',$gerar,true);
		gerarPdf($html,'Transações Cartão');

    }

	public function fechamentoCaixa()
	{
		$dataFormatada = empty($this->input->post('dataInicioCupom')) ? '' : date('Y-m-d', strtotime(str_replace('/', '-', $this->input->post('dataInicioCupom'))));
		//$data['dados']    = $this->Relatorios_model->fechamentoCaixa($dataFormatada);
		

		$emitente         = $this->Relatorios_model->agenciaEmitente();
		$data['dados']    = $this->Relatorios_model->fechamentoCaixa($dataFormatada);
		$data['dadosMes'] = $this->Relatorios_model->fechamentoCaixaMes($dataFormatada);

		// var_dump($data['dados']);die();

		$data['emitente'] = $emitente[0];
		$data['dataFech'] = $this->input->post('dataInicioCupom');
		$data['meio']     = "relatorios/operacaocartao/visualizarCaixa";
		$this->load->view('relatorios/impressaoCupom', $data);
	}

	public function formatCnpjCpf($value)
	{
		$CPF_LENGTH = 11;
		$cnpj_cpf = preg_replace("/\D/", '', $value);
		
		if (strlen($cnpj_cpf) === $CPF_LENGTH) {
			return preg_replace("/(\d{3})(\d{3})(\d{3})(\d{2})/", "\$1.\$2.\$3-\$4", $cnpj_cpf);
		} 
		
		return preg_replace("/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/", "\$1.\$2.\$3/\$4-\$5", $cnpj_cpf);
	}

	
    public function autoCompleteUsuarios(){

        $termo = strtolower($this->input->get('term'));
        $this->Relatorios_model->autoCompleteUsuarios($termo);

    }

	
	public function despesaReceita()
	{
		$data['titulo'] = 'Financeiro';
		$data['formaPagamento'] = $this->Relatorios_model->despesaReceitaFormaPag();
		$data['meio'] = 'relatorios/despesaReceita/despesaReceita';
		$this->load->view('tema/tema', $data);
	}

	public function despesaReceitaRapido()
	{
		$tipo = $this->uri->segment(3);
		$data = $this->uri->segment(4);

		if ($tipo == 'mes') {
			$dados['dataInicio'] 	= date('d/m/Y', strtotime(date('Y-m-01', strtotime($data)))); // Primeiro dia do mês
			$dados['dataFinal'] 	= date('d/m/Y', strtotime(date('Y-m-t', strtotime($data))));   // Último dia do mês
		} else {
			$anoAtual = date('Y');
			if ($tipo == '1Semestre') {
				$dados['dataInicio'] 	= date('d/m/Y', strtotime("{$anoAtual}-01-01"));
				$dados['dataFinal'] 	= date('d/m/Y', strtotime("{$anoAtual}-06-30"));
			} else {
				$dados['dataInicio'] 	= date('d/m/Y', strtotime("{$anoAtual}-07-01"));
				$dados['dataFinal'] 	= date('d/m/Y', strtotime("{$anoAtual}-12-31"));
			}
		}

		$resultado = $this->Relatorios_model->despesaReceitaRapido($tipo, $data);
		if (empty($resultado)) {
			$this->session->set_flashdata('error', 'Nenhum registro encontrado!');
			redirect('Relatorios/despesaReceita');
		}
		$dados['lancamentos'] 	= $resultado;
		$dados['titulo'] 		= "Despesas X Receitas";

		$html   = $this->load->view('relatorios/despesaReceita/imprimir', $dados, true);
		gerarPdf($html, $dados['titulo']);
	}

	public function despesaReceitaPersonalizado() 
	{
		// die(var_dump($this->input->get()));
		if (empty(trim($this->input->get('diaInicioPdf'))) ||
			empty(trim($this->input->get('diaFimPdf'))) || 
			empty(trim($this->input->get('pegarPor'))) ||
			empty(trim($this->input->get('usuario_idPdf'))))
		{
			$this->session->set_flashdata('error', 'Preencha os campos corretamente!');
			redirect('Relatorios/despesaReceita');
		}
		
		if (empty(trim($this->input->get('usuario_idPdf')))) {
			$this->session->set_flashdata('error', 'Selecione um usuário!');
			redirect('Relatorios/despesaReceita');
		}

		if($this->input->get('diaInicioPdf') > $this->input->get('diaFimPdf')) {
			$this->session->set_flashdata('error', 'Selecione um período válido!');
			redirect('Relatorios/despesaReceita');
		}


		$dados['dataInicio'] 	= date('d/m/Y', strtotime($this->input->get('diaInicioPdf')));
		$dados['dataFinal'] 	= date('d/m/Y', strtotime($this->input->get('diaFimPdf')));
	
		$dados['lancamentos'] = $this->Relatorios_model->despesaReceitaPersonalizado(
			$this->input->get('pegarPor'),
			$this->input->get('diaInicioPdf'),
			$this->input->get('diaFimPdf'),
			$this->input->get('usuario_idPdf'),
			$this->input->get('tipoConta'),
			$this->input->get('situacao'),
			$this->input->get('formaPagamento') ?? null
		);	
		if (empty($dados['lancamentos'])) {
			$this->session->set_flashdata('error', 'Nenhum registro encontrado!');
			redirect('Relatorios/despesaReceita');
		}

		$dados['titulo'] = "Despesas X Receitas";

		$html   = $this->load->view('relatorios/despesaReceita/imprimir', $dados, true);
		gerarPdf($html, $dados['titulo']);
	}
}

/* End of file Relatorios.php */
/* Location: ./application/controllers/Relatorios.php */
