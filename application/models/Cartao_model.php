<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Cartao_model extends CI_Model {

	private $tabela  = 'categoria_cartao';
	private $id      = 'categoria_cart_id';
	private $visivel = 'categoria_cart_visivel';

	public function listar()
	{
		$this->db->select('*');
		//$this->db->join('permissoes','permissao_id = usuario_permissoes_id');	
		$this->db->where($this->visivel, 1);	
		$this->db->order_by("categoria_cartao.`categoria_cart_nome_credenciadora`,categoria_cartao.`categoria_cart_descricao`", "asc");
		return $this->db->get($this->tabela)->result();	
	}

	public function pegarPorId($id)
	{
		$this->db->select('*');		
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{		
		$this->db->where('categoria_cart_cnpj_credenciadora', $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function todasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}


	public function selecionarBandeiras($termo)
	{
		$this->db->select('categoria_cart_id AS id, concat(categoria_cart_nome_credenciadora," - ",categoria_cart_descricao ) AS text');
		$this->db->where('categoria_cart_visivel', 1);

		$this->db->where("(categoria_cart_nome_credenciadora LIKE '%".$termo."%' OR categoria_cart_descricao LIKE '%".$termo."%')", NULL, FALSE);
		// $this->db->like('( sempemfanta',$termo);
		// $this->db->or_like('sempemrazao',$termo,')');	
		$this->db->limit(10);
		return $this->db->get('categoria_cartao')->result_array();
	}





}

/* End of file Cartao_model.php */
/* Location: ./application/models/Cartao_model.php */