<?php
defined('BASEPATH') or exit('No direct script access allowed');

class OperacaoCartao_model extends CI_Model
{

	public function listarOperacao($usuario, $usuaPadrao = null)
	{
		$this->db->select("*");
		$this->db->join('usuarios', ' `usuarios`.`usuario_id` = `operacao`.`usuario_id`', 'left');
		$this->db->join('cliente', '`cliente`.`cliente_id` = `operacao`.`cliente_id`', 'left');
		$this->db->join('financeiro', '`financeiro`.`idOperacao` = `operacao`.`operacao_id`', 'left');

		if ($usuaPadrao <> 1) {
			# Trazer apenas as operacao do mesmo
			$this->db->where('usuarios.usuario_id', $usuario);
		}

		$this->db->where('operacao_visivel', 1);
		$this->db->where('operacao_status', 'cartao');
		$this->db->where('financeiro_tipo', 'despesa');
		$this->db->group_by('idOperacao');
		$this->db->order_by('operacao_id', "desc");
		return $this->db->get('operacao')->result();
	}

	public function listarOperacaoImp($id)
	{
		$this->db->select("*");
		$this->db->join('usuarios ', ' `usuarios`.`usuario_id` = `operacao`.`usuario_id`', 'left');
		$this->db->join('cliente ', '`cliente`.`cliente_id` = `operacao`.`cliente_id`', 'left');
		$this->db->join('itens_operacao ', '`itens_operacao`.`operacao_id` = `operacao`.`operacao_id`', 'left');
		$this->db->where('operacao.operacao_id', $id);
		$this->db->where('operacao_visivel', 1);
		$this->db->where('operacao_status', 'cartao');
		$this->db->where('itens_operacao_visivel', 1);
		return $this->db->get('operacao')->result();
	}

	public function dadosCartaoUsados($id)
	{
		$this->db->where('taxa_visivel', 1);
		$this->db->where('taxa_usuario_id', $id);
		return $this->db->get('taxa_cartao_associado')->result();
	}
	
	public function verificarTaxaCliente($id)
	{
		$this->db->where('taxa_visivel', 1);
		$this->db->where('taxa_usuario_id', $id);
		$resultado = $this->db->get('taxa_cartao_associado')->result();
		return $resultado ? true : false;
	}

	public function dadosCartaoUsadosCsv($cliente, $bandeira, $tipo = null) 
	{
		// die(json_encode('tipo: ' . $tipo . ' bandeira: ' . $bandeira . ' cliente: ' . $cliente));
		$this->db->where('taxa_visivel', 1);
		$this->db->where('taxa_usuario_id', $cliente);
		$taxas =  $this->db->get('taxa_cartao_associado')->result();
		
		if(empty($taxas)) {
			return null;
		}

		$bandeiras = '';
		foreach ($taxas as $taxa) {
			$bandeiras .= $taxa->taxa_cartao_bandeira . ',';
		}
		$bandeiras_id = substr($bandeiras, 0, -1);
		// die(json_encode($bandeiras_id));
		$this->db->select('categoria_cart_id');
		$this->db->where('categoria_cart_visivel', 1);
		$this->db->where_in('categoria_cart_id', $bandeiras_id, false);

		if ($tipo == 'Crédito' || $tipo == 'Débito') {
			$this->db->like('categoria_cart_descricao', $bandeira);
		}
		$resultado = $this->db->get('categoria_cartao')->result();
		// die(json_encode($resultado));
		return !empty($resultado) ? $resultado[0]->categoria_cart_id : null;;
	}

	public function dadosCartaoBandeira($id)
	{
		$this->db->select('*');
		$this->db->where('categoria_cart_visivel', 1);
		$this->db->where_in('categoria_cart_id', $id, false);
		return $this->db->get('categoria_cartao')->result();
	}

	public function consultaParcela($parcela, $cartao, $usuario)
	{
		$this->db->select('taxa_cartao_' . $parcela . ' as taxa');
		$this->db->where('taxa_usuario_id', $usuario);
		$this->db->like('taxa_cartao_bandeira', $cartao);
		return $this->db->get('taxa_cartao_associado')->result();
	}

	public function consultaParcelaCsv($tipo, $parcela, $cartao, $usuario)
	{
		//$tipo = ($tipo == 'DÃ©bito') ? 'debito' : $tipo;
		if ($tipo == 'Crédito') {
			$this->db->select(
				$parcela >= 10
					? 'taxa_cartao_' . $parcela . ' as taxa'
					: ($parcela <= 0 ? 'taxa_cartao_01 as taxa' : 'taxa_cartao_0' . $parcela . ' as taxa')
			);
		} elseif ($tipo == 'Débito') {
			$this->db->select('taxa_cartao_debito as taxa');
		} else {
			$this->db->select('taxa_cartao_pix as taxa');
		}
		$this->db->where('taxa_usuario_id', $usuario);
		$this->db->where('taxa_visivel', 1);

		if ($tipo == 'Crédito' || $tipo == 'Débito') {
			$this->db->like('taxa_cartao_bandeira', $cartao);
		}
		return $this->db->get('taxa_cartao_associado')->result();
	}

	public function consultaParcelaBancoCsv($tipo, $parcela, $cartao, $usuario)
	{
		//$tipo = ($tipo == 'DÃ©bito') ? 'debito' : $tipo;
		if ($tipo == 'Crédito') {
			$this->db->select(
				$parcela >= 10
					? 'taxa_banco_' . $parcela . ' as taxa'
					: ($parcela <= 0 ? 'taxa_banco_01 as taxa' : 'taxa_banco_0' . $parcela . ' as taxa')
			);
		} elseif ($tipo == 'Débito') {
			$this->db->select('taxa_banco_debito as taxa');
		} else {
			$this->db->select('taxa_banco_pix as taxa');
		}
		$this->db->where('taxa_usuario_id', $usuario);
		$this->db->where('taxa_visivel', 1);

		if ($tipo == 'Crédito' || $tipo == 'Débito') {
			$this->db->like('taxa_cartao_bandeira', $cartao);
		}
		return $this->db->get('taxa_cartao_associado')->result();
	}

	public function consultaParcelaBanco($parcela, $cartao, $usuario)
	{
		$this->db->select('taxa_banco_' . $parcela . ' as taxa');
		$this->db->where('taxa_usuario_id', $usuario);
		$this->db->like('taxa_cartao_bandeira', $cartao);
		return $this->db->get('taxa_cartao_associado')->result();
	}

	public function consultaParcelaCartao($parcela, $cartao, $usuario)
	{
		$this->db->select('taxa_cartao_' . $parcela . ' as taxa');
		$this->db->where('taxa_usuario_id', $usuario);
		$this->db->like('taxa_cartao_bandeira', $cartao);
		return $this->db->get('taxa_cartao_associado')->result();
	}

	public function consultaCartao($cartao)
	{
		$this->db->select('*');
		$this->db->where('categoria_cart_id', $cartao);
		return $this->db->get('categoria_cartao')->result();
	}

	public function consultaCartaoDescricao($cartao)
	{
		$cartao = explode(' - ', $cartao);
		$this->db->select('*');
		$this->db->where('categoria_cart_nome_credenciadora', $cartao[0]);
		$this->db->where('categoria_cart_descricao', $cartao[1]);
		return $this->db->get('categoria_cartao')->result();
	}

	public function salvaSimulacao($dados)
	{
		$this->db->insert('simulacao_operacao', $dados);
		if ($this->db->affected_rows() == '1') {
			return true;
		}
		return false;
	}

	public function salvaVendaCartao($dados)
	{
		$this->db->insert('operacao', $dados);
		if ($this->db->affected_rows() == '1')
			return $this->db->insert_id();
		return false;
	}

	public function addItensOperacao($dados)
	{
		$this->db->insert('itens_operacao', $dados);
		if ($this->db->affected_rows() == '1') {
			return true;
		}
		return false;
	}

	public function addItensAutorizacaoOperacao($dados)
	{
		$this->db->insert('autorizacao_operacao', $dados);
		if ($this->db->affected_rows() == '1') {
			return true;
		}
		return false;
	}

	public function addItensOperacaoID($dados)
	{
		$this->db->insert('itens_operacao', $dados);
		if ($this->db->affected_rows() == '1') {
			return $this->db->insert_id();
		}
		return false;
	}

	public function buscarItensOperacao($id)
	{
		$this->db->select('*');
		$this->db->where('operacao_id', $id);
		$this->db->where('itens_operacao_visivel', 1);
		return $this->db->get('itens_operacao')->result();
	}

	public function buscarOperacao($id)
	{
		$this->db->select('*');
		$this->db->where('operacao_id', $id);
		$this->db->where('operacao_visivel', 1);
		$this->db->where('operacao_faturado', 0);
		return $this->db->get('operacao')->result();
	}

	public function buscarAutorizacoes($id)
	{
		$this->db->select('*');
		$this->db->where('operacao_id', $id);
		$this->db->where('autorizacao_operacao_visivel', 1);
		return $this->db->get('autorizacao_operacao')->result();
	}

	public function eddItensOperacao($data, $id)
	{
		$this->db->where('itens_operacao_id', $id);
		$this->db->update('itens_operacao', $data);

		if ($this->db->affected_rows() >= 0) {
			return true;
		}
		return false;
	}

	public function addAutorizacoesOperacaoID($dados)
	{
		$this->db->insert('autorizacao_operacao', $dados);
		if ($this->db->affected_rows() == '1') {
			return $this->db->insert_id();
		}
		return false;
	}

	public function eddAutorizacoesOperacao($data, $id)
	{
		$this->db->where('autorizacao_operacao_id', $id);
		$this->db->update('autorizacao_operacao', $data);

		if ($this->db->affected_rows() >= 0) {
			return true;
		}
		return false;
	}

	public function atualizarOperacao($data, $id)
	{
		$this->db->where('operacao_id', $id);
		//$this->db->update('operacao', $data);
		$this->db->set('operacao_id', $data['operacao_id']);
		$this->db->set('cliente_id', $data['cliente_id']);
		$this->db->set('operacao_total_boleto', $data['operacao_total_boleto']);
		$this->db->set('operacao_total_transacao', $data['operacao_total_transacao']);
		$this->db->set('operacao_faturado', $data['operacao_faturado']);
		$this->db->update('operacao');

		if ($this->db->affected_rows() >= 0) {
			return true;
		}
		return false;
	}

	public function get_cliente($q)
	{

		$this->db->select('*');
		$this->db->where('usuario_visivel', 1);
		$this->db->where('REPLACE(REPLACE(usuario_cpf, ".", ""), "-", "") = ', $q, FALSE);
		return $this->db->get('usuarios')->result();
	}

	public function adicionarCliente($dados)
	{
		$this->db->insert('cliente', $dados);
		if ($this->db->affected_rows() == '1') {
			return $this->db->insert_id();
		}
		return false;
	}


	public function addOperacaoFinanceiro($dados, $csv = false)
	{

		//var_dump($dados);die();

		$financeiroDespesa = array(
			'idOperacao'           => !empty($dados['operacao_id']) ? $dados['operacao_id'] : null,
			'financeiro_descricao' => 'Fatura Operação - #' . (!empty($dados['operacao_id']) ? $dados['operacao_id'] : ''),
			'financeiro_valor'     => !empty($dados['operacao_total_transacao']) ? $dados['operacao_total_transacao'] : null,
			'data_vencimento'      => !empty($dados['dataOperacao']) ? $dados['dataOperacao'] : null, // date("Y/d/m", strtotime($dados['dataOperacao'])),
			'data_pagamento'       => !empty($dados['dataOperacao']) ? $dados['dataOperacao'] : null, //date("Y/d/m", strtotime($dados['dataOperacao'])),
			'financeiro_baixado'   => 0,
			'financeiro_forn_clie_usua'    => !empty($dados['cliente_nome']) ? $dados['cliente_nome'] : null,
			'financeiro_forn_clie_usua_id' => !empty($dados['cliente_id']) ? $dados['cliente_id'] : null,
			'financeiro_bandeira_cart'     => !empty($dados['cartao_bandeira']) ? $dados['cartao_bandeira'] : null,
			'financeiro_autorizacao_NSU'   => !empty($csv) ? (!empty($dados['autorizacoes']) ? $dados['autorizacoes'] : null) : (!empty($dados['autorizacoes'][0]['autorizacao']) ? $dados['autorizacoes'][0]['autorizacao'] : null),
			'financeiro_forma_pgto' => 'Cartão de Crédito',
			'financeiro_tipo'       => 'despesa',
		);
		
		$financeiroReceita = array(
			'idOperacao'           => !empty($dados['operacao_id']) ? $dados['operacao_id'] : null,
			'financeiro_descricao' => 'Fatura Operação - #' . (!empty($dados['operacao_id']) ? $dados['operacao_id'] : ''),
			//'financeiro_valor' => !empty($dados['operacao_total_boleto']) && !empty($dados['operacao_total_transacao']) ? ($dados['operacao_total_boleto'] - $dados['operacao_total_transacao']) : null,
			'financeiro_valor'     => !empty($dados['luc_ope']) ? $dados['luc_ope'] : '0.00',			
			'data_vencimento'      => !empty($dados['dataOperacao']) ? $dados['dataOperacao'] : null, // date("Y/d/m", strtotime($dados['dataOperacao'])),
			'data_pagamento'       => !empty($dados['dataOperacao']) ? $dados['dataOperacao'] : null, //date("Y/d/m", strtotime($dados['dataOperacao'])),
			'financeiro_baixado'   => 0,
			'financeiro_forn_clie_usua'    => !empty($dados['cliente_nome']) ? $dados['cliente_nome'] : null,
			'financeiro_forn_clie_usua_id' => !empty($dados['cliente_id']) ? $dados['cliente_id'] : null,
			'financeiro_bandeira_cart'     => !empty($dados['cartao_bandeira']) ? $dados['cartao_bandeira'] : null,
			'financeiro_autorizacao_NSU'   => !empty($csv) ? (!empty($dados['autorizacoes']) ? $dados['autorizacoes'] : null) : (!empty($dados['autorizacoes'][0]['autorizacao']) ? $dados['autorizacoes'][0]['autorizacao'] : null),
			'financeiro_forma_pgto' => 'Cartão de Crédito',
			'financeiro_tipo'       => 'receita',
		);
		


		$this->db->trans_start(); // Inicia a transação

		// Primeiro INSERT
		$this->db->insert('financeiro', $financeiroDespesa);

		if ($this->db->affected_rows() == 1) {
			// Primeiro INSERT bem-sucedido

			// Segundo INSERT
			$this->db->insert('financeiro', $financeiroReceita);

			if ($this->db->affected_rows() == 1) {
				// Ambos os INSERTs foram bem-sucedidos
				$this->db->trans_commit(); // Confirma a transação
				return $this->db->insert_id();
			}
		} else {

			// Se algum dos INSERTs falhar, reverte a transação
			$this->db->trans_rollback();
			return false;
		}
	}

	public function getClienteNome($id)
	{
		$this->db->select('usuario_nome');
		$this->db->where('usuario_id', $id);
		$nome = $this->db->get('usuarios')->result();
		return $nome[0]->usuario_nome;
	}

	public function excluir($id)
	{

		$this->db->trans_start(); // Inicia a transação 

		// Primeira operação de atualização 
		$this->db->where('operacao_id', $id);
		$this->db->update('operacao', ['operacao_visivel' => 0]);

		// Segunda operação de atualização 
		$this->db->where('idOperacao', $id);
		$this->db->update('financeiro', ['financeiro_visivel' => 0]);

		$this->db->trans_complete(); // Finaliza a transação 

		if ($this->db->trans_status() === FALSE) {
			// A transação falhou, você pode executar ações de recuperação ou registro de erros aqui 
			return false;
		} else {
			// A transação foi bem-sucedida, você pode executar ações de confirmação aqui 
			return true;
		}
	}

	public function addOperacaoCSV($dados)
	{
		$this->db->insert('operacao_csv', $dados);
		if ($this->db->affected_rows() == '1') {
			return $this->db->insert_id();
		}
		return false;
	}

	public function existeRegistroUnico($dadosCsv)
	{
		$registroDuplicado = false;

		// $this->db->where('operacao_csv_id_transacao', $dadosCsv['operacao_csv_id_transacao']);
		$this->db->select('operacao_csv_id');
		if ($dadosCsv['operacao_csv_banco'] == 'PagBank') {
			if ($dadosCsv['operacao_csv_tipo_operacao'] !== 'Crédito' && $dadosCsv['operacao_csv_tipo_operacao'] !== 'Débito') {
				$this->db->where('operacao_csv_id_transacao', $dadosCsv['operacao_csv_id_transacao']);
			} else {
				$this->db->where('operacao_csv_codigo_nsu', $dadosCsv['operacao_csv_codigo_nsu']);
			}
			// $this->db->or_where('operacao_csv_codigo_nsu', $dadosCsv['operacao_csv_codigo_nsu']);
		}

		if ($dadosCsv['operacao_csv_banco'] == 'Stone') {
			// $this->db->or_where('operacao_csv_stone_id', $dadosCsv['operacao_csv_stone_id']);
			// if ($dadosCsv['operacao_csv_tipo_operacao'] !== 'Crédito' && $dadosCsv['operacao_csv_tipo_operacao'] !== 'Débito') {
			// 	$this->db->where('operacao_csv_id_transacao', $dadosCsv['operacao_csv_id_transacao']);
			// } else {
			// 	$this->db->where('operacao_csv_stone_id', $dadosCsv['operacao_csv_stone_id']);
			// }
			$this->db->where('operacao_csv_stone_id', $dadosCsv['operacao_csv_stone_id']);
		}

		$this->db->where('operacao_csv_visivel', 1);
		$query = $this->db->get('operacao_csv')->result();
		if (!empty($query)) {
			$registroDuplicado = true;
		}

		return $registroDuplicado;
	}

	public function cancelarOperacao($id)
	{
		$this->db->delete('operacao_csv', ['operacao_csv_id' => $id]);
		if ($this->db->affected_rows() == 1) {
			return true;
		}
	}

	public function lancamentosImportar()
	{
		$this->db->select(
			'operacao_csv_id,
			operacao_csv_parcelas, 
			operacao_csv_bandeira, 
			operacao_csv_valor_bruto, 
			operacao_csv_valor_liquido,
			operacao_csv_codigo_nsu,
			operacao_csv_banco,
			operacao_csv_stone_id,
			operacao_csv_importado,
			operacao_csv_tipo_operacao,
			operacao_csv_id_cliente,
			operacao_csv_id_usuario,
			operacao_csv_data_transacao, 
			data_criacao,
			usuarios.usuario_nome'
		);
		
		$this->db->where('operacao_csv_importado', 0);
		$this->db->where('operacao_csv_visivel', 1);
		
		$this->db->join('usuarios', 'usuarios.usuario_id = operacao_csv.operacao_csv_id_cliente', 'left');
		return $this->db->get('operacao_csv')->result();
	}

	public function visualizarOperacoes($data, $tipo)
	{
		// die(json_encode($data))
		
		$resultados = null;
		if ($tipo == 'mes') {
			$ano = explode('-', $data)[0];
			$mes = explode('-', $data)[1];

			$sql = 'SELECT 
						csv.operacao_csv_id,
						csv.operacao_csv_banco, 
						csv.operacao_csv_parcelas, 
						csv.operacao_csv_bandeira, 
						csv.operacao_csv_valor_bruto, 
						csv.operacao_csv_valor_liquido,
						csv.operacao_csv_codigo_nsu,
						csv.operacao_csv_stone_id,
						csv.operacao_csv_importado,
						csv.operacao_csv_tipo_operacao,
						csv.operacao_csv_id_cliente,
						csv.operacao_csv_id_usuario,
						csv.operacao_csv_data_transacao,
						csv.operacao_csv_id_operacao,
						u.usuario_nome
					FROM 
						operacao_csv csv
					LEFT JOIN
						usuarios u
					ON
						csv.operacao_csv_id_cliente = u.usuario_id
					WHERE 
						MONTH(csv.operacao_csv_data_transacao) = ? AND YEAR(csv.operacao_csv_data_transacao) = ? AND csv.operacao_csv_visivel = 1';
			// die(json_encode($sql));
			$resultados = $this->db->query($sql, array($mes, $ano));
		} else {
			// $data = date('Y-m-d', strtotime($data));
			$sql = 'SELECT 
						csv.operacao_csv_id,
						csv.operacao_csv_banco, 
						csv.operacao_csv_parcelas,
						csv.operacao_csv_bandeira, 
						csv.operacao_csv_valor_bruto, 
						csv.operacao_csv_valor_liquido,
						csv.operacao_csv_codigo_nsu,
						csv.operacao_csv_stone_id,
						csv.operacao_csv_importado,
						csv.operacao_csv_tipo_operacao,
						csv.operacao_csv_id_cliente,
						csv.operacao_csv_id_usuario,
						csv.operacao_csv_data_transacao,
						csv.operacao_csv_id_operacao,
						u.usuario_nome
					FROM 
						operacao_csv csv
					LEFT JOIN
						usuarios u
					ON
						csv.operacao_csv_id_cliente = u.usuario_id
					WHERE 
						DATE(csv.operacao_csv_data_transacao) = ? AND csv.operacao_csv_visivel = 1';
			$resultados = $this->db->query($sql, array($data));
		}

		// die(json_encode($resultados));
		return $resultados->result();
	}

	public function excluirOperacoes($operacoes) 
	{
		$this->db->trans_start();

		$this->db->where_in('operacao_csv_id_operacao', $operacoes);
		$this->db->delete('operacao_csv');

		$this->db->where_in('operacao_id', $operacoes);
		$this->db->delete('operacao');

		$this->db->where_in('operacao_id', $operacoes);
		$this->db->delete('itens_operacao');

		$this->db->where_in('idOperacao', $operacoes);
		$this->db->delete('financeiro');
		
		if ($this->db->trans_status() === FALSE) {
			$this->db->trans_rollback();
			return false;
		} else {
			$this->db->trans_commit();
			return true;
		}
	}


	public function importarOperacao($dados)
	{
		$this->db->where('operacao_csv_id', $dados['operacao_csv_id']);
		$this->db->where('operacao_csv_visivel', 1);
		$this->db->update('operacao_csv', [
			'operacao_csv_importado' => 1,
			'operacao_csv_id_operacao' => $dados['operacao_csv_id_operacao']
		]);

		if ($this->db->affected_rows() > 0) {
			return true;
		}

		return false;
	}


	public function buscarClientes($q)
	{
		$sql = "
				SELECT 
				`usuario_id` AS id, 
				`usuario_nome` AS nome, 
				'USUA' AS tipo, 
				`usuario_cpf` AS documento 
				FROM 
				 	`usuarios` 
				 WHERE 
					`usuario_nome` LIKE '%{$q}%'
					and `usuario_permissoes_id` = 3
					";

		$resultado = $this->db->query($sql)->result();

		$row_set = array();
		if (count($resultado) > 0) {
			foreach ($resultado as $row) {
				$row_set[] = array(
					'label' => $row->nome . ' | ' . $row->tipo,
					'id' => $row->id,
					'documento' => $row->documento
				);
			}
			echo json_encode($row_set);
		}
	}

	public function getClienteMaquinha($maquinha)
	{
		$this->db->select('maquina_id');
		$this->db->where('maquina_numero', $maquinha);
		// $this->db->where('maquina_visivel', 1);
		$resultado = $this->db->get('maquinas')->result();

		if (empty($resultado)) {
			return false;
		}

		$this->db->select('maquinas_clientes_cliente_id');
		$this->db->where('maquinas_clientes_maquina_id', $resultado[0]->maquina_id);
		$this->db->where('maquinas_clientes_possui', 1);
		$resultado = $this->db->get('maquinas_clientes')->result();

		if (empty($resultado)) {
			return false;
		}

		return $resultado[0]->maquinas_clientes_cliente_id;
	}
}
