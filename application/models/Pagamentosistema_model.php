<?php
class Pagamentosistema_model extends CI_Model {

    private $tabela  = 'pagamento_sistema';
    private $id      = 'pagamento_sistema_id';
    // private $visivel = 'contrato_visivel';

    function __construct() {
        parent::__construct();

    }

        public function listar()
    {
        $this->db->select('*');            
        $this->db->order_by($this->id,"desc");  
        return $this->db->get($this->tabela)->result(); 
    }


    function getAllTipos(){        
        return $this->db->get('pagamento_sistema')->result();
    }

    function getById($id){
        $this->db->where('pagamento_sistema_id',$id);
        $this->db->limit(1);
        return $this->db->get('pagamento_sistema')->row();
    }
    
	function count($table){        
		return $this->db->count_all($table);
	}
}