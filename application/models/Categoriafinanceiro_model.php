<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categoriafinanceiro_model extends CI_Model {
    
    public $tabela  = "categoria_financeiro";
    public $visivel = "categoria_fina_visivel";
    public $id   = "categoria_fina_id";

    public function listar()
    {
        $this->db->select("*");
        $this->db->where($this->visivel, 1);
        return $this->db->get($this->tabela)->result();
    }

    public function pegarPorId($id)
    {
        $this->db->select('*');     
        $this->db->where($this->id,$id);        
        $this->db->where($this->visivel, 1);
        return $this->db->get($this->tabela)->result();
    }


    public function inserir($dados)
    {
        $this->db->insert($this->tabela, $dados);

        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        
        return FALSE; 
    }

    public function editar($id,$dados)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->tabela,$dados);

        if($this->db->affected_rows() == '1')
        {
            return true;
        }

        return false;
    }


    public function excluir($id,$dados)
    {       
        $this->db->where($this->id, $id);
        $this->db->update($this->tabela,$dados);

        if($this->db->affected_rows() == '1')
        {
            return true;
        }

        return false;
    }
    

    public function visualizar($id)
    {
        $this->db->select("*");
        $this->db->where($this->chave, $id);
        $this->db->where($this->visivel, 1);
        return $this->db->get($this->tabela)->result();
    }

    public function todasPermissoes()
    {
        $this->db->select('*');     
        return $this->db->get('permissoes')->result();      
    }
}
