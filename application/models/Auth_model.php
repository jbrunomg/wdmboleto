<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_model extends CI_Model {

	private $tabela = 'usuarios';

	public function processarLogin($dados)
	{
		$this->db->select('*');
		$this->db->join('permissoes','permissao_id = usuario_permissoes_id');		
		$this->db->where($dados);
		return $this->db->get($this->tabela)->result();		
	}	

    public function pegarEmitente()
	{
		$this->db->select('*');
		return $this->db->get('emitente')->result();
	}
	
	public function editarEmitente($dados, $id)
	{
		$this->db->where('id',$id);		
		
		if($this->db->update('emitente',$dados))
		{
			return true;
		}

		return false;
	}	

	public function todosEstados()
	{
		$this->db->select('*');				
		return $this->db->get('tb_estados')->result();
	}
	public function todasCidadesIdEstado($estado)
	{
		$this->db->select('*');		
		$this->db->where('estado',$estado);		
		return $this->db->get('tb_cidades')->result();
	}
}

/* End of file Auth_model.php */
/* Location: ./application/models/Auth_model.php */