<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Arquivo_model extends CI_Model {

    private $tabela  = 'documentos';
    private $id      = 'idDocumentos';
    private $visivel = 'documento_visivel';

    public function listar()
    {
        $this->db->select('*');         
        $this->db->where($this->visivel, 1);    
        
        return $this->db->get($this->tabela)->result(); 
    }

    function getById($id){
        $this->db->where('idDocumentos',$id);
        $this->db->limit(1);
        return $this->db->get('documentos')->row();
    }

    function add($dados){
        $this->db->insert($this->tabela, $dados);        
        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        
        return FALSE;       
    }

    public function editar($id,$dados)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->tabela,$dados);

        if($this->db->affected_rows() == '1')
        {
            return true;
        }

        return false;
    }

    public function excluir($id,$dados)
    {        
        $this->db->where($this->id, $id);
        $this->db->update($this->tabela,$dados);

        if($this->db->affected_rows() == '1')
        {
            return true;
        }

        return false;
    }
    

    public function todasPermissoes()
    {
        $this->db->select('*');     
        return $this->db->get('permissoes')->result();      
    }
   
	

}

/* End of file arquivos_model.php */
/* Location: ./application/models/arquivos_model.php */