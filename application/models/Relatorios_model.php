<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Relatorios_model extends CI_Model {

	public function agenciaEmitente()
	{
		$this->db->select('*');		
		// $this->db->where('emitente_id',$id);		
		return $this->db->get('emitente')->result();
	}

	public function aniversariantesRapido($mes)
	{
		$this->db->select(' salualnome,talualniver,salualemail,salualtel01,salualtel02 ');	
		$this->db->where('estudante_visivel',1);
		$this->db->where_in('MONTH(talualniver)',$mes);
		return $this->db->get('tbalualunos')->result();
	}

	public function notasFiscaisRapido($mes,$ano)
	{
		$this->db->select('   `tbempempres`.`sempemcodsacado` AS sacado,
		  `tbdemonstrativoempr`.`demonstrativoempr_notafiscal` AS nota,	
		  `tbempempres`.`sempemrazao` AS empresa,  
		  `tbempempres`.`sempemfanta` AS fantasia, 
		  `tbdemonstrativoempr`.`demonstrativoempr_qtd_estud_cobrado` AS cobrado,
		  `tbdemonstrativoempr`.`demonstrativoempr_qtd_estud_ativo` AS ativo,
		  `tbdemonstrativoempr`.`demonstrativoempr_valor_cobrado` AS valor,
		  `tbempempres`.`sempemvenccontrato` AS vencimento,
		  `tbdemonstrativoempr`.`demonstrativoempr_valor_taxa` AS taxa  ');	

		$this->db->join('tbempempres','tbdemonstrativoempr.demonstrativoempr_empresa_id = tbempempres.pempemcodig');
		$this->db->where('demonstrativoempr_visivel',1);
		$this->db->where('demonstrativoempr_mes',$mes);
		$this->db->where('demonstrativoempr_ano',$ano);
		$this->db->order_by('tbempempres.sempemfanta','ASC');
		
		return $this->db->get('tbdemonstrativoempr')->result();
	}

	public function aniversariantesPersonalizado($dataInicial,$dataFinal)
	{
		$datas = "'".$dataInicial ."' and '". $dataFinal ."'";
		
		$this->db->select('salualnome,talualniver,salualemail,salualtel01,salualtel02');		
		$this->db->where('usuario_visivel',1);
		$this->db->where('talualniver BETWEEN ', $datas, FALSE);
		return $this->db->get('usuarios')->result();
	}


	public function imprimirdemonstrativo_OLD($empresa)
	{	
		$sql = '(SELECT
				  SUM(`tbempempres`.`sempemvalor`) 
				FROM
				  `tbcontrestempr` 
				  JOIN `tbempempres` 
				    ON `tbcontrestempr`.`iempemcodig` = `tbempempres`.`pempemcodig` 
				  JOIN `tbalualunos` 
				    ON `tbcontrestempr`.`ialualcodig` = `tbalualunos`.`palualcodig` 
				  JOIN `tbensensino` 
				    ON `tbalualunos`.`ialualinsen` = `tbensensino`.`pensencodig` 
				WHERE `empresa_visivel` = 1
				  AND contrato_visivel = 1
				  AND `iempemcodig` = '.$empresa.' 
				  AND ( ( (`contrestempr_status` = "Ativo") AND ((`contrestempr_data_renIII` >= (CURDATE())) OR (`contrestempr_data_renII` >= (CURDATE())) OR (`contrestempr_data_renI` >= (CURDATE()))  OR (`contrestempr_data_term` >= (CURDATE())) )) 
  				  OR ( (`contrestempr_status` = "Rescindido") AND (`contrestempr_data_rescisao` >= (CURDATE())) ) )
				   ) AS total';

		$this->db->select( $sql.', tbempempres.sempemrazao,tbempempres.sempemcnpj, tbempempres.sempemvalor, tbempempres.sempemvenccontrato, tbensensino.sensennome,  tbalualunos.*, tbcontrestempr.*, tbdemonstrativoempr.*');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig');
		$this->db->join('tbdemonstrativoempr','tbcontrestempr.iempemcodig = tbdemonstrativoempr.demonstrativoempr_empresa_id AND `tbdemonstrativoempr`.`demonstrativoempr_mes` = MONTH(NOW()) AND `tbdemonstrativoempr`.`demonstrativoempr_ano` = YEAR(NOW())','left');

		$this->db->where('contrato_visivel', 1);
		$this->db->where('empresa_visivel', 1);
		$this->db->where('iempemcodig',$empresa);
		$where = "( ( (`contrestempr_status` = 'Ativo') AND ((`contrestempr_data_renIII` >= (CURDATE())) OR (`contrestempr_data_renII` >= (CURDATE())) OR (`contrestempr_data_renI` >= (CURDATE()))  OR (`contrestempr_data_term` >= (CURDATE())) )) ";
		$this->db->where($where);
		$where = "( (`contrestempr_status` = 'Rescindido') AND (`contrestempr_data_rescisao` >= (CURDATE())) ) )";
		$this->db->or_where($where);		
		// $this->db->order_by("tbcontrestempr.contrestempr_id", "desc");	
		$this->db->order_by("tbalualunos.salualnome", "asc");
		return $this->db->get('tbcontrestempr')->result();	
	}

	public function imprimirdemonstrativo($empresa)
	{	
	
		$sql = '(SELECT
		  GROUP_CONCAT(`tbalualunos`.`palualcodig`) 
		FROM
		  `tbcontrestempr` 
		  JOIN `tbempempres` 
		    ON `tbcontrestempr`.`iempemcodig` = `tbempempres`.`pempemcodig` 
		  JOIN `tbalualunos` 
		    ON `tbcontrestempr`.`ialualcodig` = `tbalualunos`.`palualcodig` 
		LEFT  JOIN `tbensensino` 
		    ON `tbalualunos`.`ialualinsen` = `tbensensino`.`pensencodig` 
		WHERE `empresa_visivel` = 1
		  AND contrato_visivel = 1
		  AND `iempemcodig` = '.$empresa.' 
		  AND  ( (`contrestempr_status` = "Ativo") OR ( ( `contrestempr_status` = "Rescindido" ) AND ( MONTH(contrestempr_data_rescisao) = (MONTH(CURDATE())) ) ) )
			   ) AS grupo,';

		$sqlempresa = ' tbempempres.pempemcodig, tbempempres.sempemrazao,tbempempres.sempemcnpj, tbempempres.sempemvalor, tbempempres.sempememailgestor, tbensensino.sensennome, tbempempres.sempemvenccontrato, ';

		$this->db->select( $sql . $sqlempresa . ' tbalualunos.palualcodig, tbalualunos.salualnome , tbcontrestempr.contrestempr_data_ini , tbcontrestempr.contrestempr_data_term, tbcontrestempr.contrestempr_data_renI, tbcontrestempr.contrestempr_data_renII, tbcontrestempr.contrestempr_data_renIII, tbcontrestempr.contrestempr_data_rescisao, tbcontrestempr.contrestempr_status, tbdemonstrativoempr.* ');

		$this->db->join('tbempempres','tbcontrestempr.iempemcodig = tbempempres.pempemcodig');
		$this->db->join('tbalualunos','tbcontrestempr.ialualcodig = tbalualunos.palualcodig');
		$this->db->join('tbensensino','tbalualunos.ialualinsen = tbensensino.pensencodig','left');
		$this->db->join('tbdemonstrativoempr','tbcontrestempr.iempemcodig = tbdemonstrativoempr.demonstrativoempr_empresa_id AND `tbdemonstrativoempr`.`demonstrativoempr_mes` = MONTH(NOW()) AND `tbdemonstrativoempr`.`demonstrativoempr_ano` = YEAR(NOW())','left');

		$this->db->where('contrato_visivel', 1);
		$this->db->where('empresa_visivel', 1);
		$this->db->where('iempemcodig',$empresa);
		// Aecisa
		// if($empresa == '325'){
		// $where = " ( (`contrestempr_status` = 'Ativo')  
  //   	OR ((`contrestempr_status` = 'Rescindido') AND (contrestempr_data_rescisao BETWEEN DATE_FORMAT(CURRENT_DATE() - INTERVAL 1 MONTH, '%Y-%m-%20') AND DATE_FORMAT(CURRENT_DATE(), '%Y-%m-%19')) )) ";
		// } else {
		// $where = " ( (`contrestempr_status` = 'Ativo')  
  //   	OR ((`contrestempr_status` = 'Rescindido') AND (contrestempr_data_rescisao BETWEEN DATE_FORMAT(CURRENT_DATE() - INTERVAL 1 MONTH, '%Y-%m-%15') AND DATE_FORMAT(CURRENT_DATE(), '%Y-%m-%15')) )) ";	
		// }

		$where = " ( (`contrestempr_status` = 'Ativo')  
    	OR ((`contrestempr_status` = 'Rescindido') AND  ( MONTH(contrestempr_data_rescisao) = (MONTH(CURDATE())) ) )) ";	
		


		$this->db->where($where);	
		$this->db->order_by("tbalualunos.salualnome", "asc");
		return $this->db->get('tbcontrestempr')->result();	
	}

	public function checarDemonstrativoId($mes,$ano,$empresa)
	{

		$this->db->select('*');	

		$this->db->where('demonstrativoempr_visivel', 1);
		$this->db->where('demonstrativoempr_mes',$mes);	
		$this->db->where('demonstrativoempr_ano',$ano);
		$this->db->where('demonstrativoempr_empresa_id',$empresa);

		return $this->db->get('tbdemonstrativoempr')->result();

	}

	public function estornoDemonstrativoId($lastmes,$ano,$empresa)
	{	
		$query = "SELECT 
					(SELECT SUM(((sempemvalor / 30) * `contrestempr_estorno_dias`)) 
					FROM
					  `tbcontrestempr` 
					  JOIN `tbempempres` 
					    ON `tbcontrestempr`.`iempemcodig` = `tbempempres`.`pempemcodig` 
					  JOIN `tbalualunos` 
					    ON `tbcontrestempr`.`ialualcodig` = `tbalualunos`.`palualcodig` 
					WHERE `contrestempr_estorno` = 1 
					  AND MONTH(contrestempr_data_rescisao) = {$lastmes} 
					  AND YEAR(contrestempr_data_rescisao) = {$ano} 
					  AND `iempemcodig` = {$empresa} 
					) AS estornototal,
					  `salualnome`,
					  ((sempemvalor / 30) * `contrestempr_estorno_dias`) AS estorno,				
					  `contrestempr_data_rescisao` 
					FROM
					  `tbcontrestempr` 
					  JOIN `tbempempres` 
					    ON `tbcontrestempr`.`iempemcodig` = `tbempempres`.`pempemcodig` 
					  JOIN `tbalualunos` 
					    ON `tbcontrestempr`.`ialualcodig` = `tbalualunos`.`palualcodig` 
					WHERE `contrestempr_estorno` = 1 
					  AND MONTH(contrestempr_data_rescisao) = {$lastmes} 
					  AND YEAR(contrestempr_data_rescisao) = {$ano}  
					  AND `iempemcodig` = {$empresa} ";

		return $this->db->query($query)->result();

	}

	function imprimirpagamento($id){
        $this->db->where('pagamento_sistema_id',$id);
        $this->db->limit(1);
        return $this->db->get('pagamento_sistema')->row();
    }

	

	
	public function patrimonioRapido()
	{
		$this->db->select('patrimonio.patrimonio_nome,
						  patrimonio.patrimonio_aquisicao,
						  patrimonio.patrimonio_tipo,
						  patrimonio.patrimonio_etiqueta,
						  patrimonio.patrimonio_valor ');		
		$this->db->where('patrimonio_visivel',1);
		return $this->db->get('patrimonio')->result();
	}

	public function patrimonioPersonalizado($dataInicial,$dataFinal,$where)
	{
		$datas = "'".$dataInicial ."' and '". $dataFinal ."'";

		$this->db->select('patrimonio.patrimonio_nome,
						  patrimonio.patrimonio_aquisicao,
						  patrimonio.patrimonio_tipo,
						  patrimonio.patrimonio_etiqueta,
						  patrimonio.patrimonio_valor ');		
		$this->db->where('patrimonio_aquisicao BETWEEN ', $datas, FALSE);
		$this->db->where($where);
		return $this->db->get('patrimonio')->result();
	}

	public function membrosRapidoSimples()
	{
		$this->db->select('usuarios.usuario_nome,
						  usuarios.usuario_telefone,
						  usuarios.usuario_email ');		
		$this->db->where('usuario_visivel',1);
		return $this->db->get('usuarios')->result();
	}

	public function demonstrativofinanceiro($ano)
	{	
		$this->db->select("  `demonstrativofin_id`,`demonstrativofin_mes`,`demonstrativofin_ano`,
							 CONCAT('R$ ',`demonstrativofin_faturamento`) AS demonstrativofin_faturamento,
							 CONCAT('R$ ',`demonstrativofin_credito`) AS demonstrativofin_credito,
							 CONCAT('R$ ',`demonstrativofin_debito`) AS demonstrativofin_debito,
							 CONCAT('R$ ',`demonstrativofin_lucro_prejuizo`) AS demonstrativofin_lucro_prejuizo,
							 CONCAT('R$ ',`demonstrativofin_cc_desc_serv`) AS demonstrativofin_cc_desc_serv,
							 CONCAT('R$ ',`demonstrativofin_cp_saldo_inicial`) AS demonstrativofin_cp_saldo_inicial,
							 CONCAT('R$ ',`demonstrativofin_cp_renda_credito`) AS demonstrativofin_cp_renda_credito,
							 CONCAT('R$ ',`demonstrativofin_cp_desc_taxas`) AS demonstrativofin_cp_desc_taxas,
							 CONCAT('R$ ',`demonstrativofin_cp_mov_debito`) AS demonstrativofin_cp_mov_debito, 
							 CONCAT('R$ ',`demonstrativofin_cp_mov_credito`) AS demonstrativofin_cp_mov_credito,
							 CONCAT('R$ ',`demonstrativofin_cp_saldo_final`) AS demonstrativofin_cp_saldo_final  ");		
		$this->db->where('demonstrativofin_ano',$ano);
		$this->db->where('demonstrativofin_visivel',1);
		return $this->db->get('tbdemonstrativofin')->result();
	}

	public function demonstrativoadministrador($ano)
	{	
		$this->db->select('*');		
		$this->db->where('demonstrativoadm_ano',$ano);
		$this->db->where('demonstrativoadm_visivel',1);
		return $this->db->get('tbdemonstrativoadm')->result();
	}

	public function demonstrativorecrutamento($ano)
	{	
		$this->db->select('*');		
		$this->db->where('demonstrativorecrut_ano',$ano);
		$this->db->where('demonstrativorecrut_visivel',1);
		return $this->db->get('tbdemonstrativorecrut')->result();
	}

	public function listarinstituicao()
	{
		$this->db->select('*');		
		$this->db->where('instensino_visivel', 1);
		$this->db->order_by('tbensensino.sensennome', 'asc');		

		return $this->db->get('tbensensino')->result();	
	}

	public function instituicaoRapido()
	{
				
		$query = "SELECT TRIM(`tbensensino`.`sensennome`) AS sensennome, `tbalualunos`.`salualnome`  FROM `tbcontrestempr`

				INNER JOIN `tbalualunos` ON `tbcontrestempr`.`ialualcodig`  = `tbalualunos`.`palualcodig`

				INNER JOIN `tbensensino` ON `tbalualunos`.`ialualinsen`  = `tbensensino`.`pensencodig`
				 WHERE `contrestempr_status` = 'Ativo'
				 ORDER BY sensennome " ;

		return $this->db->query($query)->result();
		
	}

	public function listarempresa()
	{
		$this->db->select('*');
		//$this->db->join('permissoes','permissao_id = usuario_permissoes_id');	
		$this->db->where('empresa_visivel', 1);
		$this->db->order_by('tbempempres.sempemfanta', 'asc');		

		return $this->db->get('tbempempres')->result();	
	}

	public function empresainativa($mes,$ano)
	{
		//  EMPRESAS INATIVAS		
		$query = " SELECT `sempemfanta` AS inativas FROM tbempempres WHERE pempemcodig IN (
				   SELECT `demonstrativoempr_empresa_id` FROM `tbdemonstrativoempr`
				   WHERE (`demonstrativoempr_mes` = {$mes}-1 AND `demonstrativoempr_ano` = {$ano}) 
				   AND demonstrativoempr_empresa_id NOT IN ( 
				   SELECT `demonstrativoempr_empresa_id` FROM `tbdemonstrativoempr`
				   WHERE (`demonstrativoempr_mes` = {$mes} AND `demonstrativoempr_ano` = {$ano}) )
					) ";

		return $this->db->query($query)->result();
		
	}

	// EMPRESAS NOVAS
	public function empresanova($mes,$ano)
	{	
		$mesano = trim(trim($mes).'/'.trim($ano)); 

		$query = " SELECT `sempemfanta` AS novas FROM tbempempres WHERE pempemcodig IN (
				   SELECT `demonstrativoempr_empresa_id` FROM `tbdemonstrativoempr`
				   WHERE (`demonstrativoempr_mes` = {$mes} AND `demonstrativoempr_ano` = {$ano}) 
				   AND demonstrativoempr_empresa_id NOT IN ( 
				   SELECT `demonstrativoempr_empresa_id` FROM `tbdemonstrativoempr`
				   WHERE CONCAT(`demonstrativoempr_mes`,'/',`demonstrativoempr_ano`) <> '{$mesano}')
					) ";

		
		return $this->db->query($query)->result();
		
	}

	// EMPRESAS REATIVAÇÃO
	public function empresareativada($mes,$ano)
	{		 
		$mesano = trim(trim($mes).'/'.trim($ano));
		
		$query = " SELECT `sempemfanta` AS reativada FROM tbempempres WHERE pempemcodig IN (
			   SELECT `demonstrativoempr_empresa_id` FROM `tbdemonstrativoempr`
			   WHERE (`demonstrativoempr_mes` = {$mes} AND `demonstrativoempr_ano` = {$ano}) 
			   AND demonstrativoempr_empresa_id NOT IN ( 
			   SELECT `demonstrativoempr_empresa_id` FROM `tbdemonstrativoempr`
			   WHERE (`demonstrativoempr_mes` = {$mes}-1 AND `demonstrativoempr_ano` = {$ano}) ) AND demonstrativoempr_empresa_id NOT IN (
			   	SELECT `demonstrativoempr_empresa_id` FROM `tbdemonstrativoempr`
				   WHERE (`demonstrativoempr_mes` = {$mes} AND `demonstrativoempr_ano` = {$ano}) 
				   AND demonstrativoempr_empresa_id NOT IN ( 
				   SELECT `demonstrativoempr_empresa_id` FROM `tbdemonstrativoempr`
				   WHERE CONCAT(`demonstrativoempr_mes`,'/',`demonstrativoempr_ano`) <> '{$mesano}')
					)
				) ";

		return $this->db->query($query)->result();
		
	}

	// ESTUDANTES ATIVO/INATIVO
	public function estudanteAtivoInativo($tipo)
	{		 

		$query = " SELECT `salualnome`, lower(`salualemail`) AS salualemail, `contrato` FROM `vw_estudante` WHERE `contrato` = '{$tipo}' ";

		return $this->db->query($query)->result();
		
	}


	public function transacoes($mes, $ano)
	{		 

		$this->db->select('*');
		$this->db->join('cliente','cliente.cliente_id = operacao.cliente_id');	
		$this->db->join('usuarios','usuarios.usuario_id = operacao.usuario_id');
		$this->db->join('itens_operacao','itens_operacao.operacao_id = operacao.operacao_id');
		$this->db->join('autorizacao_operacao','autorizacao_operacao.operacao_id = operacao.operacao_id');
		$this->db->where('operacao_visivel', 1);
		$this->db->where('itens_operacao_visivel', 1);
		$this->db->where('autorizacao_operacao_visivel', 1);
		$this->db->where('operacao_faturado', 1);
		if($mes){
			$this->db->where('MONTH(operacao_data_cadastro)', $mes);
		}
		if($ano){
			$this->db->where('YEAR(operacao_data_cadastro)', $ano);
		}
		
		$this->db->group_by('autorizacao_operacao.operacao_id'); 
		return $this->db->get('operacao')->result();	
		
	} 

	public function transacoesPerido($ano)
	{		
		$this->db->select('*');
		$this->db->join('cliente','cliente.cliente_id = operacao.cliente_id');	
		$this->db->join('usuarios','usuarios.usuario_id = operacao.usuario_id');
		$this->db->join('itens_operacao','itens_operacao.operacao_id = operacao.operacao_id');
		$this->db->join('autorizacao_operacao','autorizacao_operacao.operacao_id = operacao.operacao_id');
		$this->db->where('operacao_visivel', 1);
		$this->db->where('itens_operacao_visivel', 1);
		$this->db->where('autorizacao_operacao_visivel', 1);
		$this->db->where('operacao_faturado', 1);
		if(!empty($ano)){
			$this->db->where('YEAR(operacao_data_cadastro) =', $ano);
		}

		$this->db->group_by('autorizacao_operacao.operacao_id'); 
		return $this->db->get('operacao')->result();	
		
	} 



	// public function transacoesCartao($mes, $ano)
	// {		 

	// 	$this->db->select(' `operacao`.`operacao_id` , `operacao_data_cadastro`, `usuario_nome`, `itens_operacao_bandeira_cartao`,`itens_operacao_numero_parcela`,
 	// 	`autorizacao_operacao_numero_nsu` ,  `operacao_total_boleto`, `operacao_total_transacao`, `operacao_taxa`');			
	// 	$this->db->join('usuarios','usuarios.usuario_id = operacao.usuario_id');
	// 	$this->db->join('itens_operacao','itens_operacao.operacao_id = operacao.operacao_id');
	// 	$this->db->join('autorizacao_operacao','autorizacao_operacao.operacao_id = operacao.operacao_id');
	// 	$this->db->where('operacao_visivel', 1);
	// 	$this->db->where('itens_operacao_visivel', 1);
	// 	$this->db->where('autorizacao_operacao_visivel', 1);
	// 	$this->db->where('operacao_faturado', 1);
	// 	if($mes){
	// 		$this->db->where('MONTH(operacao_data_cadastro)', $mes);
	// 	}
	// 	if($ano){
	// 		$this->db->where('YEAR(operacao_data_cadastro)', $ano);
	// 	}
		
	// 	$this->db->group_by('autorizacao_operacao.operacao_id'); 
	// 	return $this->db->get('operacao')->result();	
		
	// } 

	public function transacoesPeridoCart($inicio, $fim, $usuario)
	{	
		$this->db->select('*');
		//$this->db->join('cliente','cliente.cliente_id = operacao.cliente_id');	

		$this->db->join('usuarios','usuarios.usuario_id = operacao.cliente_id');
		$this->db->join('itens_operacao','itens_operacao.operacao_id = operacao.operacao_id');
		$this->db->join('autorizacao_operacao','autorizacao_operacao.operacao_id = operacao.operacao_id');
		$this->db->join('financeiro','operacao.operacao_id = financeiro.idOperacao and financeiro.financeiro_tipo = "despesa"');
		$this->db->where('operacao_visivel', 1);
		$this->db->where('itens_operacao_visivel', 1);
		$this->db->where('autorizacao_operacao_visivel', 1);
		$this->db->where('operacao_faturado', 1);

		if($usuario){
			$this->db->where('usuarios.usuario_id', $usuario);
		}

		if(!empty($inicio)){
			$this->db->where('operacao_data_cadastro >=', $inicio);
		}

		if(!empty($fim)){
			$this->db->where('operacao_data_cadastro <=', $fim);
		}

		$this->db->group_by('autorizacao_operacao.operacao_id');
		return $this->db->get('operacao')->result();
		
	} 

	public function transacoesCartao($mes, $ano)
	{		
		$this->db->select('*');
		//$this->db->join('cliente','cliente.cliente_id = operacao.cliente_id');	
		$this->db->join('usuarios','usuarios.usuario_id = operacao.cliente_id');
		$this->db->join('itens_operacao','itens_operacao.operacao_id = operacao.operacao_id');
		$this->db->join('autorizacao_operacao','autorizacao_operacao.operacao_id = operacao.operacao_id');
		$this->db->join('financeiro','operacao.operacao_id = financeiro.idOperacao and financeiro.financeiro_tipo = "despesa"');

		$this->db->where('operacao_visivel', 1);
		$this->db->where('itens_operacao_visivel', 1);
		$this->db->where('autorizacao_operacao_visivel', 1);
		$this->db->where('operacao_faturado', 1);

		if($mes){
			$this->db->where('MONTH(operacao_data_cadastro)', $mes);
		}
		if($ano){
			$this->db->where('YEAR(operacao_data_cadastro)', $ano);
		}

		$this->db->group_by('autorizacao_operacao.operacao_id'); 
		return $this->db->get('operacao')->result();	
		
	} 

	public function autoCompleteUsuarios($q)
    {   
        $sql = "SELECT `usuario_id` AS id, `usuario_nome` AS nome, 'USUA' AS tipo FROM `usuarios` WHERE `usuario_nome` like '%{$q}%' ";
      
        $resultado = $this->db->query($sql)->result();

        if(count($resultado) > 0){
            foreach ($resultado as $row){
                $row_set[] = array('label'=>$row->nome.' | '.$row->tipo,'id'=>$row->id);
            }
            echo json_encode($row_set);
        }
    }


    public function fechamentoCaixa($data) 
	{	
		$dataInicial = $data;
		$dataFinal   = $data;

        $sql = "			
     		SELECT 
      
			(SELECT SUM(`operacao_total_boleto`) AS valorTotal FROM `operacao` WHERE `operacao_data_cadastro` = '{$data}'      
			AND `operacao_visivel` = 1   ) AS valorOperacao,

			(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE  `data_vencimento` = '{$data}'     
			AND `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1   ) AS lucroMix,

			(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE  `data_vencimento` = '{$data}' 
			AND `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1 AND `idOperacao` IS NULL  ) AS despesaLoja
        ";

		   // var_dump($sql);die();	
			
        return $this->db->query($sql)->result(); // $this->db->query($query, array($dataInicial,$dataFinal))->result(); 

    }

    public function fechamentoCaixaMes($dataFormatada) 
	{		
		$dataInicial = explode('-',  $dataFormatada);

		$data = $dataInicial[1];
		$ano = $dataInicial[0];

        $sql = "			
     	SELECT 	  	 
       
		( SELECT SUM(`operacao_total_boleto`) AS valorTotal FROM `operacao` WHERE `operacao_visivel` = 1 
		AND YEAR(`operacao_data_cadastro`) = '{$ano}'   AND MONTH(`operacao_data_cadastro`) = '{$data}'  ) AS valorOperacaoMes,


		(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE `financeiro_tipo` = 'receita' AND `financeiro_visivel` = 1 
		AND  YEAR(`data_vencimento`) = '{$ano}'  AND MONTH(`data_vencimento`) = '{$data}'   ) AS lucroMixMes,

		(SELECT SUM(`financeiro_valor`) AS valorTotal FROM financeiro WHERE  `financeiro_tipo` = 'despesa' AND `financeiro_visivel` = 1 AND `idOperacao` IS NULL 
		AND  YEAR(`data_vencimento`) = '{$ano}'  AND MONTH(`data_vencimento`) = '{$data}'  ) AS despesaLojaMes  
        
        ";

		// var_dump($sql);die();	
			
        return $this->db->query($sql)->result(); // $this->db->query($query, array($dataInicial,$dataFinal))->result(); 

    }

	public function despesaReceitaRapido($tipo, $data)
	{
		$query = "SELECT idFinanceiro, financeiro_descricao, data_vencimento, financeiro_forn_clie_usua, 
						financeiro_valor, data_pagamento, financeiro_forma_pgto, financeiro_baixado, financeiro_tipo 
				FROM financeiro WHERE financeiro_visivel = 1";

		$params = [];

		if ($tipo == "mes") {
			$query .= " AND data_vencimento LIKE ?";
			$params[] = $data . '%';
			} else {
			$anoAtual = date('Y');
			if ($tipo == "1Semestre") {
				$dataInicial = "{$anoAtual}-01-01";
				$dataFinal = "{$anoAtual}-06-30";
			} else {
				$dataInicial = "{$anoAtual}-07-01";
				$dataFinal = "{$anoAtual}-12-31";
			}
			$query .= " AND data_vencimento BETWEEN ? AND ?";
			$params[] = $dataInicial;
			$params[] = $dataFinal;
		}

		// $query .= " ORDER BY data_vencimento ASC LIMIT 200";
		$query .= " ORDER BY data_vencimento ASC";

		return $this->db->query($query, $params)->result();
	}

	public function despesaReceitaFormaPag()
	{
		$query = "SELECT financeiro_forma_pgto FROM financeiro WHERE financeiro_visivel = 1 GROUP BY financeiro_forma_pgto";
		return $this->db->query($query)->result();
	}

	public function despesaReceitaPersonalizado($pegarPor, $dataInicial, $dataFinal, $usuarioId, $tipoConta, $situacao, $formaPgto)
	{
		$query = "SELECT idFinanceiro, financeiro_descricao, data_vencimento, financeiro_forn_clie_usua, 
						 financeiro_valor, data_pagamento, financeiro_forma_pgto, financeiro_baixado, financeiro_tipo 
				  FROM financeiro WHERE financeiro_visivel = 1";
	
		if (!in_array($pegarPor, ['data_vencimento', 'data_pagamento'])) {
			return false;
		}
	
		if ($dataInicial && $dataFinal) {
			$query .= " AND {$pegarPor} BETWEEN ? AND ?";
		}
	
		if ($usuarioId != 'Todos') {
			$query .= " AND financeiro_forn_clie_usua_id = ?";
		}
	
		if ($tipoConta != 0) {
			$query .= " AND financeiro_tipo = ?";
		}
	
		if ($situacao != 0) {
			$query .= " AND financeiro_baixado = ?";
		}
	
		if ($formaPgto) {
			$query .= " AND financeiro_forma_pgto = ?";
		}
	
		// $query .= " ORDER BY {$pegarPor} ASC LIMIT 200";
		$query .= " ORDER BY {$pegarPor} ASC";
	
		$params = [];
		if ($dataInicial && $dataFinal) {
			$params[] = $dataInicial;
			$params[] = $dataFinal;
		}
	
		if ($usuarioId != 'Todos') {
			$params[] = $usuarioId;
		}
	
		if ($tipoConta != 0) {
			$params[] = ($tipoConta == 1) ? 'receita' : 'despesa';
		}
	
		if ($situacao != 0) {
			$params[] = ($situacao == 1) ? 1 : 0;
		}
	
		if ($formaPgto) {
			$params[] = $formaPgto;
		}
		// die(var_dump($query));
		return $this->db->query($query, $params)->result();
	}
}

/* End of file Relatorios_model.php */
/* Location: ./application/models/Relatorios_model.php */