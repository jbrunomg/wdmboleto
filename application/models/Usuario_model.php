<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usuario_model extends CI_Model {

	private $tabela  = 'usuarios';
	private $id      = 'usuario_id';
	private $visivel = 'usuario_visivel';

	public function listar()
	{
		$this->db->select('*');
		$this->db->join('permissoes','permissao_id = usuario_permissoes_id');	
		$this->db->where($this->visivel, 1);	
		return $this->db->get($this->tabela)->result();	
	}

	public function pegarPorId($id)
	{
		$this->db->select('*');		
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}

	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			return TRUE;
		}
		
		return FALSE; 
	}

	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function todosEstados()
	{
		$this->db->select('*');				
		return $this->db->get('tb_estados')->result();
	}
	public function tadasCidadesIdEstado($estado)
	{
		$this->db->select('*');		
		$this->db->where('estado',$estado);		
		return $this->db->get('tb_cidades')->result();
	}
	public function tadasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}
    public function listarMaquinasDoUsuario($id)
    {
        $this->db->from('maquinas')
            ->select(" CONCAT(categoria_cart_nome_credenciadora,' - ', categoria_cart_cnpj_credenciadora) AS administradora_cartao, maquinas.*, usuario_id, maquinas_clientes_id ")
            ->join('maquinas_clientes','maquina_id = maquinas_clientes_maquina_id')
            ->join('usuarios', 'usuario_id = maquinas_clientes_cliente_id')
            ->join('categoria_cartao','maquina_categoria_cartao_id = categoria_cart_id')
            ->where('usuario_id',$id)
            ->where('maquinas_clientes_visivel',1)
            ->where('maquinas_clientes_possui',1);

        $query = $this->db->get();
        return $query ? $query->result() : false;
    }
    public function excluirMaquina($id)
    {
        $this->db->update('maquinas_clientes',['maquinas_clientes_visivel' => 0],"maquinas_clientes_id = $id");

        if($this->db->affected_rows() == '1') {
            return true;
        }

        return false;
    }
}

/* End of file Usuario_model.php */
/* Location: ./application/models/Usuario_model.php */