<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Taxa_model extends CI_Model {

	private $tabela  = 'taxa_cartao_associado';
	private $id      = 'taxa_id';
	private $visivel = 'taxa_visivel';

	public function listar()
	{
		$this->db->select('(SELECT 
			GROUP_CONCAT(categoria_cart_descricao) 
		FROM
			categoria_cartao 
		WHERE FIND_IN_SET(
		  	categoria_cart_id,taxa_cartao_bandeira)) AS bandeira,
			(SELECT GROUP_CONCAT(DISTINCT categoria_cart_nome_credenciadora) FROM categoria_cartao 
		WHERE FIND_IN_SET( categoria_cart_id, taxa_cartao_bandeira)
		) AS credenciadora,  taxa_cartao_associado.*, usuarios.*');		
		$this->db->join('usuarios ',' `usuarios`.`usuario_id` = `taxa_cartao_associado`.`taxa_usuario_id`' , 'left');
		$this->db->where($this->visivel, 1);			
		return $this->db->get($this->tabela)->result();	
	}

	public function pegarPorId($id)
	{
		$this->db->select('*');	
		$this->db->join('usuarios ',' `usuarios`.`usuario_id` = `taxa_cartao_associado`.`taxa_usuario_id`' , 'left');	
		$this->db->where($this->id,$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function pegarAssociado($id)
	{
		$this->db->select(' GROUP_CONCAT(`taxa_cartao_bandeira`) AS taxa_cartao_bandeira  ');				
		$this->db->where('taxa_usuario_id',$id);		
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function pegarBandeiraLivre($bandeira)
	{
		$this->db->select('*');	
		$this->db->where_not_in('categoria_cart_id', $bandeira);
		$this->db->where('categoria_cart_visivel', 1);	
		return $this->db->get('categoria_cartao')->result();		
	}


	public function inserir($dados)
	{
		$this->db->insert($this->tabela, $dados);

		if ($this->db->affected_rows() == '1')
		{
			//return TRUE;
			return $this->db->insert_id($this->tabela);
		}
		
		return FALSE; 
	}


	public function editar($id,$dados)
	{
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}


	public function excluir($id,$dados)
	{		
		$this->db->where($this->id, $id);
		$this->db->update($this->tabela,$dados);

		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;
	}

	public function todasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}

	public function todasBandeiras()
	{
		$this->db->select('*');	
		$this->db->where('categoria_cart_visivel', 1);	
		return $this->db->get('categoria_cartao')->result();		
	}

	public function todosAssociados()
	{
		$this->db->select('*');		
		return $this->db->get('usuarios')->result();		
	}


}

/* End of file Taxa_model.php */
/* Location: ./application/models/Taxa_model.php */