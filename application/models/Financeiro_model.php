<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Financeiro_model extends CI_Model {
	
	public $tabela  = "financeiro";
	public $visivel = "financeiro_visivel";
	public $chave   = "idFinanceiro";


	public function listar($limit, $start, $search, $filtro) 
	{
		$this->db->select('*');
		$this->db->from($this->tabela);
		$this->db->join('usuarios', '`usuarios`.`usuario_id` = financeiro.financeiro_forn_clie_usua_id');
		$this->db->join('categoria_cartao', 'categoria_cartao.categoria_cart_id = financeiro.financeiro_bandeira_cart');
		$this->db->where('financeiro_visivel', 1);
	
		// Aplica o filtro recebido
		if (!empty($filtro)) {
			$this->db->where($filtro);
		}
	
		$this->db->limit($limit, $start);
	
		// Execute a consulta e retorne os resultados
		$resultados = $this->db->get()->result();
	
		// Contagem total de registros sem limite
		$this->db->from($this->tabela);
		$this->db->join('usuarios', '`usuarios`.`usuario_id` = financeiro.financeiro_forn_clie_usua_id');
		$this->db->join('categoria_cartao', 'categoria_cartao.categoria_cart_id = financeiro.financeiro_bandeira_cart');
		$this->db->where('financeiro_visivel', 1);
	
		// Aplica o filtro para contar os registros
		if (!empty($filtro)) {
			$this->db->where($filtro);
		}
	
		$total_registros = $this->db->count_all_results();
	
		return array('dados' => $resultados, 'total_registros' => $total_registros);
	}
	
	public function listarId($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}


	public function add($table,$data){

        $this->db->insert($table, $data);         

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;
		}	

		return FALSE;     

    }   

	public function edit($table,$data,$fieldID,$ID){
        $this->db->where($fieldID,$ID);
        $this->db->update($table, $data);


        if ($this->db->affected_rows() >= 0)
		{
			return TRUE;
		}	

		return FALSE;   
    }


    function delete($table,$fieldID,$ID){

        $this->db->where($fieldID,$ID);

        $this->db->delete($table);

        if ($this->db->affected_rows() == '1')

		{
			return TRUE;
		}		

		return FALSE;    

    }

	public function visualizar($id)
	{
		$this->db->select("*");
		$this->db->where($this->chave, $id);
		$this->db->where($this->visivel, 1);
		return $this->db->get($this->tabela)->result();
	}
	
	public function getAllTiposCartao()
	{
		$this->db->where('categoria_cart_visivel', 1);
		return $this->db->get('categoria_cartao')->result();
	}

	public function getAllTiposCategorias()
	{
		$this->db->where('categoria_fina_visivel', 1);
		return $this->db->get('categoria_financeiro')->result();
	}

	public function autoCompleteClienteFornecedor($q)
    {   
        $sql = "SELECT `cliente_id` AS id  , `cliente_nome` AS nome, 'CLIE' AS tipo FROM clientes where cliente_nome like '%{$q}%'
                UNION    
                SELECT `fornecedor_id` AS id , `fornecedor_nome` AS nome, 'FORN' AS tipo FROM fornecedores where fornecedor_nome like '%{$q}%' 
                UNION 
                SELECT `usuario_id` AS id, `usuario_nome` AS nome, 'USUA' AS tipo FROM `usuarios` WHERE `usuario_nome` like '%{$q}%'";
      
        $resultado = $this->db->query($sql)->result();

        if(count($resultado) > 0){
            foreach ($resultado as $row){
                $row_set[] = array('label'=>$row->nome.' | '.$row->tipo,'id'=>$row->id);
            }
            echo json_encode($row_set);
        }
    }

    public function autoCompleteCliente($q)
    {   
        $sql = "SELECT `cliente_id` AS id  , `cliente_nome` AS nome, 'CLIE' AS tipo FROM cliente where cliente_nome like '%{$q}%' 
                UNION 
                SELECT `usuario_id` AS id, `usuario_nome` AS nome, 'USUA' AS tipo FROM `usuarios` WHERE `usuario_nome` like '%{$q}%' ";
      
        $resultado = $this->db->query($sql)->result();

        if(count($resultado) > 0){
            foreach ($resultado as $row){
                $row_set[] = array('label'=>$row->nome.' | '.$row->tipo,'id'=>$row->id);
            }
            echo json_encode($row_set);
        }
    }

    public function financeiroBaixarListar()
    {   
        $this->db->select("*");
        $this->db->join('usuarios',' `usuarios`.`usuario_id` = `baixar_conta`.`id_cliente_usua`');
		$this->db->order_by('baixar_conta.id', 'desc');
		return $this->db->get('baixar_conta')->result();
    }

    public function clientesPendentes($tipo)
    {
		$this->db->select("usuarios.usuario_id, usuarios.usuario_nome, SUM(financeiro.financeiro_valor) AS valor");
		$this->db->join('usuarios', 'usuarios.usuario_id = financeiro.financeiro_forn_clie_usua_id');
		$this->db->where('financeiro.financeiro_baixado', 0);
		$this->db->where('financeiro.financeiro_visivel', 1);
		$this->db->where('financeiro.financeiro_tipo', $tipo);
		$this->db->group_by('financeiro.financeiro_forn_clie_usua_id');

		return $this->db->get('financeiro')->result();
    }


    public function financeiroBaixarAdd($dados)
    {
        $this->db->insert('baixar_conta', $dados);

        if ($this->db->affected_rows() == '1')
        {
            return TRUE;
        }
        
        return FALSE; 
    }

    public function financeiroBuscaDivida($cliente, $tipo)
    {   
        $this->db->select("*");     
        $this->db->where('financeiro.`financeiro_baixado`', 0);
        $this->db->where('financeiro.`financeiro_visivel`', 1);
        $this->db->where('financeiro.`financeiro_forn_clie_usua_id`', $cliente);
		$this->db->where('financeiro.`financeiro_tipo`', $tipo);
       
		return $this->db->get('financeiro')->result();
    }

    public function baixaDividaClie($id)
	{
		$this->db->where('idFinanceiro', $id);

		$this->db->set('financeiro_baixado', 1);
		$this->db->set('data_pagamento', date('Y-m-d'));
		$this->db->set('financeiro_forma_pgto', 'Dinheiro');

		return $this->db->update('financeiro');
	}

	public function verificarBaixar($cliente, $valor_pago)
	{
		$this->db->from('financeiro');
		$this->db->where('financeiro_forn_clie_usua_id', $cliente);
		$this->db->where('financeiro_valor', $valor_pago);
		$this->db->where('data_pagamento', date('Y-m-d'));
		$this->db->where($this->visivel, 1);

		return $this->db->count_all_results() >= 1;
	}
}