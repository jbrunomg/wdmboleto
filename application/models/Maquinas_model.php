<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Maquinas_model extends MY_Model
{
    public $tabela  = 'maquinas';
    public $visivel = 'maquina_visivel';
    public $chave   = 'maquina_id';


    public function listarMaquinas()
    {
        $this->db->select(" CONCAT(categoria_cartao.categoria_cart_nome_credenciadora,' - ', categoria_cartao.categoria_cart_cnpj_credenciadora) AS administradora_cartao, maquinas.*, `usuarios`.`usuario_nome` ");
        $this->db->join('categoria_cartao','maquinas.maquina_categoria_cartao_id = categoria_cartao.categoria_cart_id');

        $this->db->join('maquinas_clientes','maquinas_clientes.`maquinas_clientes_maquina_id` =  `maquinas`.`maquina_id` 
  AND maquinas_clientes_possui = 1 AND `maquinas_clientes_visivel` = 1','LEFT');
        $this->db->join('usuarios','`usuarios`.`usuario_id` = `maquinas_clientes`.`maquinas_clientes_cliente_id`','LEFT');       
        $this->db->where($this->visivel,'1');

        return $this->db->get($this->tabela)->result();
    }


    public function pegarAdministradorasCartao()
    {
        $this->db->from('categoria_cartao')
            ->select("categoria_cart_id, CONCAT(`categoria_cart_nome_credenciadora`, ' - ', `categoria_cart_cnpj_credenciadora`) AS administradora_cartao")
            ->where('categoria_cart_visivel',1)
            ->group_by('`categoria_cart_nome_credenciadora`, `categoria_cart_cnpj_credenciadora`');
        $query = $this->db->get();
        return $query ? $query->result() : false;
    }


    public function buscarAdministradoraCartaoPorId($id)
    {
        $this->db->from('categoria_cartao')
            ->select("categoria_cart_id ,CONCAT(`categoria_cart_nome_credenciadora`, ' - ', `categoria_cart_cnpj_credenciadora`) AS administradora_cartao")
            ->where('categoria_cart_visivel',1)
            ->where('categoria_cart_id',$id)
            ->group_by('categoria_cart_cnpj_credenciadora');
        $query = $this->db->get();
        return $query ? $query->result() : false;
    }


    public function buscarMaquinaPorNumero($numero)
    {
        $this->db->from($this->tabela)
            ->select('maquina_id')
            ->where('maquina_numero',$numero);

        $query = $this->db->get();
        return $query ? $query->result() : false;
    }
    public function buscarTodosOsClientes()
    {
        $this->db->from('usuarios')
            ->select("usuario_id AS cliente_id, CONCAT(`usuario_nome`,  ' - ', `usuario_cpf`) as cliente_nome")
            ->where('usuario_visivel',1);

        $query = $this->db->get();
        return $query ? $query->result() : false;
    }
    public function buscarDadosDaMaquinasClientesPorId($id)
    {
        $this->db->from('maquinas_clientes')
            ->select('*')
            ->where('maquinas_clientes_id',$id);

        $query = $this->db->get();
        return $query ? $query->result() : false;
    }
    public function buscarClientesDaMaquina($id)
    {
        $this->db->from('maquinas_clientes')
            ->select('maquinas_clientes_id, maquinas_clientes_data_cadastro, maquinas_clientes_possui, usuarios.usuario_id, usuarios.usuario_nome, usuarios.usuario_telefone, usuarios.usuario_cpf, usuarios.usuario_email')
            ->join('maquinas', 'maquinas_clientes_maquina_id = maquina_id')
            ->join('usuarios', 'maquinas_clientes.maquinas_clientes_cliente_id = usuario_id')
            ->where('maquinas_clientes_maquina_id', $id)
            ->where('maquinas_clientes_visivel', 1)
            ->order_by('maquinas_clientes_possui', 'desc');

        $query = $this->db->get();
        return $query ? $query->result() : false;
    }
    public function buscarClienteDaMaquinaPorId($cliente_id, $maquina_id)
    {
        $this->db->from('maquinas_clientes')
            ->select('maquinas_clientes.*, usuarios.usuario_id, usuarios.usuario_nome, usuarios.usuario_telefone, usuarios.usuario_cpf, usuarios.usuario_email')
            ->where('maquinas_clientes_maquina_id', $maquina_id)
            ->where('maquinas_clientes_cliente_id', $cliente_id)
            ->join('usuarios','usuario_id = maquinas_clientes_cliente_id');

        $query = $this->db->get();
        return $query ? $query->result() : false;
    }

    public function buscarDadosDaMaquinaPorId($id) {
        // $this->db->from('maquinas')
        //     ->select('*')
        //     ->where('maquina_id',$id);

        $this->db->from('maquinas')
            ->select('*')
            ->join('categoria_cartao', 'maquinas.maquina_categoria_cartao_id = categoria_cartao.categoria_cart_id')
            ->where('maquina_id',$id)
            ->where('maquina_visivel', 1);     

        $query = $this->db->get();
        return $query ? $query->result() : false;
    }



    public function adicionarMaquinaAoCliente($dados)
    {
        $cliente_id = $dados['maquinas_clientes_cliente_id'];
        $maquina_id = $dados['maquinas_clientes_maquina_id'];
        $this->db->where('maquinas_clientes_cliente_id',$cliente_id)
            ->where('maquinas_clientes_maquina_id',$maquina_id);
        $queryVerify = $this->db->get('maquinas_clientes');

        if($queryVerify->num_rows() == 1) {

            $this->db->update('maquinas_clientes',['maquinas_clientes_visivel' => 1], "maquinas_clientes_cliente_id = $cliente_id");
            return true;
        } else {

            $this->db->trans_start();

            try {
                $id = $dados['maquinas_clientes_maquina_id'];
                $dados['maquinas_clientes_possui'] = 1;
                $this->db->update('maquinas_clientes',['maquinas_clientes_possui' => 0],"maquinas_clientes_maquina_id = $id");
                $this->db->insert('maquinas_clientes',$dados);
            } catch (Exception $e) {
                $this->db->trans_rollback();
                return false;
            }

            if($this->db->trans_status() === false) {
                $this->db->trans_rollback();
            } else {
                if ($this->db->affected_rows() == '1') {
                    return $this->db->insert_id();
                }
                $this->db->trans_commit();
            }
            return false;

        }

    }
    public function editarMaquinasClientes($id, $dados)
    {
        $this->db->update('maquinas_clientes',$dados,"maquinas_clientes_id = $id");
        if($this->db->affected_rows() == 1) {
            return true;
        }
        return false;
    }
    public function excluirCliente($id)
    {
        $alterar = [
            'maquinas_clientes_visivel' => 0
        ];
        $this->db->from('maquinas_clientes')
            ->where('maquinas_clientes_id',$id);
        $this->db->update('maquinas_clientes',$alterar);

        if($this->db->affected_rows() == '1') {
            return true;
        }

        return false;
    }
}

