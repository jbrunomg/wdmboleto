<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//class Cliente_model extends CI_Model {
class Cliente_model extends MY_Model {


	public $tabela  = 'cliente';
	public $chave   = 'cliente_id';
	public $visivel = 'cliente_visivel';


	public function pegarPorCnpj($cnpj)
	{
		$this->db->select('cliente_nome');		
		$this->db->where('cliente_cpfCnpj',$cnpj);		
		return $this->db->get($this->tabela)->result();
	}

	public function selecionarClientes($termo)
	{
		$this->db->select('cliente_id AS id, cliente_nome AS text'); 		
		$this->db->like('cliente_nome',$termo);			
		$this->db->where($this->visivel, 1);
		$this->db->limit(10);
		return $this->db->get($this->tabela)->result_array();
	}


}

/* End of file Cliente_model.php */
/* Location: ./application/models/Cliente_model.php */