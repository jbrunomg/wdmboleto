<?php
class Simulador_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }


    public $tabela = "simulador_cartao";
	public $chave  = 'idSimulador';    


    /* ROTINA PARA SIMULAR CARTAO - PARCELADO */

    function getSimular($valorSimular){
        $this->db->select('`tipo_parcela`, `numero_parcela`, percentual_parcela, (( '.$valorSimular.' * `percentual_parcela`)/100) AS valor_parcela_old, (('.$valorSimular.'/(1-`percentual_parcela`/100))-'.$valorSimular.') AS valor_parcela ');
        // $this->db->where('status',1);
        return $this->db->get('simulador_cartao')->result();
    }




    public function pegarTaxas()
	{
		$this->db->select('*');
		return $this->db->get('simulador_cartao')->result();		
	}


    public function editarTaxasExe($dados,$id)
	{			

		$this->db->where($this->chave,$id);
		$this->db->update($this->tabela,$dados);
		
		if($this->db->affected_rows() == '1')
		{
			return true;
		}

		return false;

	}


	public function todasPermissoes()
	{
		$this->db->select('*');		
		return $this->db->get('permissoes')->result();		
	}


    
}