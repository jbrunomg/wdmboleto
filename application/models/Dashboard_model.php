<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model {

	// private $tabela  = 'tbcurcursos';
	// private $id      = 'pcurcucodig';
	// private $visivel = 'curso_visivel';

public function apiDenuncia()
{

	$query = $this->db->query(" SELECT COUNT(*) AS total FROM `denuncia_api` WHERE `denuncia_sicronizada` = 0; ");

    return $query->result();

}	

public function tcePendente()
{

	$query = $this->db->query(" SELECT COUNT(*) AS total FROM tbcontrestempr WHERE ((contrestempr_data_ini + INTERVAL '10' DAY) < CURRENT_DATE ) AND contrestempr_termo_C_E = 0 ");

    return $query->result();

}

public function taPendente()
{
	$query = $this->db->query(" SELECT COUNT(*)  AS total  FROM `tbcontrestempr` WHERE (`contrestempr_data_renI` <> '0000-00-00' AND (`contrestempr_data_renI` + INTERVAL '10' DAY) < CURRENT_DATE )
    AND `contrestempr_termo_A_I` = 0 ");

    return $query->result();

}



public function graficoDenuncia($id=null)
{
	$where = '';
	if ($id) {
		$where = " id_empresa = {$id} AND ";	
	}

	$sql = "
		SELECT 'Jan' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 1 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 1 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 1 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Fev' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 2 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 2 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 2 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Mar' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 3 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 3 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 3 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Abr' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 4 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 4 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 4 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Mai' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 5 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 5 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 5 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Jun' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 6 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 6 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 6 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Jul' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 7 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 7 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 7 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Ago' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 8 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 8 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 8 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Set' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 9 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 9 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 9 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Out' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 10 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 10 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 10 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Nov' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 11 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 11 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 11 ) AS fim
	FROM denuncia

	UNION

	SELECT 'Dez' AS Ano, 
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) AND  MONTH(denuncia_data_cadastro) = 12 ) AS atual,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(denuncia_data_cadastro) = 12 ) AS meio,
	( SELECT  COUNT(*) FROM denuncia WHERE {$where} YEAR(denuncia_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(denuncia_data_cadastro) = 12 ) AS fim
	FROM denuncia ";
  	

	
    $query = $this->db->query($sql);

    return $query->result();
	
	}


public function graficoEmpresa()
{

    $query = $this->db->query("
		    	SELECT 'Jan' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 1 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 1 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 1 ) AS fim
		FROM empresa


		UNION

		SELECT 'Fev' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 2 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 2 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 2 ) AS fim
		FROM empresa

		UNION

		SELECT 'Mar' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 3 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 3 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 3 ) AS fim
		FROM empresa

		UNION

		SELECT 'Abr' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 4 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 4 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 4 ) AS fim
		FROM empresa

		UNION

		SELECT 'Mai' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 5 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 5 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 5 ) AS fim
		FROM empresa

		UNION

		SELECT 'Jun' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 6 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 6 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 6 ) AS fim
		FROM empresa

		UNION

		SELECT 'Jul' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 7 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 7 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 7 ) AS fim
		FROM empresa

		UNION

		SELECT 'Ago' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 8 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 8 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 8 ) AS fim
		FROM empresa

		UNION

		SELECT 'Set' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 9 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 9 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 9 ) AS fim
		FROM empresa

		UNION

		SELECT 'Out' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 10 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 10 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 10 ) AS fim
		FROM empresa

		UNION

		SELECT 'Nov' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 11 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 11 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 11 ) AS fim
		FROM empresa

		UNION

		SELECT 'Dez' AS Ano, 
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) AND  MONTH(empresa_data_cadastro) = 12 ) AS atual,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -1  AND  MONTH(empresa_data_cadastro) = 12 ) AS meio,
		( SELECT  COUNT(*) FROM empresa WHERE YEAR(empresa_data_cadastro) = YEAR(CURDATE()) -2  AND  MONTH(empresa_data_cadastro) = 12 ) AS fim
		FROM empresa


  	");

    return $query->result();
	
}


public function graficoStatus($id=null)
{
	
	$sql = "SELECT COUNT(denuncia_status) AS `status`, denuncia_status FROM `denuncia` ";
	
	if ($id) {
		$sql = $sql." WHERE id_empresa = {$id} ";	
	}
	$sql = $sql." GROUP BY `denuncia_status`";	


   $query = $this->db->query($sql);

   return $query->result();

}	


	


}

/* End of file Curso_model.php */
/* Location: ./application/models/Curso_model.php */