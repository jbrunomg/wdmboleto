
<?php //var_dump(($this->session->userdata('usuario_permissoes_id') == '1') OR ($this->session->userdata('usuario_permissoes_id') == '2'));die(); ?>
<!-- Table with togglable columns -->
<div class="panel panel-flat"  <?php echo (($this->session->userdata('usuario_permissoes_id') == '1') OR ($this->session->userdata('usuario_permissoes_id') == '2') ) ? 'style="display: block;"' : 'style="display: none;"'; ?> >
	<div class="panel-heading">
		<h5 class="panel-title">Configurar Taxas do Cartão (Débito e Crédito)</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<form action="<?php echo base_url(); ?>simulador/editarTaxasExe" method="post">

	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

	
	<table class="table table-togglable table-hover">
		<thead>
			<tr>
				<th data-toggle="true" >#</th>
				<th data-hide="phone,tablet"  style="text-align: center;" >Tipo</th>
				<th data-hide="phone,tablet" style="text-align: right; padding: 0px 90px 0px 0px; ">Taxa %</th>												
			</tr>
		</thead>
		<tbody>	

 		    <?php foreach ($taxas as $t) {  ?>
                <tr>
                 	<td ><?php echo $t->numero_parcela; ?></td> 
                  	<td style="text-align: center;"><?php echo $t->tipo_parcela; ?></td>                                  
	                <td>                                       
	                	<input type="text" style="margin-left: 85%" class="money col-lg-1" placeholder="0,00" value="<?php echo $t->percentual_parcela; ?>" name="<?php echo $t->idSimulador; ?>" >&nbsp;%        
	                </td>
                </tr>
            <?php } ?>
		
			<tr>
				<td colspan="3">
					<div class="text-right">
						<button type="submit" class="btn bg-teal">Configurar<i class="icon-arrow-right14 position-right"></i></button>
					</div>
				</td>
			</tr>
		
		</tbody>			
	</table>		
	</form>
</div>
<!-- /table with togglable columns -->

<script src="<?php echo base_url();?>js/maskmoney.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $(".money").maskMoney();
   
    });
   
</script>