

<!-- Table with togglable columns -->
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Simular Compra</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>                
                <li><a data-action="close"></a></li>
            </ul>
        </div>        
    </div>

    <br>  

    <form action="<?php echo base_url(); ?>simulador/simular" method="post">

        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

        <div class="panel-body" >

            <div class="row">
                <div class="form-group col-md-3">
                    <label for="vlprod">Digitar Valor da Compra:*</label>
                    <input type="text"  name="vlprod" autocomplete="off" id="vlprod"  class="form-control form-control-sm money" placeholder="0,00" maxlength="8">                   
                </div>
                <div class="form-group col-md-6">
                    <label for="vlentr">Digitar Valor da Entrada em Espécie:</label>
                    <input type="text" name="vlentr" autocomplete="off" id="vlentr" class="form-control form-control-sm money"  placeholder="0,00"  maxlength="8">                                
                </div>
                <div class="from-group col-md-3">
                    <label for="data_nascimento">Limite Máximo de Parcelas:</label>
                    <input type="number" id="qtparc" name="qtparc"  value="18" placeholder="" class="form-control form-control-sm"  maxlength="2" readonly="" />
                </div>
            </div>
        </div>

        <div class="form-actions">
            <div class="span12 text-center">
                <input type="reset" class="btn" value="Limpar" />                            
                <button type="submit" class="btn btn-success"><i class="icon-credit-card icon-white"></i> Calcular</button>                          
            </div>
        </div>   
      
    </form>

        <br>                  
        <div align=center>
            <strong>SIMULADOR DE PARCELAS</strong> 
        </div>
</div>
<!-- /table with togglable columns -->

    <!-- Info alert -->
    <div class="alert alert-info bg-white alert-styled-left alert-arrow-left alert-dismissible">
        <button type="button" class="close" data-dismiss="alert"><span>&times;</span></button>
        <h6 class="alert-heading font-weight-semibold mb-1">Static top navbar</h6>
        As taxas e condições informadas podem sofrer alteração a qualquer tempo sem prévio aviso..
    </div>
    <!-- /info alert -->  


 <?php // var_dump($resultado);die(); ?>    

<?php if(isset($resultado) ){ ?>
<!-- Table with togglable columns -->
<div class="panel panel-flat">

    <div class="panel-body">

        <!-- Basic tabs -->
        <div class="row">
            <div class="col-md-12">
                <!-- <legend class="text-semibold"><i class="icon-stats-growth position-left"></i> Taxas</legend> -->
                <div class="card">
                    <div class="card-body">
                        <div class="tabbable">
                            <ul class="nav nav-tabs nav-tabs-highlight nav-justified">
                                <li class="active"><a href="#highlighted-justified-tab1" data-toggle="tab">Simulação</a></li>                                              
                            </ul>
                        </div>

                        <div class="tab-content">
                            <div class="tab-pane active" id="highlighted-justified-tab1" >

                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="vlprod">Valor da Compra:*</label>
                                        <input type="text"  id="vlprod" name="vlprod" autocomplete="off"  class="form-control form-control-sm money" value="<?php echo number_format($simulado['valorCompra'],2,',','.'); ?>" disabled="">                   
                                    </div>
                                    <div class="form-group col-md-4">
                                        <label for="vlentr">Valor da Entrada em Espécie:</label>
                                        <input type="text" id="vlentr" name="vlentr" autocomplete="off"  class="form-control form-control-sm money"  value="<?php echo number_format($simulado['valorEntrada'],2,',','.'); ?>" disabled="">                                
                                    </div>
                                    <div class="from-group col-md-4">
                                        <label for="data_nascimento">Pagamento Restante em Espécie:*</label>
                                        <input type="text" id="qtparc" name="qtparc" autocomplete="off"  class="form-control form-control-sm"  value="<?php echo number_format($simulado['valorSimular'],2,',','.') ?>" disabled=""  />
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="form-group col-md-4">
                                        <label for="vlprod">Pagamento Restante em Débito:*</label>
                                        <input type="text"  name="vlprod" autocomplete="off" id="vlprod"  class="form-control form-control-sm money" value="<?php echo (number_format($resultado[0]->valor_parcela + $simulado['valorSimular'],2,',','.') ) ?>" disabled="" >                   
                                    </div>                           
                                    <div class="from-group col-md-4">
                                        <label for="data_nascimento">Quantidade Máxima de Parcelas:</label>
                                        <input type="number" id="qtparc" name="qtparc"  value="18" placeholder="" class="form-control form-control-sm"  maxlength="2" disabled="" />
                                    </div>
                                </div>  


                                <h2 class="text-center fontbold">Opções de Parcelamento</h2>


                                <!--Tab 1 Simulação-->
                            
                                <?php if (!$resultado) { ?>           

                                    <table class="table table-bordered ">
                                        <thead>
                                            <tr style="backgroud-color: #2D335B">
                                                <th>Parcelas</th>
                                                <th>Valor da Parcela</th>
                                                <th>Total da Compra</th>                                                   
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td colspan="6">Nenhuma simulação Cadastrada</td>
                                            </tr>
                                        </tbody>
                                    </table>

                                <?php } else { ?>      

                                    <table class="table table-bordered ">
                                        <thead>
                                            <tr style="backgroud-color: #2D335B">
                                                <th>Parcelas</th>
                                                <th>Valor da Parcela</th>
                                                <th>Total da Compra</th>  
                                            </tr>
                                        </thead>
                                        <tbody>

                                            <?php foreach ($resultado as $v) {  
                                               // var_dump( ($simulado['valorSimular'] .' - '. $v->valor_parcela) );die();
                                                if ($v->tipo_parcela == 'debito') {
                                                    continue;                                                                                                      
                                                } 

                                                $vlParcela = ($simulado['valorSimular'] + $v->valor_parcela) / $v->numero_parcela;
                                                
                                                if ($v->tipo_parcela == 'pix') {
                                                $vlParcela =  $simulado['valorSimular'] + $v->valor_parcela;                                                                                                       
                                                } 

                                                ?>  
                                                    <tr>                                          
                                                        <td class="text-center"><?php echo $v->tipo_parcela; ?></td>
                                                        <td class="text-center"><?php echo number_format($vlParcela,2,',','.'); ?></td>
                                                        <td class="text-center"><?php echo number_format(($simulado['valorSimular'] + $v->valor_parcela),2,',','.'); ?></td>
                                                    </tr>
                                            <?php } ?>            

                                            <tr>
                                            </tr>
                                        </tbody>
                       
                                    </table>
                                <?php  } ?> 
                            </div>

                            <br>
                            <br>
                            <div class="text-center">
                                <a onclick="#" href="<?php echo base_url(); ?>simulador" class="btn btn-warning ">Retornar</a>
                            </div>
                            <br>                        

                        </div>
                    </div>
                </div>
            </div>       
        </div>
        <!-- /basic tabs --> 

    </div>          

</div>
<!-- /table with togglable columns -->
<?php  } ?> 



<script src="<?php echo base_url()?>js/jquery.validate.js"></script>
<script src="<?php echo base_url();?>public/assets/js/maskmoney2.js"></script>
<script src="<?php echo base_url();?>assets/js/jquery.mask.js"></script>
<script type="text/javascript">

    $(document).ready(function(){
        $(".money").maskMoney();
   
    });
      // $(document).ready(function(){

      //  $(".money").maskMoney();

      //  $('#formSimulador').validate({
      //   rules : {
      //       vlprod:{ required: true},               

      //   },
      //   messages: {
      //       vlprod :{ required: 'Campo Requerido.'}
      //   },

      //   errorClass: "help-inline",
      //   errorElement: "span",
      //   highlight:function(element, errorClass, validClass) {
      //       $(element).parents('.control-group').addClass('error');
      //   },
      //   unhighlight: function(element, errorClass, validClass) {
      //       $(element).parents('.control-group').removeClass('error');
      //       $(element).parents('.control-group').addClass('success');
      //   }
      //  });

           
      // });
</script>




