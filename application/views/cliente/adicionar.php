<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Cliente</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados do Cliente:</legend>

					<!-- <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->

				<div class="form-group">
          <label class="control-label col-lg-2">CPF:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="CPF" data-mask="999.999.999-99" data-mask-selectonfocus="true" name="cliente_cpfCnpj" id="cpfCnpj" value="<?php echo set_value('cliente_cpfCnpj'); ?>">
          <?php echo form_error('cliente_cpfCnpj'); ?>
          </div>                    
        </div> 

        <div class="form-group">
          <label class="control-label col-lg-2">Nome da Cliente:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Nome Cliente" name="cliente_nome" id="nome" value="<?php echo set_value('cliente_nome'); ?>">
          <?php  echo form_error('cliente_nome'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">RG:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="RG"  data-mask-selectonfocus="true" name="cliente_rgie" id="rgie" value="<?php echo set_value('cliente_rgie'); ?>">
          <?php echo form_error('cliente_rgie'); ?>
          </div>                    
        </div> 

        <div class="form-group"> 
          <label class="control-label col-lg-2">Data Nascimento:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control classData" placeholder="Data Nascimento" name="cliente_dtNascimento" id="dtNascimento" value="<?php echo set_value('cliente_dtNascimento'); ?>">
          <?php  echo form_error('cliente_dtNascimento'); ?>
          </div>                    
        </div>
 
        <div class="form-group">
          <label class="control-label col-lg-2">Sexo:</label>
          <div class="col-lg-5">
              <select class="form-control" name="cliente_sexo" id="cliente_sexo">
                <option value="">Selecione</option>
                  <option value="Masculino">Masculino</option>
                  <option value="Feminino">Feminino</option>
              </select>
            </div>
        </div>

        <legend class="text-bold">Dados Residencial:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">CEP:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Cep"  name="cliente_cep" id="cep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo set_value('cliente_cep'); ?>">
          <?php echo form_error('cliente_cep'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Endereço:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Rua/Av/Trav" name="cliente_rua" id="endereco" value="<?php echo set_value('cliente_rua'); ?>">
          <?php echo form_error('cliente_rua'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Numero:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="numero"   name="cliente_numero" id="numero" value="<?php echo set_value('cliente_numero'); ?>">
          <?php echo form_error('cliente_numero'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Complemento:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Complemento"  name="cliente_complemento" id="complemento" value="<?php echo set_value('cliente_complemento'); ?>">
          <?php echo form_error('cliente_complemento'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Bairro:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Bairro" name="cliente_bairro" id="bairro" value="<?php echo set_value('cliente_bairro');  ?>">
          <?php echo form_error('cliente_bairro'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Estado:</label>
          <div class="col-lg-5">
                <select  class="form-control" name="cliente_estado" id="estado" >
                  <option value="">Selecione</option>
                    <?php foreach ($estados as $estado) { ?>
                        <option value="<?php echo $estado->uf; ?>"><?php echo $estado->nome; ?></option>
                    <?php } ?>
                </select>
                <?php echo form_error('estado'); ?>
            </div>                                  
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Cidade:</label>
          <div class="col-lg-5">
              <select  class="form-control" name="cliente_cidade" id="cidade"> 
                  <?php foreach ($cidades as $valor) { ?>
                    <option value="<?php echo $valor->nome; ?>"><?php echo $valor->nome; ?></option>
                  <?php } ?>

              <!--     <?php foreach ($cidades as $valor) { ?>
                    <?php $selected = ($valor->id == $dados[0]->cidade)?'SELECTED': ''; ?>
                        <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
                  <?php } ?> -->
              </select>
          <?php echo form_error('cidade'); ?> 
          </div>                                                                  
        </div>



        <legend class="text-bold">Dados Contato:</legend>

        <div class="form-group">
          <label class="control-label col-lg-2">Telefone Fixo:</label>
          <div class="col-lg-5">
            <input  type="tel" class="form-control" placeholder="(99)9999-9999" data-mask="(99)9999-9999" data-mask-selectonfocus="true" name="cliente_telefone" id="telefone" value="<?php echo set_value('cliente_telefone'); ?>">
          <?php echo form_error('cliente_telefone'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Telefone Celular:</label>
          <div class="col-lg-5">
            <input  type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="cliente_telefone2" id="telefone2" value="<?php echo set_value('cliente_telefone2'); ?>">
          <?php echo form_error('cliente_telefone2'); ?>
          </div>                    
        </div>
        <div class="form-group">
          <label class="control-label col-lg-2">Telefone Celular II:</label>
          <div class="col-lg-5">
            <input  type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="cliente_telefone3" id="telefone3" value="<?php echo set_value('cliente_telefone3'); ?>">
          <?php echo form_error('cliente_telefone3'); ?>
          </div>                    
        </div>

        <div class="form-group">
          <label class="control-label col-lg-2">Email:</label>
          <div class="col-lg-5">
            <input  type="text" placeholder="seu@email.com" class="form-control" name="cliente_email" id="email" value="<?php echo set_value('cliente_email'); ?>">
          <?php echo form_error('cliente_email'); ?>
          </div>                     
        </div> 
   

               		
				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>