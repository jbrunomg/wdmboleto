<!-- Column selectors -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aClientes')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>				
				<th>Nome</th>	
				<th>CPF</th>					
				<th>Contato</th>				
				<th>Email</th>														
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { ?>
				
				<tr>     								
					<td><?php echo $valor->cliente_nome; ?></td>
					<td><?php echo $valor->cliente_cpfCnpj; ?></td> 					
					<td><?php echo $valor->cliente_telefone; ?></td>
					<td><?php echo $valor->cliente_email; ?></td>
																									
					<td>																				
						<ul class="icons-list">						
								<?php if(checarPermissao('eClientes')){ ?>
								<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->cliente_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('dClientes')){ ?>
								<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->cliente_id; ?>"><i class="icon-trash"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('vClientes')){ ?>
								<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->cliente_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
								<?php } ?>
						</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors -->

<script type="text/javascript">
$(document).ready(function(){


   $(document).on('click', 'a', function(event) {
        
        var empresa = $(this).attr('empresa');
        $('#idClientes').val(empresa);

        // var span = '<span> <?php echo base_url(); ?>cotacaofornecedor/carregarcotacao/'+fornecedor+'</span>';

        // var span = '<span class="token attr-name"> &lt;a href="http://ouvidoria.carvalhal.com.br/denunciaiframe/carregarIframe/'+empresa+'" target="_blank" title="Faça seu relatos de forma anônima!" &gt; &lt;img src="http://ouvidoria.carvalhal.com.br/public/assets/images/button_ouvidoria.png" alt="Faça seu relatos de forma anônima!" /&gt; &lt;/a&gt; </span>';
         var span = '<span class="token attr-name"> &lt;a href="<?php echo base_url(); ?>apidenuncia/carregarapi/'+empresa+'" target="_blank" title="Faça seu relatos de forma anônima!" &gt; &lt;img src="<?php echo base_url(); ?>public/assets/images/button_ouvidoria.png" alt="Faça seu relatos de forma anônima!" /&gt; &lt;/a&gt; </span>';

        
		document.getElementById("iframe").innerHTML = span;

		// var span2 = '<span class="token attr-name"> &lt;a href="http://ouvidoria.carvalhal.com.br/denunciaiframe/carregarIframe/'+empresa+'" target="_blank" title="Faça seu relatos de forma anônima!" &gt; &lt;img src="http://ouvidoria.carvalhal.com.br/public/assets/images/button_denuncia.png" alt="Faça seu relatos de forma anônima!" /&gt; &lt;/a&gt; </span>';
		var span2 = '<span class="token attr-name"> &lt;a href="<?php echo base_url(); ?>apidenuncia/carregarapi/'+empresa+'" target="_blank" title="Faça seu relatos de forma anônima!" &gt; &lt;img src="<?php echo base_url(); ?>public/assets/images/button_denuncia.png" alt="Faça seu relatos de forma anônima!" /&gt; &lt;/a&gt; </span>';

		document.getElementById("iframe2").innerHTML = span2;

    });

});

</script>