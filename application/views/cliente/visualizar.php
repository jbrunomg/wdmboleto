<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Visualização do Cliente</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">



		<div class="tabbable">
			<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
				<li class="active"><a href="#highlighted-justified-tab1" data-toggle="tab">Dados Cliente:</a></li>
				<li><a href="#highlighted-justified-tab2" data-toggle="tab">Dados Maquinas:</a></li>
			
			</ul>

			<div class="tab-content">
				<div class="tab-pane active" id="highlighted-justified-tab1" style="padding: 1%; margin-left: 0">
					
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>" method="post" enctype="multipart/form-data">
			


			<fieldset class="content-group">
				<legend class="text-bold">Dados Cliente:</legend>Dados Cliente

				<!-- <input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->

				<input  type="hidden" name="cliente_id" value="<?php echo $dados[0]->cliente_id; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">CPF:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Cnpj" data-mask="999.999.999-99" data-mask-selectonfocus="true" name="cliente_cpfCnpj" id="cpfCnpj" value="<?php echo $dados[0]->cliente_cpfCnpj; ?>">
					<?php echo form_error('cliente_cpfCnpj'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Cliente:</label>
					<div class="col-lg-5">
						<input  disabled type="text" class="form-control" placeholder="Nome Cliente" name="cliente_nome" id="nome" value="<?php echo $dados[0]->cliente_nome; ?>">
					<?php echo form_error('cliente_nome'); ?>
					</div>										
				</div>

			    <div class="form-group">
		          <label class="control-label col-lg-2">Data Nascimento:</label>
		          <div class="col-lg-5">
		            <input  disabled type="text" class="form-control classData" placeholder="Data Nascimento" name="cliente_dtNascimento" id="dtNascimento" value="<?php echo date('d/m/Y', strtotime($dados[0]->cliente_dtNascimento));  ?>">
		          <?php  echo form_error('cliente_dtNascimento'); ?>
		          </div>                    
		        </div>
		 
		        <div class="form-group">
		          <label class="control-label col-lg-2">Sexo:</label>
		          <div class="col-lg-5">
		              <select disabled class="form-control" name="cliente_sexo" id="cliente_sexo">
		                <option value="">Selecione</option>
		                <option value="1" <?php echo ($dados[0]->cliente_sexo == 1)?'selected':''; ?>>Masculino</option>
				        <option value="2" <?php echo ($dados[0]->cliente_sexo == 2)?'selected':''; ?>>Feminino</option>
		             
		              </select>
		            </div>
		        </div>

		
				<legend class="text-bold">Dados Residencial:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">CEP:</label>
					<div class="col-lg-5">
						<input  disabled type="text" class="form-control" name="cliente_cep" id="cep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo $dados[0]->cliente_cep; ?>">
					<?php echo form_error('cliente_cep'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Endereço:</label>
					<div class="col-lg-5">
						<input  disabled type="text" class="form-control" placeholder="Rua/Av/Trav" name="cliente_rua" id="rua" value="<?php echo $dados[0]->cliente_rua; ?>">
					<?php echo form_error('cliente_rua'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Numero:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" name="cliente_numero" id="numero" value="<?php echo $dados[0]->cliente_numero; ?>">
					<?php echo form_error('cliente_numero'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Complemento:</label>
					<div class="col-lg-5">
						<input  disabled type="text" class="form-control" name="cliente_complemento" id="complemento" value="<?php echo $dados[0]->cliente_complemento; ?>">
					<?php echo form_error('cliente_complemento'); ?>
					</div>										
				</div>


				<div class="form-group">
					<label class="control-label col-lg-2">Bairro:</label>
					<div class="col-lg-5">
						<input  disabled type="text" class="form-control" name="cliente_bairro" id="bairro" value="<?php echo $dados[0]->cliente_bairro;  ?>">
					<?php echo form_error('cliente_bairro'); ?>
					</div>										
				</div>

				<div class="form-group">
                	<label class="control-label col-lg-2">Estado:</label>
                	<div class="col-lg-5">
                        <select disabled class="form-control" name="cliente_estado" id="estado" >
                        	<option value="">Selecione</option>
                            <?php foreach ($estados as $valor) { ?>
                            	<?php $selected = ($valor->uf == $dados[0]->cliente_estado)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->uf; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                        <?php echo form_error('cliente_estado'); ?>
                    </div>			                            
                </div>			                        
				<div class="form-group">
					<label class="control-label col-lg-2">Cidade:</label>
					<div class="col-lg-5">
                        <select disabled class="form-control" name="cliente_cidade" id="cidade">
                            
                            <?php foreach ($cidades as $valor) { ?>
                            	<?php $selected = ($valor->nome == $dados[0]->cliente_cidade)?'SELECTED': ''; ?>
		                              <option value="<?php echo $valor->nome; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
		                        <?php } ?>
                        </select>
                    <?php echo form_error('cliente_cidade'); ?>	
                    </div>				                            	                            
				</div>	

				<legend class="text-bold">Dados Contato:</legend>

				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Fixo:</label>
					<div class="col-lg-5">
						<input disabled type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="cliente_telefone" id="telefone" value="<?php echo $dados[0]->cliente_telefone; ?>">
					<?php echo form_error('cliente_telefone'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular I:</label>
					<div class="col-lg-5">
						<input disabled type="tel"  class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="cliente_telefone2" id="telefone2" value="<?php echo $dados[0]->cliente_telefone2; ?>">
					<?php echo form_error('cliente_telefone2'); ?>
					</div>										
				</div>
				<div class="form-group">
					<label class="control-label col-lg-2">Telefone Celular II:</label>
					<div class="col-lg-5">
						<input disabled type="tel" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="cliente_telefone3" id="telefon3" value="<?php echo $dados[0]->cliente_telefone3; ?>">
					<?php echo form_error('cliente_telefone3'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Email:</label>
					<div class="col-lg-5">
						<input disabled type="text" placeholder="seu@email.com" class="form-control" name="cliente_email" id="email" value="<?php echo $dados[0]->cliente_email; ?>">
					<?php echo form_error('cliente_email'); ?>
					</div>										 
				</div>	

                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Voltar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>

				</div>

				<div class="tab-pane" id="highlighted-justified-tab2">
					Food truck fixie locavore, accusamus mcsweeney's marfa nulla single-origin coffee squid laeggin.
				</div>

			</div>
		</div>


					

		</div>
	</div>
