    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.load("current", {packages:["corechart"]});
      google.charts.setOnLoadCallback(drawChart);
      google.charts.setOnLoadCallback(drawChart2);
      google.charts.setOnLoadCallback(drawChart3);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Mês', '<?php echo date('Y') ?>', '<?php echo date('Y') -1 ?>', '<?php echo date('Y') -2 ?>'],

          <?php foreach ($dadosempresa as $valor ) { ?>

        
          [<?php echo "'".$valor->Ano."'"; ?> , <?php echo $valor->atual; ?>, <?php echo $valor->meio; ?>, <?php echo $valor->fim; ?>],


          <?php } ?>

        ]);

        var options = {
          chart: {
          //  title: 'Grafico',
          //  subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          },
          bars: 'vertical', // Required for Material Bar Charts.
      vAxis: {format: 'decimal'}
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_empresa'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }

      function drawChart2() {
        var data = google.visualization.arrayToDataTable([
          ['Mês', '<?php echo date('Y') ?>', '<?php echo date('Y') -1 ?>', '<?php echo date('Y') -2 ?>'],

          <?php foreach ($dadosdenuncia as $valor ) { ?>

        
          [<?php echo "'".$valor->Ano."'"; ?> , <?php echo $valor->atual; ?>, <?php echo $valor->meio; ?>, <?php echo $valor->fim; ?>],


          <?php } ?>

        ]);

        var options = {
          chart: {
          //  title: 'Grafico',
          //  subtitle: 'Sales, Expenses, and Profit: 2014-2017',
          },
          bars: 'vertical', // Required for Material Bar Charts.
      vAxis: {format: 'decimal'}
        };

        var chart = new google.charts.Bar(document.getElementById('barchart_vaga'));

        chart.draw(data, google.charts.Bar.convertOptions(options));
      }

      function drawChart3() {
        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
        
          <?php foreach ($dadosstatus as $valor ) { ?>

        
          [<?php echo "'".$valor->denuncia_status."'"; ?> , <?php echo $valor->status; ?>],


          <?php } ?>

        ]);

        var options = {
          //title: 'My Daily Activities',
          is3D: true,
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart_3d'));
        chart.draw(data, options);
      }
    </script>


    <!-- Noticias INICIO -->
                
    <div class="panel panel-flat col-md-12 content" >

      <div class="panel-heading">
        <h6 class="panel-title">Notícias</h6>
        <div class="heading-elements">
          <ul class="icons-list">
            <li><a data-action="collapse"></a></li>
            <li><a data-action="reload"></a></li>
            <li><a data-action="close"></a></li>
          </ul>
        </div>
      </div>


      <div class="panel-body">
        <div class="row">

          <?php foreach ($noticias as $not) { ?>
          <div class="col-lg-3">
            <ul class="media-list content-group">
             
              <li class="media stack-media-on-mobile">
                <div class="media-left">
                  <div class="thumb">
                    <a href="#">
                      <img src="assets/images/demo/flat/1.png" class="img-responsive img-rounded media-preview" alt="">
                      <span class="zoom-image"><i class="icon-play3"></i></span>
                    </a>
                  </div>
                </div>

                <div class="media-body">
                  <h6 class="media-heading"><a href="#"><?php echo $not['titulo']; ?></a></h6>
                    <ul class="list-inline list-inline-separate text-muted mb-5">
                      <li><i class="icon-calendar position-left"></i><?php echo $not['mes'];   ?></li>
                      <li><?php echo $not['dia'];   ?></li>
                    </ul>
                    <p><?php echo substr($not['descricao'],0,400);?><a class="read-more" href="<?php echo $not['link'];   ?>"  target="_blank"> Continuar lendo...</a></p>                            
                </div>
              </li> 
            </ul>
          </div>
          <?php } ?>


        </div>
      </div>
    </div>              
  <!-- Noticias FIM --> 
  

  <!-- Grafico  2 -->
  <div class="panel panel-flat col-md-6 content" >

    <div class="panel-heading">
      <h5 class="panel-title">Gráfico cubo 01</h5>
      <div class="heading-elements">
        <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!-- <li><a data-action="reload"></a></li> -->
                <!-- <li><a data-action="close"></a></li> -->
              </ul>
          </div>
    </div>

    <div class="panel-body">



    <div id="barchart_empresa" style="width: 100%; height: 500px;"></div>


      
    </div>
  </div>
  <!-- Grafico  2 FIM -->

  <!-- Grafico  3 -->
  <div class="panel panel-flat col-md-6 content" >

    <div class="panel-heading">
      <h5 class="panel-title">Gráfico cubo 02</h5>
      <div class="heading-elements">
        <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li> 
                <li><a data-action="close"></a></li> 
              </ul>
          </div>
    </div>

    <div class="panel-body">

 

    <div id="barchart_vaga" style="width: 100%; height: 500px;"></div>


      
    </div>
  </div> 
  <!-- Grafico  2 FIM -->      
	
  <!-- Grafico  1 -->
	<div class="panel panel-flat col-md-12 content" >

		<div class="panel-heading">
			<h5 class="panel-title">Gráfico pizza 01</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<li><a data-action="reload"></a></li>
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		</div>


		<div class="panel-body">

    <div id="piechart_3d" style="width: 900px; height: 500px;"></div>
			
		</div>
	</div>
<!-- Grafico  1 FIM-->


  


	




  <script type="text/javascript">

  /* ------------------------------------------------------------------------------
 *
 *  # Echarts - chart combinations
 *
 *  Chart combination configurations
 *
 *  Version: 1.0
 *  Latest update: August 1, 2015
 *
 * ---------------------------------------------------------------------------- */

$(function () {
    //alert();
    // Set paths
    // ------------------------------

    require.config({
        paths: {
            echarts: 'public/assets/js/plugins/visualization/echarts'
        }
    });



    // Configuration
    // ------------------------------

    require(

        // Add necessary charts
        [
          
          'echarts/chart/pie'        
        ],


        // Charts setup
        function (ec, limitless) {


            // Initialize charts
            // ------------------------------


            var connect_pie = ec.init(document.getElementById('connect_pie'), limitless);
            var connect_column = ec.init(document.getElementById('connect_column'), limitless);

            //var candlestick_scatter = ec.init(document.getElementById('candlestick_scatter'), limitless);



            // Charts options
            // ------------------------------

       

            //
            // Column and pie connection
            //

            // Pie options
            connect_pie_options = {

                // Add title
                title: {
                    text: 'Mês atual',
                    subtext: 'Manipule os dados para ter outra pespectiva',
                    x: 'center'
                },

                // Add tooltip
                tooltip: {
                    trigger: 'item',
                    formatter: "{a} <br/>{b}: {c} ({d}%)"
                },

                // Add legend
                legend: {
                    orient: 'vertical',
                    x: 'left',
                    data: ['Semana 1','Semana 2','Semana 3','Semana 4','Semana 5']
                },

                // Enable drag recalculate
                calculable: true,

                // Add series
                series: [{
                    name: 'Eventos',
                    type: 'pie',
                    radius: '75%',
                    center: ['50%', '57.5%'],
                    data: [
                        {value: 335, name: 'Semana 1'},
                        {value: 310, name: 'Semana 2'},
                        {value: 234, name: 'Semana 3'},
                        {value: 135, name: 'Semana 4'},
                        {value: 1548, name: 'Semana 5'}
                    ]
                }]
            };

            // Column options
            connect_column_options = {

                // Setup grid
                grid: {
                    x: 40,
                    x2: 47,
                    y: 35,
                    y2: 25
                },

                // Add tooltip
                tooltip: {
                    trigger: 'axis',
                    axisPointer: {
                        type: 'shadow'
                    }
                },

                // Add legend
                legend: {
                    data: ['Semana 1','Semana 2','Semana 3','Semana 4','Semana 5']
                },

                // Add toolbox
                toolbox: {
                    show: true,
                    orient: 'vertical',
                    x: 'right', 
                    y: 35,
                    feature: {
                        mark: {
                            show: false,
                            title: {
                                mark: 'Markline switch',
                                markUndo: 'Undo markline',
                                markClear: 'Clear markline'
                            }
                        },
                        magicType: {
                            show: true,
                            title: {
                                line: 'Visualize em Linhas',
                                bar: 'Visualize em barras',
                            },
                            type: ['line', 'bar']
                        },
                        restore: {
                            show: true,
                            title: 'Atualizar'
                        },
                        saveAsImage: {
                            show: true,
                            title: 'Salvar Imagem',
                            lang: ['Save']
                        }
                    }
                },

                // Enable drag recalculate
                calculable: true,

                // Horizontal axis
                xAxis: [{
                    type: 'category',
                    data: ['Jan','Fev','Mar','Abr','Mai','Jun','Jul','Ago','Set','Out','Nov','Dez']
                }],

                // Vertical axis
                yAxis: [{
                    type: 'value',
                    splitArea: {show: true}
                }],

                // Add series
                series: [
                    {
                        name: 'Semana 1',
                        type: 'bar',
                        stack: 'Total',
                        data: [320, 332, 301, 334, 390, 330, 320]
                    },
                    {
                        name: 'Semana 2',
                        type: 'bar',
                        stack: 'Total',
                        data: [120, 132, 101, 134, 90, 230, 210]
                    },
                    {
                        name: 'Semana 3',
                        type: 'bar',
                        stack: 'Total',
                        data: [220, 182, 191, 234, 290, 330, 310]
                    },
                    {
                        name: 'Semana 4',
                        type: 'bar',
                        stack: 'Total',
                        data: [150, 232, 201, 154, 190, 330, 410]
                    },
                    {
                        name: 'Semana 5',
                        type: 'bar',
                        stack: 'Total',
                        data: [820, 932, 901, 934, 1290, 1330, 1320]
                    }
                ]
            };

            // Connect charts
            connect_pie.connect(connect_column);
            connect_column.connect(connect_pie);



            // Apply options
            // ------------------------------

                   
            connect_pie.setOption(connect_pie_options);
            connect_column.setOption(connect_column_options);



            // Resize charts
            // ------------------------------

            window.onresize = function () {
                setTimeout(function (){
                
                    
                    connect_pie.resize();
                    connect_column.resize();
                }, 200);
            }
        }
    );
});


  </script>







