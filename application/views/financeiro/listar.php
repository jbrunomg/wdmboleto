<style>
    .ui-front {
      z-index: 1051;
    }
    .ui-datepicker{
      z-index: 99999 !important;
    }
  </style>

<section class="content">
    <div class="panel panel-flat">
        <div class="panel-heading">
            <div class="row">
                <div class="col-md-12">
                    <?php if (checarPermissao('eFinanceiro')) { ?>
                        <a data-toggle="modal" data-target="#modalReceita" role="button" class="btn bg-teal-400 btn-labeled btn-labeled-right mr-2" title="Cadastrar nova receita">
                            <b><i class="icon-plus2"></i></b> Nova Receita
                        </a>
                        <a href="#modalDespesa" data-toggle="modal" role="button" class="btn bg-danger-400 btn-labeled btn-labeled-right" title="Cadastrar nova despesa">
                            <b><i class="icon-plus2"></i></b> Nova Despesa
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-3">
                    <div class="form-group">
                        <label for="cliente">Cliente / Fornecedor</label>
                        <input type="text" class="form-control" name="cliente" id="clie_forn" value="<?php echo set_value('cliente'); ?>">
                        <input type="hidden" id="clie_forn_id" name="clie_forn_id" value="<?php echo set_value('clie_forn_id'); ?>">
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label>Período <i class="icon-info-sign tip-top" data-original-title="Lançamentos com vencimento no período."></i></label>
                        <select name="periodo" id="periodo" class="form-control">
                            <option value="dia">Dia</option>
                            <option value="semana">Semana</option>
                            <option value="mes">Mês</option>
                            <option value="ano">Ano</option>
                            <option value="todos">Todos</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label>Situação <i class="icon-info-sign tip-top" data-original-title="Lançamentos com situação específica ou todos."></i></label>
                        <select name="situacao" id="situacao" class="form-control">
                            <option value="todos">Todos</option>
                            <option value="previsto">À Vencer</option>
                            <option value="atrasado">Vencidos</option>
                            <option value="realizado">Realizado</option>
                            <option value="pendente">Pendências</option>
                        </select>
                    </div>
                </div>
                <div class="col-xs-3">
                    <div class="form-group">
                        <label>Tipo <i class="icon-info-sign tip-top" data-original-title="Tipo receita ou despesa"></i></label>
                        <select name="tipo" id="tipo" class="form-control">
                            <option value="">Ambos</option>
                            <option value="receita">Receita</option>
                            <option value="despesa">Despesa</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-xs-3">
                    <button type="button" id="btnFiltrar" class="btn btn-block bg-blue btn-flat">Filtrar</button>
                </div>
            </div>
            <div class="mt-3">
                <table id="tableFinanceiro" class="table table-bordered table-striped" width="100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Tipo</th>
                            <th>Cliente / Fornecedor</th>
                            <th>Vencimento</th>
                            <th>Status</th>
                            <th>Valor</th>
                            <th>Ações</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</section>

<?php $this->load->view('financeiro/modals/adicionarReceita');?>  
<?php $this->load->view('financeiro/modals/adicionarDespesa');?>  
<?php $this->load->view('financeiro/modals/editar');?>  
<?php $this->load->view('financeiro/modals/excluir');?>   

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-maskmoney@3.0.2/dist/jquery.maskMoney.min.js"></script>
<script>var baseUrl = '<?php echo base_url() ?>';</script>  
<script src="<?php echo base_url(); ?>public/assets/js/options/financeiro.js"></script>
