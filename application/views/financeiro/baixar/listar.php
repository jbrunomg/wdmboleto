<div class="panel panel-flat">
	<div class="panel-heading">
        <?php if (checarPermissao('eFinanceiro')) { ?>
            <a data-toggle="modal" data-target="#baixar-receita" role="button" class="btn bg-teal  tip-bottom" title="Baixar receita"><i class="icon-plus2 position-right"></i></i> Baixar Receita</a>
            <a href="#baixar-despesa" data-toggle="modal" role="button" class="btn btn-danger tip-bottom" title="Baixar despesa"><i class="icon-plus2 position-right"></i></i> Baixar Despesa</a>
        <?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>
				<th>#</th>
				<th>Cliente</th>
				<th>Valor</th>
				<th>Usuário</th>
				<th>Tipo</th>
				<th>Data</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { ?>
				<tr>
					<td><?php echo $valor->id; ?></td>
					<td><?php echo $valor->usuario_nome; ?></td>
					<td><?php echo $valor->valor_baixa; ?></td> 					
					<td><?php echo $valor->id_usuario; ?></td>
					<td><?php echo ucwords($valor->tipo); ?></td>
					<td><?php echo date('d/m/Y H:i:s', strtotime($valor->data_baixa)); ?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>

<div class="modal fade" id="baixar-receita">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalBaixarLoja">Recebimento</h4>
            </div>
            <div class="modal-body" id="modalBodyBaixar">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="cliente_nomeModal_receita">Cliente</label>
                            <select required class="form-control input-sm kb-pad" name="cliente_nomeModal_receita" id="cliente_nomeModal_receita">
                                <option value="">Selecione</option>
                                <?php foreach ($clientesReceitasPendentes as $cliente) { ?>
                                <option data-valor="<?php echo $cliente->valor; ?>" value="<?php echo $cliente->usuario_id; ?>"><?php echo $cliente->usuario_nome; ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="datepickerCadastroIncio">Data</label>
                            <input type="text" name="datepickerBaixar" id="datepickerBaixar" placeholder="dd/mm/aaaa" value="<?= date('d/m/Y'); ?>" class="form-control input-sm kb-text" >
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="total_debito_receita"><span style="color: red;">Total de Débito</span></label>
                            <input type="text" style="background-color:yellow; color: red;" name="total_debito_receita" id="total_debito_receita" disabled  value="" class="form-control input-sm kb-pad" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="tipo_receita">Recebimento</label>
                            <select required class="form-control input-sm kb-pad" name="cliente_tipo_receita" id="tipo_receita">
                                <option value="Dinheiro">Dinheiro</option>
                                <option value="Cheque">Cheque</option>
                                <option value="Cartão de Crédito">Cartão de Crédito</option>
                                <option value="Cartão de Débito">Cartão de Débito</option>
                                <option value="Crédito Loja">Crédito Loja</option>
                                <option value="Vale Alimentação">Vale Alimentação</option>
                                <option value="Vale Refeição">Vale Refeição</option>
                                <option value="Vale Presente">Vale Presente</option>
                                <option value="Vale Combustível">Vale Combustível</option>
                                <option value="Duplicata Mercantil">Duplicata Mercantil</option>
                                <option value="Boleto Bancário">Boleto Bancário</option>
                                <option value="Transferência Bancária">Transferência Bancária</option>
                                <option value="Sem pagamento">Sem pagamento</option>
                                <option value="Outros">Outros</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label class="control-label" for="valor_pago_receita">Valor Pago</span></label>
                            <input type="text" style="color: green;"  name="valor_pago_receita" id="valor_pago_receita" value="" class="form-control input-sm kb-text" >
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label class="control-label" for="debito_receita"><span style="color: red;">Debito</span></label>
                            <input type="text" style="background-color:yellow; color: red;" name="debito_receita" id="debito_receita" disabled value="" class="form-control input-sm kb-text" >
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="this.disabled = true;" id='btn-confirm-receita' class="btn btn-success">Confirmar Recebimento</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="baixar-despesa">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="modalBaixarLoja">Recebimento</h4>
            </div>
            <div class="modal-body" id="modalBodyBaixar">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="form-group">
                            <label class="control-label" for="cliente_nomeModal_despesa">Cliente</label>
                            <select required class="form-control input-sm kb-pad" name="cliente_nomeModal_despesa" id="cliente_nomeModal_despesa">
                                <option value="">Selecione</option>
                                <?php foreach ($clientesDespesasPendentes as $cliente) { ?>
                                <option data-valor="<?php echo $cliente->valor; ?>" value="<?php echo $cliente->usuario_id; ?>"><?php echo $cliente->usuario_nome; ?> </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="datepickerCadastroIncio">Data</label>
                            <input type="text" name="datepickerBaixar" id="datepickerBaixar" placeholder="dd/mm/aaaa" value="<?= date('d/m/Y'); ?>" class="form-control input-sm kb-text" >
                        </div>
                    </div>
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="total_debito_despesa"><span style="color: red;">Total de Débito</span></label>
                            <input type="text" style="background-color:yellow; color: red;" name="total_debito_despesa" id="total_debito_despesa" disabled  value="" class="form-control input-sm kb-pad" >
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6">
                        <div class="form-group">
                            <label class="control-label" for="tipo_despesa">Recebimento</label>
                            <select required class="form-control input-sm kb-pad" name="cliente_tipo_despesa" id="tipo_despesa">
                                <option value="Dinheiro">Dinheiro</option>
                                <option value="Cheque">Cheque</option>
                                <option value="Cartão de Crédito">Cartão de Crédito</option>
                                <option value="Cartão de Débito">Cartão de Débito</option>
                                <option value="Crédito Loja">Crédito Loja</option>
                                <option value="Vale Alimentação">Vale Alimentação</option>
                                <option value="Vale Refeição">Vale Refeição</option>
                                <option value="Vale Presente">Vale Presente</option>
                                <option value="Vale Combustível">Vale Combustível</option>
                                <option value="Duplicata Mercantil">Duplicata Mercantil</option>
                                <option value="Boleto Bancário">Boleto Bancário</option>
                                <option value="Transferência Bancária">Transferência Bancária</option>
                                <option value="Sem pagamento">Sem pagamento</option>
                                <option value="Outros">Outros</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label class="control-label" for="valor_pago_despesa">Valor Pago</span></label>
                            <input type="text" style="color: green;"  name="valor_pago_despesa" id="valor_pago_despesa" value="" class="form-control input-sm kb-text">
                        </div>
                    </div>
                    <div class="col-xs-3">
                        <div class="form-group">
                            <label class="control-label" for="debito_despesa"><span style="color: red;">Debito</span></label>
                            <input type="text" style="background-color:yellow; color: red;" name="debito_despesa" id="debito_despesa" disabled value="" class="form-control input-sm kb-text">
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                <button type="button" onclick="this.disabled = true;" id='btn-confirm-despesa' class="btn btn-success">Confirmar Recebimento</button>
            </div>
        </div>
    </div>
</div>
<div class="example-modal">
  <div class="modal" id="modalNull">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <p id="msg"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-success" onclick="ativarBotaoRecebimento()" data-dismiss="modal">ok</button>
        </div>
      </div>      
    </div>          
  </div>        
</div>
<div class="example-modal">
  <div class="modal" id="modalAlerta">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
        </div>
        <div class="modal-body">
          <p id="msgAlert"></p>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger pull-left" data-dismiss="modal" id="modalAlertaNao">Não</button>
          <button type="button" onclick="this.disabled = true;" class="btn btn-success" id="modalAlertaSim">Sim</button>
        </div>
      </div>
    </div>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/jquery-maskmoney@3.0.2/dist/jquery.maskMoney.min.js"></script>
<script type="text/javascript" src="https://mix.mixcellpe.com.br/assets/dist/js/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-migrate/1.4.1/jquery-migrate.min.js" integrity="sha512-t0ovA8ZOiDuaNN5DaQQpMn37SqIwp6avyoFQoW49hOmEYSRf8mTCY2jZkEVizDT+IZ9x+VHTZPpcaMA5t2d2zQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

<script type="text/javascript">
  let validation = true;
  function abreModalNull(msg) {
    $("#modalNull").modal({
      show: true
    });
    $('#msg').text(msg);
    validation = false;
  }

  function ativarBotaoRecebimento() {
    $("#btn-confirm-receita").removeAttr("disabled");
  }

  function abreModalAlert(msg) {
    $("#modalAlerta").modal({
      show: true
    });
    $('#msgAlert').text(msg);
  }

  $('#cliente_nomeModal_receita').change(function() {
     $('#total_debito_receita').val($('#cliente_nomeModal_receita').find(':selected').attr('data-valor'));
  });

  $('#cliente_nomeModal_despesa').change(function() {
     $('#total_debito_despesa').val($('#cliente_nomeModal_despesa').find(':selected').attr('data-valor'));
  });

  $('#valor_pago_receita').change(function() {
      valor = $('#total_debito_receita').val();
      valor_pago = $('#valor_pago_receita').val();
      $('#debito_receita').val(valor - valor_pago);
  });

  $('#valor_pago_despesa').change(function() {
      valor = $('#total_debito_despesa').val();
      valor_pago = $('#valor_pago_despesa').val();
      $('#debito_despesa').val(valor - valor_pago);
  });

  $('#btn-confirm-receita').click(function(event) {
    var valor_total = $('#cliente_nomeModal_receita').find(':selected').attr('data-valor');
    var valor_pago = $('#valor_pago_receita').val();
    var valor_debito = $('#debito_receita').val();
    var cliente = $('#cliente_nomeModal_receita').find(':selected').val();
    var formaPgto = $('#tipo_receita').val();
    var validacao = false;

    if (!cliente || !valor_pago) {
        abreModalNull('Valores de Cliente ou Valor Pago, em branco preencher por gentileza!!!');
    } else {
        $("#btn-confirm-receita").attr("disabled", false);

        if (validation) {
            validacao = verificarBaixar(cliente, valor_pago)
        }

        validation = true;

        if (!validacao) {
            $.ajax({
                method: "POST",
                url: base_url+"financeiro/financeiroBaixarAdd/receita",
                dataType: "JSON",
                data: {
                    valor_total: valor_total,
                    valor_pago: valor_pago,
                    valor_debito: valor_debito,
                    cliente: cliente,
                    formaPgto
                },
                success: function(data)
                {
                    location.reload();
                }
            });
        }
      }
  });

  $('#btn-confirm-despesa').click(function(event) {
    var valor_total = $('#cliente_nomeModal_despesa').find(':selected').attr('data-valor');
    var valor_pago = $('#valor_pago_despesa').val();
    var valor_debito = $('#debito_despesa').val();
    var cliente = $('#cliente_nomeModal_despesa').find(':selected').val();
    var formaPgto = $('#tipo_despesa').val();
    var validacao = false;

    if (!cliente || !valor_pago) {
        abreModalNull('Valores de Cliente ou Valor Pago, em branco preencher por gentileza!!!');
    } else {
        $("#btn-confirm-despesa").attr("disabled", false);

        if (validation) {
            validacao = verificarBaixar(cliente, valor_pago)
        }

        validation = true;

        if (!validacao) {
            $.ajax({
                method: "POST",
                url: base_url+"financeiro/financeiroBaixarAdd/despesa",
                dataType: "JSON",
                data: {
                    valor_total: valor_total,
                    valor_pago: valor_pago,
                    valor_debito: valor_debito,
                    cliente: cliente,
                    formaPgto
                },
                success: function(data)
                {
                    location.reload();
                }
            });
        }
      }
  });

  function verificarBaixar(cliente, valor_debito) {
    var data = $.ajax({
          method: "POST",
          url: base_url+"financeiro/verificarBaixar/",
          dataType: "JSON",
          async: false,
          data: { valor_pago:valor_pago, cliente: cliente},
          success: function(data)
          {
            if(data == true){
              abreModalAlert('Já existe baixa com esse valor no dia de hoje para esse cliente, Deseja Continuar?');
             
            }
            data = JSON.parse(data);
        }
    });

    return JSON.parse(data.responseText);
  }

  $('#modalAlertaNao').click(function(event) {
    location.reload();
  });

  $('#modalAlertaSim').click(function(event) {
    var tipoReceita = $("#cliente_nomeModal_receita").val();

    if (tipoReceita) {
        var valor_total = $('#cliente_nomeModal_receita').find(':selected').attr('data-valor');
        var valor_pago = $('#valor_pago_receita').val();
        var valor_debito = $('#debito_receita').val();
        var cliente = $('#cliente_nomeModal_receita').find(':selected').val();
        var formaPgto = $('#tipo_receita').val();

        $.ajax({
            method: "POST",
            url: base_url+"financeiro/financeiroBaixarAdd/receita",
            dataType: "JSON",
            data: {
                valor_total: valor_total,
                valor_pago: valor_pago,
                valor_debito: valor_debito,
                cliente: cliente,
                formaPgto: formaPgto
            },
            success: function(data)
            {
            location.reload();
            }
            
        });
    } else {
        var valor_total = $('#cliente_nomeModal_despesa').find(':selected').attr('data-valor');
        var valor_pago = $('#valor_pago_despesa').val();
        var valor_debito = $('#debito_despesa').val();
        var cliente = $('#cliente_nomeModal_despesa').find(':selected').val();
        var formaPgto = $('#tipo_despesa').val();

        $.ajax({
            method: "POST",
            url: base_url+"financeiro/financeiroBaixarAdd/despesa",
            dataType: "JSON",
            data: {
                valor_total: valor_total,
                valor_pago: valor_pago,
                valor_debito: valor_debito,
                cliente: cliente,
                formaPgto: formaPgto
            },
            success: function(data)
            {
            location.reload();
            }
            
        });
    }
  });
</script>