
  <!-- Modal editar lançamento -->
  <!-- Modal -->
  <div class="modal fade" id="modalEditar" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <form id="formEditar" action="<?php echo base_url() ?>index.php/financeiro/editar" method="post">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            <h3 id="myModalLabel">Editar Lançamento</h3>
          </div>
          <div class="modal-body">
            <div class="col-xs-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>
            <div class="col-xs-12" style="margin-left: 0">
              <label for="fornecedor">Fornecedor / Empresa*</label>
              <input class="form-control" id="fornecedorEditar" type="text" name="fornecedor" />
            </div>
            <div class="col-xs-12" style="margin-left: 0">
              <label for="descricao">Descrição</label>
              <input class="form-control" id="descricaoEditar" type="text" name="descricao" />
              <input id="urlAtualEditar" type="hidden" name="urlAtual" value="" />
            </div>

            <div class="col-xs-4" style="margin-left: 0">
              <label for="valor">Valor*</label>
              <input type="hidden" name="tipo" value="despesa" />
              <input type="hidden" id="idEditar" name="id" value="" />
              <input type="hidden" id="idVenda" name="idVenda" value="" />

              <input class="form-control moneyx" type="text" name="valor" id="valorEditar" />
            </div>
            <div class="col-xs-4">
              <label for="vencimento">Data Vencimento*</label>
              <input class="form-control datepicker" type="text" name="vencimento" id="vencimentoEditar" />
            </div>
            <div class="col-xs-4">
              <label for="vencimento">Tipo*</label>
              <select class="form-control" name="tipo" id="tipoEditar">
                <option value="receita">Receita</option>
                <option value="despesa">Despesa</option>
              </select>
            </div>
            <div class="col-xs-2" style="margin-left: 0">
              <label for="pago">Foi Pago?</label><BR>
              <input id="pagoEditar" type="checkbox" name="pago" value="1" />
            </div>
            <div id="divPagamentoEditar" class="span8" style=" display: none">
              <div class="col-xs-5">
                <label for="pagamento">Data Pagamento</label>
                <input class="form-control datepicker" id="pagamentoEditar" type="text" name="pagamento" />
              </div>
              <div class="col-xs-5">
                <label for="formaPgto">Forma Pgto</label>
                <select name="formaPgto" id="formaPgtoEditar" class="form-control">
                  <option value="Dinheiro">Dinheiro</option>
                  <option value="Cheque">Cheque</option>
                  <option value="Cartão de Crédito">Cartão de Crédito</option>
                  <option value="Cartão de Débito">Cartão de Débito</option>
                  <option value="Crédito Loja">Crédito Loja</option>
                  <option value="Vale Alimentação">Vale Alimentação</option>
                  <option value="Vale Refeição">Vale Refeição</option>
                  <option value="Vale Presente">Vale Presente</option>
                  <option value="Vale Combustível">Vale Combustível</option>
                  <option value="Duplicata Mercantil">Duplicata Mercantil</option>
                  <option value="Boleto Bancário">Boleto Bancário</option>
                  <option value="Transferência Bancária">Transferência Bancária</option>
                  <option value="Sem pagamento">Sem pagamento</option>
                  <option value="Outros">Outros</option>
                </select>
              </div>
            </div>

            <div class="control-group" id="divDadosCartao" style=" display: none">
              <!-- 
                01 - Visa / Visa Electron
                02 - Mastercard / Maestro
                03 - American Express
                04 - Sorocred
                05 - Diners Club
                06 - Elo
                07 - Hipercard
                08 - Aura
                09 - Cabal
                99 - Outros
                -->
              <div class="col-xs-12" style="margin-left: 0">
                <div class="col-xs-6">
                  <label for="bandeiraCart">Bandeira Cartão</label>
                  <select class="form-control" name="bandeiraCart" id="bandeira_cart">
                    <option value="">Selecione Bandeira</option>

                    <?php $cartao_id =  "<input id='bandeira_cart'/>"; ?>
                    <?php foreach ($cartao as $c) { ?>
                      <?php $selected = '';
                      if ($c->categoria_cart_id == $cartao_id) {
                        $selected = 'selected';
                      } ?>
                      <option value="<?php echo $c->categoria_cart_id; ?>" <?php echo $selected ?>><?php echo $c->categoria_cart_descricao . ' - ' . $c->categoria_cart_nome_credenciadora; ?></option>
                    <?php } ?>
                  </select>

                </div>
                <div class="col-xs-6">
                  <label for="n_autorizacao_cartao">Nº Autorização(NSU)</label>
                  <input id="Id_cartao" class="form-control" type="text" name="autorizacao_nsu" />
                </div>
              </div>
            </div>


          </div>
          <div class="modal-footer"></div>
          <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelarEditar">Cancelar</button>
            <button class="btn btn-primary">Salvar Alterações</button>
          </div>
        </form>
      </div>
    </div>
  </div>