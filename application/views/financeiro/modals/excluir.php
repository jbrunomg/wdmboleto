  <!-- Modal Excluir lançamento-->
  <!-- Modal -->
  <div class="modal fade" id="modalExcluir" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h3 id="myModalLabel">Excluir Lançamento</h3>
        </div>
        <div class="modal-body">
          <h5 style="text-align: center">Deseja realmente excluir esse lançamento?</h5>
          <input name="id" id="idExcluir" type="hidden" value="" />
        </div>
        <div class="modal-footer">
          <button class="btn" data-dismiss="modal" aria-hidden="true" id="btnCancelExcluir">Cancelar</button>
          <button class="btn btn-danger" id="btnExcluir">Excluir Lançamento</button>
        </div>
      </div>

    </div>
  </div>