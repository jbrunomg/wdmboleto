  <!-- Modal nova despesa -->
  <!-- Modal -->
  <div class="example-modal">
    <div class="modal" id="modalDespesa">
      <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
          <form id="formDespesa" action="<?php echo base_url() ?>index.php/financeiro/adicionarDespesa" method="post">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h3 id="myModalLabel">Adicionar Despesa</h3>
            </div>
            <div class="modal-body">
              <div class="col-xs-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>
              <div class="col-xs-12" style="margin-left: 0">
                <label for="fornecedor">Fornecedor / Empresa*</label>
                <input class="form-control" id="clie_forn_despesa" type="text" name="fornecedor" />
                <input id="clie_forn_despesa_id" class="form-control" type="hidden" name="clie_forn_despesa_id" value="" />
              </div>

              <div class="col-xs-12" style="margin-left: 0">
                <label for="descricao">Descrição</label>
                <input class="form-control" id="descricao" type="text" name="descricao" />
                <input id="urlAtual" type="hidden" name="urlAtual" value="<?php echo current_url() ?>" />
              </div>

              <div class="col-xs-12" style="margin-left: 0">
                <label for="">Categoria / Grupo:</label>
                <select name="categoria" id="categoria" class="form-control">
                  <option value="0">Selecione</option>
                  <?php foreach ($categoriaFin as $c) { ?>
                    <option value="<?php echo $c->categoria_fina_id; ?>"><?php echo $c->categoria_fina_descricao; ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-xs-12" style="margin-left: 0"><br>
                <span class="btn btn-primary form-control" id="novaFormaPagamentoDes">Adicionar mais formas de pagamento</span>
              </div>

              <br>
              <br>


              <div class="col-xs-12" style="margin-left: 0" id="formasPagamentoNovaDes"></div>

              <div class="col-xs-12" style="margin-left: 0" id="formasPagamentoDes">
                <div class="col-xs-12" style="margin-left: 0">
                  <div class="col-xs-3" style="margin-left: 0">
                    <label for="valor">Valor*</label>
                    <input type="hidden" name="tipo" value="despesa" />
                    <input type="hidden" id="qtdFormaPagamentoDes" name="qtdFormaPagamentoDes" value="despesa" />
                    <input class="form-control moneyx" type="text" name="valor" />
                  </div>

                  <div class="col-xs-4">
                    <label for="vencimento">Data Vencimento*</label>
                    <input class="form-control datepicker" type="text" name="vencimento" />
                  </div>
                  <div class="col-xs-4">
                    <label for="formaPgto">Forma Pgto</label>
                    <select name="formaPgto" class="form-control">
                      <!-- REGRA DA SEFAZ -->
                      <!--
              01 - Dinheiro
              02 - Cheque
              03 - Cartão de Crédito
              04 - Cartão de Débito
              05 - Crédito Loja
              10 - Vale Alimentação
              11 - Vale Refeição
              12 - Vale Presente
              13 - Vale Combustível
              14 - Duplicata Mercantil
              15 - Boleto Bancário
              90 - Sem pagamento
              99 - Outros
              -->

                      <option value="Dinheiro">Dinheiro</option>
                      <option value="Cheque">Cheque</option>
                      <option value="Cartão de Crédito">Cartão de Crédito</option>
                      <option value="Cartão de Débito">Cartão de Débito</option>
                      <option value="Crédito Loja">Crédito Loja</option>
                      <option value="Vale Alimentação">Vale Alimentação</option>
                      <option value="Vale Refeição">Vale Refeição</option>
                      <option value="Vale Presente">Vale Presente</option>
                      <option value="Vale Combustível">Vale Combustível</option>
                      <option value="Duplicata Mercantil">Duplicata Mercantil</option>
                      <option value="Boleto Bancário">Boleto Bancário</option>
                      <option value="Transferência Bancária">Transferência Bancária</option>
                      <option value="Sem pagamento">Sem pagamento</option>
                      <option value="Outros">Outros</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-xs-12" style="margin-left: 0">
                <div class="col-xs-4" style="margin-left: 0">
                  <label for="pago">Foi Pago?</label><br>
                  <input id="pago" type="checkbox" name="pago" value="1" />
                </div>
                <div id="divPagamento" class="span8" style=" display: none">
                  <div class="col-xs-6">
                    <label for="pagamento">Data Pagamento</label>
                    <input class="form-control datepicker" id="pagamento" type="text" name="pagamento" />
                  </div>
                </div>
              </div>

            </div>
            <div class="modal-footer">
              <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
              <button class="btn btn-danger">Adicionar Despesa</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
