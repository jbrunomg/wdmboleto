

  <!-- Modal nova receita -->
  <!-- Modal -->
  <div class="example-modal">
    <div class="modal" id="modalReceita">
      <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
          <form id="formReceita" action="<?php echo base_url() ?>index.php/financeiro/adicionarReceita" method="post">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
              <h3 id="myModalLabel">Adicionar Receita</h3>
            </div>
            <div class="modal-body">

              <div class="col-xs-12 alert alert-info" style="margin-left: 0"> Obrigatório o preenchimento dos campos com asterisco.</div>

              <div class="col-xs-12">
                <label for="cliente">Cliente*</label>
                <!--                 <input class="form-control" id="clie_receita" type="text" name="cliente" />
                <input id="clie_receita_id" class="span12" type="hidden" name="clie_receita_id" value=""  /> -->
                <input type="text" class="form-control" name="clie_receita" id="clie_receita" value="<?php echo set_value('cliente'); ?>">
                <input type="hidden" id="clie_receita_id" name="clie_receita_id" value="<?php echo set_value('clie_receita_id'); ?>">
              </div>

              <div class="col-xs-12">
                <label class="control-label" for="descricao">Descrição</label>
                <div class="form-group">
                  <input class="form-control" id="descricao" type="text" name="descricao" />
                  <input id="urlAtual" type="hidden" name="urlAtual" value="<?php echo current_url() ?>" />
                </div>
              </div>

              <div class="col-xs-12">
                <label class="control-label" for="">Categoria / Grupo:</label>
                <select name="categoria" id="categoria" class="form-control">
                  <option value="0">Selecione</option>
                  <?php foreach ($categoriaFin as $c) { ?>
                    <option value="<?php echo $c->categoria_fina_id; ?>"><?php echo $c->categoria_fina_descricao; ?></option>
                  <?php } ?>
                </select>
              </div>

              <div class="col-xs-12"><br>
                <span class="btn btn-primary col-xs-12" id="novaFormaPagamento">Adicionar mais formas de pagamento</span>
                <br><br>
              </div>

              <div class="col-xs-12" style="margin-left: 0" id="formasPagamentoNova"></div>
              <div class="col-xs-12" style="margin-left: 0" id="formasPagamento">
                <div class="col-xs-12" style="margin-left: 0">
                  <div class="col-xs-3" style="margin-left: 0">
                    <label for="valor">Valor*</label>
                    <input type="hidden" name="tipo" value="receita" />
                    <input type="hidden" id="qtdFormaPagamento" name="qtdFormaPagamento" value="receita" />
                    <input class="form-control moneyx" type="text" name="valor" />
                  </div>

                  <div class="col-xs-4">
                    <label for="vencimento">Data Vencimento*</label>
                    <input class="form-control datepicker" type="text" name="vencimento" />
                  </div>
                  <div class="col-xs-4">
                    <label for="formaPgto">Forma Pgto</label>
                    <select name="formaPgto" class="form-control">
                      <option value="Dinheiro">Dinheiro</option>
                      <option value="Cheque">Cheque</option>
                      <option value="Cartão de Crédito">Cartão de Crédito</option>
                      <option value="Cartão de Débito">Cartão de Débito</option>
                      <option value="Crédito Loja">Crédito Loja</option>
                      <option value="Vale Alimentação">Vale Alimentação</option>
                      <option value="Vale Refeição">Vale Refeição</option>
                      <option value="Vale Presente">Vale Presente</option>
                      <option value="Vale Combustível">Vale Combustível</option>
                      <option value="Duplicata Mercantil">Duplicata Mercantil</option>
                      <option value="Boleto Bancário">Boleto Bancário</option>
                      <option value="Transferência Bancária">Transferência Bancária</option>
                      <option value="Sem pagamento">Sem pagamento</option>
                      <option value="Outros">Outros</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="col-xs-12" style="margin-left: 0">
                <div class="col-xs-4" style="margin-left: 0">
                  <label for="recebido">Recebido?</label><br>
                  <input id="recebido" type="checkbox" name="recebido" value="1" />
                </div>
                <div id="divRecebimento" class="span8" style=" display: none">
                  <div class="col-xs-6">
                    <label for="pagamento">Data Pagamento</label>
                    <input class="form-control datepicker" id="recebimento" type="text" name="recebimento" />
                  </div>
                </div>
              </div>
            </div>

            <div class="modal-footer">
              <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
              <button class="btn btn-success">Adicionar Receita</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>