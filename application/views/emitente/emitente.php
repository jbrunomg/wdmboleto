<!-- Form horizontal -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Emitente</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<!-- <li><a data-action="reload"></a></li> -->
			                		<!-- <li><a data-action="close"></a></li> -->
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<form class="form-horizontal" action="<?php echo current_url();?>" method="post" enctype="multipart/form-data">

								<!-- <form class="form-horizontal" action="<?php echo current_url(); ?>" method="post"> -->

								<fieldset class="content-group">
									<legend class="text-bold">Dados Empresa:</legend>

								
									<input type="hidden" name="id" value="<?php echo $dados[0]->id; ?>" />	

									
									<div class="form-group">
										<label class="control-label col-lg-2">CPF/CNPJ:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" placeholder="" data-mask="" data-mask-selectonfocus="true" name="cnpj" id="cnpj" value="<?php echo $dados[0]->cnpj; ?>">
										<?php echo form_error('cnpj'); ?>
										</div>										
									</div>


									<div class="form-group">
										<label class="control-label col-lg-2">Nome:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" placeholder="Nome do Empresa" name="nome" id="nome" value="<?php echo $dados[0]->nome; ?>">
										<?php echo form_error('nome'); ?>
										</div>										
									</div>


								<legend class="text-bold">Dados Logradouro:</legend>
					<!-- 				<div class="form-group">
										<label class="control-label col-lg-2">CEP:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="cep" id="cep" data-mask="99999-999" data-mask-selectonfocus="true" value="<?php echo $dados[0]->cep; ?>">
										<?php echo form_error('cep'); ?>
										</div>										
									</div> -->

									<div class="form-group">
										<label class="control-label col-lg-2">Endereço:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="rua" id="rua" value="<?php echo $dados[0]->rua; ?>">
										<?php echo form_error('rua'); ?>
										</div>										
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Numero:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="numero" id="numero" value="<?php echo $dados[0]->numero; ?>">
										<?php echo form_error('numero'); ?>
										</div>										
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Bairro:</label>
										<div class="col-lg-5">
											<input type="text" class="form-control" name="bairro" id="bairro" value="<?php echo $dados[0]->bairro; ?>">
										<?php echo form_error('bairro'); ?>
										</div>										
									</div>				

									<div class="form-group">
			                        	<label class="control-label col-lg-2">UF:</label>
			                        	<div class="col-lg-5">
				                            <select  class="form-control" name="uf" id="estado" >
				                            	<option value="">Selecione</option>
				                                <?php foreach ($estados as $valor) { ?>
				                                	<?php $selected = ($valor->id == $dados[0]->uf)?'SELECTED': ''; ?>
				  		                              <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
				  		                        <?php } ?>
				                            </select>
				                            <?php echo form_error('estado'); ?>
			                            </div>			                            
			                        </div>			                        
									<div class="form-group">
										<label class="control-label col-lg-2">Cidade:</label>
										<div class="col-lg-5">
				                            <select class="form-control" name="cidade" id="cidade">
				                                
				                                <?php foreach ($cidades as $valor) { ?>
				                                	<?php $selected = ($valor->id == $dados[0]->cidade)?'SELECTED': ''; ?>
				  		                              <option value="<?php echo $valor->id; ?>" <?php echo $selected; ?>><?php echo $valor->nome; ?></option>
				  		                        <?php } ?>
				                            </select>
			                            <?php echo form_error('cidade'); ?>	
			                            </div>				                            	                            
									</div>


													
									<legend class="text-bold">Dados Contato:</legend>


									<div class="form-group">
										<label class="control-label col-lg-2">Telefone:</label>
										<div class="col-lg-5">
											<input type="tel" name="telefone" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="telefone" id="telefone" value="<?php echo $dados[0]->telefone; ?>">
										<?php echo form_error('telefone'); ?>
										</div>										
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Celular:</label>
										<div class="col-lg-5">
											<input type="tel" name="celular" class="form-control" placeholder="(99)99999-9999" data-mask="(99)99999-9999" data-mask-selectonfocus="true" name="celular" id="celular" value="<?php echo $dados[0]->celular; ?>">
										<?php echo form_error('celular'); ?>
										</div>										
									</div>
									<div class="form-group">
										<label class="control-label col-lg-2">Email:</label>
										<div class="col-lg-5">
											<input type="email" name="email"  placeholder="seu@email.com" class="form-control" name="email" id="email" value="<?php echo $dados[0]->email; ?>">
										<?php echo form_error('email'); ?>
										</div>										 
									</div>



			                        <div class="form-group">
										<label class="col-lg-2 control-label">Imagem:</label>
										<div class="col-lg-5">
											<div class="uploader bg-teal">
												<input class="file-styled" type="file" name="arquivo">
												<span class="filename" style="-moz-user-select: none;">Nenhum arquivo selecionado</span>
												<span class="action" style="-moz-user-select: none;">
												<i class="icon-cloud-upload2"></i>
												</span>
											</div>
										</div>
									</div>
			                      		
								</fieldset>
								<div class="text-right">
									<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
								</div>
							</form>
						</div>
					</div>
