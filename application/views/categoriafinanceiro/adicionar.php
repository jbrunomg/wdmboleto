<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Centro de Custo</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados da categoria:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

			        <div class="form-group">
			          <label class="control-label col-lg-2">Nome da Centro de Custo:</label>
			          <div class="col-lg-5">
			            <input  type="text" class="form-control" placeholder="Nome Categoria" name="categoria_fina_descricao" id="categoria_fina_descricao" value="<?php echo set_value('categoria_fina_descricao'); ?>">
			          <?php echo form_error('categoria_fina_descricao'); ?>
			          </div>                    
			        </div> 

			 <!--        <div class="form-group"> 
			          <label class="control-label col-lg-2">Nível:</label>
			          <div class="col-lg-4">
			            <select class="form-control" name="scurcunivel" id="scurcunivel">
			              <option value="">Selecione</option>
			              <option value="Medio">Médio</option>
			              <option value="Tecnico">Técnico</option>                  
			              <option value="Superior">Superior</option>
			              <option value="Pos">Pós Graduação</option>
			            </select>
			          </div>
			        </div>  -->                 		

				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
