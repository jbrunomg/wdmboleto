<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Edição de Centro de Custo</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Centro de Custo:</legend>

				<input  type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input  type="hidden" name="categoria_fina_id" value="<?php echo $dados[0]->categoria_fina_id; ?>" />

				<div class="form-group">
					<label class="control-label col-lg-2">Nome da Curso:</label>
					<div class="col-lg-5">
						<input  type="text" class="form-control" placeholder="Nome Curso" name="categoria_fina_descricao" id="categoria_fina_descricao" value="<?php echo $dados[0]->categoria_fina_descricao; ?>">
					<?php echo form_error('categoria_fina_descricao'); ?>
					</div>										
				</div>



                </fieldset>  
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
