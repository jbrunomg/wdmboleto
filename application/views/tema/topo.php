<?php
$menu = $this->uri->segment(1);

$this->dashboard = "";
/* CLIENTE */
$this->mClientes = "";
$this->cliente = "";

/* OPERACOES */
$this->mOperacoes = "";
$this->operacao = "";
$this->vendaBoleto = "";
$this->operacaocartao = "";
$this->vendaCartao = "";

/* CARTOES */
$this->mCartoes = "";
$this->cartao = "";
$this->taxa = "";
$this->maquinas = "";

/* FINACEIRO */
$this->mFinanceiro = "";
$this->financeiro = "";
$this->categoriafinanceiro = "";
$this->baixaParcial = "";

/* RELATORIO */
$this->mRelatorios = "";
$this->relatorios = "";

/* CONFIGURACOES */
$this->mConfiguracoes = "";
$this->usuario = "";
$this->permissao = "";
$this->Auth = "";

switch ($menu) {
    case 'Dashboard':
        $this->dashboard = 'active';
        break;

    /* CLIENTE */
    case 'cliente':
        $this->mClientes = 'active';
        $this->cliente   = 'active';
        break;   

    /* OPERACOES */
    case 'operacao':
        $this->mOperacoes = 'active';
        $this->operacao = 'active';
        break;

    case 'vendaBoleto':
        $this->mOperacoes  = 'active';
        $this->vendaBoleto = 'active';        
        break;

    case 'operacaocartao':
        $this->mOperacoes     = 'active';
        $this->operacaocartao = 'active';        
        break; 

    case 'vendaCartao':
        $this->mOperacoes  = 'active';
        $this->vendaCartao = 'active';        
        break;       

    /* CARTOES */    
    case 'cartao':
        $this->mCartoes = 'active';
        $this->cartao   = 'active';
        break;

    case 'taxa':
        $this->mCartoes = 'active';
        $this->taxa   = 'active';
        break;

    case 'maquinas':
        $this->mCartoes = 'active';
        $this->maquinas = 'active';
        break;

    /* FINANCEIRO */
    case 'financeiro':
        $this->mFinanceiro = 'active';
        $this->financeiro  = 'active';        
        break;
    case 'categoriafinanceiro':
        $this->mFinanceiro = 'active';
        $this->categoriafinanceiro = 'active';        
        break;

   
    default:
        $dashboard = 'active';
        break;
}

if ($this->uri->segment(2) == 'financeiroBaixarListar') {
    $this->mFinanceiro = 'active';
    $this->financeiro = '';
    $this->baixaParcial = 'active';
}
?>


<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>WDM - MixPay</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/core.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/components.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/colors.css" rel="stylesheet" type="text/css">
	<link href="<?php echo base_url(); ?>public/assets/css/timepicki.css" rel="stylesheet" type="text/css">	
	<!-- /global stylesheets -->	

	
	<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/core/libraries/jquery.min.js"></script>	
	<script type="text/javascript" src="<?php echo base_url()?>public/assets/js/pages/form_select2.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>public/assets/js/pages/form_layouts.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>public/assets/js/plugins/forms/selects/select2.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>public/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url()?>public/assets/js/maps/google/basic/basic.js"></script>

    <script type="text/javascript" src="<?php echo base_url()?>public/assets/js/maskmoney.js"></script>


	<script type="text/javascript">
	var base_url = '<?php echo base_url(); ?>';
	var token = '<?php echo $this->security->get_csrf_hash(); ?>';
	</script>
	

</head>

<?php 
$this->CI = get_instance();

$classe =  $this->CI->uri->segment(1);
$metodo =  $this->CI->uri->segment(2); 

// var_dump($metodo);die();
?>

<body  <?php  echo (($classe == 'operacao' OR $classe == 'operacaocartao' ) AND $metodo <> null) ? 'class="sidebar-xs"' : '' ; ?> >
<!-- Main content -->
	<div class="content-wrapper">
		
