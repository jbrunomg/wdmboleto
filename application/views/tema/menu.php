<!-- Main navbar -->
	<div class="navbar navbar-inverse">
		<div class="navbar-header">
			<!-- <a class="navbar-brand" href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>public/assets/images/logo_light.png" alt=""></a> -->
			<a class="navbar-brand" href="<?php echo base_url(); ?>"><i class="icon-barcode2"></i> <span><strong>&nbsp;&nbsp;&nbsp;  WDM-CardPay </strong></span></a>
			

			<ul class="nav navbar-nav pull-right visible-xs-block">
				<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
				<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
			</ul>
		</div>

		<div class="navbar-collapse collapse" id="navbar-mobile">

			<ul class="nav navbar-nav">
				<li>
					<a class="sidebar-control sidebar-main-toggle hidden-xs">
						<i class="icon-paragraph-justify3"></i>
					</a>
				</li>

				<?php 
			    if (isset($dadostce[0]->total)) {
			      $totaltce = $dadostce[0]->total;                  
                } else {
                  $totaltce = 0; 
                }

                if (isset($dadosta[0]->total)) {
                  $totalta = $dadosta[0]->total;                 
                } else {
                   $totalta = 0;		
                }
				
				?>

				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						<i class="icon-git-compare"></i>
						<span class="visible-xs-inline-block position-right">Notificações</span>
						<span class="badge bg-warning-400" id="somaNotificacao"></span>
					</a>
					
					<div class="dropdown-menu dropdown-content">
						<div class="dropdown-content-heading">
							Notificações
							<ul class="icons-list">
								<li><a href="#"><i class="icon-sync"></i></a></li>
							</ul>
						</div>

						<ul class="media-list dropdown-content-body width-350">
							
<!-- 							<li class="media">
								<div class="media-left">
									<a href="#" class="btn border-primary text-primary btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-pull-request"></i></a>
								</div>

								<div class="media-body">
									<a href="#">????? </a> <span class="text-semibold">Pendente</span> no sistema.
									<div class="media-annotation">Total: <span id="apiNotificacao"></span></div>
								</div>
							</li>

							<li class="media">
								<div class="media-left">
									<a href="#" class="btn border-warning text-warning btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-commit"></i></a>
								</div>
								
								<div class="media-body">
									<a href="#">???? </a> <span class="text-semibold">Pendente</span> no sistema.
									<div class="media-annotation">Total: <span id="taNotificacao"></span></div>
								</div>
							</li>

							<li class="media">
								<div class="media-left">
									<a href="#" class="btn border-info text-info btn-flat btn-rounded btn-icon btn-sm"><i class="icon-git-branch"></i></a>
								</div>
								
								<div class="media-body">									
									<a href="#">???? </a> <span class="text-semibold">Vencido</span> no sistema.
									<div class="media-annotation">Total 03</div>
								</div>
							</li> -->

						</ul>

						<div class="dropdown-content-footer">
							<a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
						</div>
					</div>
				</li>				
			</ul>



			<ul class="nav navbar-nav navbar-right">			
				<li class="dropdown dropdown-user">
					<a class="dropdown-toggle" data-toggle="dropdown">
						<?php $foto = ($this->session->userdata('usuario_imagem') != '')? $this->session->userdata('usuario_imagem') :'public/assets/images/placeholder.jpg' ?>
						<img src="<?php echo base_url().$foto; ?>" alt="">
						<span><?php echo $this->session->userdata('usuario_nome');?></span>
						<i class="caret"></i>
					</a>

					<ul class="dropdown-menu dropdown-menu-right">
						<li><a href="#"><i class="icon-user-plus"></i> Meu Perfil</a></li>
						<li class="divider"></li>
						<li><a href="#"><i class="icon-cog5"></i> Configuração</a></li>
						<li><a href="<?php echo base_url() ?>Auth/sair"><i class="icon-switch2"></i> Sair</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</div>
	<!-- /main navbar -->


	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			<div class="sidebar sidebar-main">
				<div class="sidebar-content">

					<!-- User menu -->
					<div class="sidebar-user">
						<div class="category-content">
							<div class="media">
								<?php $foto = ($this->session->userdata('usuario_imagem') != '')? $this->session->userdata('usuario_imagem') :'public/assets/images/placeholder.jpg' ?>
								<a href="#" class="media-left"><img src="<?php echo base_url().$foto; ?>" class="img-circle img-sm" alt=""></a>
								<div class="media-body">
									<span class="media-heading text-semibold"><?php echo $this->session->userdata('usuario_nome');?></span>
									<div class="text-size-mini text-muted">
										<i class="icon-user-tie text-size-small"></i> &nbsp;<?php echo $this->session->userdata('permissao_nome');?>
									</div>
								</div>

								<div class="media-right media-middle">
									<ul class="icons-list">
										
									</ul>
								</div>
							</div>
						</div>
					</div>
					<!-- /user menu -->


					<!-- Main navigation -->
					<div class="sidebar-category sidebar-category-visible">
						<div class="category-content no-padding">
							<ul class="navigation navigation-main navigation-accordion">

								<!-- Main -->
								<li class="navigation-header"><span>Menu</span> <i class="icon-menu" title="Main pages"></i></li>
								<li class="<?php echo $this->dashboard; ?>"   ><a href="<?php echo base_url(); ?>Dashboard"><i class="icon-home4"></i> <span>Dashboard</span></a></li>						
								<!-- /main -->

								<!-- INICIO MENU BOLETO -->
								<?php if(checarPermissao('vClientes')){ ?>		
								<li>
									<a href=""><i class="icon-users"></i> <span>Clientes</span></a>
									<ul>
										<li class="<?php echo $this->cliente; ?>"><a href="<?php echo base_url() ?>cliente/">Clientes</a></li>				
									</ul>
								</li>
								<?php } ?>

								<li class="<?php echo $this->mOperacoes; ?>">
									<a href=""><i class="icon-barcode2"></i> <span>Operações</span></a>
									<ul>
										<?php if(checarPermissao('vOperacaoBoleto')){ ?>	
										<li class="<?php echo $this->operacao; ?>"><a href="<?php echo base_url() ?>operacao">Listar Operações</a></li>	
										<li class="<?php echo $this->vendaBoleto; ?>"><a href="<?php echo base_url() ?>operacao/vendaBoleto">Venda Boleto</a></li>
										<?php } ?>				
											
										<?php if(checarPermissao('vOperacaoCartao')){ ?>	
										<li class="<?php echo $this->operacaocartao; ?>"><a href="<?php echo base_url() ?>operacaocartao">Operação Cartão</a></li>	
										<li class="<?php echo $this->vendaCartao; ?>"><a href="<?php echo base_url() ?>operacaocartao/vendaCartao">Venda Cartão</a></li>
										<li class="<?php echo $this->vendaCartao; ?>"><a href="<?php echo base_url() ?>operacaocartao/vendaCartaoCSV">Venda Cartão .CSV</a></li>
										<li class="<?php echo $this->vendaCartao; ?>"><a href="<?php echo base_url() ?>operacaocartao/vendaCartaoCSV2">Venda Cartão .CSV2</a></li>
										<?php } ?>																	
										<?php if(checarPermissao('cSimulador')){ ?>
										<li><a href="<?php echo base_url(); ?>simulador">Simulador Cartão</a></li>	
										<?php } ?>																
									</ul>
								</li>	

								<li  class="<?php echo $this->mCartoes; ?>">
									<a href=""><i class="icon-credit-card"></i> <span>Cartões</span></a>
									<ul>			
										<?php if(checarPermissao('vCartoes')){ ?>	
										<li class="<?php echo $this->cartao; ?>" ><a href="<?php echo base_url() ?>cartao/">Cartão</a></li>
										<?php } ?>		
										<?php if(checarPermissao('vTaxasAssociados')){ ?>	
										<li class="<?php echo $this->taxa; ?>" ><a href="<?php echo base_url() ?>taxa/">Taxas Associados</a></li>										
										<?php } ?>
										<?php if(checarPermissao('vMaquinetaCartao')){ ?>
                                        <li class="<?php echo $this->maquinas; ?>" ><a href="<?php echo base_url() ?>maquinas/">Maquinas</a></li>
                                        <?php } ?>
									</ul>
								</li>	


								<?php if(checarPermissao('vFinanceiro')){ ?>		
									<li>
										<a href=""><i class="icon-coins"></i> <span>Financeiro</span></a>
										<ul>
											<li class="<?php echo $this->financeiro; ?>" ><a href="<?php echo base_url() ?>financeiro/">Contas</a></li>			
											<li class="<?php echo $this->baixaParcial; ?>" ><a href="<?php echo base_url() ?>financeiro/financeiroBaixarListar">Baixa Parcial</a></li>
										</ul>
										<ul>
											<li class="<?php echo $this->categoriafinanceiro; ?>"  ><a href="<?php echo base_url() ?>categoriafinanceiro/">Centro de Custo</a></li>			
										</ul>
									</li>
								<?php } ?>







								<!-- FIM MENU BOLETO -->								


								<li class="">
									<a href="#"><i class="icon-stack"></i><span>Relatórios</span></a>									
									<ul>								

										<?php if(checarPermissao('vCliente')){ ?>
										<li class="">										
											<a href="#" class="has-ul"><i class="icon-graduation"></i> Clientes</a>
											<ul class="hidden-ul"  >
												<li><a href="<?php echo base_url(); ?>relatorios/aniversariantes"><i class="icon-gift"></i> Aniversariantes</a></li>									
												
												<li><a href="<?php echo base_url(); ?>relatorios/clienteAtivoInativo"><i class="icon-wrench"></i> Ativo/Inativo</a></li>
											</ul>
										</li>
										<?php } ?>

										<?php if(checarPermissao('vDepartamento')){ ?>
										<li class="">										
											<a href="#" class="has-ul"><i class="icon-cash3"></i> Departamentos</a>
											<ul class="hidden-ul"  >
												<li><a href="<?php echo base_url(); ?>relatorios/departamentos"><i class="icon-clippy"></i> Departamentos </a></li>									
												
												<li><a href="#"><i class="icon-wrench"></i> ....</a></li>
											</ul>
										</li>
										<?php } ?>
										
										<?php if(checarPermissao('vDenuncia')){ ?>
										<li class="">
											<a href="#" class="has-ul"><i class="icon-chart"></i> Denúncia</a>
											<ul class="hidden-ul">		

												<li><a href="<?php echo base_url(); ?>relatorios/subdepartamentos"><i class="icon-briefcase"></i> Denúncias </a></li>								

											</ul>
										</li>
										<?php } ?>									

										<?php if(checarPermissao('vCategoria')){ ?>
										<li>
											<a href="#"><i class="icon-office"></i> Empresa</a>
											<ul class="hidden-ul">

												<li><a href="<?php echo base_url(); ?>relatorios/categoria"><i class="icon-briefcase"></i> Categorias</a></li>												

											</ul>
										</li>
										<?php } ?>	
																											
										<?php if(checarPermissao('cPermissoes')){ ?>
										<li><a href="<?php echo base_url(); ?>pagamentosistema/"><i class="icon-coins"></i>Pagamento do Sistema</a></li>																	
										<?php } ?>

										<?php if(checarPermissao('cPermissoes')){ ?>
										<li><a href="<?= base_url(); ?>relatorios/despesaReceita"><i class="icon-coins"></i>Despesas X Receitas</a></li>																	
										<li><a href="<?= base_url(); ?>relatorios/transacoesBoleto"><i class="icon-barcode2"></i>Transações - Boleto</a></li>
										<li><a href="<?= base_url(); ?>relatorios/transacoesCartao"><i class="icon-barcode2"></i>Transações - Cartão</a></li>		
										<?php } ?>

									</ul>
								</li>	


								<li>
									<a href="#"><i class="icon-gear"></i> <span>Configurações</span></a>
									<ul>
										<?php if(checarPermissao('vUsuarios')){ ?>
											<li><a href="<?php echo base_url(); ?>usuario">Usuários</a></li>
										<?php } ?>
										<?php if(checarPermissao('vPermissoes')){ ?>
										<li><a href="<?php echo base_url(); ?>permissao">Permissões</a></li>	
										<?php } ?>												
										<?php if(checarPermissao('cEmitente')){ ?>
										<li><a href="<?php echo base_url(); ?>Auth/emitente">Emitente</a></li>
										<?php } ?>	
										<?php if(checarPermissao('cSimulador')){ ?>
										<li><a href="<?php echo base_url(); ?>simulador/configurar">Taxas Simulador</a></li>	
										<?php } ?>								
									</ul>
								</li>
													
							</ul>
						</div>
					</div>
					<!-- /main navigation -->

				</div>
			</div>
			<!-- /main sidebar -->
			<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold"><?php echo ucfirst($this->uri->segment(1)); ?></span>  <?php echo ucfirst($this->uri->segment(2)); ?></h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Estatísticas</span></a>								
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="<?php echo base_url(); ?>Dashboard"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="<?php echo base_url($this->uri->segment(1)); ?>"><?php echo ucfirst($this->uri->segment(1)); ?></a></li>
							<li class="active"><?php echo ucfirst($this->uri->segment(2)); ?></li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>	
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">