<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Taxa</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados do Taxa:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

	      			<div class="form-group">						
						<label class="control-label col-lg-2">Nome Associado:</label>
						<div class="col-lg-5">
							<select class="select-search" name="taxa_usuario_id" id="associado">
								<!-- <optgroup label="Mountain Time Zone"> -->
								    <option value=""></option>
								<?php foreach ($usuario as $valor){ ?>
			                  		<option value="<?php echo $valor->usuario_id; ?>"><?php echo $valor->usuario_nome;?></option>
			            	    <?php } ?>
								<!-- </optgroup> -->
							</select>
						</div>  
					</div>


					<div class="form-group">						
						<label class="control-label col-lg-2">Bandeira Cartão:</label>
						<div class="col-lg-5">
						<select multiple="multiple" class="select" name="taxa_cartao_bandeira[]" id="bandeira">
							<!-- <optgroup label="Rede Card"> -->
						<!-- 	<?php foreach ($bandeira as $valor){ ?>
			                  <option value="<?php echo $valor->categoria_cart_id; ?>"><?php echo $valor->categoria_cart_nome_credenciadora.' - '.$valor->categoria_cart_descricao ;?></option>
			                <?php } ?> -->
			                <!-- </optgroup> -->
						</select>
						</div>
					</div>

		
		

			    <div class="card mb-4 mb-lg-0">
		          <div class="card-body">
		            <p><strong>Bandeiras</strong></p>
		          <!--   <img class="me-2" width="45px"
		              src="<?php echo base_url();?>public/assets/images/cart/visa.svg"
		              alt="Visa" /> -->
		            <img class="me-2" width="400px"
		              src="<?php echo base_url();?>public/assets/images/cart/aceitacao_cartao.png"
		              alt="Cartoes" />
		       
		          </div>
		        </div>                		

				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
