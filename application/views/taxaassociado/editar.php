
				<!-- 2 columns form -->				
				<form action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/editarExe" class="form-horizontal" method="post">	
					<div class="panel panel-flat">
						<div class="panel-heading">
					    	<h5 class="panel-title">Ficha taxa Associados</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<!-- <li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li> -->
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<div class="row">
								<div class="col-md-6">
									<fieldset>
									   <legend class="text-semibold"><i class="icon-reading position-left"></i> Associados detalhes</legend>

									   <!-- <input  type="hidden" class="form-control" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" /> -->
								       <input  type="hidden" class="form-control" name="taxa_id" value="<?php echo $dados[0]->taxa_id; ?>" />

										<div class="form-group">
										   <label class="col-lg-3 control-label">Nome Associado:</label>
											<div class="col-lg-9">
											    <input disabled type="text" class="form-control" placeholder="Associado"  value="<?php echo $dados[0]->usuario_nome; ?>">
											    <?php echo form_error('usuario_nome'); ?>
											</div>
										</div>

										<div class="form-group">
										    <label class="col-lg-3 control-label">Bandeira Cartão:</label>
											<div class="col-lg-9">
												<select disabled multiple="multiple" class="select" >
													<!-- <optgroup label="Rede Card"> -->		            
												<?php 
												foreach($bandeira as $key => $b) { $select = "";                    
												foreach($taxaBand as $bandeira){                                
													if($b->categoria_cart_id == $bandeira){
														$select = "selected";
													}
													}
													echo "<option value='".$b->categoria_cart_id."'".$select.">".$b->categoria_cart_nome_credenciadora.' - '.$b->categoria_cart_descricao."</option>";
												}?> 
													<!-- </optgroup> -->
												</select>
									    	</div>
										</div>



									</fieldset>
								</div>


								<div class="col-md-6">
								<legend class="text-semibold"><i class="icon-stats-growth position-left"></i> Taxas</legend>
									<div class="card">
										<div class="card-body">
											<div class="tabbable">
												<ul class="nav nav-tabs nav-tabs-highlight nav-justified">
													<li class="active"><a href="#highlighted-justified-tab1" data-toggle="tab">Juros</a></li>
													<li><a href="#highlighted-justified-tab2" data-toggle="tab">Banco</a></li>

												</ul>
											</div>

											<div class="tab-content">
         						 				<div class="tab-pane active" id="highlighted-justified-tab1" >
												    <div class="form-group">
												    	<label class="control-label col-lg-2">PIX:</label>
														<div class="col-lg-4">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_pix" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_pix; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-2">Débito:</label>
														<div class="col-lg-4">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_debito" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_debito; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-lg-1">1x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_01" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_01; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">2x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_02" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_02; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">3x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_03" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_03; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-lg-1">4x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_04" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_04; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">5x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_05" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_05; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">6x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_06" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_06; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-lg-1">7x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_07" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_07; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">8x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_08" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_08; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">9x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_09" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_09; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-lg-1">10x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_10" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_10; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">11x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_11" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_11; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">12x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_12" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_12; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
													</div>

													<div class="form-group">
														<label class="control-label col-lg-1">13x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_13" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_13; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">14x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_14" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_14; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">15x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_15" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_15; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
													</div>


													<div class="form-group">
														<label class="control-label col-lg-1">16x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_16" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_16; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">17:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_17" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_17; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-1">18x:</label>
														<div class="col-lg-3">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_cartao_18" placeholder="Valor" value="<?php echo $dados[0]->taxa_cartao_18; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
													</div>
												</div>

												<div class="tab-pane" id="highlighted-justified-tab2">
													<div class="form-group">
														<label class="control-label col-lg-2">PIX:</label>
														<div class="col-lg-4">
															<div class="input-group">
																<input type="text" class="form-control money" name="taxa_banco_pix" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_pix; ?>">
																<span class="input-group-addon">%</span>
															</div>
														</div>
														<label class="control-label col-lg-2">Débito:</label>
														<div class="col-lg-4">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_debito" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_debito; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-lg-1">1x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_01" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_01; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">2x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_02" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_02; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">3x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_03" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_03; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-lg-1">4x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_04" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_04; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">5x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_05" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_05; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">6x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_06" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_06; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
														</div>


														<div class="form-group">
															<label class="control-label col-lg-1">7x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_07" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_07; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">8x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_08" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_08; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">9x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_09" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_09; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-lg-1">10x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_10" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_10; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">11x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_11" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_11; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">12x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_12" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_12; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
														</div>

														<div class="form-group">
															<label class="control-label col-lg-1">13x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_13" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_13; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">14x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_14" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_14; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">15x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_15" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_15; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
														</div>


														<div class="form-group">
															<label class="control-label col-lg-1">16x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_16" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_16; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">17:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_17" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_17; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
															<label class="control-label col-lg-1">18x:</label>
															<div class="col-lg-3">
																<div class="input-group">
																	<input type="text" class="form-control money" name="taxa_banco_18" placeholder="Valor" value="<?php echo $dados[0]->taxa_banco_18; ?>">
																	<span class="input-group-addon">%</span>
																</div>
															</div>
														</div>
													</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="text-right">
								<button type="submit" class="btn bg-teal">Salvar <i class="icon-arrow-right14 position-right"></i></button>
							</div>
						</div>
					</div>
				</form>
				<!-- /2 columns form -->
		


		<script type="text/javascript">

    $(document).ready(function(){

        $(".money").maskMoney();

		// Quando uma guia/tab é clicada
		$('.nav-link').click(function() {
			// Remove a classe "active" de todas as guias
			$('.nav-link').removeClass('active');

			// Adiciona a classe "active" à guia/tab clicada
			$(this).addClass('active');

			// Obtém o ID da guia/tab clicada
			var tabId = $(this).attr('href');

			// Oculta todas as abas
			$('.tab-pane').removeClass('show active');

			// Exibe a aba com o ID correspondente
			$(tabId).addClass('show active');
		});

  	});
	

    </script>