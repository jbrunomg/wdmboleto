<!-- Horizontal form options -->
<div class="col-md-12 row">

<!-- Basic layout-->
<form action="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/editarExe" class="form-horizontal" method="post">
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Editar Permissão</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<li><a data-action="close"></a></li>
            	</ul>
        	</div>
		</div>
		
		

		<div class="panel-body">
		<div class="col-md-6">
			<div class="form-group">
				<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />
				<label class="col-lg-3 control-label">Descrição:</label>

				<input type="hidden" name="id" value="<?php echo $dados[0]->permissao_id;?>" />
				<label class="col-lg-3 control-label">Descrição:</label>
				<div class="col-lg-9">
					<input type="text" class="form-control" name="descricao" placeholder="Descrição da Permissão" value="<?php  echo $dados[0]->permissao_nome; ?>">
				</div>
			</div>
			<div class="text-right">
				<button type="submit" class="btn bg-teal">Editar <i class="icon-arrow-right14 position-right"></i></button>
			</div>
		</div>
		</div>
	</div>
</form>
<!-- /basic layout -->
</div>