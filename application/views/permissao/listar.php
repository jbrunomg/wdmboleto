<!-- Table with togglable columns -->

<div class="panel panel-flat">

	<div class="panel-heading">			
			<?php if(checarPermissao('aPermissoes')){ ?>
			<div class="col-md-3 col-sm-6 row">
				<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
			</div>
			<?php } ?>
		<!-- <h5 class="panel-title">Listagem de Perfis</h5>	 -->													
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>
	<br>
	

	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>
				<th data-hide="phone">#</th>
				<th data-toggle="true">Descrição</th>
				<th data-hide="phone,tablet">Situação</th>
				<th data-hide="phone,tablet">Data Alteração</th>
				<th data-toggle="true">Ações</th>									
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { 

				$ativo = ($valor->permissao_situacao)? 'Ativo':'Inativo'; 
				$class = ($valor->permissao_situacao)? 'class="label label-success"':'class="label label-danger"'; 
			?>
				<tr>
					<td><?php echo $valor->permissao_id; ?></td>
					<td><?php echo $valor->permissao_nome; ?></td>
					<td><span <?php echo $class ?>><?php echo $ativo ?></span></td>
					<td><?php echo date('d-m-Y',strtotime($valor->permissao_atualizacao)); ?></td>
					<td>
						<ul class="icons-list">
								
								<?php if(checarPermissao('ePermissoes')){ ?>
								<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->permissao_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('dPermissoes')){ ?>
								<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->permissao_id; ?>"><i class="icon-trash"></i></a></li>
								<?php } ?>
								<?php if(checarPermissao('cPermissoes')){ ?>						
								<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/configurar/<?php echo $valor->permissao_id; ?>" data-popup="tooltip" title="Configurar Permissões"><i class="icon-cog7"></i></a></li>
								<?php } ?>
								
						
						</ul>
					</td>															
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /table with togglable columns -->