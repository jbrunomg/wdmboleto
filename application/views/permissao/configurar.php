<?php $p = unserialize($dados[0]->permissao_permissoes);?>

<!-- Table with togglable columns -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Configurar permissão <?php echo $dados[0]->permissao_nome; ?></h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<form action="<?php echo base_url(); ?>permissao/configurarExe/" method="post">

	<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

	<input type="hidden" value="<?php echo $dados[0]->permissao_id; ?>" name="id">
	<table class="table table-togglable table-hover">
		<thead>
			<tr>
				<th data-toggle="true">Atividade</th>
				<th data-hide="phone,tablet">Visualizar</th>
				<th data-hide="phone,tablet">Adicionar</th>
				<th data-hide="phone,tablet">Editar</th>
				<th data-hide="phone">Excluir</th>	
				<th data-hide="phone">Outros</th>									
			</tr>
		</thead>
		<tbody>	


	<tr>
		<td>Clientes</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vClientes" class="control-success" <?php echo (in_array('vClientes',$p))? 'checked="checked"': '' ?>>
					Visualizar Clientes
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aClientes" class="control-success" <?php echo (in_array('aClientes',$p))? 'checked="checked"': '' ?>>
					Adicionar Clientes
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eClientes" class="control-success" <?php echo (in_array('eClientes',$p))? 'checked="checked"': '' ?>>
					Editar Clientes
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dClientes" class="control-success" <?php echo (in_array('dClientes',$p))? 'checked="checked"': '' ?>>
					Excluir Clientes
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

 	<tr>
		<td>OperacaoBoleto</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vOperacaoBoleto" class="control-success" <?php echo (in_array('vOperacaoBoleto',$p))? 'checked="checked"': '' ?>>
					Visualizar OperacaoBoleto
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aOperacaoBoleto" class="control-success" <?php echo (in_array('aOperacaoBoleto',$p))? 'checked="checked"': '' ?>>
					Adicionar OperacaoBoleto
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eOperacaoBoleto" class="control-success" <?php echo (in_array('eOperacaoBoleto',$p))? 'checked="checked"': '' ?>>
					Editar OperacaoBoleto
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dOperacaoBoleto" class="control-success" <?php echo (in_array('dOperacaoBoleto',$p))? 'checked="checked"': '' ?>>
					Excluir OperacaoBoleto
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>
 

 	<tr>
		<td>OperacaoCartao</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vOperacaoCartao" class="control-success" <?php echo (in_array('vOperacaoCartao',$p))? 'checked="checked"': '' ?>>
					Visualizar OperacaoCartao
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aOperacaoCartao" class="control-success" <?php echo (in_array('aOperacaoCartao',$p))? 'checked="checked"': '' ?>>
					Adicionar OperacaoCartao
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eOperacaoCartao" class="control-success" <?php echo (in_array('eOperacaoCartao',$p))? 'checked="checked"': '' ?>>
					Editar OperacaoCartao
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dOperacaoCartao" class="control-success" <?php echo (in_array('dOperacaoCartao',$p))? 'checked="checked"': '' ?>>
					Excluir OperacaoCartao
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>MaquinetaCartao</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vMaquinetaCartao" class="control-success" <?php echo (in_array('vMaquinetaCartao',$p))? 'checked="checked"': '' ?>>
					Visualizar MaquinetaCartao
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aMaquinetaCartao" class="control-success" <?php echo (in_array('aMaquinetaCartao',$p))? 'checked="checked"': '' ?>>
					Adicionar MaquinetaCartao
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eMaquinetaCartao" class="control-success" <?php echo (in_array('eMaquinetaCartao',$p))? 'checked="checked"': '' ?>>
					Editar MaquinetaCartao
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dMaquinetaCartao" class="control-success" <?php echo (in_array('dMaquinetaCartao',$p))? 'checked="checked"': '' ?>>
					Excluir MaquinetaCartao
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	<tr>
		<td>Cartoes</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vCartoes" class="control-success" <?php echo (in_array('vCartoes',$p))? 'checked="checked"': '' ?>>
					Visualizar Cartoes
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aCartoes" class="control-success" <?php echo (in_array('aCartoes',$p))? 'checked="checked"': '' ?>>
					Adicionar Cartoes
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eCartoes" class="control-success" <?php echo (in_array('eCartoes',$p))? 'checked="checked"': '' ?>>
					Editar Cartoes
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dCartoes" class="control-success" <?php echo (in_array('dCartoes',$p))? 'checked="checked"': '' ?>>
					Excluir Cartoes
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

	

	<tr>
		<td>Taxas Associados</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vTaxasAssociados" class="control-success" <?php echo (in_array('vTaxasAssociados',$p))? 'checked="checked"': '' ?>>
					Visualizar Taxas Associados
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aTaxasAssociados" class="control-success" <?php echo (in_array('aTaxasAssociados',$p))? 'checked="checked"': '' ?>>
					Adicionar Taxas Associados
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eTaxasAssociados" class="control-success" <?php echo (in_array('eTaxasAssociados',$p))? 'checked="checked"': '' ?>>
					Editar Taxas Associados
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dTaxasAssociados" class="control-success" <?php echo (in_array('dTaxasAssociados',$p))? 'checked="checked"': '' ?>>
					Excluir Taxas Associados
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>



	<tr>
		<td>Associados</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vUsuarios" class="control-success" <?php echo (in_array('vUsuarios',$p))? 'checked="checked"': '' ?>>
					Visualizar Associados
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aUsuarios" class="control-success" <?php echo (in_array('aUsuarios',$p))? 'checked="checked"': '' ?>>
					Adicionar Associados
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eUsuarios" class="control-success" <?php echo (in_array('eUsuarios',$p))? 'checked="checked"': '' ?>>
					Editar Associados
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dUsuarios" class="control-success" <?php echo (in_array('dUsuarios',$p))? 'checked="checked"': '' ?>>
					Excluir Associados
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

<!-- 	<tr>
		<td>Arquivo</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vArquivo" class="control-success" <?php echo (in_array('vArquivo',$p))? 'checked="checked"': '' ?>>
					Visualizar Arquivo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aArquivo" class="control-success" <?php echo (in_array('aArquivo',$p))? 'checked="checked"': '' ?>>
					Adicionar Arquivo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eArquivo" class="control-success" <?php echo (in_array('eArquivo',$p))? 'checked="checked"': '' ?>>
					Editar Arquivo
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dArquivo" class="control-success" <?php echo (in_array('dArquivo',$p))? 'checked="checked"': '' ?>>
					Excluir Arquivo
				</label>
			</div>
		</td>	
		<td></td>													
	</tr> -->


	<tr>
		<td>Financeiro</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vFinanceiro" class="control-success" <?php echo (in_array('vFinanceiro',$p))? 'checked="checked"': '' ?>>
					Visualizar Financeiro
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aFinanceiro" class="control-success" <?php echo (in_array('aFinanceiro',$p))? 'checked="checked"': '' ?>>
					Adicionar Financeiro
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eFinanceiro" class="control-success" <?php echo (in_array('eFinanceiro',$p))? 'checked="checked"': '' ?>>
					Editar Financeiro
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dFinanceiro" class="control-success" <?php echo (in_array('dFinanceiro',$p))? 'checked="checked"': '' ?>>
					Excluir Financeiro
				</label>
			</div>
		</td>	
		<td></td>													
	</tr>

<!-- 	<tr>
		<td>Newsletter</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="vNewsletter" class="control-success" <?php echo (in_array('vNewsletter',$p))? 'checked="checked"': '' ?>>
					Visualizar Newsletter
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="aNewsletter" class="control-success" <?php echo (in_array('aNewsletter',$p))? 'checked="checked"': '' ?>>
					Adicionar Newsletter
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="eNewsletter" class="control-success" <?php echo (in_array('eNewsletter',$p))? 'checked="checked"': '' ?>>
					Editar Newsletter
				</label>
			</div>
		</td>
		<td>
			<div class="checkbox">
				<label>
					<input type="checkbox" name="permissao[]" value="dNewsletter" class="control-success" <?php echo (in_array('dNewsletter',$p))? 'checked="checked"': '' ?>>
					Excluir Newsletter
				</label>
			</div>
		</td>	
		<td></td>													
	</tr> -->
    


		

				<tr>
					<td>Permissões</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="vPermissoes" class="control-success" <?php echo (in_array('vPermissoes',$p))? 'checked="checked"': '' ?>>
								Visualizar Permissoes
							</label>
						</div>
					</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="aPermissoes" class="control-success" <?php echo (in_array('aPermissoes',$p))? 'checked="checked"': '' ?>>
								Adicionar Permissoes
							</label>
						</div>
					</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="ePermissoes" class="control-success" <?php echo (in_array('ePermissoes',$p))? 'checked="checked"': '' ?>>
								Editar Permissoes
							</label>
						</div>
					</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="dPermissoes" class="control-success" <?php echo (in_array('dPermissoes',$p))? 'checked="checked"': '' ?>>
								Excluir Permissoes
							</label>
						</div>
					</td>
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="cPermissoes" class="control-success" <?php echo (in_array('cPermissoes',$p))? 'checked="checked"': '' ?>>
								Configurar Permissoes
							</label>
						</div>
					</td>		
					<td></td>													
				</tr>


				<tr>
					<td>Configuração</td>
		
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="cEmitente" class="control-success" <?php echo (in_array('cEmitente',$p))? 'checked="checked"': '' ?>>
								Configurar Emitente
							</label>
						</div>
					</td>	
					
					<td>
						<div class="checkbox">
							<label>
								<input type="checkbox" name="permissao[]" value="cSimulador" class="control-success" <?php echo (in_array('cSimulador',$p))? 'checked="checked"': '' ?>>
								Configurar Simulador
							</label>
						</div>
					</td>

					<td></td>													
				</tr>
		
				<tr>
					<td colspan="6">
						<div class="text-right">
							<button type="submit" class="btn bg-teal">Configurar<i class="icon-arrow-right14 position-right"></i></button>
						</div>
					</td>
				</tr>
		
		</tbody>			
	</table>		
	</form>
</div>
<!-- /table with togglable columns -->