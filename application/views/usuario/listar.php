<!-- Column selectors -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aUsuarios')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>
				<th>#</th>
				<th>Nome</th>
				<th>E-mail</th>
				<th>Telefone</th>							
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach ($dados as $valor) { ?>
				<?php $foto = ($valor->usuario_imagem != '')?$valor->usuario_imagem:'assets/themes/assets/images/placeholder.jpg' ?>
				<tr>
					<td>
						<div class="media-left media-middle">					
							<img class="img-circle" src="<?php echo base_url().$foto; ?>" alt="">
						</div>
					</td>
					<td><?php echo $valor->usuario_nome; ?></td>
					<td><?php echo $valor->usuario_email; ?></td>
					<td><?php echo $valor->usuario_telefone; ?></td>
														
					<td>																				
							<ul class="icons-list">
									<?php if(checarPermissao('eUsuarios')){ ?>
									<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->usuario_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
									<?php } ?>
									<?php if(checarPermissao('dUsuarios')){ ?>
									<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->usuario_id; ?>"><i class="icon-trash"></i></a></li>
									<?php } ?>
									<?php if(checarPermissao('vUsuarios')){ ?>
									<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->usuario_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
									<?php } ?>																				
								
							</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors -->