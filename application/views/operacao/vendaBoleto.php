<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">
<form action="#" method="post" id="form_submit">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-8">
                    <div class="card">
                        <legend class="text-semibold"><i class="icon-barcode2 position-left"></i>Dados do Boleto</legend>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-6" style="display: none">
                                    <label for="codigo_boleto">Código do Boleto <span style="color: red;">(DATA DO BOLETO)</span></label>
                                    <input type="hidden" name="codigo_boleto" id="codigo_boleto" class="form-control form-control-sm" disabled="" value="BOLETO DE SIMULAÇÃO">
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="valor">Valor (R$)</label>
                                    <input type="text" name="valor"  autocomplete="off" id="valor" class="form-control form-control-sm money" placeholder="0,00">
                                    <span class="erroValor" style="color:red; display:none;">Valor em Branco!</span>
                                </div>
                                <div class="form-group col-md-3">
                                   <label for="cartao_bandeira">Cartão / Bandeira</label>
                                   <select name="cartao_bandeira" autocomplete="off" id="cartao_bandeira" class="form-control">
                                        <option value="">Selecione</option>
                                        <?php foreach ($cartao as $fo) { ?>
                                            <option value="<?php echo $fo->categoria_cart_id; ?>"><?php echo $fo->categoria_cart_nome_credenciadora.' - '.$fo->categoria_cart_descricao; ?></option>
                                        <?php  } ?>  
                                    </select>
                                    <span class="erroCartao" style="color:red; display:none;">Cartão em Branco!</span>
                                </div>
                                <div class="col-md-3">
                                    <label for="numero_parcelas">Número de Parcelas</label>
                                    <select name="numero_parcelas" autocomplete="off" id="numero_parcelas" class="form-control">
                                        <option value="">Selecione</option>
                                        <option value="debito">Débito</option>
                                        <option value="01">1x</option>
                                        <option value="02">2x</option>
                                        <option value="03">3x</option>
                                        <option value="04">4x</option>
                                        <option value="05">5x</option>
                                        <option value="06">6x</option>
                                        <option value="07">7x</option>
                                        <option value="08">8x</option>
                                        <option value="09">9x</option>
                                        <option value="10">10x</option>
                                        <option value="11">11x</option>
                                        <option value="12">12x</option>
                                        <option value="13">13x</option>
                                        <option value="14">14x</option>
                                        <option value="15">15x</option>
                                        <option value="16">16x</option>
                                        <option value="17">17x</option>
                                        <option value="18">18x</option>
                                    </select>
                                    <span class="erroParcelar" style="color:red; display:none;">Parcela em Branco!</span>
                                </div>
                                <div class="col-md-2">
                                    <label for="valor_parcelado">Valor Parcelado </label>
                                    <input type="text" name="valor_parcelado" autocomplete="off" id="valor_parcelado" class="form-control form-control-sm money" placeholder="0,00" disabled>
                                </div>
                                <div class="col-md-2" style="display: none">
                                    <label for="valor_parcela">Valor Parcela</label>
                                    <input type="text" name="valor_parcela" autocomplete="off" id="valor_parcela" class="form-control form-control-sm money" placeholder="0,00" disabled>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" id="btn_adicionar_boleto" class="btn btn-sm btn-block btn-info" style="margin-top: 27px;" onclick="adicionar_boleto()">Adicionar</button>
                                </div>

                            </div>
                            <div class="row">
                                
                            </div>
                            <hr>
                            <div class="row" style="overflow-x: auto;">
                                <div class="form-group col-md-12">
                                    <table id="table_boletos" class="table table-hover table-bordered table-sm">
                                        <thead>
                                            <tr>
                                                <th width="40%">Código Boleto</th>
                                                <th>N. Parcelas</th>
                                                <th>Cartão / Bandeira</th>
                                                <th width="15%">Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row center">
                                <div class="form-group col-md-10 offset-md-4" style="text-align: left;">
                                    <span class="lbl_tt_transacao">Total da Transação</span>
                                    <br>
                                    <span style="font-size: 20px; font-weight: bold;">R$ </span><span style="font-size: 35px; font-weight: 700;" id="total_transacao">0,00</span>
                                </div>
                                <div class="form-group col-md-2" style="text-align: right;">
                                    <span style="font-size: 12px;">Total dos Boletos</span>
                                    <br>
                                    <span style="font-size: 10px;">R$ </span><span style="font-size: 20px;" id="total_boletos">0,00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card">
                        <legend class="text-semibold"><i class="icon-coins position-left"></i> Pagamento</legend>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-8">
                                    <label for="autorizacao">Código de Autorização</label>
                                    <input type="text" name="autorizacao" id="autorizacao" class="form-control form-control-sm" maxlength="6">
                                </div>
                                <div class="form-group col-md-4">
                                    <button type="button" id="btn_adicionar_autorizacao" class="btn btn-sm btn-block btn-info" style="margin-top: 27px;" onclick="adicionar_autorizacao()">Adicionar</button>
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="overflow-x: auto;">
                                <div class="form-group col-md-12">
                                    <table id="table_autorizacao" class="table table-sm table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Código Autorização</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="text-center">
                         <button type="button" id="btn_salvar" class="btn btn-sm btn-block btn-primary" onclick="salvar()">Continuar Venda</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://plentz.github.io/jquery-maskmoney/javascripts/jquery.maskMoney.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){

    $(".money").maskMoney();

});

$('#cartao_bandeira').change(function(){
   $('#numero_parcelas').val(''); 
   $('.erroCartao').hide(); 
   $('#btn_adicionar_boleto').prop('disabled', false);
});

$('#valor').change(function(){
   if($('#numero_parcelas').prop('disabled')){
     adicionarValores();
   }
   $('.erroValor').hide(); 
   $('#btn_adicionar_boleto').prop('disabled', false);
});

$('#numero_parcelas').change(function(){
    adicionarValores();
});    


function adicionarValores() {
   var parcela = $('#numero_parcelas').val();
   var cartao = $('#cartao_bandeira').val(); 
   var valor = $('#valor').val().replace(',',''); 
   var porcentagem = consultaParcela(parcela, cartao);
   var valorJuros = (valor * porcentagem) / 100;
   var valorCalculado = (parseFloat(valor) + parseFloat(valorJuros));
   var parcelaVP = parcela === 'debito' ? '1' : parcela;
   var valorParcelado = (parseFloat(valorCalculado) / parcelaVP);
   valorParcelado = parseFloat(valorParcelado).toFixed(2);
 
   if(!cartao){

        $('.erroCartao').show(); 
        $('#btn_adicionar_boleto').prop('disabled', true);
    
   }else if(!valor){

        $('.erroValor').show(); 
        $('#btn_adicionar_boleto').prop('disabled', true);

   } else{

        let dados = {
            simulacao_bandeira_cartao: cartao,
            simulacao_valor: valor,
            simulacao_numero_parcela: parcela,
            simulacao_valor_parcela: valorCalculado.toFixed(2)
        };
        let response = requisicao("/operacao/salvaSimulacao", dados);

        if(response){
            $('#valor_parcela').val(parseFloat(valorCalculado.toFixed(2)).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })); 
            $('#total_transacao').html(parseFloat(valorCalculado.toFixed(2)).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })); 
            $('#total_boletos').html(parseFloat(valor).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 }));
            $('#valor_parcelado').val(parseFloat(valorParcelado).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })); 
        }else{
            swal('Erro!', 'Não foi possivel salvar a simulação.', 'error');
        }

   }
    
}

function consultaParcela(parcela, cartao) {
    var result="";
    $.ajax({
        method: "POST",
        url: base_url+"operacao/consultaParcela/",
        dataType: "JSON",
        data: { parcela: parcela, cartao: cartao},
        async: false,
        success:function(data) {
            result = data; 
        }
    });

    return result;
}

function adicionar_boleto(){
    let codigo_boleto = $("#codigo_boleto").val();
    let valor = $("#valor").val().replace(',','');
    var cartao = $('#cartao_bandeira').val(); 
    var parcela = $('#numero_parcelas').val();
    var valor_transacao = $("#total_transacao").html();
    var cartaoNome = consultaCartao(cartao);
    var bandeiraNome = consultaBandeira(cartao);

    if(!(codigo_boleto))
    {
        swal('Aviso!', 'Código do boleto deve ser informado.', 'info');
        return false;
    }

    if(!(valor))
    {
        swal('Aviso!', 'Valor deve ser informado.', 'info');
        return false;
    }

    if(!(cartao))
    {
        swal('Aviso!', 'Cartão deve ser informado.', 'info');
        return false;
    }

    if(!(parcela))
    {
        swal('Aviso!', 'Parcela deve ser informada.', 'info');
        return false;
    }
    let body = "";
    parcelaNome = parcela;
    parcela = parcela == "debito" ? '0' : parcela;

    body += "<tr>";
        body += "<td>"+codigo_boleto+"</td>";
        body += "<td>"+parcelaNome+"</td>";
        body += "<td>"+cartaoNome+" - "+bandeiraNome+"</td>";
        body += "<td dataPacela="+parcela+" dataCartao="+cartao+" dataValorT="+valor_transacao+">"+parseFloat(valor).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })+"</td>";
        body += "<td><button type='button' id='btn_remover_boleto' class='btn btn-sm btn-danger' onclick='drop_row_boletos(this.parentNode.parentNode.rowIndex)'>Remover</button></td>";
    body += "</tr>";

    $("#table_boletos tbody").append(body);
    calcular_juros();
    $("#valor").val("").focus();
    $('#numero_parcelas').val(parcelaNome).prop('disabled', true); 
    $('#valor_parcela').val(''); 
    $('#valor_parcelado').val(''); 
    $('#cartao_bandeira').val(cartao).prop('disabled', true); 
    $("#codigo_boleto").val("BOLETO DE SIMULAÇÃO");
}

function consultaCartao(cartao) {
    var result="";
    $.ajax({
        method: "POST",
        url: base_url+"operacao/consultaCartao/",
        dataType: "JSON",
        data: {cartao: cartao},
        async: false,
        success:function(data) {
            result = data; 
        }
    });

    return result;
}

function consultaBandeira(cartao) {
    var result="";
    $.ajax({
        method: "POST",
        url: base_url+"operacao/consultaBandeira/",
        dataType: "JSON",
        data: {cartao: cartao},
        async: false,
        success:function(data) {
            result = data; 
        }
    });

    return result;
}

function drop_row_boletos(row){
    swal({ 
            title: "Aviso!",
            text: "Você realmente deseja excluir esse registro da tabela?", 
            type: "warning", 
            showCancelButton: true, 
            cancelButtonText: "Não",
            confirmButtonClass: "btn-danger", 
            confirmButtonText: "Sim",
            closeOnConfirm: false 
        }, 
        function(){
            document.getElementById('table_boletos').deleteRow(row);
            calcular_juros();
            swal("Aviso!", "Registro excluido com sucesso.", "success");
        });

}

function adicionar_autorizacao(){
    let autorizacao = $("#autorizacao").val();

    if(!autorizacao)
    {
        swal('Aviso!', 'Você deve informar o código de autorização.', 'info');
        return false;
    }

    let body = "";

    body += "<tr>";
        body += "<td>"+autorizacao+"</td>";
        body += "<td><button type='button' id='btn_remover_autorizacao' class='btn btn-sm btn-danger' onclick='drop_row_autorizacao(this.parentNode.parentNode.rowIndex)'>Remover</button></td>";
    body += "</tr>";

    $("#table_autorizacao tbody").append(body);

    $("#autorizacao").val("").focus();
}

function drop_row_autorizacao(row){
    document.getElementById('table_autorizacao').deleteRow(row);
}

function salvar(){
    let total_transacao = $("#total_transacao").html().replace('.','').replace(',','.');
    let total_boletos = $("#total_boletos").html().replace('.','').replace(',','.');
    let cartoes_bandeira = new Array();
    let parcelas = new Array();
    let boletos = new Array();
    let autorizacoes = new Array();
    let index = 0;

    $("#table_boletos tbody tr").map(function(){
        parcelas[index] = {parcelas: $(this).find('td').eq(1).text()};
        cartoes_bandeira[index] = {cartao_bandeira: $(this).find('td').eq(2).text()};
        index++;
    });

    index = 0;

    $("#table_boletos tbody tr").map(function(){
        boletos[index] = {codigo_boleto: $(this).find('td').eq(0).text(),
                          parcela: $(this).find('td').eq(1).text(),
                          cartao_bandeira: $(this).find('td').eq(2).text(),
                          valor: $(this).find('td').eq(3).text().replace('.','').replace(',','.'),
                          valor_tr : $(this).find('td').eq('3').attr('dataValorT').replace('.','').replace(',','.')};

        index++;
    });

    index = 0;

    $("#table_autorizacao tbody tr").map(function(){
        autorizacoes[index] = {autorizacao: $(this).find('td').eq(0).text()};
        index++;
    });

    if(boletos.length == 0 || autorizacoes.length == 0)
    {
        swal('Aviso!', 'Todos os campos devem ser preenchidos.', 'info');
        return false;
    }

    // var contator = verificarCartao(cartoes_bandeira, parcelas);
    // if(!(autorizacoes.length >= parseInt(contator)))
    // {
    //     swal('Aviso!', 'Precisamos de mais autorização!', 'info');
    //     return false;
    // }

    let dados = {
        boletos: boletos,
        autorizacoes: autorizacoes,
        total_transacao: total_transacao,
        total_boletos: total_boletos,
    };

    let response = requisicao("/operacao/salvaVendaBoleto", dados);
    if(response){
        swal('Número da Simulação #'+response+'', 'Simulação salva com sucesso!', 'success');
        $("#id_venda").val(response);
        window.location.href = base_url+"operacao/vendaBoletoConfirmacao/"+response+"";
    }else{
        swal('Erro!', 'Não foi possivel salvar a venda.', 'error');
    }
}

function requisicao(url, dados = ""){
    var data = $.ajax({
        url: base_url+url,
        async: false,
        type: 'post',
        datatype: 'json',
        data: dados,
        success: function(data)
        {
          data = JSON.parse(data);
        }
    });

    return JSON.parse(data.responseText);
}

function calcular_juros()
{
    var val_table = total_boletos = 0;
    var total_transacao = 0;

    $("#table_boletos tbody tr").map(function(dados){
        val_table = $(this).find('td').eq('3').text().replace('.','').replace(',','.');
        val_parcela = $(this).find('td').eq('3').attr('dataPacela');
        val_cartao = $(this).find('td').eq('3').attr('dataCartao');
        val_parcela = val_parcela == '0' ? 'debito' : val_parcela;
        porcentagem = consultaParcela(val_parcela, val_cartao);
        val_table = parseFloat(val_table);
        total_boletos = total_boletos + val_table;
        total_transacao = total_transacao + val_table + ((val_table * porcentagem) / 100);
    });

    if (total_boletos <= 0) {
        $('#numero_parcelas').val('').prop('disabled', false); 
        $('#cartao_bandeira').val('').prop('disabled', false);    
    }

    $("#total_boletos").html(parseFloat(total_boletos.toFixed(2)).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 }));
    $("#total_transacao").html(parseFloat(total_transacao.toFixed(2)).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 }));
}


function verificarCartao(cartoes, parcelas)
{
    let cartao = 0;
    for (let i = 0; i < cartoes.length; i++) {
    //   if ((cartoes[0].cartao_bandeira != cartoes[i].cartao_bandeira) || (parcelas[0].parcelas != parcelas[i].parcelas)) {
      if ((cartoes[0].cartao_bandeira != cartoes[i].cartao_bandeira) || (parcelas[0].parcelas == parcelas[i].parcelas)) {
         cartao++; 
      }
    }
    return cartao;
}


</script>


