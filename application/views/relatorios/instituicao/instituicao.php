<div class="panel panel-flat col-md-3">
<div class="panel-heading">
	<h5 class="panel-title text-center">Relatórios Rápidos</h5>							
</div>
	<div class="category-content">
		<div class="row row-condensed">
			<div class="col-xs-12">
				<a href="<?php echo base_url();?>relatorios/instituicaoRapido" class="btn bg-teal-400 btn-block btn-float btn-float-lg" ><i class="icon-file-text2"></i> <span>Total Instituicao com alunos ativos</span></a>
			</div>
		</div>
	</div>
</div>
<!-- Form horizontal -->

<div class="col-md-9">
<div class="panel panel-flat col-md-12">
<div class="panel-heading">
	<h5 class="panel-title text-center">Relatórios Personalizados</h5>							
</div>
	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?>relatorios/instituicaoPersonalizado/" method="post">
			
			<div class="col-md-12">
	            <div class="panel panel-body border-top-teal">
		            <div class="text-center">
		                <h6 class="no-margin text-semibold">Mês/Ano:</h6>                
		            </div>              
		            <div class="text-center">
		                <input  type="Month" class="form-control" placeholder="mes_ano" name="mes_ano" id="mes_ano" required value="<?php echo set_value('mes_ano'); ?>">
		                  <?php echo form_error('mes_ano'); ?>                
		            </div>           
		        </div>
		    </div>

	 		<div class="col-md-12">
	           		<label class="control-label col-lg-2">Instituição:</label>
	            <div class="col-lg-5">
	                <select class="form-control" name="instituicao" id="instituicao">
	                    <option value="">Selecione</option>
	                      <?php foreach ($instituicao as $valor) { ?>
	                          <option value="<?php echo $valor->pensencodig; ?>"><?php echo mb_strtoupper($valor->sensennome); ?></option>
	                    <?php } ?>                     
	                </select>
	            </div>
	        </div> 

			<div class="text-right">
				<button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
			</div>
		</form>
	</div>
</div>
</div>