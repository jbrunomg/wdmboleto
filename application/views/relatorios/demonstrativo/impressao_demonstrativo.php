<?php 

$mes = date('m'); // Mês desejado, pode ser por ser obtido por POST, GET, etc.
$ano = date('Y'); // Ano atual
$ultimo_dia = date('t', mktime(0,0,0,$mes,'01',$ano)); // Mágica, plim!

$lastmes = date('m', strtotime('-1 months'));


setlocale(LC_ALL, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');

?>

<table class="table">	
	<tbody>		
		<tr>	
			<td style="font-size:12px;">
				<?php 
				    echo $emitente->emitente_nome."<br>";
					echo "Fone  : ".$emitente->emitente_telefone."<br>";
					echo "E-mail: ".$emitente->emitente_email."<br>";								
				?>
			</td>	

			<td class="text-right"><img src="<?php echo $emitente->emitente_imagem;?>" width="100px" heigth="100px"></td>
			
		</tr>		
	</tbody>
</table>
<hr style="margin-top:-10px">
<br/>
<?php echo 'Recife, '. utf8_encode(strftime(' %d, de %B de %Y')) ?>
<br/>
<br/>
<h5 class="text-center" style="margin-top:-10px"><?php echo $titulo; ?></h5>		
	<br/>
	<br/>	
	<div class="panel-body">		
		Discriminamos, através deste, o valor de <code><?php echo 'R$ '. number_format((end($estudante)->SomaTotal - end($estorno)->estornototal), 2, ".", ""); ?></code> ( <?php echo extenso((end($estudante)->SomaTotal - end($estorno)->estornototal)) ?> ) recorrente à Empresa: <code><?php echo $contrato[0]->sempemrazao; ?></code>  referente à taxa alusiva aos estagiários relacionados abaixo:	     
	</div>
<table class="table table-bordered">
	<thead>
		<tr>
			<th style="padding: 5px;fonte-size:12" class="text-center">Código</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Estagiário</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Periodo</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Valor</th>
			<th style="padding: 5px;fonte-size:12" class="text-center">Término</th>	
		</tr>
	</thead>
		<tbody>

			<?php
				$contador = 1;
				$cobrado = 0;
				$ativo = 0; 
				$totalServico = 0;

			foreach ($contrato as $valor) { 

				if ($valor->contrestempr_status == 'Ativo') {						
				$ativo++;							
				} else {
				$cobrado++;						
				}

				$totalServico = $totalServico + $estudante[$valor->palualcodig]->valorTotal;

				?>

				<?php if ($valor->palualcodig = $estudante[$valor->palualcodig]->palualcodig ) { ?>
				<tr>							
					<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $contador++; ?></td> <!-- Código -->
					<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo $valor->salualnome; ?></td> <!-- Estagiário -->
					<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo date('d/m/y', strtotime($estudante[$valor->palualcodig]->dataInicio)).' a '.date('d/m/y', strtotime($estudante[$valor->palualcodig]->datafim)); ?></td> <!-- Periodo contrato rescindido-->
					<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo 'R$ '. number_format($estudante[$valor->palualcodig]->valorTotal, 2, ".", ""); ?></td>  <!-- Valor -->							
					
					<?php if ($estudante[$valor->palualcodig]->termino < date('Y-m-d') ) { ?>
					    <td style="padding: 5px;fonte-size:12" class="text-center"><code><?php echo date('d/m/Y', strtotime($estudante[$valor->palualcodig]->termino )); ?></code></td> <!-- Término para contrato (Vencido) -->
					<?php } else { ?> 
						<td style="padding: 5px;fonte-size:12" class="text-center"><?php echo date('d/m/Y', strtotime($estudante[$valor->palualcodig]->termino )); ?></td> <!-- Término contrato -->
					<?php } ?> 													


				</tr>
					
				<?php } ?>

			<?php } ?>	
				

		</tbody>	
</table>

	<div class="panel-body">
		<?php 
		$totalEstorno = 0;
		if (count($estorno) >= 1) {  ?>	

			<label>Estorno:</label><br />
			<?php				
			foreach ($estorno as $valor) {

			$totalEstorno = $totalEstorno + $valor->estorno;

			?>				
			<div class="form-control-static">
				<?php echo $valor->salualnome; ?> -  Estorno <span><code><?php echo 'R$ '. number_format($valor->estorno, 2, ".", ""); ?></span></code>, Foi cobrado mês: <?php echo $lastmes; ?> rescindido no dia: <?php echo date('d/m/Y', strtotime($valor->contrestempr_data_rescisao ));  ?> .<br />	     
			</div>
			<?php } ?>
		<?php } ?>   
	</div>

	<?php if ($contrato[0]->demonstrativoempr_obs <> '') {  ?>
		<label  class="control-label col-lg-2">Observação:</label><br/>
        <?php echo $contrato[0]->demonstrativoempr_obs; ?>
        <br/>
        <br/>
	<?php  } ?>	


	<br/>
	<span>Valor Total: <?php echo 'R$'. number_format($totalServico , 2, ",", "."); ?></span><br />
	<span>Estorno: <?php echo 'R$'. number_format($totalEstorno , 2, ",", "."); ?></span><br />
	<span>Nota Fiscal: <?php echo  $contrato[0]->demonstrativoempr_notafiscal; ?></span><br />

	<span>Vencimento: <?php echo  $contrato[0]->sempemvenccontrato.'/'.date('m').'/'.date('y'); ?></span>
	<br/>
	<br/>
	<br/>
	<div class="text-center">
	<span><i><u>Paula Nascimento</u></i></span><br/>
	<span><i>Assessoria administrativa / Depto. Financeiro</i></span><br/>
	<span><i>NUDEP LTDA</i></span>
	</div>

	<?php   $Totalextenso =  extenso($totalServico - $totalEstorno); ?>

	<script type="text/javascript">

	var valor = '<?php echo  "R$". number_format($totalServico - $totalEstorno, 2, ",", ".");  ?>';
	var valorExtenso =  '<?php echo  $Totalextenso;  ?>';

	$("#totalServico").text(valor);
	$("#totalExtenso").text(valorExtenso);


	</script>



