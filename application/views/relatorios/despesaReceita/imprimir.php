<?php
@set_time_limit(300);
ini_set('max_execution_time', 300);
ini_set('max_input_time', 300);
ini_set('memory_limit', '512M');
?>

<head>
    <title>WDM-Controller</title>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="<?= base_url(); ?>public/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>public/assets/css/bootstrap-responsive.min.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>public/assets/css/fullcalendar.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>public/assets/css/main.css" />
    <link rel="stylesheet" href="<?= base_url(); ?>public/assets/css/blue.css" class="skin-color" />
    <!-- <script type="text/javascript"  src="<?= base_url(); ?>js/jquery-1.10.2.min.js"></script> -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body style="background-color: transparent">
    <div class="container-fluid">
        <!-- <?= var_dump($lancamentos[0]); ?> -->
        <div class="row-fluid">
            <div class="span12">
                <div class="widget-box">
                    <div class="widget-title">
                        <h4 style="text-align: center">
                            Relatório Financeiro - <?= $titulo ?>
                        </h4>
                        <h6 style="text-align: center">
                            Periodo: <?= $dataInicio ?> à <?= $dataFinal ?>
                        </h6>
                        <h6 style="text-align: center">
                            Gerado em: <?= date('d/m/Y'); ?>
                        </h6>
                    </div>
                    <div class="widget-content">
                        <table class="table-responsive-md table-striped">
                            <thead>
                                <tr>
                                    <th style="font-size: 1.2em; padding: 5px;">Data Vencimento</th>
                                    <th style="font-size: 1.2em; padding: 5px;">Cliente/Fornecedor</th>
                                    <th style="font-size: 1.2em; padding: 5px;">Valor</th>
                                    <th style="font-size: 1.2em; padding: 5px;">Data Pagamento</th>
                                    <th style="font-size: 1.2em; padding: 5px;">Forma de Pagamento</th>
                                    <th style="font-size: 1.2em; padding: 5px;">Situação</th>
                                    <!-- <th style="font-size: 1.2em; padding: 5px;">Tipo</th> -->
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $totais = [
                                    (object)[
                                        'tipo' => 'Receita',
                                        'valores' => [
                                            'CartaoCredito' => 0,
                                            'CartaoDebito' => 0,
                                            'Cheque' => 0,
                                            'Dinheiro' => 0,
                                            'PagamentoPix' => 0,
                                            'Boleto' => 0,
                                            'PagOnline' => 0,
                                            'TransfBancaria' => 0,
                                            'Outros' => 0,
                                            'SemPagamento' => 0,
                                            'total' => 0
                                        ]
                                    ],
                                    (object)[
                                        'tipo' => 'Despesa',
                                        'valores' => [
                                            'CartaoCredito' => 0,
                                            'CartaoDebito' => 0,
                                            'Cheque' => 0,
                                            'Dinheiro' => 0,
                                            'PagamentoPix' => 0,
                                            'Boleto' => 0,
                                            'PagOnline' => 0,
                                            'TransfBancaria' => 0,
                                            'Outros' => 0,
                                            'SemPagamento' => 0,
                                            'total' => 0
                                        ]
                                    ]
                                ];

                                $saldo = 0;
                                $totalReceita = 0;
                                $totalDespesa = 0;

                                foreach ($lancamentos as $l) {

                                    // Se o tipo for "receita"
                                    if ($l->financeiro_tipo == 'receita') {
                                        $totais[0]->valores['total'] += $l->financeiro_valor;
                                        $totalReceita += $l->financeiro_valor;
                                    }

                                    // Se o tipo for "despesa"
                                    if ($l->financeiro_tipo == 'despesa') {
                                        $totais[1]->valores['total'] += $l->financeiro_valor;
                                        $totalDespesa += $l->financeiro_valor;
                                    }

                                    switch ($l->financeiro_forma_pgto) {
                                        case 'Cartão de Crédito':
                                            $l->financeiro_tipo == 'receita' ? $totais[0]->valores['CartaoCredito'] += $l->financeiro_valor : $totais[1]->valores['CartaoCredito'] += $l->financeiro_valor;
                                            break;
                                        case 'Cartão de Débito':
                                            $l->financeiro_tipo == 'receita' ? $totais[0]->valores['CartaoDebito'] += $l->financeiro_valor : $totais[1]->valores['CartaoDebito'] += $l->financeiro_valor;
                                            break;
                                        case 'Cheque':
                                            $l->financeiro_tipo == 'receita' ? $totais[0]->valores['Cheque'] += $l->financeiro_valor : $totais[1]->valores['Cheque'] += $l->financeiro_valor;
                                            break;
                                        case 'Dinheiro':
                                            $l->financeiro_tipo == 'receita' ? $totais[0]->valores['Dinheiro'] += $l->financeiro_valor : $totais[1]->valores['Dinheiro'] += $l->financeiro_valor;
                                            break;
                                        case 'Pagamento Instantâneo (PIX)':
                                            $l->financeiro_tipo == 'receita' ? $totais[0]->valores['PagamentoPix'] += $l->financeiro_valor : $totais[1]->valores['PagamentoPix'] += $l->financeiro_valor;
                                            break;
                                        case 'Outros':
                                            $l->financeiro_tipo == 'receita' ? $totais[0]->valores['Outros'] += $l->financeiro_valor : $totais[1]->valores['Outros'] += $l->financeiro_valor;
                                            break;
                                        case 'Boleto Bancário':
                                            $l->financeiro_tipo == 'receita' ? $totais[0]->valores['Boleto'] += $l->financeiro_valor : $totais[1]->valores['Boleto'] += $l->financeiro_valor;
                                            break;
                                        case 'Pagamento Online':
                                            $l->financeiro_tipo == 'receita' ? $totais[0]->valores['PagOnline'] += $l->financeiro_valor : $totais[1]->valores['PagOnline'] += $l->financeiro_valor;
                                            break;
                                        case 'Transferência Bancária':
                                            $l->financeiro_tipo == 'receita' ? $totais[0]->valores['TransfBancaria'] += $l->financeiro_valor : $totais[1]->valores['TransfBancaria'] += $l->financeiro_valor;
                                            break;
                                        case 'Sem pagamento':
                                            $l->financeiro_tipo == 'receita' ? $totais[0]->valores['SemPagamento'] += $l->financeiro_valor : $totais[1]->valores['SemPagamento'] += $l->financeiro_valor;
                                            break;
                                    }
                                }

                                $saldo = $totalReceita - $totalDespesa;
                                ?>

                                <?php foreach ($lancamentos as $l) : ?>
                                    <tr>
                                        <td style="border-bottom: 1px solid black;">
                                            <?= date('d/m/Y', strtotime($l->data_vencimento)) ?>
                                        </td>
                                        <td style="border-bottom: 1px solid black;">
                                            <?= trim($l->financeiro_forn_clie_usua) ?>
                                            <br />
                                            <?= trim($l->financeiro_descricao) ?>
                                        </td>
                                        <td style="border-bottom: 1px solid black; color: <?= $l->financeiro_tipo  == 'despesa' ? 'red' : 'green' ?>">
                                            <?= is_numeric($l->financeiro_valor) ? 'R$ ' . number_format($l->financeiro_valor, 2, ',', '.') : '' ?>
                                        </td>
                                        <td style="border-bottom: 1px solid black;">
                                            <?= date('d/m/Y', strtotime($l->data_pagamento)) ?>
                                        </td>
                                        <td style="border-bottom: 1px solid black;">
                                            <?= $l->financeiro_forma_pgto ?>
                                        </td>
                                        <td style="border-bottom: 1px solid black;">
                                            <?= ($l->financeiro_baixado == 1) ? 'Pago' : 'Pendente' ?>
                                        </td>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>

                <!-- <?= var_dump($totais) ?> -->
                <?php foreach ($totais as $t) : ?>
                    <h2 colspan="5" style="text-align: right; color: <?= $t->tipo == 'Receita' ? 'green' : 'red' ?>;">
                        <strong>
                            <?= $t->tipo == 'Receita' ? 'Receita' : 'Despesa' ?>
                        </strong>
                    </h2>
                    <h5 colspan="5" style="text-align: right; color: grey">
                        <strong>
                            Cartão Crédito: R$ <?= number_format($t->valores['CartaoCredito'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <h5 colspan="5" style="text-align: right; color: grey">
                        <strong>
                            Cartão Débito: R$ <?= number_format($t->valores['CartaoDebito'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <h5 colspan="5" style="text-align: right; color: grey">
                        <strong>
                            Cheque: R$ <?= number_format($t->valores['Cheque'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <h5 colspan="5" style="text-align: right; color: grey">
                        <strong>
                            Dinheiro: R$ <?= number_format($t->valores['Dinheiro'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <h5 colspan="5" style="text-align: right; color: grey">
                        <strong>
                            Pagamento Instantâneo (PIX): R$ <?= number_format($t->valores['PagamentoPix'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <h5 colspan="5" style="text-align: right; color: grey">
                        <strong>
                            Boleto: R$ <?= number_format($t->valores['Boleto'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <h5 colspan="5" style="text-align: right; color: grey">
                        <strong>
                            Pagamento Online: R$ <?= number_format($t->valores['PagOnline'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <h5 colspan="5" style="text-align: right; color: grey">
                        <strong>
                            Transferência Bancaria: R$ <?= number_format($t->valores['TransfBancaria'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <h5 colspan="5" style="text-align: right; color: grey">
                        <strong>
                            Outros: R$ <?= number_format($t->valores['Outros'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <h5 colspan="5" style="text-align: right; color: blue">
                        <strong>
                            Sem Pagamento: R$ <?= number_format($t->valores['SemPagamento'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <h5 colspan="5" style="text-align: right; color: <?= $t->tipo == 'Receita' ? 'green' : 'red' ?>;">
                        <strong>
                            Total: R$ <?= number_format($t->valores['total'], 2, ',', '.') ?>
                        </strong>
                    </h5>
                    <hr>
                <?php endforeach ?>

                <h5 colspan="2" style="text-align: right;">
                    <strong style="color: <?= ($saldo < 0) ? 'red' : (($saldo == 0) ? 'grey' : 'green') ?>">
                        Saldo: R$ <?= number_format($saldo, 2, ',', '.') ?>
                    </strong>
                </h5>
            </div>
        </div>
    </div>
</body>

</html>