<?php if ($this->session->flashdata('error')) : ?>
    <div class="alert alert-danger">
        <?= $this->session->flashdata('error') ?>
    </div>
<?php endif ?>

<div class="panel panel-flat col-md-3">
    <div class="panel-heading">
        <h5 class="panel-title text-center">Relatórios Rápidos</h5>
    </div>
    <div class="category-content">
        <div class="row row-condensed">
            <div class="col-xs-12">
                <a target="_blank" href="<?= base_url(); ?>relatorios/despesaReceitaRapido/mes/<?= date('Y-m'); ?>" class="btn bg-teal-400 btn-block btn-float btn-float-lg">
                    <i class="icon-file-text2"></i>
                    <span>Relatorio do Mês</span>
                </a>
                <a target="_blank" href="<?= base_url(); ?>relatorios/despesaReceitaRapido/1Semestre/<?= date('Y'); ?>" class="btn bg-teal-400 btn-block btn-float btn-float-lg">
                    <i class="icon-file-text2"></i>
                    <span>Fluxo Caixa Anual 1º Semestre</span>
                </a>
                <a target="_blank" href="<?= base_url(); ?>relatorios/despesaReceitaRapido/2Semestre/<?= date('Y'); ?>" class="btn bg-teal-400 btn-block btn-float btn-float-lg">
                    <i class="icon-file-text2"></i>
                    <span>Fluxo Caixa Anual 2º Semestre</span>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="col-md-9">
    <div class="panel panel-flat col-md-12">
        <div class="panel-heading">
            <h5 class="panel-title text-center">Relatórios Personalizados</h5>
        </div>

        <div class="panel panel-flat col-md-12">
            <div class="panel-heading">
                <h5 class="panel-title text-center">Vencimento:</h5>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="<?= base_url(); ?>relatorios/despesaReceitaPersonalizado/" method="get" target="_blank">
                    <input type="hidden" name="pegarPor" value="data_vencimento">

                    <div class="col-lg-6">
                        <label>Início: <span style="color: red;">*</span></label>
                        <input type="date" class="form-control daterange-single diaInicioPdf" name="diaInicioPdf" id="diaInicioPdf" value="" required>
                    </div>
                    <div class="col-lg-6">
                        <label>Fim: <span style="color: red;">*</span></label>
                        <input type="date" class="form-control daterange-single diaFimPdf" name="diaFimPdf" id="diaFimPdf" value="" required>
                    </div>
                    <div class="col-lg-6">
                        <label>Cliente: <span style="color: red;">*</span></label>
                        <input type="text" class="form-control daterange-single usuario_nomePdf" name="usuario_nomePdf" value="Todos" required>
                        <input type="hidden" class="form-control daterange-single usuario_idPdf" name="usuario_idPdf" value="Todos">
                    </div>
                    <div class="col-lg-3">
                        <label>Tipo: <span style="color: red;">*</span></label>
                        <select class="form-control" name="tipoConta" id="tipoConta" required>
                            <option value="0">Ambos</option>
                            <option value="1">Receita</option>
                            <option value="2">Despesa</option>
                        </select>
                    </div>
                    <div class="col-lg-3">
                        <label>Situação: <span style="color: red;">*</span></label>
                        <select class="form-control" name="situacao" id="situacao" required>
                            <option value="0">Ambas</option>
                            <option value="1">Baixado</option>
                            <option value="2">Pendente</option>
                        </select>
                    </div>
                    <div class="text-right col-lg-12" style="padding-top: 25px;">
                        <button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>

        <div class="panel panel-flat col-md-12">
            <div class="panel-heading">
                <h5 class="panel-title text-center">Pagamento:</h5>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="<?= base_url(); ?>relatorios/despesaReceitaPersonalizado/" method="get" target="_blank">
                    <input type="hidden" name="pegarPor" value="data_pagamento">

                    <div class="col-lg-6">
                        <label>Início: <span style="color: red;">*</span></label>
                        <input type="date" class="form-control daterange-single diaInicioPdf" name="diaInicioPdf" id="diaInicioPdf" value="" required>
                    </div>

                    <div class="col-lg-6">
                        <label>Fim: <span style="color: red;">*</span></label>
                        <input type="date" class="form-control daterange-single diaFimPdf" name="diaFimPdf" id="diaFimPdf" value="" required>
                    </div>

                    <div class="col-lg-6">
                        <label>Cliente: <span style="color: red;">*</span></label>
                        <input type="text" class="form-control daterange-single usuario_nomePdf" name="usuario_nomePdf" value="Todos" required>
                        <input type="hidden" class="form-control daterange-single usuario_idPdf" name="usuario_idPdf" value="Todos">
                    </div>

                    <div class="col-lg-3">
                        <label>Tipo: <span style="color: red;">*</span></label>
                        <select class="form-control" name="tipoConta" id="tipoConta" required>
                            <option value="0">Ambos</option>
                            <option value="1">Receita</option>
                            <option value="2">Despesa</option>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <label>Situação: <span style="color: red;">*</span></label>
                        <select class="form-control" name="situacao" id="situacao" required>
                            <option value="0">Ambas</option>
                            <option value="1">Baixado</option>
                            <option value="2">Pendente</option>
                        </select>
                    </div>

                    <div class="text-right col-lg-12" style="padding-top: 25px;">
                        <button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>

        <div class="panel panel-flat col-md-12">
            <div class="panel-heading">
                <h5 class="panel-title text-center">Forma de Pagamento</h5>
            </div>
            <div class="panel-body">
                <form class="form-horizontal" action="<?= base_url(); ?>relatorios/despesaReceitaPersonalizado/" method="get" target="_blank">
                    <input type="hidden" name="tipoRelatorio" value="financeiro_forma_pgto">

                    <fieldset class="col-lg-12">
                        <div class="row">
                            <div>
                                <div class="form-check col-lg-2">
                                    <input class="form-check-input" type="radio" name="pegarPor" id="gridRadios1" value="data_vencimento" checked>
                                    <label class="form-check-label" for="gridRadios1">
                                        Vencimento
                                    </label>
                                </div>
                                <div class="form-check col-lg-2">
                                    <input class="form-check-input" type="radio" name="pegarPor" id="gridRadios2" value="data_pagamento">
                                    <label class="form-check-label" for="gridRadios2">
                                        Pagamento
                                    </label>
                                </div>
                            </div>
                        </div>
                    </fieldset>

                    <div class="col-lg-6">
                        <label>Início: <span style="color: red;">*</span></label>
                        <input type="date" class="form-control daterange-single diaInicioPdf" name="diaInicioPdf" id="diaInicioPdf" value="" required>
                    </div>

                    <div class="col-lg-6">
                        <label>Fim: <span style="color: red;">*</span></label>
                        <input type="date" class="form-control daterange-single diaFimPdf" name="diaFimPdf" id="diaFimPdf" value="" required>
                    </div>

                    <div class="col-lg-3">
                        <label>Cliente: <span style="color: red;">*</span></label>
                        <input type="text" class="form-control daterange-single usuario_nomePdf" name="usuario_nomePdf" value="Todos" required>
                        <input type="hidden" class="form-control daterange-single usuario_idPdf" name="usuario_idPdf" value="Todos">
                    </div>

                    <div class="col-lg-3">
                        <label>Forma de Pagamento: <span style="color: red;">*</span></label>
                        <select class="form-control" name="formaPagamento" id="formaPagamento" required>
                            <?php foreach ($formaPagamento as $formaPag): ?>
                                <option value="<?= $formaPag->financeiro_forma_pgto ?>"><?= $formaPag->financeiro_forma_pgto ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <label>Tipo: <span style="color: red;">*</span></label>
                        <select class="form-control" name="tipoConta" id="tipoConta" required>
                            <option value="0">Ambos</option>
                            <option value="1">Receita</option>
                            <option value="2">Despesa</option>
                        </select>
                    </div>

                    <div class="col-lg-3">
                        <label>Situação: <span style="color: red;">*</span></label>
                        <select class="form-control" name="situacao" id="situacao" required>
                            <option value="0">Ambas</option>
                            <option value="1">Baixado</option>
                            <option value="2">Pendente</option>
                        </select>
                    </div>

                    <div class="text-right col-lg-12" style="padding-top: 25px;">
                        <button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        function setUpAutocomplete(inputId, hiddenInputId) {
            $(inputId).autocomplete({
                source: "<?php echo base_url(); ?>/relatorios/autoCompleteUsuarios",
                minLength: 1,
                select: function(event, ui) {
                    $(hiddenInputId).val(ui.item.id);
                }
            });
        }

        setUpAutocomplete(".usuario_nome", ".usuario_id");
        setUpAutocomplete(".usuario_nomePdf", ".usuario_idPdf");

        $(".diaFimPdf, .diaInicioPdf").val(new Date().toISOString().slice(0, 10));

        $(".usuario_nomePdf").on("blur", function() {
            if ($.trim($(this).val()) === "") {
                $(".usuario_idPdf").val("Todos");
                $(this).val("Todos");
            }
        });

        $(".form-horizontal").submit(function(e) {
            e.preventDefault();
            if ($(this).find(".usuario_idPdf").val() == "" || $(this).find(".usuario_nomePdf").val() == "") {
                swal({
                    title: "Erro!",
                    text: "Selecione um cliente.",
                    type: "error",
                    confirmButtonColor: "#2196F3"
                });
                return;
            } else if ($(this).find(".tipoRelatorio").val() == "") {
                swal({
                    title: "Erro!",
                    text: "Selecione um tipo de relatório.",
                    type: "error",
                    confirmButtonColor: "#2196F3"
                });
                return;
            } else if ($(this).find(".diaInicioPdf").val() == "" || $(this).find(".diaFimPdf").val() == "" || $(this).find(".diaInicioPdf").val() > $(this).find(".diaFimPdf").val()) {
                swal({
                    title: "Erro!",
                    text: "Selecione um intervalo de datas válido.",
                    type: "error",
                    confirmButtonColor: "#2196F3"
                });
                return;
            }

            e.target.submit();
        });
    });
</script>