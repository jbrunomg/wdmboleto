<div class="panel panel-flat col-md-3">
<div class="panel-heading">
	<h5 class="panel-title text-center">Relatórios Rápidos</h5>							
</div>
	<div class="category-content">
		<div class="row row-condensed">
			<div class="col-xs-12">
				<a href="<?php echo base_url();?>relatorios/membrosRapidoSimples" class="btn bg-teal-400 btn-block btn-float btn-float-lg" ><i class="icon-file-text2"></i> <span>Membros Nome, Telefone e E-mail</span></a>		
			</div>
		</div>
	</div>
</div>
<!-- Form horizontal -->

<div class="col-md-9">
<div class="panel panel-flat col-md-12">
<div class="panel-heading">
	<h5 class="panel-title text-center">Relatórios Personalizados</h5>							
</div>
	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?>relatorios/dizimosOfertasPersonalizado/" method="post">
			<!-- <div class="form-group">
				<label class="control-label col-lg-2">Data Conversão Início:</label>
				<div class="col-lg-4">																						
					<input type="text" class="form-control daterange-single" name="dataInicio" id="dataInicio" value="">
				</div>
				<label class="control-label col-lg-2">Data Conversão Fim:</label>
				<div class="col-lg-4">																						
					<input type="text" class="form-control daterange-single" name="dataFim" id="dataFim" value="">
				</div>									
			</div>		 -->



			<!-- <div class="form-group">
				
					<div class="form-group">
		            	<label class="control-label col-lg-1">Tipo:</label>
		            	<div class="col-lg-5">			            	
		                 <select class="form-control" name="tipoDizimoOferta" id="tipoDizimoOferta">
		                    <option value="">Selecione</option>
		                   	<?php foreach ($tipodizimo as $value) { ?>       
		                   			<option value="<?php echo $value->id ?>"><?php echo $value->descricao ?></option>
		                   	<?php } ?>               				                                
		                  </select>
		               
		            </div>	

		            <label class="control-label col-lg-1">Culto:</label>
		            	<div class="col-lg-5">			            	
		                 <select class="form-control" name="culto" id="culto">
		                    <option value="">Selecione</option>
		                   	<?php foreach ($culto as $value) { ?>       
		                   			<option value="<?php echo $value->culto_id ?>"><?php echo $value->culto_descricao ?></option>
		                   	<?php } ?>               				                                
		                  </select>
		               
		            </div>			
		          </div>
            </div>				 -->

            			

			<div class="text-right">
				<button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
			</div>

		</form>
	</div>
</div>
</div>