<table class="table">	
	<tbody>		
		<tr>	
			<td style="font-size:12px;">
				<?php 
				    echo $emitente->emitente_nome."<br>";
					echo "Fone  : ".$emitente->emitente_telefone."<br>";
					echo "E-mail: ".$emitente->emitente_email."<br>";
								
				?>
			</td>		
			<td class="text-right"><img src="<?php echo $emitente->emitente_imagem;?>" width="100px" heigth="100px"></td>
			
		</tr>		
	</tbody>
</table>
<hr style="margin-top:-10px">
<h3 class="text-center" style="margin-top:-10px"><?php echo $titulo; ?></h3>
<table class="table">
	<thead>
		<tr>			
			<th style="padding: 5px;fonte-size:12">Nome</th>
			<th style="padding: 5px;fonte-size:12">Tipo</th>
			<th style="padding: 5px;fonte-size:12">Etiqueta</th>
			<th style="padding: 5px;fonte-size:12">Data Aquisição</th>
			<th style="padding: 5px;fonte-size:12">Valor</th>
		</tr>
	</thead>
	<tbody>
		<?php 
			$cont = 0;
			$valorTotal = 0;
		?>
		<?php foreach ($dados as $valor){ ?>
			<?php 
				$cont++; 
				$valorTotal += $valor->patrimonio_valor; 
			?>			
			<tr>				
				<td style="padding: 5px;fonte-size:12"><?php echo $valor->patrimonio_nome; ?></td>
				<td style="padding: 5px;fonte-size:12"><?php echo $valor->patrimonio_tipo; ?></td>
				<td style="padding: 5px;fonte-size:12"><?php echo $valor->patrimonio_etiqueta; ?></td>
				<td style="padding: 5px;fonte-size:12"><?php echo date('d-m-Y', strtotime($valor->patrimonio_aquisicao)); ?></td>
				<td style="padding: 5px;fonte-size:12"><?php echo $valor->patrimonio_valor ?></td>
			</tr>
		
		<?php } ?>
		<tr>
				<td colspan="5" style="text-align: right;">________________</td>
			</tr>
			<tr>
				<td colspan="5" style="text-align: right;"><span  style="font-size: 15px;margin-right: 100px;"><b><?php echo "Total ".number_format($valorTotal,2,",","."); ?></b></span></td>
			</tr>
	</tbody>
</table>
		

