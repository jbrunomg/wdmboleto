<h3 class="text-center" style="margin-top:-10px"><?php echo $titulo; ?></h3>
<hr style="margin-top:-10px">
<table class="table">
	<thead>
        <tr>
            <th style="font-size:12px; background-color:#4D4D4D; color:white; border:1px solid black; text-align:center; padding:5px;">Data Transação</th>
            <th style="font-size:12px; background-color:#4D4D4D; color:white; border:1px solid black; text-align:center; padding:5px;">Lojista</th>
            <th style="font-size:12px; background-color:#4D4D4D; color:white; border:1px solid black; text-align:center; padding:5px;">Bruto</th>
			<?php if($tipo == '1'){ ?>
				<th style="font-size:12px; background-color:#4D4D4D; color:white; border:1px solid black; text-align:center; padding:5px;">Liquido</th>
			<?php } ?>
            <th style="font-size:12px; background-color:#4D4D4D; color:white; border:1px solid black; text-align:center; padding:5px;">Rep. Loja</th>
			<?php if($tipo == '1'){ ?>
				<th style="font-size:12px; background-color:#4D4D4D; color:white; border:1px solid black; text-align:center; padding:5px;">Luc. Operação</th>
			<?php } ?>
			<th style="font-size:12px; background-color:#4D4D4D; color:white; border:1px solid black; text-align:center; padding:5px;">
				Código NSU
			</th>
        </tr>
	</thead>
	<tbody>
		<?php
	
		$totalBruto = 0;
		$totalrepLoja = 0;
		$totalLiquido = 0;
		$totalLucOpe = 0;

		foreach ($dados as $p){ 
            $descLoja = $p->operacao_total_boleto * ($p->operacao_taxa/ 100);
			$repLoja  = $p->operacao_total_boleto - $descLoja;
			$descReal = $p->operacao_total_boleto * ($p->operacao_taxa_banco/ 100) ;
			$liquido  = $p->operacao_total_boleto - $descReal;
			$lucOpe   = $liquido - $repLoja;


			$totalBruto += $p->operacao_total_boleto;
			$totalrepLoja += $repLoja;
			$totalLiquido += $liquido;
			$totalLucOpe += $lucOpe;
            ?>
			<tr>				
				<td style="fonte-size:12; text-align:center;"><?php echo date('d/m/Y', strtotime(str_replace('-','/',$p->operacao_data_cadastro))) ?></td>
				<td style="fonte-size:12; text-align:center;"><?php echo $p->usuario_nome ?></td>
				<td style="fonte-size:12; text-align:center;"><?php echo 'R$ '.number_format($p->operacao_total_boleto, 2, ',', '.')  ?></td>
				<?php if($tipo == '1'){ ?>
					<td style="fonte-size:12; text-align:center;"><?php echo 'R$ '.number_format($liquido, 2, ',', '.') ?></td>
				<?php } ?>
				<td style="fonte-size:12; text-align:center;"><?php echo 'R$ '.number_format($repLoja, 2, ',', '.') ?></td>
				<?php if($tipo == '1'){ ?>
					<td style="fonte-size:12; text-align:center;"><?php echo 'R$ '.number_format($lucOpe, 2, ',', '.') ?></td>
				<?php } ?>
				<td style="fonte-size:12; text-align:center;">
					<?php echo $p->financeiro_autorizacao_NSU ?>
				</td>
			</tr>
		<?php } ?>
			<tr>                
				<td colspan="2" style="font-size:12px; text-align:center;"></td>
				<td style="font-size:12px; text-align:center;"><strong><?php echo 'Total Bruto: R$ '.number_format($totalBruto, 2, ',', '.') ?></strong></td>
				<?php if($tipo == '1'){ ?>
					<td style="font-size:12px; text-align:center;"><strong><?php echo 'Total Liquido: R$ '.number_format($totalLiquido, 2, ',', '.') ?></strong></td>
				<?php } ?>
				<td style="font-size:12px; text-align:center;"><strong><?php echo 'Total Rep. Loja: R$ '.number_format($totalrepLoja, 2, ',', '.') ?></strong></td>
				<?php if($tipo == '1'){ ?>
					<td style="font-size:12px; text-align:center;"><strong><?php echo 'Luc. Operação: R$ '.number_format($totalLucOpe, 2, ',', '.') ?></strong></td>
				<?php } ?>
			</tr>
	</tbody>
</table>
		


