<div class="panel panel-flat col-md-3">
    <div class="panel-heading">
        <h5 class="panel-title text-center">Relatórios Rápidos</h5>							
    </div>
	<div class="category-content">
		<div class="row row-condensed">
			<div class="col-xs-12">
				<a href="<?php echo base_url();?>relatorios/transacoesCartPersonalizada/<?php echo date('m'); ?>" class="btn bg-teal-400 btn-block btn-float btn-float-lg" ><i class="icon-file-text2"></i> <span>Transações Mês Atual</span></a>
				<a href="<?php echo base_url();?>relatorios/transacoesCartPersonalizada/<?php echo date('m')-1; ?>" class="btn bg-teal-400 btn-block btn-float btn-float-lg" ><i class="icon-file-text2"></i> <span>Transações Mês Anterior</span></a>
			</div>
		</div>
	</div>
</div>
<!-- Form horizontal -->

<div class="col-md-9">
    <div class="panel panel-flat col-md-12">
        <div class="panel-heading">
            <h5 class="panel-title text-center">Relatórios Personalizados Período</h5>							
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="<?php echo base_url();?>relatorios/transacoesCartPersonalizadaPeriodo/" method="post">
                <div class="form-group">
                    <div class="col-lg-4">	
                        <label>Início:</label>																					
                        <input type="text" class="form-control daterange-single" name="anoInicioPeriodoCliente" id="anoInicioPeriodoCliente" value="">
                    </div>	
                    <div class="col-lg-4">		
                        <label>Fim:</label>																				
                        <input type="text" class="form-control daterange-single" name="anoFimPeriodoCliente" id="anoFimPeriodoCliente" value="">
                    </div>
                    <div class="col-lg-4">	
                        <label>Cliente:</label>																					
                        <input type="text" class="form-control daterange-single" name="usuario_nome" id="usuario_nome" value="">
                        <input type="hidden" class="form-control daterange-single" name="usuario_id" id="usuario_id">
                    </div>	
                </div>										
                <div class="text-right">
                    <button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="panel panel-flat col-md-12">
        <div class="panel-heading">
            <h5 class="panel-title text-center">Fechamento Caixa - Cupom</h5>                          
        </div>
        <div class="panel-body">
            <form id="formRelatorio" class="form-horizontal" action="<?php echo base_url();?>relatorios/fechamentoCaixa/" method="post" target="formTarget">
                <div class="form-group">
                    <div class="col-lg-4">  
                        <label class="control-label col-lg-2">Data:</label>                                                                                    
                        <input type="text" class="form-control daterange-single" name="dataInicioCupom" id="dataInicioCupom" value="" required>
                    </div>                                              
                </div>                                          
                <div class="text-right">
                    <button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-md-3">
</div> 
<div class="col-md-9">
    <div class="panel panel-flat col-md-12">
        <div class="panel-heading">
            <h5 class="panel-title text-center">Relatórios Personalizados Período - PDF</h5>                          
        </div>
        <div class="panel-body">
            <form class="form-horizontal" target="_blank" action="<?php echo base_url();?>relatorios/transacoesCartPdfPeriodo/" method="post">
                <div class="form-group">
                    <div class="col-lg-3">  
                        <label>Início:</label>                                                                                  
                        <input type="text" class="form-control daterange-single" name="diaInicioPdf" id="diaInicioPdf" value="">
                    </div>  
                    <div class="col-lg-3">      
                        <label>Fim:</label>                                                                             
                        <input type="text" class="form-control daterange-single" name="diaFimPdf" id="diaFimPdf" value="">
                    </div>
                    <div class="col-lg-3">  
                        <label>Cliente:</label>                                                                                 
                        <input type="text" class="form-control daterange-single" name="usuario_nomePdf" id="usuario_nomePdf" value="">
                        <input type="hidden" class="form-control daterange-single" name="usuario_idPdf" id="usuario_idPdf">
                    </div>  
                    <div class="col-lg-3">      
                        <label>Tipo:</label>                                                                             
                        <select class="form-control" name="tipo" id="tipo">
                            <option value="0">Logista<opition>
                            <option value="1">ADM<opition>
                        </select>
                    </div>
                </div>                                      
                <div class="text-right">
                    <button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/jquery.mask.min.js"></script>
<script>

    $(document).ready(function(){

        $("#anoInicioPeriodoCliente").mask('00/00/0000', {placeholder: '00/00/0000'});
        $("#anoFimPeriodoCliente").mask('00/00/0000', {placeholder: '00/00/0000'});

        $("#diaInicioPdf").mask('00/00/0000', {placeholder: '00/00/0000'});
        $("#diaFimPdf").mask('00/00/0000', {placeholder: '00/00/0000'});

        $("#dataInicioCupom").mask('00/00/0000', {placeholder: '00/00/0000'});
        

        function setUpAutocomplete(inputId, hiddenInputId) {
            $(inputId).autocomplete({
                source: "<?php echo base_url(); ?>/relatorios/autoCompleteUsuarios",
                minLength: 1,
                select: function(event, ui) {
                    $(hiddenInputId).val(ui.item.id);
                }
            });
        }

        setUpAutocomplete("#usuario_nome", "#usuario_id");
        setUpAutocomplete("#usuario_nomePdf", "#usuario_idPdf");

        document.getElementById('formRelatorio').addEventListener('submit', function(event) {
            event.preventDefault(); // Impede o envio padrão do formulário
            var form = event.target;

            // Abre a nova janela com as especificações de tamanho
            var myWindow = window.open('', 'formTarget', 'toolbar=no,location=no,directories=no,status=no,menubar=yes,scrollbars=yes,resizable=yes,width=350,height=600');

            // Envia o formulário para a nova janela
            form.submit();
        });
       
    });

    
</script>