<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>
<!--
 /* Font Definitions */
 @font-face
  {font-family:"Cambria Math";
  panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
  {font-family:Calibri;
  panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
  {font-family:Cambria;
  panose-1:2 4 5 3 5 4 6 3 2 4;}
 /* Style Definitions */
 p.MsoNormal, li.MsoNormal, div.MsoNormal
  {margin:0cm;
  margin-bottom:.0001pt;
  text-autospace:none;
  font-size:11.0pt;
  font-family:"Cambria","serif";}
p.MsoHeader, li.MsoHeader, div.MsoHeader
  {mso-style-link:"Cabeçalho Char";
  margin:0cm;
  margin-bottom:.0001pt;
  text-autospace:none;
  font-size:11.0pt;
  font-family:"Cambria","serif";}
p.MsoFooter, li.MsoFooter, div.MsoFooter
  {mso-style-link:"Rodapé Char";
  margin:0cm;
  margin-bottom:.0001pt;
  text-autospace:none;
  font-size:11.0pt;
  font-family:"Cambria","serif";}
p.MsoBodyText, li.MsoBodyText, div.MsoBodyText
  {mso-style-link:"Corpo de texto Char";
  margin:0cm;
  margin-bottom:.0001pt;
  text-autospace:none;
  font-size:9.0pt;
  font-family:"Cambria","serif";
  font-weight:bold;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
  {margin:0cm;
  margin-bottom:.0001pt;
  text-autospace:none;
  font-size:11.0pt;
  font-family:"Cambria","serif";}
span.CabealhoChar
  {mso-style-name:"Cabeçalho Char";
  mso-style-link:Cabeçalho;
  font-family:"Cambria","serif";}
span.RodapChar
  {mso-style-name:"Rodapé Char";
  mso-style-link:Rodapé;
  font-family:"Cambria","serif";}
span.CorpodetextoChar
  {mso-style-name:"Corpo de texto Char";
  mso-style-link:"Corpo de texto";
  font-family:"Cambria","serif";}
p.TableParagraph, li.TableParagraph, div.TableParagraph
  {mso-style-name:"Table Paragraph";
  margin-top:1.45pt;
  margin-right:0cm;
  margin-bottom:0cm;
  margin-left:1.55pt;
  margin-bottom:.0001pt;
  text-autospace:none;
  font-size:11.0pt;
  font-family:"Cambria","serif";}
.MsoChpDefault
  {font-size:10.0pt;
  font-family:"Calibri","sans-serif";}
 /* Page Definitions */
 @page WordSection1
  {size:595.5pt 842.0pt;
  margin:45.0pt 38.0pt 14.0pt 38.0pt;}
div.WordSection1
  {page:WordSection1;}
-->
</style>

</head>

<body lang=PT-BR>

<div class=WordSection1>

<p class=MsoBodyText align=center style='margin-left:4.5pt;text-align:center'><span
style='font-size:10.0pt;font-family:"Times New Roman","serif";position:relative;
top:.5pt;font-weight:normal'><img width=150 height=130 id="Imagem 4"
src="<?php echo $emitente[0]->url_logo; ?>"></span></p>

<p class=MsoBodyText><span lang=PT style='font-size:10.0pt;font-family:"Times New Roman","serif";
font-weight:normal'>&nbsp;</span></p>

<p class=MsoBodyText style='margin-top:.35pt'><span lang=PT style='font-size:
5.0pt;font-family:"Times New Roman","serif";font-weight:normal'>&nbsp;</span></p>

<p class=MsoNormal><span lang=PT>&nbsp;</span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
 style='margin-left:8.8pt;background:#D9D9D9;border-collapse:collapse;
 border:1px solid black'>
 <tr style='height:11.0pt'>
  <td width=675 valign=bottom style='width:506.05pt;border:solid windowtext 1.5pt;
  padding:0cm 3.5pt 0cm 3.5pt;height:11.0pt'>
  <p class=MsoBodyText align=right style='margin-left:-.8pt;text-align:right'><span
  lang=PT style='font-weight:normal'>Vendedor:  </span><span lang=PT><?php echo $dados[0]->usuario_nome; ?></span></p>
  <p class=MsoBodyText align=right style='margin-left:-.8pt;text-align:right'><span
  lang=PT>&nbsp;</span></p>
  </td>
 </tr>
</table>

<p class=MsoBodyText style='margin-left:4.5pt'><span lang=PT>&nbsp;</span></p>

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0
 style='margin-left:8.8pt;border-collapse:collapse;border:1px solid black'>
 <tr style='page-break-inside:avoid;height:20.0pt'>
  <td width=675 style='width:506.05pt;border:solid windowtext 1.5pt;background:
  #D9D9D9;padding:0cm 3.5pt 0cm 3.5pt;height:20.0pt'>
  <center class=MsoBodyText align=center style='margin-left:-.8pt;text-align:center'><span
  lang=PT>PEDIDO Nº<?php echo $dados[0]->operacao_id; ?></span></center>
  <p class=MsoBodyText align=center style='margin-left:-.8pt;text-align:center'><span
  lang=PT>&nbsp;</span></p>
  </td>
 </tr>
</table>

<p class=MsoBodyText><span lang=PT style='font-size:7.0pt;font-family:"Times New Roman","serif";
font-weight:normal'>&nbsp;</span></p>

<p class=MsoBodyText><span lang=PT style='font-size:7.0pt;font-family:"Times New Roman","serif";
font-weight:normal'>&nbsp;</span></p>

<table class=TableNormal border=1 cellspacing=0 cellpadding=0 style='margin-left:
 6.7pt;border-collapse:collapse;border:1px solid black'>
 <tr style='height:12.65pt'>
  <td width=675 colspan=4 valign=top style='width:506.05pt;border:solid windowtext 1.5pt;
  background:#ECF0F5;padding:0cm 0cm 0cm 0cm;height:12.65pt'>
  <p class=TableParagraph style='margin-top:1.15pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:1.2pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:8.5pt'>&nbsp;DADOS DO CLIENTE</span></b></p>
  </td>
 </tr>
 <tr style='height:13.0pt'>
  <td width=79 valign=top style='width:49.3pt;border:solid windowtext 1.5pt;
  border-top:1px solid black;padding:0cm 0cm 0cm 0cm;height:13.0pt'>
  <p class=TableParagraph style='margin-top:1.5pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:1.2pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:8.5pt'>&nbsp;Nome:</span></b></p>
  </td>
  <td width=265 valign=top style='width:199.05pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:13.0pt'>
  <p class=TableParagraph><span lang=PT style='font-size:8.5pt'>&nbsp;<?php echo $dados[0]->usuario_nome; ?></span></p>
  </td>
  <td width=66 valign=top style='width:40.75pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:13.0pt'>
  <p class=TableParagraph style='margin-top:1.5pt'><b><span lang=PT
  style='font-size:8.5pt'>&nbsp;RG/CPF:</span></b></p>
  </td>
  <td width=264 valign=top style='width:197.95pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:13.0pt'>
  <p class=TableParagraph style='margin-left:1.6pt'><span lang=PT
  style='font-size:8.5pt'>&nbsp;<?php  
  echo formatar_cpf_cnpj($dados[0]->usuario_cpf);
  function formatar_cpf_cnpj($doc) {
     $doc = preg_replace("/[^0-9]/", "", $doc);
     $qtd = strlen($doc);
     if($qtd >= 11) {
         if($qtd === 11 ) {
             $docFormatado = substr($doc, 0, 3) . '.' .
                             substr($doc, 3, 3) . '.' .
                             substr($doc, 6, 3) . '-' .
                             substr($doc, 9, 2);
         } else {
             $docFormatado = substr($doc, 0, 2) . '.' .
                             substr($doc, 2, 3) . '.' .
                             substr($doc, 5, 3) . '/' .
                             substr($doc, 8, 4) . '-' .
                             substr($doc, -2);
         }
         return $docFormatado;
     }
  }
  ?></span></p>
  </td>
 </tr>
 <tr style='height:23.55pt'>
  <td width=79 valign=top style='width:40.3pt;border:solid windowtext 1.5pt;
  border-top:1px solid black;padding:0cm 0cm 0cm 0cm;height:12.65pt'>
  <p class=TableParagraph style='margin-top:.95pt;margin-right:.45pt;
  margin-bottom:0cm;margin-left:1.2pt;margin-bottom:.0001pt;line-height:10.5pt'><b><span
  lang=PT style='font-size:8.5pt'>&nbsp; Dt<span style='letter-spacing:.05pt'> </span><span
  style='letter-spacing:-.05pt'> Nasc:</span></span></b></p>
  </td>
  <td width=265 valign=top style='width:199.05pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:12.65pt'>
  <p class=TableParagraph style='margin-top:6.75pt'><span lang=PT
  style='font-size:8.5pt'>
  &nbsp;<?php echo date("d/m/Y", strtotime($dados[0]->cliente_dtNascimento)); ?></span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:12.65pt'>
  <p class=TableParagraph style='margin-top:6.8pt'><b><span lang=PT
  style='font-size:8.5pt'>&nbsp;Sexo:</span></b></p>
  </td>
  <td width=264 valign=top style='width:197.95pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:12.65pt'>
  <p class=TableParagraph style='margin-top:6.75pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:1.6pt;margin-bottom:.0001pt'><span lang=PT style='font-size:
  8.5pt'>&nbsp;<?php echo $dados[0]->usuario_sexo; ?></span></p>
  </td>
 </tr>
 <tr style='height:12.65pt'>
  <td width=79 valign=top style='width:40.3pt;border:solid windowtext 1.5pt;
  border-top:1px solid black;padding:0cm 0cm 0cm 0cm;height:12.65pt'>
  <p class=TableParagraph style='margin-top:1.5pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:1.2pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:8.5pt'>&nbsp;Telefone:</span></b></p>
  </td>
  <td width=265 valign=top style='width:199.05pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:12.65pt'>
  <p class=TableParagraph><span lang=PT style='font-size:8.5pt'>&nbsp;<?php echo $dados[0]->usuario_telefone; ?></span></p>
  </td>
  <td width=66 valign=top style='width:49.75pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:12.65pt'>
  <p class=TableParagraph style='margin-top:1.5pt'><b><span lang=PT
  style='font-size:8.5pt'>&nbsp;E-mail:</span></b></p>
  </td>
  <td width=264 valign=top style='width:197.95pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:12.65pt'>
  <p class=TableParagraph style='margin-left:1.6pt'><span lang=PT
  style='font-size:8.5pt'>&nbsp;<?php echo $dados[0]->usuario_email; ?></span></p>
  </td>
 </tr>
</table>

<p class=MsoBodyText style='margin-top:.1pt'><span lang=PT style='font-family:
"Times New Roman","serif";font-weight:normal'>&nbsp;</span></p>

<!-- <table class=TableNormal border=1 cellspacing=0 cellpadding=0 style='margin-left:
 6.75pt;border-collapse:collapse;border:1px solid black'> -->
 <table class=TableNormal border=1 cellspacing=0 cellpadding=0 style='margin-left:
 6.7pt;border-collapse:collapse;border:1px solid black'>

<!--  <tr style='height:18.65pt'>
  <td width=675 colspan=4 valign=top style='width:505.95pt;border:solid windowtext 1.5pt;
  border-bottom:solid windowtext 1.0pt;background:#ECF0F5;padding:0cm 0cm 0cm 0cm;
  height:18.65pt'>
  <p class=TableParagraph style='margin-top:3.75pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.1pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:9.0pt'>&nbsp;BOLETOS</span></b></p>
  </td>
 </tr> -->

  <tr style='height:12.65pt'>
  <td width=675 colspan=4 valign=top style='width:506.05pt;border:solid windowtext 1.5pt;
  background:#ECF0F5;padding:0cm 0cm 0cm 0cm;height:12.65pt'>
  <p class=TableParagraph style='margin-top:1.15pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:1.2pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:8.5pt'>&nbsp;CARTÃO</span></b></p>
  </td>
 </tr>



 <tr style='height:19.0pt'>
  <td width=541 valign=top style='width:405.45pt;border:solid windowtext 1.5pt;
  border-top:1px solid black;padding:0cm 0cm 0cm 0cm;height:19.0pt'>
  <p class=TableParagraph style='margin-top:4.1pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.1pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:9.0pt'>&nbsp;Código<span style='letter-spacing:.55pt'> </span>Cartão</span></b></p>
  </td>
  <td width=134 valign=top style='width:100.5pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:19.0pt'>
  <p class=TableParagraph style='margin-top:4.1pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.5pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:9.0pt'>&nbsp;Total Parcela</span></b></p>
  </td>
  <td width=134 valign=top style='width:100.5pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:19.0pt'>
  <p class=TableParagraph style='margin-top:4.1pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.5pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:9.0pt'>&nbsp;Valor Transação</span></b></p>
  </td>
  <td width=134 valign=top style='width:100.5pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:19.0pt'>
  <p class=TableParagraph style='margin-top:4.1pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.5pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:9.0pt'>&nbsp;Valor Cartão</span></b></p>
  </td>
 </tr>
 <?php foreach ($dados as $d) { ?>
 <tr style='height:19.0pt'>
  <td width=541 valign=top style='width:405.45pt;border:solid windowtext 1.5pt;
  border-top:1px solid black;padding:0cm 0cm 0cm 0cm;height:19.0pt'>
  <p class=TableParagraph style='margin-top:4.05pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.1pt;margin-bottom:.0001pt'><span lang=PT style='font-size:
  9.0pt'>&nbsp;<?php echo $d->itens_operacao_numero_boleto; ?></span></p>
  </td>
  <td width=134 valign=top style='width:100.5pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:19.0pt'>
  <p class=TableParagraph style='margin-top:4.05pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.5pt;margin-bottom:.0001pt'><span lang=PT style='font-size:
  9.0pt'>&nbsp;
  <?php 
  echo $d->itens_operacao_numero_parcela == 'debito' 
  ? 'Debito ' . $d->itens_operacao_valor_transacao 
  : ($d->itens_operacao_numero_parcela == 'pix' 
      ? 'PIX ' . $d->itens_operacao_valor_transacao 
      : intval($d->itens_operacao_numero_parcela) . 'X ' . number_format(($d->itens_operacao_valor_transacao / intval($d->itens_operacao_numero_parcela)), 2, ',', '.'));
?></span></p>
  </td>
  <td width=134 valign=top style='width:100.5pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:19.0pt'>
  <p class=TableParagraph style='margin-top:4.05pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.5pt;margin-bottom:.0001pt'><span lang=PT style='font-size:
  9.0pt'>&nbsp;<?php echo number_format($d->itens_operacao_valor_transacao, 2, ',', '.'); ?></span></p>
  </td>
  <td width=134 valign=top style='width:100.5pt;border-top:1px solid black;border-left:
  1px solid black;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;
  padding:0cm 0cm 0cm 0cm;height:19.0pt'>
  <p class=TableParagraph style='margin-top:4.05pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.5pt;margin-bottom:.0001pt'><span lang=PT style='font-size:
  9.0pt'>&nbsp;<?php echo number_format($d->itens_operacao_valor_boleto, 2, ',', '.'); ?></span></p>
  </td>
 </tr>
 <?php } ?>
</table>

<p class=MsoBodyText><span lang=PT style='font-size:7.0pt;font-family:"Times New Roman","serif";
font-weight:normal'>&nbsp;</span></p>

<p class=MsoBodyText><span lang=PT style='font-size:7.0pt;font-family:"Times New Roman","serif";
font-weight:normal'>&nbsp;</span></p>

<table class=MsoNormalTable cellspacing=0 cellpadding=0  style='margin-left:5.8pt;'>
<tr style='height:19.0pt'>
  <td  width=690 colspan=2></td>
  <td width=134 valign=top style='width:100.5pt;
  padding:0cm 0cm 0cm 0cm;height:19.0pt'>
  <p class=TableParagraph style='margin-top:4.1pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.5pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:13.0pt; font-weight:bold;'>Total Transação</span></b></p>
  </td>
  <td width=134 valign=top style='width:100.5pt;
  padding:0cm 0cm 0cm 0cm;height:19.0pt'>
  <p class=TableParagraph style='margin-top:4.1pt;margin-right:0cm;margin-bottom:
  0cm;margin-left:4.5pt;margin-bottom:.0001pt'><b><span lang=PT
  style='font-size:13.0pt'>Total Cartão</span></b></p>
  </td>
  </tr>
 <tr>
  <td  width=690 colspan=2></td>
  <td width=134 valign=top style='width:100.5pt;
    padding:0cm 0cm 0cm 0cm;height:19.0pt'>
    <p class=TableParagraph style='margin-top:4.05pt;margin-right:0cm;margin-bottom:
    0cm;margin-left:4.5pt;margin-bottom:.0001pt'><span lang=PT style='font-size:
    15.0pt; font-weight:bold;'><?php echo number_format($dados[0]->operacao_total_transacao, 2, ',', '.'); ?></span></p>
    </td>
    <td width=134 valign=top style='width:100.5pt;
    padding:0cm 0cm 0cm 0cm;height:19.0pt'>
    <p class=TableParagraph style='margin-top:4.05pt;margin-right:0cm;margin-bottom:
    0cm;margin-left:4.5pt;margin-bottom:.0001pt'><span lang=PT style='font-size:
    15.0pt'><?php echo number_format($dados[0]->operacao_total_boleto, 2, ',', '.'); ?></span></p>
    </td>
 </tr>
</table>

<!-- <p class=MsoBodyText><span lang=PT style='font-size:10.0pt;font-family:"Times New Roman","serif";
font-weight:normal'>&nbsp;</span></p>

<p class=MsoBodyText><span lang=PT style='font-size:10.0pt;font-family:"Times New Roman","serif";
font-weight:normal'>&nbsp;</span></p> -->

<p class=MsoBodyText style='margin-top:.2pt'><span lang=PT style='font-size:
8.5pt;font-family:"Times New Roman","serif";font-weight:normal'>   </span></p> 

<table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 align=left
 style='border-collapse:collapse;border:1px solid black;margin-left:4.8pt;margin-right:
 4.8pt'>
 <tr style='height:16.5pt'>
  <td width=212 valign=top style='width:159.0pt;border:solid windowtext 1.5pt;
  background:rgb(236, 240, 245);padding:0cm 3.5pt 0cm 3.5pt;height:16.5pt'>
  <p class=MsoNormal align=center style='margin-top:.9pt;margin-right:0cm;
  margin-bottom:0cm;margin-left:.85pt;margin-bottom:.0001pt;text-align:center'><b><span
  lang=PT style='font-size:9.0pt'>CÓDIGO<span style='letter-spacing:-.25pt'> </span>DE<span
  style='letter-spacing:-.2pt'> </span>AUTORIZAÇÃO</span></b></p>
  </td>
 </tr>
 <?php foreach ($autorizacao as $a) { ?>
  <tr style='height:16.5pt'>
      <td width=212 valign=top style='width:159.0pt;border:solid windowtext 1.5pt;
      border-top:1px solid black;padding:0cm 3.5pt 0cm 3.5pt;height:16.5pt'>
      <p class=MsoBodyText align=center style='text-align:center'><span lang=PT
      style='font-size:10.0pt;font-weight:normal'><?php echo $a->autorizacao_operacao_numero_nsu; ?></span></p>
      </td>
  </tr>
 <?php } ?>
</table>

<p class=MsoBodyText><span lang=PT style='font-size:10.0pt;font-weight:normal'>&nbsp;</span></p>

<p class=MsoBodyText style='margin-top:.1pt'><span lang=PT style='font-size:
11.0pt;font-weight:normal'>&nbsp;</span></p>

<p class=MsoBodyText style='margin-top:.3pt'><span lang=PT style='font-size:
4.0pt;font-weight:normal'>&nbsp;</span></p>

<p class=MsoBodyText style='margin-top:4.95pt;margin-right:6.6pt;margin-bottom:
0cm;margin-left:6.9pt;margin-bottom:.0001pt;text-align:justify;line-height:
100%'><span lang=PT>Declaro para os devidos fins que as informações acima
fornecidas por mim são verdadeiras e assumo total<span style='letter-spacing:
.05pt'> </span>responsabilidade<span style='letter-spacing:.3pt'> </span>junto<span
style='letter-spacing:.35pt'> </span>ao(s)<span style='letter-spacing:.3pt'> </span>CEDENTE(s)<span
style='letter-spacing:.35pt'> </span>pelos<span style='letter-spacing:.3pt'> </span>dados<span
style='letter-spacing:.35pt'> </span>contidos<span style='letter-spacing:.3pt'>
</span>no(s)<span style='letter-spacing:.35pt'> </span>boleto(s)<span
style='letter-spacing:.35pt'> </span>aqui<span style='letter-spacing:.3pt'> </span>apresentador(s).</span></p>

<p class=MsoBodyText style='margin-top:3.35pt;margin-right:6.4pt;margin-bottom:
0cm;margin-left:6.9pt;margin-bottom:.0001pt;text-align:justify;line-height:
100%'><span lang=PT>Estou<span style='letter-spacing:.05pt'> </span>ciente,<span
style='letter-spacing:.05pt'> </span>também,<span style='letter-spacing:.05pt'>
</span>que<span style='letter-spacing:.05pt'> </span>o(s)<span
style='letter-spacing:.05pt'> </span>boleto(s)<span style='letter-spacing:.05pt'>
</span>aqui<span style='letter-spacing:.05pt'> </span>apresentado(s)<span
style='letter-spacing:.05pt'> </span>e<span style='letter-spacing:.05pt'> </span>com<span
style='letter-spacing:.05pt'> </span>dados<span style='letter-spacing:.05pt'> </span>acima<span
style='letter-spacing:.05pt'> </span>descritos,<span style='letter-spacing:
.05pt'> </span>será(ão)<span style='letter-spacing:.05pt'> </span>liquidado(s)<span
style='letter-spacing:.05pt'> </span>em<span style='letter-spacing:.05pt'> </span>ate<span
style='letter-spacing:.05pt'> </span>24<span style='letter-spacing:.05pt'> </span>horas<span
style='letter-spacing:.05pt'> </span>úteis.<span style='letter-spacing:.05pt'> </span>E<span
style='letter-spacing:.05pt'> </span>a<span style='letter-spacing:.05pt'> </span>comprovação<span
style='letter-spacing:.05pt'> </span>de<span style='letter-spacing:.05pt'> </span>sua<span
style='letter-spacing:.05pt'> </span>liquidação<span style='letter-spacing:
.05pt'> </span>deverá<span style='letter-spacing:.05pt'> </span>ser<span
style='letter-spacing:.05pt'> </span>consultada  junto  ao<span
style='letter-spacing:.05pt'> </span>CEDENTE<span style='letter-spacing:.6pt'> </span>em<span
style='letter-spacing:.6pt'> </span>até<span style='letter-spacing:.6pt'> </span>48<span
style='letter-spacing:.6pt'> </span>horas<span style='letter-spacing:.6pt'> </span>úteis.</span></p>

<p class=MsoBodyText style='margin-top:3.4pt;margin-right:108.15pt;margin-bottom:
0cm;margin-left:6.9pt;margin-bottom:.0001pt;line-height:230%'><span lang=PT><span
style='letter-spacing:.3pt'> </span>Anexo, também,<span style='letter-spacing:.35pt'> </span>uma<span
style='letter-spacing:.35pt'> </span>cópia<span style='letter-spacing:.3pt'> </span>do<span
style='letter-spacing:.35pt'> </span>meu<span style='letter-spacing:.35pt'> </span>RG<span
style='letter-spacing:.3pt'> </span>ou<span style='letter-spacing:.35pt'> </span>CNH<span
style='letter-spacing:.35pt'> </span>de<span style='letter-spacing:.3pt'> </span>livre<span
style='letter-spacing:.35pt'> </span>e<span style='letter-spacing:.35pt'> </span>espontânea<span
style='letter-spacing:.35pt'> </span>vontade.<span style='letter-spacing:-2.05pt'><br>
</span>Assinatura:</span>
</p>

<p class=MsoBodyText style='margin-top:7.9pt;margin-right:108.15pt;margin-bottom:
0cm;margin-left:6.9pt;margin-bottom:.0001pt;line-height:230%'>
<span lang=PT>________________________________________________________________________________<br>
<span style='letter-spacing:.05pt'> </span>Assinatura do Vendedor:</span></p>

<p class=MsoBodyText style='margin-top:7.9pt;margin-right:0cm;margin-bottom:
0cm;margin-left:6.9pt;margin-bottom:.0001pt'><span lang=PT>________________________________________________________________________________</span></p>

<p class=MsoBodyText><span lang=PT style='font-size:10.0pt'>&nbsp;</span></p>

<p class=MsoBodyText style='margin-top:.5pt'><span lang=PT style='font-size:
11.0pt'>&nbsp;</span></p>

<p class=MsoBodyText align=center style='margin-top:.05pt;text-align:center'><span
lang=PT>Olinda (PE), <?php

setlocale(LC_TIME, 'pt_BR', 'pt_BR.utf-8', 'pt_BR.utf-8', 'portuguese');
date_default_timezone_set('America/Sao_Paulo');
echo utf8_encode((strftime('%d de %B de %Y', strtotime('today'))));

?></span></p>

</div>

</body>

</html>
