<style type="text/css" media="all">
	@media print {
		.no-print { display: none; }
		#wrapper { max-width: 480px; width: 100%; min-width: 250px; margin: 0 auto; }
	}
</style>
<body>
<div class='container' style='max-width:300px;padding:5px 5px;'<table width="100%">
<tr>
<h2 style='text-align:center;font-size: 16px;margin-top:0px;margin-bottom:5px;'><i>Fechamento - Caixa - <?php echo $dataFech ?></i></h2>
<td colspan="2"><div style='font-size:8px;'><?php echo $emitente[0]->emitente_nome ?><br>CNPJ:<?php echo $emitente[0]->emitente_cnpj ?> I.E.:<?php echo $emitente[0]->emitente_ie ?><br><?php echo $emitente[0]->emitente_rua ?><br><?php echo $emitente[0]->emitente_bairro ?>, <?php echo $emitente[0]->emitente_cidade.' - '.$emitente[0]->emitente_uf?></div><hr></td>
</tr>
<tr>
<td colspan="3" class="menor"><div style='text-align:center;font-size:9px;'>Cupom - Documento auxiliar<br> da Nota de Consumidor</td>
</tr>
<tr>
<td colspan="3" class="menor"><br><b>Não permite aproveitamento de crédito de ICMS<b></td>
</tr>
</div></table>
<hr>
<table width='100%'>

<tr>
<td colspan="7" align="center"><div style='text-align:center; font-weight: bolder;'>FATURAMENTO</div></td>
</tr>	

<tr>
<td><div style='text-align:left;font-size:7px;'>DINHEIRO</div></td>
<td><div style='text-align:left;font-size:7px;'>PIX</div></td>
<td><div style='text-align:left;font-size:7px;'>CRED LOJ</div></td>
<td><div style='text-align:left;font-size:7px;'>CART BT</div></td>
<td><div style='text-align:left;font-size:7px;'>DESC CART</div></td>
<!-- <td><div style='text-align:left;font-size:7px;'>DESPESA</div></td> -->
<td><div style='text-align:right;font-size:7px;'>FATURAMENTO</div></td>
</tr>

<?php if(!$dados){ ?>

<tr>
    <td colspan="4"><STRONG>CAIXA SEM REGISTRO DO DIA</STRONG></td>
</tr>

</table>
<table width="100%">

<?php } else {

	foreach ($dados as $d){
?>
<tr>
<td align="left"><div style='text-align:left;font-size:7px;'><?php  echo '$ '. number_format(($d->dinheiro) ,2,",",".") ?></div></td>
<td align="left"><div style='text-align:left;font-size:7px;'><?php  echo '$ '. number_format($d->pix,2,",",".") ?></div></td>
<td align="left"><div style='text-align:left;font-size:7px;'><?php  echo '$ '. number_format($d->credLoja,2,",",".") ?></div></td>
<td align="left"><div style='text-align:left;font-size:7px;'><?php  echo '$ '. number_format($d->cartDC,2,",",".") ?></div></td>
<td align="left"><div style='text-align:left;font-size:7px;'><?php  echo '$ '. number_format($d->descCart,2,",",".") ?></div></td>
<!-- <td align="left"><div style='text-align:left;font-size:7px;'><?php  echo '$ '. number_format($d->despesa,2,",",".") ?></div></td> -->
<td align="right"><div style='text-align:right;font-size:7px;'><?php  echo '$ '. number_format($d->saldo,2,",",".") ?></div></td>
</tr>

</table>
<hr>
<table width="100%">

<tr>
<td colspan="7" align="center"><div style='text-align:center; font-weight: bolder;'>DESPESA</div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'>DESPESA (DINHEIRO)</div></td>
<td align="right"><div style='text-align:right;'><?php echo 'R$ '. number_format($d->despesaDinheiro,2,".",".") ?></div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'>DESPESA (PIX)</div></td>
<td align="right"><div style='text-align:right;'><?php echo 'R$ '. number_format($d->despesaPix,2,".",".") ?></div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'>CRED LOJA </div></td>
<td align="right"><div style='text-align:right;'><?php echo 'R$ '.  number_format('0.00',2,".",".") ?></div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'>DESPESA CARTÃO</div></td>
<td align="right"><div style='text-align:right;'><?php echo 'R$ '. number_format($d->despesaCartao,2,".",".") ?></div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'>TOTAL DESPESA </div></td>
<td align="right"><div style='text-align:right;'><?php echo 'R$ '. number_format($d->despesa,2,".",".") ?></div></td>
</tr>

<tr>
<td colspan="7" align="center"><div style='text-align:center; font-weight: bolder;'>RECEBIMENTO LIQUIDO</div></td>
</tr>
<tr>

<tr>
<td align="left"><div style='text-align:left;'>DINHEIRO RECEBIDO </div></td>
<td align="right"><div style='text-align:right;'><?php echo 'R$ '. number_format(($dados[0]->dinheiro - $d->despesaDinheiro),2,".",".")   ?></div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'>PIX </div></td>
<td align="right"><div style='text-align:right;'><?php echo 'R$ '. number_format(($d->pix - $d->despesaPix),2,".",".") ?></div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'>CRED LOJA </div></td>
<td align="right"><div style='text-align:right;'><?php echo 'R$ '.  number_format($d->credLoja,2,".",".") ?></div></td>
</tr>
<?php $liqCart = (($dados[0]->cartDC - $dados[0]->descCart) - $d->despesaCartao); ?>
<tr>
<td align="left"><div style='text-align:left;'>CARTÃO  </div></td>
<td align="right"><div style='text-align:right;'><?php echo 'R$ '.  number_format($liqCart,2,".",".") ?></div></td>
</tr>
<tr>
<td align="left"><div style='text-align:left;'>TOTAL RECEBIMENTO </div></td>
<td align="right"><div style='text-align:right;'><?php echo 'R$ '. number_format((($dados[0]->dinheiro - $d->despesaDinheiro)+($d->pix - $d->despesaPix)+($liqCart)+$d->credLoja),2,".",".") ?></div></td>
</tr>


<?php } }?>

<tr>
<td></td>
<td></td>
</tr>
<tr>
<td></td>
<td></td>
</tr>


<tr>
<td align="left"><div style='text-align:left;'><strong>....</strong></div></td>
<td align="right"><div style='text-align:right;'><strong></strong></div></td>
</tr>



<tr>
<td align="left"><div style='text-align:left;'>Fechamento Caixa <br>(Lei Federal 12.741/2012)</div></td>
<td align="right"><div style='text-align:right;'></div></td>
</tr>
</table>
<hr><table width="100%"'>
<tr>
<td  colspan="3"><div style='word-break: break-all;font-size:9px'>Consulte fechamento caixa detalhado no <br> menu -> financeiro <br /></div></td>
</tr>
</table>

<table width="80%" align="center">
<tr>
<td colspan="3"><b><?php echo $emitente[0]->emitente_nome ?></b></td>
</tr>
</table>

<table width="80%" align="center">
<tr>
<td>Cupom nº <?php echo '' ?></td>
<td>Série: 001</td>
<td><?php echo date("d/m/Y h:i:sa"); ?></td>
</tr>
<tr>
<td colspan="3"><b><?php echo $this->session->userdata('usuario_nome'); ?></b></td>
</tr>
<tr>
<td colspan="3">Protocolo de autorização: <?php echo '' ?><br />Data de autorização: <?php echo $dataFech ?> </td>
</tr>
</table>
<table width="100%">
<tr>
<td  colspan="3"><img src="https://chart.googleapis.com/chart?cht=qr&chs=300x300&chl=http%3A%2F%2Fwww.nfe.fazenda.gov.br%2Fportal%2Fconsulta.aspx%3FtipoConsulta%3Dcompleta%26tipoConteudo%3DXbSeqxE8pl8%3DMobLanche_PDVPARATODOS.COM.BR&choe=UTF-8&chld=L|4" style='max-width:150px;'></td>
</tr>
</table>
<table width="100%" class="no-print">
	<tr>
		<td colspan="3">
			<button
				style="border: 0; cursor: pointer; background: #367fa9; display: block; width: 100%; padding: 10px; text-align: center; text-transform: uppercase;color: #fff"
				href="javascript:window.print()"
				id="web_print" class="btn btn-block btn-primary"
				onClick="window.print();return false;">
				Impressão Web
			</button>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<button
				style="border: 0; cursor: pointer; background: #e08e0b; display: block; width: 100%; padding: 10px; text-align: center; text-transform: uppercase;color: #fff"
				href="javascript:window.print()"
				id="web_print" class="btn btn-block btn-primary"
				onClick="javascript:window.close();return false;">
				Fechar
			</button>
		</td>
	</tr>

</table>
<div style="clear:both;"></div>
<div class="col-xs-12 no-print" style="background:#F5F5F5; padding:10px;">
	<font size="-2">
		<p style="font-weight:bold;">Favor alterar as configurações de impressão de seu browser</p>
		<p style="text-transform: capitalize;"><strong>FireFox:</strong> Arquivo &gt; Configurar impressora &gt; Margem &amp;Cabeçalho/Rodapé --Nenhum--</p>
		<p style="text-transform: capitalize;"><strong>Chrome:</strong> Menu &gt; Impressora &gt; Disabilitar Cabeçalho/Rodapé Opções &amp; Setar margem em branco</p></div>
<font>
	<div style="clear:both;"></div>
</div>
</body>
</html>
