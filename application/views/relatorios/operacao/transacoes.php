<div class="panel panel-flat col-md-3">
    <div class="panel-heading">
        <h5 class="panel-title text-center">Relatórios Rápidos</h5>							
    </div>
	<div class="category-content">
		<div class="row row-condensed">
			<div class="col-xs-12">
				<a href="<?php echo base_url();?>relatorios/transacoesPersonalizada/<?php echo date('m'); ?>" class="btn bg-teal-400 btn-block btn-float btn-float-lg" ><i class="icon-file-text2"></i> <span>Transações Mês Atual</span></a>
				<a href="<?php echo base_url();?>relatorios/transacoesPersonalizada/<?php echo date('m')-1; ?>" class="btn bg-teal-400 btn-block btn-float btn-float-lg" ><i class="icon-file-text2"></i> <span>Transações Mês Anterior</span></a>
			</div>
		</div>
	</div>
</div>
<!-- Form horizontal -->
<div class="col-md-9">
    <div class="panel panel-flat col-md-12">
        <div class="panel-heading">
            <h5 class="panel-title text-center">Relatórios Personalizados</h5>							
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="<?php echo base_url();?>relatorios/transacoesPersonalizada/" method="post">
                <div class="form-group">
                    <label class="control-label col-lg-2">Ano:</label>
                    <div class="col-lg-4">																						
                        <input type="text" class="form-control daterange-single" name="anoInicio" id="anoInicio" value="">
                    </div>												
                </div>											
                <div class="text-right">
                    <button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="panel panel-flat col-md-12">
        <div class="panel-heading">
            <h5 class="panel-title text-center">Relatórios Personalizados Período</h5>							
        </div>
        <div class="panel-body">
            <form class="form-horizontal" action="<?php echo base_url();?>relatorios/transacoesPersonalizadaPeriodo/" method="post">
                <div class="form-group">
                    <label class="control-label col-lg-2">Início:</label>
                    <div class="col-lg-4">																						
                        <input type="text" class="form-control daterange-single" name="anoInicioPeriodo" id="anoInicioPeriodo" value="">
                    </div>	
                    <label class="control-label col-lg-2">Fim:</label>
                    <div class="col-lg-4">																						
                        <input type="text" class="form-control daterange-single" name="anoFimPeriodo" id="anoFimPeriodo" value="">
                    </div>	
                </div>										
                <div class="text-right">
                    <button type="submit" class="btn bg-teal-400">Imprimir<i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/jquery.mask.min.js"></script>
<script>

    $(document).ready(function(){

        $("#anoInicioPeriodo").mask('00/00/0000', {placeholder: '00/00/0000'});
        $("#anoFimPeriodo").mask('00/00/0000', {placeholder: '00/00/0000'});
       
    });
</script>