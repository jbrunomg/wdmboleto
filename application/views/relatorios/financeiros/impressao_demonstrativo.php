	<?php 
//echo "<pre>";
//	var_dump($financeiro);die(); 
	?>
	<table border="1">
		<thead>
			<?php if($tipo ==  'recrutamento' || $tipo == 'ambos'){ ?>	
			
			<tr >
				<th style="background: #CECBCB;text-align: center" colspan="14">RECRUTAMENTO E SELEÇÃO</th>			
			</tr>
			<tr >
				<th style="background: #625A5A;" colspan="14">&nbsp;</th>			
			</tr>
			<tr style="text-align: center">
				<th style="background: #CECBCB;">ATIVIDADES</th>
				<th style="background: #CECBCB;">JANEIRO</th>
				<th style="background: #CECBCB;">FEVEREIRO</th>
				<th style="background: #CECBCB;">MARÇO</th>
				<th style="background: #CECBCB;">ABRIL</th>
				<th style="background: #CECBCB;">MAIO</th>
				<th style="background: #CECBCB;">JUNHO</th>
				<th style="background: #CECBCB;">JULHO</th>
				<th style="background: #CECBCB;">AGOSTO</th>
				<th style="background: #CECBCB;">SETEMBRO</th>
				<th style="background: #CECBCB;">OUTUBRO</th>
				<th style="background: #CECBCB;">NOVEMBRO</th>
				<th style="background: #CECBCB;">DEZEMBRO</th>
				<th style="background: #CECBCB;">TOTAL</th>
			</tr>
		</thead>
		<tbody>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">ESTUDANTES INSCRITOS</td>
				<td><?php echo isset($recrutamento[1]->demonstrativorecrut_estud_inscr)?$recrutamento[1]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[2]->demonstrativorecrut_estud_inscr)?$recrutamento[2]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[3]->demonstrativorecrut_estud_inscr)?$recrutamento[3]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[4]->demonstrativorecrut_estud_inscr)?$recrutamento[4]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[5]->demonstrativorecrut_estud_inscr)?$recrutamento[5]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[6]->demonstrativorecrut_estud_inscr)?$recrutamento[6]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[7]->demonstrativorecrut_estud_inscr)?$recrutamento[7]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[8]->demonstrativorecrut_estud_inscr)?$recrutamento[8]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[9]->demonstrativorecrut_estud_inscr)?$recrutamento[9]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[10]->demonstrativorecrut_estud_inscr)?$recrutamento[10]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[11]->demonstrativorecrut_estud_inscr)?$recrutamento[11]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td><?php echo isset($recrutamento[12]->demonstrativorecrut_estud_inscr)?$recrutamento[12]->demonstrativorecrut_estud_inscr:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">ESTUDANTES ENCAMINHADOS</td>
				<td><?php echo isset($recrutamento[1]->demonstrativorecrut_estud_encamin)?$recrutamento[1]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[2]->demonstrativorecrut_estud_encamin)?$recrutamento[2]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[3]->demonstrativorecrut_estud_encamin)?$recrutamento[3]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[4]->demonstrativorecrut_estud_encamin)?$recrutamento[4]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[5]->demonstrativorecrut_estud_encamin)?$recrutamento[5]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[6]->demonstrativorecrut_estud_encamin)?$recrutamento[6]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[7]->demonstrativorecrut_estud_encamin)?$recrutamento[7]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[8]->demonstrativorecrut_estud_encamin)?$recrutamento[8]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[9]->demonstrativorecrut_estud_encamin)?$recrutamento[9]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[10]->demonstrativorecrut_estud_encamin)?$recrutamento[10]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[11]->demonstrativorecrut_estud_encamin)?$recrutamento[11]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td><?php echo isset($recrutamento[12]->demonstrativorecrut_estud_encamin)?$recrutamento[12]->demonstrativorecrut_estud_encamin:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">ESTUDANTES SELECIONADOS</td>
				<td><?php echo isset($recrutamento[1]->demonstrativorecrut_estud_selec)?$recrutamento[1]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[2]->demonstrativorecrut_estud_selec)?$recrutamento[2]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[3]->demonstrativorecrut_estud_selec)?$recrutamento[3]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[4]->demonstrativorecrut_estud_selec)?$recrutamento[4]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[5]->demonstrativorecrut_estud_selec)?$recrutamento[5]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[6]->demonstrativorecrut_estud_selec)?$recrutamento[6]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[7]->demonstrativorecrut_estud_selec)?$recrutamento[7]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[8]->demonstrativorecrut_estud_selec)?$recrutamento[8]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[9]->demonstrativorecrut_estud_selec)?$recrutamento[9]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[10]->demonstrativorecrut_estud_selec)?$recrutamento[10]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[11]->demonstrativorecrut_estud_selec)?$recrutamento[11]->demonstrativorecrut_estud_selec:0; ?></td>
				<td><?php echo isset($recrutamento[12]->demonstrativorecrut_estud_selec)?$recrutamento[12]->demonstrativorecrut_estud_selec:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">VAGAS PREENCHIDAS</td>
				<td><?php echo isset($recrutamento[1]->demonstrativorecrut_vagas_pree)?$recrutamento[1]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[2]->demonstrativorecrut_vagas_pree)?$recrutamento[2]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[3]->demonstrativorecrut_vagas_pree)?$recrutamento[3]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[4]->demonstrativorecrut_vagas_pree)?$recrutamento[4]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[5]->demonstrativorecrut_vagas_pree)?$recrutamento[5]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[6]->demonstrativorecrut_vagas_pree)?$recrutamento[6]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[7]->demonstrativorecrut_vagas_pree)?$recrutamento[7]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[8]->demonstrativorecrut_vagas_pree)?$recrutamento[8]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[9]->demonstrativorecrut_vagas_pree)?$recrutamento[9]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[10]->demonstrativorecrut_vagas_pree)?$recrutamento[10]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[11]->demonstrativorecrut_vagas_pree)?$recrutamento[11]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td><?php echo isset($recrutamento[12]->demonstrativorecrut_vagas_pree)?$recrutamento[12]->demonstrativorecrut_vagas_pree:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">VAGAS CANCELADAS</td>
				<td><?php echo isset($recrutamento[1]->demonstrativorecrut_vagas_canc)?$recrutamento[1]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[2]->demonstrativorecrut_vagas_canc)?$recrutamento[2]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[3]->demonstrativorecrut_vagas_canc)?$recrutamento[3]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[4]->demonstrativorecrut_vagas_canc)?$recrutamento[4]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[5]->demonstrativorecrut_vagas_canc)?$recrutamento[5]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[6]->demonstrativorecrut_vagas_canc)?$recrutamento[6]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[7]->demonstrativorecrut_vagas_canc)?$recrutamento[7]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[8]->demonstrativorecrut_vagas_canc)?$recrutamento[8]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[9]->demonstrativorecrut_vagas_canc)?$recrutamento[9]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[10]->demonstrativorecrut_vagas_canc)?$recrutamento[10]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[11]->demonstrativorecrut_vagas_canc)?$recrutamento[11]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td><?php echo isset($recrutamento[12]->demonstrativorecrut_vagas_canc)?$recrutamento[12]->demonstrativorecrut_vagas_canc:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">VAGAS SUSPENSAS</td>
				<td><?php echo isset($recrutamento[1]->demonstrativorecrut_vagas_susp)?$recrutamento[1]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[2]->demonstrativorecrut_vagas_susp)?$recrutamento[2]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[3]->demonstrativorecrut_vagas_susp)?$recrutamento[3]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[4]->demonstrativorecrut_vagas_susp)?$recrutamento[4]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[5]->demonstrativorecrut_vagas_susp)?$recrutamento[5]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[6]->demonstrativorecrut_vagas_susp)?$recrutamento[6]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[7]->demonstrativorecrut_vagas_susp)?$recrutamento[7]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[8]->demonstrativorecrut_vagas_susp)?$recrutamento[8]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[9]->demonstrativorecrut_vagas_susp)?$recrutamento[9]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[10]->demonstrativorecrut_vagas_susp)?$recrutamento[10]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[11]->demonstrativorecrut_vagas_susp)?$recrutamento[11]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td><?php echo isset($recrutamento[12]->demonstrativorecrut_vagas_susp)?$recrutamento[12]->demonstrativorecrut_vagas_susp:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<th colspan="14">&nbsp;</th>			
			</tr>
			<?php } ?>


			<?php if($tipo ==  'administrador' || $tipo == 'ambos'){ ?>	

			<tr >
				<th style="background: #CECBCB;text-align: center" colspan="14">SETOR ADMINISTRATIVO</th>			
			</tr>
			<tr >
				<th style="background: #625A5A;" colspan="14">&nbsp;</th>			
			</tr>
			<tr style="text-align: center">
				<th style="background: #CECBCB;">ATIVIDADES</th>
				<th style="background: #CECBCB;">JANEIRO</th>
				<th style="background: #CECBCB;">FEVEREIRO</th>
				<th style="background: #CECBCB;">MARÇO</th>
				<th style="background: #CECBCB;">ABRIL</th>
				<th style="background: #CECBCB;">MAIO</th>
				<th style="background: #CECBCB;">JUNHO</th>
				<th style="background: #CECBCB;">JULHO</th>
				<th style="background: #CECBCB;">AGOSTO</th>
				<th style="background: #CECBCB;">SETEMBRO</th>
				<th style="background: #CECBCB;">OUTUBRO</th>
				<th style="background: #CECBCB;">NOVEMBRO</th>
				<th style="background: #CECBCB;">DEZEMBRO</th>
				<th style="background: #CECBCB;">TOTAL</th>
			</tr>
		</thead>
		<tbody>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">ESTAGIÁRIOS CONTRATADOS MÊS (TCEs feitos)</td>
				<td><?php echo isset($administrador[1]->demonstrativoadm_contratados)?$administrador[1]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[2]->demonstrativoadm_contratados)?$administrador[2]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[3]->demonstrativoadm_contratados)?$administrador[3]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[4]->demonstrativoadm_contratados)?$administrador[4]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[5]->demonstrativoadm_contratados)?$administrador[5]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[6]->demonstrativoadm_contratados)?$administrador[6]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[7]->demonstrativoadm_contratados)?$administrador[7]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[8]->demonstrativoadm_contratados)?$administrador[8]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[9]->demonstrativoadm_contratados)?$administrador[9]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[10]->demonstrativoadm_contratados)?$administrador[10]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[11]->demonstrativoadm_contratados)?$administrador[11]->demonstrativoadm_contratados:0; ?></td>
				<td><?php echo isset($administrador[12]->demonstrativoadm_contratados)?$administrador[12]->demonstrativoadm_contratados:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">ESTAGIÁRIOS COBRADOS MÊS</td>
				<td><?php echo isset($administrador[1]->demonstrativoadm_estag_cabrados)?$administrador[1]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[2]->demonstrativoadm_estag_cabrados)?$administrador[2]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[3]->demonstrativoadm_estag_cabrados)?$administrador[3]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[4]->demonstrativoadm_estag_cabrados)?$administrador[4]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[5]->demonstrativoadm_estag_cabrados)?$administrador[5]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[6]->demonstrativoadm_estag_cabrados)?$administrador[6]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[7]->demonstrativoadm_estag_cabrados)?$administrador[7]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[8]->demonstrativoadm_estag_cabrados)?$administrador[8]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[9]->demonstrativoadm_estag_cabrados)?$administrador[9]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[10]->demonstrativoadm_estag_cabrados)?$administrador[10]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[11]->demonstrativoadm_estag_cabrados)?$administrador[11]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td><?php echo isset($administrador[12]->demonstrativoadm_estag_cabrados)?$administrador[12]->demonstrativoadm_estag_cabrados:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">EMPRESAS ATIVAS</td>
				<td><?php echo isset($administrador[1]->demonstrativoadm_empr_ativas)?$administrador[1]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[2]->demonstrativoadm_empr_ativas)?$administrador[2]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[3]->demonstrativoadm_empr_ativas)?$administrador[3]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[4]->demonstrativoadm_empr_ativas)?$administrador[4]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[5]->demonstrativoadm_empr_ativas)?$administrador[5]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[6]->demonstrativoadm_empr_ativas)?$administrador[6]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[7]->demonstrativoadm_empr_ativas)?$administrador[7]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[8]->demonstrativoadm_empr_ativas)?$administrador[8]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[9]->demonstrativoadm_empr_ativas)?$administrador[9]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[10]->demonstrativoadm_empr_ativas)?$administrador[10]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[11]->demonstrativoadm_empr_ativas)?$administrador[11]->demonstrativoadm_empr_ativas:0; ?></td>
				<td><?php echo isset($administrador[12]->demonstrativoadm_empr_ativas)?$administrador[12]->demonstrativoadm_empr_ativas:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">ESTAGIÁRIOS ATIVOS</td>
				<td><?php echo isset($administrador[1]->demonstrativoadm_estag_ativos)?$administrador[1]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[2]->demonstrativoadm_estag_ativos)?$administrador[2]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[3]->demonstrativoadm_estag_ativos)?$administrador[3]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[4]->demonstrativoadm_estag_ativos)?$administrador[4]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[5]->demonstrativoadm_estag_ativos)?$administrador[5]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[6]->demonstrativoadm_estag_ativos)?$administrador[6]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[7]->demonstrativoadm_estag_ativos)?$administrador[7]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[8]->demonstrativoadm_estag_ativos)?$administrador[8]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[9]->demonstrativoadm_estag_ativos)?$administrador[9]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[10]->demonstrativoadm_estag_ativos)?$administrador[10]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[11]->demonstrativoadm_estag_ativos)?$administrador[11]->demonstrativoadm_estag_ativos:0; ?></td>
				<td><?php echo isset($administrador[12]->demonstrativoadm_estag_ativos)?$administrador[12]->demonstrativoadm_estag_ativos:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">ESTAGIÁRIOS RESCINDIDOS</td>
				<td><?php echo isset($administrador[1]->demonstrativoadm_estag_rescindidos)?$administrador[1]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[2]->demonstrativoadm_estag_rescindidos)?$administrador[2]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[3]->demonstrativoadm_estag_rescindidos)?$administrador[3]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[4]->demonstrativoadm_estag_rescindidos)?$administrador[4]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[5]->demonstrativoadm_estag_rescindidos)?$administrador[5]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[6]->demonstrativoadm_estag_rescindidos)?$administrador[6]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[7]->demonstrativoadm_estag_rescindidos)?$administrador[7]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[8]->demonstrativoadm_estag_rescindidos)?$administrador[8]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[9]->demonstrativoadm_estag_rescindidos)?$administrador[9]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[10]->demonstrativoadm_estag_rescindidos)?$administrador[10]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[11]->demonstrativoadm_estag_rescindidos)?$administrador[11]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td><?php echo isset($administrador[12]->demonstrativoadm_estag_rescindidos)?$administrador[12]->demonstrativoadm_estag_rescindidos:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<th colspan="14">&nbsp;</th>			
			</tr>
			<?php } ?>
			<?php if($tipo ==  'financeiro' || $tipo == 'ambos'){ ?>	
			<tr >
				<th style="background: #CECBCB;text-align: center" colspan="14">SETOR FINANCEIRO</th>			
			</tr>
			<tr >
				<th style="background: #625A5A;" colspan="14">&nbsp;</th>			
			</tr>
			<tr style="text-align: center">
				<th style="background: #CECBCB;">ATIVIDADES</th>
				<th style="background: #CECBCB;">JANEIRO</th>
				<th style="background: #CECBCB;">FEVEREIRO</th>
				<th style="background: #CECBCB;">MARÇO</th>
				<th style="background: #CECBCB;">ABRIL</th>
				<th style="background: #CECBCB;">MAIO</th>
				<th style="background: #CECBCB;">JUNHO</th>
				<th style="background: #CECBCB;">JULHO</th>
				<th style="background: #CECBCB;">AGOSTO</th>
				<th style="background: #CECBCB;">SETEMBRO</th>
				<th style="background: #CECBCB;">OUTUBRO</th>
				<th style="background: #CECBCB;">NOVEMBRO</th>
				<th style="background: #CECBCB;">DEZEMBRO</th>
				<th style="background: #CECBCB;">TOTAL</th>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">FATURAMENTO</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_faturamento)?$financeiro[1]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_faturamento)?$financeiro[2]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_faturamento)?$financeiro[3]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_faturamento)?$financeiro[4]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_faturamento)?$financeiro[5]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_faturamento)?$financeiro[6]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_faturamento)?$financeiro[7]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_faturamento)?$financeiro[8]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_faturamento)?$financeiro[9]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_faturamento)?$financeiro[10]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_faturamento)?$financeiro[11]->demonstrativofin_faturamento:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_faturamento)?$financeiro[12]->demonstrativofin_faturamento:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">CRÉDITO</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_credito)?$financeiro[1]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_credito)?$financeiro[2]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_credito)?$financeiro[3]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_credito)?$financeiro[4]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_credito)?$financeiro[5]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_credito)?$financeiro[6]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_credito)?$financeiro[7]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_credito)?$financeiro[8]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_credito)?$financeiro[9]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_credito)?$financeiro[10]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_credito)?$financeiro[11]->demonstrativofin_credito:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_credito)?$financeiro[12]->demonstrativofin_credito:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">DÉBITO / despesas do mês</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_debito)?$financeiro[1]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_debito)?$financeiro[2]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_debito)?$financeiro[3]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_debito)?$financeiro[4]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_debito)?$financeiro[5]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_debito)?$financeiro[6]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_debito)?$financeiro[7]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_debito)?$financeiro[8]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_debito)?$financeiro[9]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_debito)?$financeiro[10]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_debito)?$financeiro[11]->demonstrativofin_debito:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_debito)?$financeiro[12]->demonstrativofin_debito:0; ?></td>
				<td></td>
			</tr>
			<tr style="color: #2CB6E8;text-align: center;">
				<td style="background: #2CB6E8;color: #000;text-align: left;">LUCRO / PREJUÍZO</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_lucro_prejuizo)?$financeiro[1]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_lucro_prejuizo)?$financeiro[2]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_lucro_prejuizo)?$financeiro[3]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_lucro_prejuizo)?$financeiro[4]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_lucro_prejuizo)?$financeiro[5]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_lucro_prejuizo)?$financeiro[6]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_lucro_prejuizo)?$financeiro[7]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_lucro_prejuizo)?$financeiro[8]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_lucro_prejuizo)?$financeiro[9]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_lucro_prejuizo)?$financeiro[10]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_lucro_prejuizo)?$financeiro[11]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_lucro_prejuizo)?$financeiro[12]->demonstrativofin_lucro_prejuizo:0; ?></td>
				<td></td>
			</tr>	
			<tr style="text-align: center">
				<th colspan="14">&nbsp;</th>			
			</tr>

			<tr style="text-align: center">
				<th style="background: #CECBCB;">CAIXA - CONTA CORRENTE</th>
				<th style="background: #CECBCB;">JANEIRO</th>
				<th style="background: #CECBCB;">FEVEREIRO</th>
				<th style="background: #CECBCB;">MARÇO</th>
				<th style="background: #CECBCB;">ABRIL</th>
				<th style="background: #CECBCB;">MAIO</th>
				<th style="background: #CECBCB;">JUNHO</th>
				<th style="background: #CECBCB;">JULHO</th>
				<th style="background: #CECBCB;">AGOSTO</th>
				<th style="background: #CECBCB;">SETEMBRO</th>
				<th style="background: #CECBCB;">OUTUBRO</th>
				<th style="background: #CECBCB;">NOVEMBRO</th>
				<th style="background: #CECBCB;">DEZEMBRO</th>
				<th style="background: #CECBCB;">TOTAL</th>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">DESCONTOS/ SERVIÇOS</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_cc_desc_serv)?$financeiro[1]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_cc_desc_serv)?$financeiro[2]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_cc_desc_serv)?$financeiro[3]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_cc_desc_serv)?$financeiro[4]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_cc_desc_serv)?$financeiro[5]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_cc_desc_serv)?$financeiro[6]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_cc_desc_serv)?$financeiro[7]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_cc_desc_serv)?$financeiro[8]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_cc_desc_serv)?$financeiro[9]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_cc_desc_serv)?$financeiro[10]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_cc_desc_serv)?$financeiro[11]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_cc_desc_serv)?$financeiro[12]->demonstrativofin_cc_desc_serv:0; ?></td>
				<td></td>
			</tr>

			<tr style="text-align: center">
				<th colspan="14">&nbsp;</th>			
			</tr>

			<tr >
				<th style="background: #CECBCB;">CAIXA - CONTA POUPANÇA</th>
				<th style="background: #CECBCB;">JANEIRO</th>
				<th style="background: #CECBCB;">FEVEREIRO</th>
				<th style="background: #CECBCB;">MARÇO</th>
				<th style="background: #CECBCB;">ABRIL</th>
				<th style="background: #CECBCB;">MAIO</th>
				<th style="background: #CECBCB;">JUNHO</th>
				<th style="background: #CECBCB;">JULHO</th>
				<th style="background: #CECBCB;">AGOSTO</th>
				<th style="background: #CECBCB;">SETEMBRO</th>
				<th style="background: #CECBCB;">OUTUBRO</th>
				<th style="background: #CECBCB;">NOVEMBRO</th>
				<th style="background: #CECBCB;">DEZEMBRO</th>
				<th style="background: #CECBCB;">TOTAL</th>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">SALDO INICIAL</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_cp_saldo_inicial)?$financeiro[1]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_cp_saldo_inicial)?$financeiro[2]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_cp_saldo_inicial)?$financeiro[3]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_cp_saldo_inicial)?$financeiro[4]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_cp_saldo_inicial)?$financeiro[5]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_cp_saldo_inicial)?$financeiro[6]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_cp_saldo_inicial)?$financeiro[7]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_cp_saldo_inicial)?$financeiro[8]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_cp_saldo_inicial)?$financeiro[9]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_cp_saldo_inicial)?$financeiro[10]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_cp_saldo_inicial)?$financeiro[11]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_cp_saldo_inicial)?$financeiro[12]->demonstrativofin_cp_saldo_inicial:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">RENDA / CRÉDITO JUROS</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_cp_renda_credito)?$financeiro[1]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_cp_renda_credito)?$financeiro[2]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_cp_renda_credito)?$financeiro[3]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_cp_renda_credito)?$financeiro[4]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_cp_renda_credito)?$financeiro[5]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_cp_renda_credito)?$financeiro[6]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_cp_renda_credito)?$financeiro[7]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_cp_renda_credito)?$financeiro[8]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_cp_renda_credito)?$financeiro[9]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_cp_renda_credito)?$financeiro[10]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_cp_renda_credito)?$financeiro[11]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_cp_renda_credito)?$financeiro[12]->demonstrativofin_cp_renda_credito:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">DESCONTOS (DEB IRRF) E TAXAS</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_cp_desc_taxas)?$financeiro[1]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_cp_desc_taxas)?$financeiro[2]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_cp_desc_taxas)?$financeiro[3]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_cp_desc_taxas)?$financeiro[4]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_cp_desc_taxas)?$financeiro[5]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_cp_desc_taxas)?$financeiro[6]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_cp_desc_taxas)?$financeiro[7]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_cp_desc_taxas)?$financeiro[8]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_cp_desc_taxas)?$financeiro[9]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_cp_desc_taxas)?$financeiro[10]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_cp_desc_taxas)?$financeiro[11]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_cp_desc_taxas)?$financeiro[12]->demonstrativofin_cp_desc_taxas:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">MOVIMENTAÇÃO MESAL ( DÉBITOS)</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_cp_mov_debito)?$financeiro[1]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_cp_mov_debito)?$financeiro[2]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_cp_mov_debito)?$financeiro[3]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_cp_mov_debito)?$financeiro[4]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_cp_mov_debito)?$financeiro[5]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_cp_mov_debito)?$financeiro[6]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_cp_mov_debito)?$financeiro[7]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_cp_mov_debito)?$financeiro[8]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_cp_mov_debito)?$financeiro[9]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_cp_mov_debito)?$financeiro[10]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_cp_mov_debito)?$financeiro[11]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_cp_mov_debito)?$financeiro[12]->demonstrativofin_cp_mov_debito:0; ?></td>
				<td></td>
			</tr>
			<tr style="text-align: center">
				<td style="background: #2CB6E8;text-align: left;">MOVIMENTAÇÃO MESAL ( CRÉDITO)</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_cp_mov_credito)?$financeiro[1]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_cp_mov_credito)?$financeiro[2]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_cp_mov_credito)?$financeiro[3]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_cp_mov_credito)?$financeiro[4]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_cp_mov_credito)?$financeiro[5]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_cp_mov_credito)?$financeiro[6]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_cp_mov_credito)?$financeiro[7]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_cp_mov_credito)?$financeiro[8]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_cp_mov_credito)?$financeiro[9]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_cp_mov_credito)?$financeiro[10]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_cp_mov_credito)?$financeiro[11]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_cp_mov_credito)?$financeiro[12]->demonstrativofin_cp_mov_credito:0; ?></td>
				<td></td>
			</tr>
			<tr  style="color: #2CB6E8;text-align: center">
				<td style="background: #2CB6E8;color: #000;text-align: left;">SALDO FINAL</td>
				<td><?php echo  isset($financeiro[1]->demonstrativofin_cp_saldo_final)?$financeiro[1]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[2]->demonstrativofin_cp_saldo_final)?$financeiro[2]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[3]->demonstrativofin_cp_saldo_final)?$financeiro[3]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[4]->demonstrativofin_cp_saldo_final)?$financeiro[4]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[5]->demonstrativofin_cp_saldo_final)?$financeiro[5]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[6]->demonstrativofin_cp_saldo_final)?$financeiro[6]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[7]->demonstrativofin_cp_saldo_final)?$financeiro[7]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[8]->demonstrativofin_cp_saldo_final)?$financeiro[8]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[9]->demonstrativofin_cp_saldo_final)?$financeiro[9]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[10]->demonstrativofin_cp_saldo_final)?$financeiro[10]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[11]->demonstrativofin_cp_saldo_final)?$financeiro[11]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td><?php echo  isset($financeiro[12]->demonstrativofin_cp_saldo_final)?$financeiro[12]->demonstrativofin_cp_saldo_final:0; ?></td>
				<td></td>
			</tr>
			<?php } ?>
		</tbody>
	</table>