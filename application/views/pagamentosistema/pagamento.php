    <!-- Bordered striped table -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Pagamentos</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>

        <div class="panel-body">
            Pagamentos
        </div>

        <div class="table-responsive">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Descrição</th>
                        <th>Data de Vencimento</th>
                        <th>Data de Pagamento</th>
                        <th>Valor</th>
                        <th>Status</th>
                        <th></th>                        
                    </tr>
                </thead>
                <tbody>

                    <?php foreach ($dados as $r) {
                        $status = ($r->pagamento_sistema_status == 1)?'PAGO':'PENDENTE';
                        $pagamento = ($r->pagamento_sistema_data_pagamento)?date('d/m/Y', strtotime($r->pagamento_sistema_data_pagamento)):'';
                        echo '<tr>';
                        echo '<td>'.$r->pagamento_sistema_id.'</td>';
                        echo '<td>'.$r->pagamento_sistema_descricao.'</td>';
                        echo '<td>'.date('d/m/Y', strtotime($r->pagamento_sistema_data_vencimento)).'</td>';
                        echo '<td>'.$pagamento.'</td>';
                        echo '<td>'.$r->pagamento_sistema_valor.'</td>'; 
                        echo '<td>'.$status.'</td>';                            
                        echo '<td>';
                        if($pagamento <> ''){
                             echo '<a style="margin: 1%" href="'.base_url().'pagamentosistema/visualizar/'.$r->pagamento_sistema_id.'" class="btn tip-top" title="Visualizar Pagamento"><i class="icon-eye"></i></a>  '; 
                        }
                       
                        echo '</td>';
                        echo '</tr>';
                    }?>
                    <tr>
                        
                    </tr>

                </tbody>
            </table>
        </div>
    </div>
    <!-- /bordered striped table -->