<!-- Column selectors -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aOperacaoCartao')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/vendaCartao">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>
			    <th>#</th>				
				<th>data</th>	
				<th>Nome/CPF</th>					
				<th>Valor</th>				
				<th>Total</th>	
				<th>Status</th>																		
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($dados as $valor) { ?>
				
				<tr>     								
					<td><?php echo $valor->operacao_id; ?></td>
					<td><?php echo date(('d/m/Y'), strtotime($valor->operacao_data_cadastro)); ?></td>
					<td><?php echo $valor->usuario_nome.'<br>'.$valor->usuario_cpf ?></td>					 					
					<td><?php echo number_format($valor->operacao_total_boleto, 2, '.', ','); ?></td>
					<td><?php echo number_format($valor->operacao_total_transacao, 2, '.', ','); ?></td>	
					<td><?php echo ($valor->financeiro_baixado == 0) ? 'Pedente' : 'Pago' ; ?></td>																									

					<td>																				
						<ul class="icons-list">						
							<?php if(checarPermissao('eOperacaoCartao') and $valor->operacao_faturado == 0){ ?>
							<li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/vendaCartaoConfirmacao/<?php echo $valor->operacao_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
							<?php } ?>
							<?php if(checarPermissao('dOperacaoCartao') and $valor->financeiro_baixado ==0){ ?>
							<li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->operacao_id; ?>"><i class="icon-trash"></i></a></li>
							<?php } ?>
							<?php if(checarPermissao('vOperacaoCartao')){ ?>
							<li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->operacao_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>	
							<?php if( $valor->operacao_faturado == 1){ ?>
							<li class="text-brown-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/imprimir/<?php echo $valor->operacao_id; ?>" target="_blank" data-popup="tooltip" title="Visualizar"><i class="icon-printer"></i></a></li>
							<?php }} ?>
						</ul>																			
					</td>						
				</tr>
			<?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors -->

