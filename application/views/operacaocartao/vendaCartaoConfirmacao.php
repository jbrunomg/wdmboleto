<form action="#" method="post" id="form_submit">
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <legend class="text-semibold"><i class="icon-user position-left"></i>Dados do Cliente</legend>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="rg_cpf">CPF</label>
                                    <input type="text" readonly name="rg_cpf" autocomplete="off" id="rg_cpf" value="<?php echo $this->session->userdata('usuario_cpf');?>" class="form-control form-control-sm" placeholder="000.000.000-00" maxlength="14">
                                    <input type="hidden" autocomplete="off" id="cliente_id" value="<?php echo $this->session->userdata('usuario_id');?>"/>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="nome">Nome</label>
                                    <input type="text" readonly name="nome" value="<?php echo strtoupper($this->session->userdata('usuario_nome'));?>" autocomplete="off" id="nome" class="form-control form-control-sm">
                                </div>
                                <div class="from-group col-md-3">
                                    <label for="data_nascimento">Data de Nascimento</label>
                                    <input type="text" readonly name="data_nascimento" autocomplete="off" id="data_nascimento" class="form-control form-control-sm" placeholder="00/00/0000" maxlength="10">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="celular">Celular</label>
                                    <input type="tel" readonly name="celular" id="celular" value="<?php echo $this->session->userdata('usuario_celular');?>" autocomplete="off" class="form-control form-control-sm" placeholder="(00)00000-0000" maxlength="14">
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="email">E-Mail</label>
                                    <input type="email" readonly name="email" id="email" value="<?php echo $this->session->userdata('usuario_email');?>" autocomplete="off" class="form-control form-control-sm">
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="sexo">Sexo</label>
                                    <select name="sexo" disabled id="sexo" autocomplete="off" class="form-control form-control-sm">
                                        <option value="">Selecione</option>
                                        <option value="Masculino" <?php echo ($this->session->userdata('usuario_sexo') == '1')?'selected':''; ?>>Masculino</option>
				                         <option value="Feminino" <?php echo ($this->session->userdata('usuario_sexo') == '2')?'selected':''; ?>>Feminino</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-flat">
        <div class="panel-body">
            <div class="row mt-3">
                <div class="form-group col-md-8">
                    <div class="card">
                        <legend class="text-semibold"><i class="icon-barcode2 position-left"></i>Dados do Boleto</legend>
                        <div class="card-body">
                            <div class="row" style="display: none">
                                <div class="form-group col-md-6">
                                    <label for="codigo_boleto">Código do Boleto <span style="color: red;">(DATA DO BOLETO)</span></label>
                                    <input type="text" name="codigo_boleto" autocomplete="off" id="codigo_boleto" class="form-control form-control-sm">
                                </div>
                                <div class="form-group col-md-4">
                                   <label for="cartao_bandeira">Cartão / Bandeira</label>
                                   <select name="cartao_bandeira" autocomplete="off" id="cartao_bandeira" class="form-control">
                                        <option value="">Selecione</option>
                                        <?php foreach ($cartao as $fo) { ?>
                                            <option value="<?php echo $fo->categoria_cart_id; ?>"><?php echo $fo->categoria_cart_nome_credenciadora.' - '.$fo->categoria_cart_descricao; ?></option>
                                        <?php  } ?>  
                                    </select>
                                    <span class="erroCartao" style="color:red; display:none;">Cartão em Branco!</span>
                                </div>
                                <div class="form-group col-md-2">
                                    <label for="valor">Valor (R$)</label>
                                    <input type="text" name="valor"  autocomplete="off" id="valor" class="form-control form-control-sm money" placeholder="0,00">
                                    <span class="erroValor" style="color:red; display:none;">Valor em Branco!</span>
                                </div>
                            </div>
                            <div class="row" style="display: none">
                                <div class="col-md-4">
                                    <label for="numero_parcelas">Número de Parcelas</label>
                                    <select name="numero_parcelas" autocomplete="off" id="numero_parcelas" class="form-control">
                                        <option value="">Selecione</option>
                                        <option value="debito">Débito</option>
                                        <option value="01">1x</option>
                                        <option value="02">2x</option>
                                        <option value="03">3x</option>
                                        <option value="04">4x</option>
                                        <option value="05">5x</option>
                                        <option value="06">6x</option>
                                        <option value="07">7x</option>
                                        <option value="08">8x</option>
                                        <option value="09">9x</option>
                                        <option value="10">10x</option>
                                        <option value="11">11x</option>
                                        <option value="12">12x</option>
                                        <option value="13">13x</option>
                                        <option value="14">14x</option>
                                        <option value="15">15x</option>
                                        <option value="16">16x</option>
                                        <option value="17">17x</option>
                                        <option value="18">18x</option>
                                    </select>
                                    <span class="erroParcelar" style="color:red; display:none;">Parcela em Branco!</span>
                                </div>
                                <div class="col-md-2">
                                    <label for="valor_parcelado">Valor Parcelado </label>
                                    <input type="text" name="valor_parcelado" autocomplete="off" id="valor_parcelado" class="form-control form-control-sm money" placeholder="0,00">
                                </div>
                                <div class="col-md-2">
                                    <label for="valor_parcela">Valor Parcela</label>
                                    <input type="text" name="valor_parcela" autocomplete="off" id="valor_parcela" class="form-control form-control-sm money" placeholder="0,00">
                                </div>
                                <div class="col-md-4">
                                    <button type="button" id="btn_adicionar_boleto" class="btn btn-sm btn-block btn-info" style="margin-top: 27px;" onclick="adicionar_boleto()">Adicionar</button>
                                </div>
                            </div>
                            <hr>
                            <input type="hidden" id="index_hidden"/>
                            <div class="row" style="overflow-x: auto;">
                                <div class="form-group col-md-12">
                                    <table id="table_boletos" class="table table-hover table-bordered table-sm">
                                        <thead>
                                             <tr>
                                                <th width="80%">Cartão</th>
                                                <!-- <th>N. Parcelas</th>
                                                <th>Cartão / Bandeira</th> -->
                                                <th width="15%">Valor</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="row center">
                                <div class="form-group col-md-10 offset-md-4" style="text-align: left;">
                                    <span class="lbl_tt_transacao">Total da Transação</span>
                                    <br>
                                    <span style="font-size: 20px; font-weight: bold;">R$ </span><span style="font-size: 35px; font-weight: 700;" id="total_transacao">0,00</span>
                                </div>
                                <div class="form-group col-md-2" style="text-align: right;">
                                    <span style="font-size: 12px;">Total dos Cartão</span>
                                    <br>
                                    <span style="font-size: 10px;">R$ </span><span style="font-size: 20px;" id="total_boletos">0,00</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group col-md-4">
                    <div class="card">
                        <div class="card-header">
                            <span>Pagamento</span>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="form-group col-md-8" style="display: none">
                                    <label for="autorizacao">Código de Autorização</label>
                                    <input type="text" name="autorizacao" id="autorizacao" class="form-control form-control-sm" maxlength="6" placeholder="">
                                </div>
                                <div class="form-group col-md-4" style="display: none">
                                    <button type="button" id="btn_adicionar_autorizacao" class="btn btn-sm btn-block btn-info" style="margin-top: 27px;" onclick="adicionar_autorizacao()">Adicionar</button>
                                </div>
                            </div>
                            <hr>
                            <div class="row" style="overflow-x: auto;">
                                <div class="form-group col-md-12">
                                    <table id="table_autorizacao" class="table table-sm table-hover table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Código Autorização</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                        <button type="button" id="btn_finalizar" class="btn btn-sm btn-block btn-danger" onclick="finalizar()">Finalizar</button>
                        </div>
                    </div>
                </div>   
            </div>
        </div>
    </div> 
</form>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<!-- <script src="https://plentz.github.io/jquery-maskmoney/javascripts/jquery.maskMoney.min.js"></script> -->
<script src="<?php echo base_url();?>public/assets/js/maskmoney.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>public/assets/js/jquery.mask.min.js"></script>
<script type="text/javascript">

$(document).ready(function(){

    $(".money").maskMoney();

});

$(document).ready(function(){

    $("#data_nascimento").mask('00/00/0000', {placeholder: '00/00/0000'});
    $("#celular").mask('(00)00000-0000', {placeholder: '(00)00000-0000'});
    $("#rg_cpf").mask('000.000.000-00', {placeholder: '000.000.000-00'});
    $("#rg_cpf").focus();

    let id = '<?php echo $this->uri->segment(3) ?>';
    let dados = {
        id: id
    }

    let responseOperacao = requisicao("/operacaocartao/buscarOperacao", dados);
    if(responseOperacao.length === 0)
    {
        swal({ 
            title: "Aviso!",
            text: "Essa venda não existe", 
            type: "warning", 
            confirmButtonClass: "btn-danger", 
            confirmButtonText: "OK",
            closeOnConfirm: false 
        }, 
        function(){
            window.location.href = base_url+"operacaocartao/vendaCartao";
        });
    }

    let responseItensOperacao = requisicao("/operacaocartao/buscarItensOperacao", dados);
    let responseAutorizacoes = requisicao("/operacaocartao/buscarAutorizacoes", dados);
    let boletos = "";
    let autorizacoes = "";
    let index = 0;

    responseItensOperacao.map(function(data){

        if(data.itens_operacao_tipo === '0'){
            let cartao = consultaCartaoDescricao(data.itens_operacao_bandeira_cartao);
            boletos += "<tr>";
                boletos += "<td><input type='text' id='boleto_"+index+"' data_pulo='false' data_tipo="+data.itens_operacao_tipo+" onkeyup='somenteNumeros(this)' value='"+data.itens_operacao_bandeira_cartao+"' class='form-control form-control-sm' style='width: 100%;' readonly></td>";
                // boletos += "<td>"+data.itens_operacao_numero_parcela+"</td>";
                // boletos += "<td>"+data.itens_operacao_bandeira_cartao+"</td>";
                boletos += "<td dataPacela="+data.itens_operacao_numero_parcela+" dataCartao="+cartao+">"+parseFloat(data.itens_operacao_valor_boleto).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })+"</td>";
                // boletos += "<td><button type='button' id='btn_editar_boleto_"+index+"' class='btn btn-sm btn-primary' onclick='editar_boleto("+index+","+data.itens_operacao_id+")'>Editar</button></td>";
                // boletos += "<td><button type='button' id='btn_remover_boleto' class='btn btn-sm btn-danger' onclick='drop_row_boletos(this.parentNode.parentNode.rowIndex,"+data.itens_operacao_id+")'>Remover</button></td>";
            boletos += "</tr>";
            index++;
        }

       
    });

    if(responseItensOperacao.length === 1){

        responseItensOperacao.map(function(data){
            if(data.itens_operacao_numero_boleto === 'BOLETO DE SIMULAÇÃO' || data.itens_operacao_tipo === '1'){
                let cartao = consultaCartaoDescricao(data.itens_operacao_bandeira_cartao);
                boletos += "<tr>";
                    if(data.itens_operacao_tipo === '1'){
                      boletos += "<td><input type='text' id='boleto_"+index+"' data_pulo='false' data_tipo="+data.itens_operacao_tipo+" onkeyup='somenteNumeros(this)' value='"+data.itens_operacao_numero_boleto+"' class='form-control form-control-sm' style='width: 100%;' readonly></td>";
                    }else{
                    //   boletos += "<td><input type='text' id='boleto_"+index+"' data_pulo='false' data_tipo="+data.itens_operacao_tipo+" onkeyup='somenteNumeros(this)' value='PIX' class='form-control form-control-sm' style='width: 100%;' readonly></td>";
                    }
                    // boletos += "<td dataPacela="+data.itens_operacao_numero_parcela+" dataCartao="+cartao+">"+parseFloat(data.itens_operacao_valor_boleto).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })+"</td>";
                    // boletos += "<td><button type='button' id='btn_editar_pix_"+index+"' class='btn btn-sm btn-primary' onclick='editar_pix("+index+","+data.itens_operacao_id+")'>Editar</button></td>";
                boletos += "</tr>";
                index++;
            }

       
    });


    }



    $("#table_boletos tbody").append(boletos);
    $("#index_hidden").val(index);

    responseAutorizacoes.map(function(data){
        autorizacoes += "<tr>";
            autorizacoes += "<td>"+data.autorizacao_operacao_numero_nsu+"</td>";
            // autorizacoes += "<td><button type='button' id='btn_remover_autorizacao' class='btn btn-sm btn-danger' onclick='drop_row_autorizacao(this.parentNode.parentNode.rowIndex, "+data.autorizacao_operacao_id+")'>Remover</button></td>";
        autorizacoes += "</tr>";
    });

    $("#table_autorizacao tbody").append(autorizacoes);
    $("#total_transacao").html(parseFloat(responseOperacao[0].operacao_total_transacao).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 }));
    $("#total_boletos").html(parseFloat(responseOperacao[0].operacao_total_boleto).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 }));
});

$('#cartao_bandeira').change(function(){
   $('#numero_parcelas').val(''); 
   $('.erroCartao').hide(); 
   $('#btn_adicionar_boleto').prop('disabled', false);
});

$('#valor').change(function(){
   $('#numero_parcelas').val(''); 
   $('.erroValor').hide(); 
   $('#btn_adicionar_boleto').prop('disabled', false);
});

$('#numero_parcelas').change(function(){
   
   var parcela = $('#numero_parcelas').val();
   var cartao = $('#cartao_bandeira').val(); 
   var valor = $('#valor').val().replace(',',''); 
   var porcentagem = consultaParcela(parcela, cartao);
   var valorJuros = (valor * porcentagem) / 100;
   var valorCalculado = (parseFloat(valor) + parseFloat(valorJuros));
   var parcelaVP = parcela === 'debito' ? '1' : parcela;
   var valorParcelado = (parseFloat(valorCalculado) / parcelaVP);
   
   if(!cartao){

        $('.erroCartao').show(); 
        $('#btn_adicionar_boleto').prop('disabled', true);
    
   }else if(!valor){

        $('.erroValor').show(); 
        $('#btn_adicionar_boleto').prop('disabled', true);

   } else{

        $('#valor_parcela').val(parseFloat(valorCalculado.toFixed(2)).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })); 
        $('#total_transacao').html(parseFloat(valorCalculado.toFixed(2)).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })); 
        $('#total_boletos').html(parseFloat(valor).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })); 
        $('#valor_parcelado').val(parseFloat(valorParcelado).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })); 

   }
    
});

function consultaParcela(parcela, cartao) {
    var result="";
    $.ajax({
        method: "POST",
        url: base_url+"operacaocartao/consultaParcela/",
        dataType: "JSON",
        data: { parcela: parcela, cartao: cartao},
        async: false,
        success:function(data) {
            result = data; 
        }
    });

    return result;
}


$('#rg_cpf').change(function(){

    let dados = {
        rg_cpf: $('#rg_cpf').val()
    };

    let response = requisicao("/operacaocartao/get_cliente", dados);

    if(response){
        data = response[0]['cliente_dtNascimento'] ? dataSQLparaBR(response[0]['cliente_dtNascimento']) : '';
        $("#nome").val(response[0]['cliente_nome']);
        $("#cliente_id").val(response[0]['cliente_id']);
        $("#data_nascimento").val(data);
        $("#celular").val(response[0]['cliente_telefone']);
        $("#email").val(response[0]['cliente_email']);
        $("#sexo").val(response[0]['cliente_sexo']);
        $("#codigo_boleto").focus();
    }else{

        let verificar = TestaCPF($('#rg_cpf').val().match(/\d/g).join(""));
        if (verificar) {

            swal('Aviso!', 'Desculpe, não conseguimos localizar o cliente, mas cadastramos o CPF no nosso sistema.', 'info');

        } else {

            swal('Aviso!', 'Desculpe, mas CPF digitado é inválido. Tente Novamente!', 'info');
            
        }



    }
});

function adicionar_boleto()
{
    let codigo_boleto = $("#codigo_boleto").val();
    let valor = $("#valor").val().replace(',','');
    let boleto_existe = 0;
    let index = $("#index_hidden").val();
    var cartao = $('#cartao_bandeira').val(); 
    var parcela = $('#numero_parcelas').val();
    var cartaoNome = consultaCartao(cartao);
    var bandeiraNome = consultaBandeira(cartao);
    var valor_transacao = $("#total_transacao").html();

    if(!(codigo_boleto))
    {
        swal('Aviso!', 'Código do boleto deve ser informado.', 'info');
        return false;
    }

    if(!(valor))
    {
        swal('Aviso!', 'Valor deve ser informado.', 'info');
        return false;
    }

    if(!(cartao))
    {
        swal('Aviso!', 'Cartão deve ser informado.', 'info');
        return false;
    }

    if(!(parcela))
    {
        swal('Aviso!', 'Parcela deve ser informada.', 'info');
        return false;
    }
     
    let contador = 0;
    $("#table_boletos tbody tr").map(function(){
        if($('#boleto_'+contador+'').val() == codigo_boleto){
            swal('Boleto já adicionado!', 'Este boleto já foi adicionado a tabela.', 'warning');
            boleto_existe = 1;
        }
        contador++;
    });
    if(boleto_existe == 1)
        return false;

    let id = '<?php echo $this->uri->segment(3) ?>';
    let dados = {

        operacao_id : id,
        itens_operacao_numero_boleto : codigo_boleto, 
        itens_operacao_valor_boleto : valor,  
        itens_operacao_bandeira_cartao : cartaoNome+" - "+bandeiraNome,
        itens_operacao_numero_parcela : parcela,
        itens_operacao_valor_transacao : valor_transacao.replace('.','').replace(',','.'),
        itens_operacao_data_cadastro : '<?php echo date("Y/m/d") ?>'	 
    };

    let idBoleto = requisicao("/operacaocartao/addItensOperacao", dados);

    if(!(idBoleto))
    {
        swal('Aviso!', 'Erro ao adiconar boleto.', 'error');
        return false;
    }

    let body = "";
    parcelaNome = parcela;
    parcela = parcela == "debito" ? '0' : parcela;

    body += "<tr>";
        body += "<td><input type='text' id='boleto_"+index+"' onkeyup='somenteNumeros(this);' value='"+codigo_boleto+"' class='form-control form-control-sm' style='width: 100%;' readonly></td>";
        body += "<td>"+parcelaNome+"</td>";
        body += "<td>"+cartaoNome+" - "+bandeiraNome+"</td>";
        body += "<td dataPacela="+parcela+" dataCartao="+cartao+" dataValorT="+valor_transacao+">"+parseFloat(valor).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 })+"</td>";
        body += "<td><button type='button' id='btn_editar_boleto_"+index+"' class='btn btn-sm btn-primary' onclick='editar_boleto("+index+","+idBoleto+")'>Editar</button></td>";
        body += "<td><button type='button' id='btn_remover_boleto' class='btn btn-sm btn-danger' onclick='drop_row_boletos(this.parentNode.parentNode.rowIndex,"+idBoleto+")'>Remover</button></td>";
    body += "</tr>";

    $("#table_boletos tbody").append(body);

    calcular_juros();

    index++;

    $("#valor").val("");
    $('#numero_parcelas').val(''); 
    $('#valor_parcela').val(''); 
    $('#valor_parcelado').val(''); 
    $('#cartao_bandeira').val(''); 
    $("#codigo_boleto").val("").focus();
    $("#index_hidden").val(index);
}

function editar_pix(index, id)
{
    if($("#boleto_"+index).attr('readonly')) {
        $("#btn_editar_pix_"+index).html('Salvar');
        $("#boleto_"+index).removeAttr('readonly');
        $("#btn_editar_boleto_0").html('Desabilitado');
        $("#btn_editar_boleto_0").attr('disabled', true);
    }
    else {
        codigo_boleto = $("#boleto_"+index).val();
        i = 0;
        codigos = new Array();
        $("#table_boletos tbody tr").map(function(){
            if(i != index)
               codigos[i] = {codigo_boleto:$('#boleto_'+i+'').val()};

            i++;
        });
        i = 0;

        codigos.map(function(cod){
            if(codigo_boleto == cod.codigo_boleto){
                swal({ 
                    title: "Aviso!",
                    text: "Este boleto já foi adicionado a tabela.", 
                    type: "warning", 
                    confirmButtonClass: "btn-danger", 
                    confirmButtonText: "OK",
                    closeOnConfirm: false 
                }, 
                function(){
                    window.location.href = base_url+"operacaocartao/vendaBoletoConfirmacao/"+'<?php echo $this->uri->segment(3) ?>';
                });
            }
        });

        let dados = {
            itens_operacao_id : id,
            itens_operacao_numero_boleto : codigo_boleto,
            itens_operacao_tipo : 1	 
        };
        let response = requisicao("/operacaocartao/eddItensOperacao", dados);

        if(!(response))
        {
            swal({ 
                title: "Aviso!",
                text: "Boleto Inválido!", 
                type: "error", 
                confirmButtonClass: "btn-danger", 
                confirmButtonText: "OK",
                closeOnConfirm: false 
            }
            );
        }else{
            swal("Aviso!", "Registro atualizado com sucesso.", "success");
        }

        $("#boleto_"+index).attr('data_tipo', '1');
        $("#boleto_0").attr('data_pulo', 'true');
        $("#btn_editar_pix_"+index).html('Editar');
        $("#boleto_"+index).attr('readonly', 'readonly');
    }
}

function editar_boleto(index, id)
{
    if($("#boleto_"+index).attr('readonly')) {
        $("#btn_editar_boleto_"+index).html('Salvar');
        $("#boleto_"+index).removeAttr('readonly');
        $("#btn_editar_pix_1").html('Desabilitado');
        $("#btn_editar_pix_1").attr('disabled', true);
    }
    else {
        codigo_boleto = $("#boleto_"+index).val();
        i = 0;
        codigos = new Array();
        $("#table_boletos tbody tr").map(function(){
            if(i != index)
               codigos[i] = {codigo_boleto:$('#boleto_'+i+'').val()};

            i++;
        });
        i = 0;

        codigos.map(function(cod){
            if(codigo_boleto == cod.codigo_boleto){
                swal({ 
                    title: "Aviso!",
                    text: "Este boleto já foi adicionado a tabela.", 
                    type: "warning", 
                    confirmButtonClass: "btn-danger", 
                    confirmButtonText: "OK",
                    closeOnConfirm: false 
                }, 
                function(){
                    window.location.href = base_url+"operacaocartao/vendaBoletoConfirmacao/"+'<?php echo $this->uri->segment(3) ?>';
                });
            }
        });

        let dados = {
            itens_operacao_id : id,
            itens_operacao_numero_boleto : codigo_boleto	 
        };
        let response = requisicao("/operacaocartao/eddItensOperacao", dados);

        if(!(response))
        {
            swal({ 
                title: "Aviso!",
                text: "Boleto Inválido!", 
                type: "error", 
                confirmButtonClass: "btn-danger", 
                confirmButtonText: "OK",
                closeOnConfirm: false 
            }
            // , 
            // function(){
            //     window.location.href = base_url+"operacaocartao/vendaBoletoConfirmacao/"+'<?php echo $this->uri->segment(3) ?>';
            // }
            );
            $("#boleto_"+index).val('BOLETO DE SIMULAÇÃO');

        }else{
            swal("Aviso!", "Registro atualizado com sucesso.", "success");
        }

        $("#btn_editar_boleto_"+index).html('Editar');
        $("#boleto_"+index).attr('readonly', 'readonly');
    }
}

function drop_row_boletos(row, id){

    swal({ 
        title: "Aviso!",
        text: "Você realmente deseja excluir esse registro da tabela?", 
        type: "warning", 
        showCancelButton: true, 
        cancelButtonText: "Não",
        confirmButtonClass: "btn-danger", 
        confirmButtonText: "Sim",
        closeOnConfirm: false 
    }, 
    function(){
        let dados = {
            itens_operacao_id : id,
            itens_operacao_visivel : 0	 
        };
        let response = requisicao("/operacaocartao/eddItensOperacao", dados);
        document.getElementById('table_boletos').deleteRow(row);
        calcular_juros();
        swal("Aviso!", "Registro excluido com sucesso.", "success");
    });

}

function adicionar_autorizacao()
{
    let autorizacao = $("#autorizacao").val();
    let autorizacao_existe = 0;
    let contador = 0;
    $("#table_autorizacao tbody tr").map(function(){
        if($(this).find('td').eq('0').text() == autorizacao){
            swal('Autorizacao já adicionada!', 'Este autorizacao já foi adicionada a tabela.', 'warning');
            autorizacao_existe = 1;
        }
        contador++;
    });
    if(autorizacao_existe == 1)
        return false;

    if(!(autorizacao))
    {
        swal('Aviso!', 'Você deve informar o código de autorização.', 'info');
        return false;
    }

    let id = '<?php echo $this->uri->segment(3) ?>';
    let dados = {

        operacao_id : id,
        autorizacao_operacao_numero_nsu : autorizacao, 
        autorizacao_operacao_data_cadastro : '<?php echo date("Y/m/d") ?>'	 
    };

    let idAutorizacao = requisicao("/operacaocartao/addAutorizacoesOperacao", dados);

    if(!(idAutorizacao))
    {
        swal('Aviso!', 'Erro ao adiconar autorização.', 'error');
        return false;
    }

    let body = "";
    body += "<tr>";
        body += "<td>"+autorizacao+"</td>";
        body += "<td><button type='button' id='btn_remover_autorizacao' class='btn btn-sm btn-danger' onclick='drop_row_autorizacao(this.parentNode.parentNode.rowIndex, "+idAutorizacao+")'>Remover</button></td>";
    body += "</tr>";

    $("#table_autorizacao tbody").append(body);
    $("#autorizacao").val("").focus();
}

function drop_row_autorizacao(row, id)
{
    swal({ 
        title: "Aviso!",
        text: "Você realmente deseja excluir esse registro da tabela?", 
        type: "warning", 
        showCancelButton: true, 
        cancelButtonText: "Não",
        confirmButtonClass: "btn-danger", 
        confirmButtonText: "Sim",
        closeOnConfirm: false 
    }, 
    function(){
        let dados = {
            autorizacao_operacao_id : id,
            autorizacao_operacao_visivel : 0	 
        };
        let response = requisicao("/operacaocartao/eddAutorizacoesOperacao", dados);
        document.getElementById('table_autorizacao').deleteRow(row);
        swal("Aviso!", "Registro excluido com sucesso.", "success");
    });

}

function finalizar()
{

    let id_venda = '<?php echo $this->uri->segment(3) ?>';
    let id_cliente =  $("#cliente_id").val();
    let cartoes_bandeira = new Array();
    let parcelas = new Array();
    let autorizacoes = new Array();
    let codigos = new Array();
    let index = 0;
    let not = 0;
    let cpf = $('#rg_cpf').val();

    if (cpf) {
        if(!(id_cliente)){
            let dados = {
                cliente_cpfCnpj: $('#rg_cpf').val().match(/\d/g).join(""),
                cliente_nome: $('#nome').val(),
                cliente_dtNascimento: FormataStringData($('#data_nascimento').val()),
                cliente_telefone: $('#celular').val(),
                cliente_email: $('#email').val(),
                cliente_sexo: $('#sexo').val(),
            };
            let result = requisicao("/operacaocartao/adicionarCliente", dados);
            id_cliente = result;
            $("#cliente_id").val(id_cliente);

        }
    }

    $("#table_boletos tbody tr").map(function(){
        parcelas[index] = {parcelas: $(this).find('td').eq(1).text()};
        cartoes_bandeira[index] = {cartao_bandeira: $(this).find('td').eq(2).text()};
        index++;
    });

    index = 0;

    $("#table_autorizacao tbody tr").map(function(){
        autorizacoes[index] = {autorizacao: $(this).find('td').eq(0).text()};
        index++;
    });

    index = 0;

    $("#table_boletos tbody tr").map(function(){
        codigos[index] = {codigo_boleto:$('#boleto_'+index+'').val(), codigo_tipo:$('#boleto_'+index+'').attr('data_tipo'), condigo_pulo: $('#boleto_'+index+'').attr('data_pulo')};
        index++;
    });

    index = 0;

    // codigos.map(function(cod){
    //     if(cod.condigo_pulo === 'false'){
    //         if(cod.codigo_tipo === '0'){
    //             if(cod.codigo_boleto == '' || cod.codigo_boleto == 'BOLETO DE SIMULAÇÃO' || cod.codigo_boleto.lenght > 0){
    //                 swal('Aviso!', 'Ainda está faltando codigos de boletos.', 'info');
    //                 not = 1;
    //             }
    //         }else{
    //             if(cod.codigo_boleto == '' || cod.codigo_boleto == 'PIX' || cod.codigo_boleto.lenght > 0){
    //                 swal('Aviso!', 'Ainda está faltando codigos de boletos.', 'info');
    //                 not = 1;
    //             }
    //         }
    //     }
    // });

    if(not == 1)
       return false;

    if(!(id_venda))
    {
        swal('Aviso!', 'Não conseguimos capturar o numero da venda.', 'info');
        return false;
    }

    if(!(id_cliente))
    {
        swal('Aviso!', 'Venda sem cliente.', 'info');
        return false;
    }

    if(parcelas.length == 0 || autorizacoes.length == 0)
    {
        swal('Aviso!', 'Todos os campos devem ser preenchidos.', 'info');
        return false;
    }

    // var contator = verificarCartao(cartoes_bandeira, parcelas);
    // if(!(autorizacoes.length >= parseInt(contator)))
    // {
    //     swal('Aviso!', 'Precisamos de mais autorização!', 'info');
    //     return false;
    // }

    let dados = {
        operacao_id: id_venda,
        cliente_id: id_cliente,
        operacao_total_boleto: $("#total_boletos").html().replace('.','').replace(',','.'),
        operacao_total_transacao: $("#total_transacao").html().replace('.','').replace(',','.'),
        operacao_faturado: 1
    };

    let response = requisicao('/operacaocartao/finalizar_venda', dados);
    if(response)
    {
        swal('Sucesso!', 'Venda finalizada com sucesso.', 'success');
        window.location.href = base_url+'operacaocartao/vendaCartao';
        window.open(base_url+"operacaocartao/imprimir/"+id_venda);

    }
    else
    {
        swal('Erro!', 'Não conseguimos finalizar a sua venda.', 'error');
    }
}


function requisicao(url, dados = ""){
    var data = $.ajax({
        url: base_url+url,
        async: false,
        type: 'post',
        datatype: 'json',
        data: dados,
        success: function(data)
        {
          data = JSON.parse(data);
        }
    });

    return JSON.parse(data.responseText);
}

function calcular_juros()
{
    var val_table = total_boletos = 0;
    var total_transacao = 0;

    $("#table_boletos tbody tr").map(function(dados){
        val_table = $(this).find('td').eq('3').text().replace('.','').replace(',','.');
        val_parcela = $(this).find('td').eq('3').attr('dataPacela');
        val_cartao = $(this).find('td').eq('3').attr('dataCartao');
        val_parcela = val_parcela == '0' ? 'debito' : val_parcela;
        porcentagem = consultaParcela(val_parcela, val_cartao);
        val_table = parseFloat(val_table);
        total_boletos = total_boletos + val_table;
        total_transacao = total_transacao + val_table + ((val_table * porcentagem) / 100);
    });

    let id = '<?php echo $this->uri->segment(3) ?>';
    let dados = {

        operacao_id : id,
        operacao_total_boleto : parseFloat(total_boletos).toFixed(2), 
        operacao_total_transacao : parseFloat(total_transacao).toFixed(2)	 
    };
    
    let response = requisicao("/operacaocartao/atualizarOperacao", dados);

    if(!(response))
    {
        swal('Aviso!', 'Erro ao atualizar a operação. Entre em contato com Suporte!', 'error');
        return false;
    }

    $("#total_boletos").html(parseFloat(total_boletos.toFixed(2)).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 }));
    $("#total_transacao").html(parseFloat(total_transacao.toFixed(2)).toLocaleString('pt-BR', { currency: 'BRL', minimumFractionDigits: 2 }));
}

function consultaCartao(cartao) {
    var result="";
    $.ajax({
        method: "POST",
        url: base_url+"operacaocartao/consultaCartao/",
        dataType: "JSON",
        data: {cartao: cartao},
        async: false,
        success:function(data) {
            result = data; 
        }
    });

    return result;
}

function consultaCartaoDescricao(cartao) {
    var result="";
    $.ajax({
        method: "POST",
        url: base_url+"operacaocartao/consultaCartaoDescricao/",
        dataType: "JSON",
        data: {cartao: cartao},
        async: false,
        success:function(data) {
            result = data; 
        }
    });

    return result;
}
function consultaBandeira(cartao) {
    var result="";
    $.ajax({
        method: "POST",
        url: base_url+"operacaocartao/consultaBandeira/",
        dataType: "JSON",
        data: {cartao: cartao},
        async: false,
        success:function(data) {
            result = data; 
        }
    });

    return result;
}

function dataSQLparaBR(data)
{
    var data = data.split("-");
    var data = data[2]+"/"+data[1]+"/"+data[0]; //0 - Ano; 1 - Mês; 2 - Dia;
    return data;
}

function FormataStringData(data) {
  var dia  = data.split("/")[0];
  var mes  = data.split("/")[1];
  var ano  = data.split("/")[2];

  return ano + '-' + ("0"+mes).slice(-2) + '-' + ("0"+dia).slice(-2);
  // Utilizo o .slice(-2) para garantir o formato com 2 digitos.
}

function verificarCartao(cartoes, parcelas)
{
    let cartao = 1;
    for (let i = 0; i < cartoes.length; i++) {
      if ((cartoes[0].cartao_bandeira != cartoes[i].cartao_bandeira) || (parcelas[0].parcelas != parcelas[i].parcelas)) {
         cartao++; 
      }
    }
    return cartao;
}

function TestaCPF(strCPF) {
    var Soma;
    var Resto;
    Soma = 0;
  if (strCPF == "00000000000") return false;

  for (i=1; i<=9; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (11 - i);
  Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(9, 10)) ) return false;

  Soma = 0;
    for (i = 1; i <= 10; i++) Soma = Soma + parseInt(strCPF.substring(i-1, i)) * (12 - i);
    Resto = (Soma * 10) % 11;

    if ((Resto == 10) || (Resto == 11))  Resto = 0;
    if (Resto != parseInt(strCPF.substring(10, 11) ) ) return false;
    return true;
}

function somenteNumeros(num) {
  //  var er = /[^0-9.\s]/;
    // var er = '';
    // er.lastIndex = 0;
    // var campo = num;
    // if (er.test(campo.value)) {
    //     campo.value = "";
    // }
}



</script>
