<style>
    .modal-content {
        background-color: #f3f3f3;
        box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
    }

    .modal-body {
        padding: 20px;
    }

    .modal-title {
        color: #333;
    }

    .close {
        position: absolute;
        right: 15px;
        top: 10px;
        opacity: 0.5;
        font-size: 1.5rem;
    }

    .close:hover {
        opacity: 1;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-folder-upload position-left"></i> Arquivo CSV</h6>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body" <?= ($this->session->userdata('usuario_padrao') == 0) ? 'style="display: none;"' : 'style="display: block;"'; ?>>

                <div class="row">
                    <div class="form-group col-md-12">
                        <label for="csv">Arquivo CSV</label>
                        <input type="file" name="csv" id="csv" class="form-control form-control-sm" accept=".csv">
                    </div>
                </div>
            </div>

            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-user position-left"></i>DADOS DO CLIENTE</h6>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <!-- <li><a data-action="collapse"></a></li> -->
                        <!-- <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body" <?= ($this->session->userdata('usuario_padrao') == 0) ? 'style="display: none;"' : 'style="display: block;"'; ?>>
                <div class="row">
                    <div class="form-group col-md-6">
                        <label for="nome">Nome</label>
                        <input type="text" name="nome" id="clie_csv" <?= ($this->session->userdata('usuario_padrao') == 0) ? 'readonly' : ''; ?> value="<?= (!$this->session->userdata('usuario_padrao') == 0) ? '' : strtoupper($this->session->userdata('usuario_nome')); ?>" class="form-control form-control-sm">
                    </div>

                    <!-- <?= var_dump($this->session->userdata('usuario_id')) ?> -->
                    <div class="form-group col-md-6">
                        <label for="rg_cpf">CPF</label>
                        <input type="text" name="rg_cpf" id="rg_cpf" <?= ($this->session->userdata('usuario_padrao') == 0) ? 'readonly' : ''; ?> class="form-control form-control-sm" value="<?php echo (!$this->session->userdata('usuario_padrao') == 0) ? '' : $this->session->userdata('usuario_cpf'); ?>" placeholder="000.000.000-00">
                        <input type="hidden" id="clie_csv_id" value="<?= (!$this->session->userdata('usuario_padrao') == 0) ? '' : $this->session->userdata('usuario_id'); ?>" />
                    </div>
                </div>
                <div class="form-group col-md-12">
                    <button type="button" id="btn_adicionar_csv" class="btn btn-sm btn-block btn-info" style="margin-top: 27px;">Processar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-calendar52 position-left"></i> Lançamentos não importados</h6>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body" <?= ($this->session->userdata('usuario_padrao') == 0) ? 'style="display: none;"' : 'style="display: block;"'; ?>>

                <div class="card-body">
                    <div class="row" style="overflow-x: auto;">
                        <div class="form-group col-md-12" style="max-height: 500px;">
                            <table id="table_csv" class="table table-hover table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <!-- <th width="40%">Código Boleto</th> -->
                                        <th>N. Parcelas</th>
                                        <th>Cartão / Bandeira</th>
                                        <th>Tipo</th>
                                        <th>Valor Bruto</th>
                                        <th>Data Operação</th>
                                        <th>Código Autorização</th>
                                        <th>Banco</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <?php var_dump($lancamentos_importar) ?> -->
                                    <?php
                                    $valor_bruto_total = 0;
                                    $valor_liquido_total = 0;
                                    foreach ($lancamentos_importar as $key => $value) :
                                        $valor_bruto_total += $value->operacao_csv_valor_bruto;
                                        $valor_liquido_total += $value->operacao_csv_valor_liquido;
                                    ?>
                                        <tr>
                                            <td><?= $value->operacao_csv_parcelas; ?></td>
                                            <td><?= $value->operacao_csv_bandeira; ?></td>
                                            <td><?= $value->operacao_csv_tipo_operacao; ?></td>
                                            <td><?= $value->operacao_csv_valor_bruto; ?></td>
                                            <td data-data_operacao="<?= date('Y-m-d', strtotime($value->operacao_csv_data_transacao)) ?>"><?= date('d/m/Y', strtotime($value->operacao_csv_data_transacao)) ?></td>
                                            <td><?= $value->operacao_csv_banco == "Stone" ? $value->operacao_csv_stone_id : $value->operacao_csv_codigo_nsu; ?></td>
                                            <td data-id="<?= $value->operacao_csv_id ?>" data-id-cli="<?= $value->operacao_csv_id_cliente ?>" data-id-user="<?= $value->operacao_csv_id_usuario ?>"><?= $value->operacao_csv_banco; ?></td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="total_valores" class="row center" style="<?= count($lancamentos_importar) > 0  ? 'display: block' : 'display: none' ?>">
                        <div class="form-group col-md-10 offset-md-4" style="text-align: left;">
                            <span>Total Bruto</span>
                            <br>
                            <span style="font-size: 20px; font-weight: bold;">R$ </span><span style="font-size: 35px; font-weight: 700;" id="total_bruto"><?= number_format($valor_bruto_total, 2, ',', '.') ?></span>
                        </div>
                        <div class="form-group col-md-2" style="text-align: right;">
                            <span>Total Líquido</span>
                            <br>
                            <span style="font-size: 20px; font-weight: bold;">R$ </span><span style="font-size: 35px; font-weight: 700;" id="total_liquido"><?= number_format($valor_liquido_total, 2, ',', '.') ?></span>
                        </div>
                    </div>
                </div>
                <div id="btns_csv" class="form-group col-md-12" style="<?= count($lancamentos_importar) > 0  ? 'display: block' : 'display: none' ?>">
                    <button type="button" id="btn_importar_csv" class="btn btn-sm btn-block btn-info" style="margin-top: 27px;">Importar Todos</button>
                    <button type="button" id="btn_cancelar_csv" class="btn btn-sm btn-block btn-danger" style="margin-top: 10px;">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-eye position-left"></i> Visualizar lançamentos</h6>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body" <?= ($this->session->userdata('usuario_padrao') == 0) ? 'style="display: none;"' : 'style="display: block;"'; ?>>
                <div class="card-body">

                    <div class="row">
                        <!-- Seletor para escolher entre data/dia ou mês -->
                        <div class="form-group col-md-4">
                            <label for="tipo-selecao">Buscar por</label>
                            <select id="tipo-selecao" class="form-control form-control-sm">
                                <option value="mes">Mês</option>
                                <option value="data">Data</option>
                            </select>
                        </div>

                        <!-- Input para selecionar a data ou mês, dependendo da escolha -->
                        <div class="form-group col-md-4" id="selecao-mes">
                            <label for="csv-mes-ano">Selecione o mês e o ano</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <select id="selecao-mes-ano" class="form-control form-control-sm">
                                        <option value="">Selecione o mês</option>
                                        <option value="01">Janeiro</option>
                                        <option value="02">Fevereiro</option>
                                        <option value="03">Março</option>
                                        <option value="04">Abril</option>
                                        <option value="05">Maio</option>
                                        <option value="06">Junho</option>
                                        <option value="07">Julho</option>
                                        <option value="08">Agosto</option>
                                        <option value="09">Setembro</option>
                                        <option value="10">Outubro</option>
                                        <option value="11">Novembro</option>
                                        <option value="12">Dezembro</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="selecao-ano" id="selecao-ano" placeholder="Ano" class="form-control form-control-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-4" id="selecao-dia" style="display: none;">
                            <label for="csv-dia">Selecione a data</label>
                            <input type="date" name="csv-dia" id="csv-dia" class="form-control form-control-sm">
                        </div>

                        <!-- Botão para processar -->
                        <div class="form-group col-md-4">
                            <button type="button" id="btn_buscar" class="btn btn-sm btn-block btn-info" style="margin-top: 27px;">Buscar</button>

                        </div>
                    </div>

                    <div class="row" style="overflow-x: auto;">
                        <div class="form-group col-md-12">
                            <button id="delete_selected" class="btn btn-danger" disabled style="margin-bottom: 5px;">Excluir Selecionados</button>
                            <table id="table_csv_visualizar" class="table table-hover table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" id="select_all" title="Selecionar Todos">
                                            Todos
                                        </th>
                                        <th>N. Parcelas</th>
                                        <th>Cartão / Bandeira</th>
                                        <th>Tipo</th>
                                        <th width="15%">Valor</th>
                                        <th>Data Operação</th>
                                        <th>Código Autorização</th>
                                        <th>Banco</th>
                                        <th>Cliente</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- <div class="row center">
                        <div class="form-group col-md-10 offset-md-4" style="text-align: left;">
                            <span class="lbl_tt_transacao">Total da Transação</span>
                            <br>
                            <span style="font-size: 20px; font-weight: bold;">R$ </span><span style="font-size: 35px; font-weight: 700;" id="total_transacao">0,00</span>
                        </div>
                        <div class="form-group col-md-2" style="text-align: right;">
                            <span style="font-size: 12px;">Total dos Cartão</span>
                            <br>
                            <span style="font-size: 10px;">R$ </span><span style="font-size: 20px;" id="total_boletos">0,00</span>
                        </div>
                    </div> -->
                </div>

            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://plentz.github.io/jquery-maskmoney/javascripts/jquery.maskMoney.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/js/jquery.mask.min.js"></script>

<script type="text/javascript">
    // funcoes //
    function formatarNumero(numero) {
        let numeroString = numero.replace('.', ',');
        let partes = numeroString.split(',');

        partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');

        return partes.join(',');
    }
    // fucoes //


    jQuery(document).ready(function($) {
        $("#clie_csv").autocomplete({
            source: "<?= base_url(); ?>/operacaocartao/autoCompleteCliente",
            minLength: 1,
            select: function(event, ui) {
                // console.log(ui.item.documento)
                $('#clie_csv').val(ui.item.label);
                $('#clie_csv_id').val(ui.item.id);
                $('#rg_cpf').val(ui.item.documento);
                return false;
            }
        });
    });

    $('#tipo-selecao').change(function() {
        if ($(this).val() == 'mes') {
            $('#selecao-dia').hide();
            $('#selecao-mes').show();

            $('#selecao-mes-ano').val('');
            $('#selecao-ano').val('');

        } else {
            $('#selecao-dia').show();
            $('#selecao-mes').hide();

            $('#csv-dia').val('');
        }
    });

    $('#select_all').on('change', function() {
        const isChecked = $(this).is(':checked');
        $('.select_row').prop('checked', isChecked);
        toggleDeleteButton();
    });

    function toggleDeleteButton() {
        const hasChecked = $('.select_row:checked').length > 0;
        $('#delete_selected').prop('disabled', !hasChecked);
    }

    $(document).on('change', '.select_row', function() {
        toggleDeleteButton();
    });

    $('#delete_selected').on('click', function() {
        const selectedIds = [];
        $('.select_row:checked').each(function() {
            selectedIds.push($(this).data('id-operacao'));
            // $(this).closest('tr').remove();
        });

        if (selectedIds.length > 0) {
            fetch(base_url + 'operacaocartao/excluirOperacoes', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({ ids: selectedIds })
            })
            .then(response => response.json())
            .then(responseData => {
                // console.log(responseData.status); 
                if (responseData.status == true) {  
                    swal({
                        title: "Sucesso!",
                        text: "Registros excluídos com sucesso.",
                        type: "success",
                        showConfirmButton: true,
                        allowEscapeKey : false,
                        allowOutsideClick: false
                    }, function() {
                        window.location.reload();   
                    });     
                } else {
                    swal('Erro!', 'Ocorreu um erro ao excluir os registros, tente novamente mais tarde!', 'error');
                }
            })
            .catch(error => {
                console.error('Erro ao excluir os registros:', error);  // Log do erro
                swal('Erro!', 'Ocorreu um erro ao excluir os registros, tente novamente mais tarde!', 'error');
            });
        }

        $('#delete_selected').prop('disabled', true);
    });

    $('#btn_buscar').click(function() {
        let tipo = $('#tipo-selecao').val();
        let data = tipo == 'mes' ? $('#selecao-ano').val() + '-' + $('#selecao-mes-ano').val() : $('#csv-dia').val();
        let btn = $(this);

        if (($('#selecao-mes-ano').val() == '' || $('#selecao-ano').val() == '') && tipo == 'mes') {
            swal('Aviso!', 'Selecione uma data.', 'info');
            return false;
        } else if (!data) {
            swal('Aviso!', 'Selecione uma data.', 'info');
            return false;
        }

        btn.prop('disabled', true);
        btn.text('Carregando...');

        let dados = {
            data: data,
            tipo: tipo
        };

        let formData = new FormData();

        for (let chave in dados) {
            if (dados.hasOwnProperty(chave)) {
                formData.append(chave, dados[chave]);
            }
        }

        fetch(base_url + 'operacaocartao/visualizarOperacoes', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                btn.prop('disabled', false);
                btn.text('Buscar');
                // console.log(data);
                // return false;
                if (data.status == 'true') {
                    let tabela = $('#table_csv_visualizar');
                    tabela.find('tbody tr').remove();

                    data.dados.forEach(function(item) {
                        let dataString = item.operacao_csv_data_transacao;
                        let data = new Date(dataString);
                        let dia = data.getDate();
                        let mes = data.getMonth() + 1;
                        let ano = data.getFullYear();

                        let dataFormatada = (dia < 10 ? '0' : '') + dia + '/' + (mes < 10 ? '0' : '') + mes + '/' + ano;

                        let row =
                            `<tr>
                                <td>
                                    <input type="checkbox" class="select_row" data-id-operacao="${item.operacao_csv_id_operacao}">
                                </td>
                                <td>${item.operacao_csv_parcelas || ''}</td>
                                <td>${item.operacao_csv_bandeira || ''}</td>
                                <td>${item.operacao_csv_tipo_operacao || ''}</td>
                                <td>${item.operacao_csv_valor_bruto || ''}</td>
                                <td data-data_operacao="${item.operacao_csv_data_transacao}">${dataFormatada}</td>
                                <td>${item.operacao_csv_banco === 'Stone' 
                                        ? (item.operacao_csv_stone_id || '') 
                                        : (item.operacao_csv_codigo_nsu || '')}
                                </td>
                                <td data-id-operacao="${item.operacao_csv_id_operacao}" data-id="${item.operacao_csv_id}" data-id-cli="${item.operacao_csv_id_cliente}" data-id-user="${item.operacao_csv_id_usuario}">
                                    ${item.operacao_csv_banco}
                                </td>
                                <td>${item.usuario_nome}</td>
                            </tr>`;
                        tabela.find('tbody').append(row);
                    });
                } else {
                    swal('Erro!', 'Registro não encontrado.', 'error');
                }
            })
            .catch(error => {
                console.error(error);
                swal('Erro!', 'Ocorreu um erro ao buscar os registros.', 'error');
                btn.prop('disabled', false);
                btn.text('Buscar');
            });
    });

    $('#btn_cancelar_csv').click(function() {
        let btn = $(this);
        // swal({
        //         title: 'Aviso!',
        //         text: 'Selecione um cliente.',
        //         type: 'success',
        //         buttons: true,
        //         dangerMode: true,
        //     }, function(isConfirm) {

        //     });
        // swal('Aviso!', 'Nenhum registro para importar.', 'success');
        // return false;
        tabela = $('#table_csv').find('tbody tr');

        if (tabela.length == 0) {
            swal('Aviso!', 'Nenhum registro para cancelar.', 'info');
            return false;
        }

        btn.prop('disabled', true);
        btn.text('Carregando...');

        let dados = [];

        for (let i = 0; i < tabela.length; i++) {
            dados.push({
                id: tabela.eq(i).find('td').eq(6).data('id')
            });
        }

        // console.log(dados);
        // return false;
        let formData = new FormData();
        formData.append('dados', JSON.stringify(dados));

        fetch(base_url + 'operacaocartao/cancelarOperacoes', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                if (data.status == 'true') {
                    swal({
                        title: 'Sucesso!',
                        text: 'Registros cancelados com sucesso.',
                        type: 'success',
                        buttons: true,
                        dangerMode: true,
                    }, function(isConfirm) {
                        window.location.reload();
                    });
                } else {
                    swal({
                        title: 'Erro!',
                        text: 'Não foi possível cancelar os registros.',
                        type: 'error',
                        buttons: true,
                        dangerMode: true,
                    }, function(isConfirm) {
                        window.location.reload();
                    });
                }
            })
            .catch(error => {
                swal({
                    title: 'Erro!',
                    text: 'Ocorreu um erro ao cancelar os registros.',
                    type: 'error',
                    buttons: true,
                    dangerMode: true,
                }, function(isConfirm) {
                    window.location.reload();
                });
            }).finally(() => {
                btn.prop('disabled', false);
                btn.text('Cancelar');
            });
    });


    $('#btn_importar_csv').click(function() {
        btn = $(this);
        tabela = $('#table_csv').find('tbody tr');

        if (tabela.length == 0) {
            swal('Aviso!', 'Nenhum registro para importar.', 'info');
            return false;
        }

        btn.prop('disabled', true);
        btn.text('Carregando...');

        let dados = [];
        for (let i = 0; i < tabela.length; i++) {
            // console.log(tabela.eq(i).find('td').eq(0).text());
            dados.push({
                parcelas: tabela.eq(i).find('td').eq(0).text(),
                bandeira: tabela.eq(i).find('td').eq(1).text(),
                tipo: tabela.eq(i).find('td').eq(2).text(),
                valor_bruto: tabela.eq(i).find('td').eq(3).text(),
                data_operacao: tabela.eq(i).find('td').eq(4).data('data_operacao'),
                codigo: tabela.eq(i).find('td').eq(5).text(),
                banco: tabela.eq(i).find('td').eq(6).text(),
                cliente: tabela.eq(i).find('td').eq(6).data('id-cli'),
                usuario: tabela.eq(i).find('td').eq(6).data('id-user'),
                id: tabela.eq(i).find('td').eq(6).data('id')
            });
        }

        let formData = new FormData();
        formData.append('dados', JSON.stringify(dados));

        fetch(base_url + 'operacaocartao/importarOperacoes', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                // console.log(data);
                // return false;
                if (data.status == 'true') {
                    swal({
                        title: 'Sucesso!',
                        text: 'Registros importados com sucesso.',
                        type: 'success',
                        buttons: true,
                        dangerMode: true,
                    }, function(isConfirm) {
                        window.location.reload();
                    });
                } else {
                    swal({
                        title: 'Erro!',
                        text: 'Não foi possível importar os registros.',
                        type: 'error',
                        buttons: true,
                        dangerMode: true,
                    }, function(isConfirm) {
                        window.location.reload();
                    });
                }
            })
            .catch(error => {
                console.error(error);
                swal({
                    title: 'Erro!',
                    text: 'Ocorreu um erro ao importar os registros.',
                    type: 'error',
                    buttons: true,
                    dangerMode: true,
                }, function(isConfirm) {
                    window.location.reload();
                });
            }).finally(() => {
                btn.prop('disabled', false);
                btn.text('Importar Todos');
            });
    });

    $('#btn_adicionar_csv').click(function() {
        let btn = $(this);

        let input = $('#csv')[0].files[0];

        if ($('#rg_cpf').val() === '' || $('#clie_csv_id').val() === '') {

            return false;
        }

        let cliente_id = $('#clie_csv_id').val();

        if (!input) {
            swal('Aviso!', 'Selecione um arquivo CSV.', 'info');
            return false;
        }

        $('body').css('cursor', 'progress');

        btn.prop('disabled', true);
        btn.text('Carregando...');

        let formData = new FormData();
        formData.append('csvFile', input);
        formData.append('cliente_id', cliente_id);

        fetch(base_url + 'operacaocartao/uploadCsv', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                // console.log(data);
                // return false;
                $('body').css('cursor', 'auto');

                if (data.status === 'true') {
                    if (data.qtdAdicionados > 0) {
                        swal('Sucesso!', 'Foram adicionados ' + data.qtdAdicionados + ' de ' + data.qtdRegistros + ' registros.', 'success');
                    } else {
                        if (data.qtdRegistros > 0) {
                            swal('Aviso!', 'Nenhum novo registro adicionado. Todos os ' + data.qtdRegistros + ' registros já existiam.', 'info');
                        } else {
                            swal('Aviso!', 'Nenhum registro processado.', 'info');
                        }
                    }

                    let tabela = $('#table_csv');
                    tabela.find('tbody tr').remove();

                    let valor_bruto_total = 0;
                    let valor_liquido_total = 0;

                    data.dados.forEach(function(item) {
                        valor_bruto_total += parseFloat(item.operacao_csv_valor_bruto);
                        valor_liquido_total += parseFloat(item.operacao_csv_valor_liquido);

                        let dataString = item.operacao_csv_data_transacao;
                        let data = new Date(dataString);
                        let dia = data.getDate();
                        let mes = data.getMonth() + 1;
                        let ano = data.getFullYear();

                        let dataFormatada = (dia < 10 ? '0' : '') + dia + '/' + (mes < 10 ? '0' : '') + mes + '/' + ano;

                        let row = '<tr>';
                        row += '<td>' + (item.operacao_csv_parcelas ? item.operacao_csv_parcelas : '') + '</td>';
                        row += '<td>' + (item.operacao_csv_bandeira ? item.operacao_csv_bandeira : '') + '</td>';
                        row += '<td>' + (item.operacao_csv_tipo_operacao ? item.operacao_csv_tipo_operacao : '') + '</td>';
                        row += '<td>' + (item.operacao_csv_valor_bruto ? item.operacao_csv_valor_bruto : '') + '</td>';
                        row += '<td data-data_operacao="' + item.operacao_csv_data_transacao + '">' + dataFormatada + '</td>';
                        row += '<td>' + ((item.operacao_csv_banco == 'Stone') ?
                            (item.operacao_csv_stone_id ? item.operacao_csv_stone_id : '') :
                            (item.operacao_csv_codigo_nsu ? item.operacao_csv_codigo_nsu : '')) + '</td>';
                        row += '<td data-id="' + item.operacao_csv_id + '" data-id-cli="' + item.operacao_csv_id_cliente + '" data-id-user="' + item.operacao_csv_id_usuario + '">' + item.operacao_csv_banco + '</td>';
                        row += '</tr>';
                        tabela.find('tbody').append(row);
                    });

                    $('#csv').val(null);
                    if (data.dados.length > 0) {
                        $('#btns_csv').show();

                        $('#total_valores').show();
                        $('#total_bruto').text(formatarNumero(valor_bruto_total.toFixed(2)));
                        $('#total_liquido').text(formatarNumero(valor_liquido_total.toFixed(2)));
                    } else {
                        $('#btns_csv').hide();

                        $('#total_valores').hide();
                    }
                } else {
                    swal('Erro!', data.message ? data.message : 'Não foi possível carregar o arquivo.', 'error');
                }
            })
            .catch(error => {
                console.error(error);
                swal('Erro!', 'Ocorreu um erro ao carregar o arquivo.', 'error');
                $('body').css('cursor', 'auto');
            }).finally(() => {
                btn.prop('disabled', false);
                btn.text('Processar');
            });
    });
</script>