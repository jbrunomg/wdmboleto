<style>
    .modal-content {
        background-color: #f3f3f3;
        box-shadow: 0px 0px 15px rgba(0, 0, 0, 0.1);
    }

    .modal-body {
        padding: 20px;
    }

    .modal-title {
        color: #333;
    }

    .close {
        position: absolute;
        right: 15px;
        top: 10px;
        opacity: 0.5;
        font-size: 1.5rem;
    }

    .close:hover {
        opacity: 1;
    }
</style>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css">

<div class="row" style="margin-bottom: 20px;">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-folder-upload position-left"></i> Arquivo CSV</h6>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body" <?= ($this->session->userdata('usuario_padrao') == 0) ? 'style="display: none;"' : 'style="display: block;"'; ?>>
                <div class="row">
                    <form id="FormCsv">
                        <div class="form-group col-md-8">
                            <label for="csv">Arquivo CSV</label>
                            <input type="file" name="csv" id="csv" class="form-control form-control-sm" accept=".csv">
                        </div>

                        <div class="form-group col-md-4">
                            <input type="submit" id="btnFormCsv" class="btn btn-sm btn-block btn-info" style="margin-top: 27px;" value="Carregar">
                        </div>
                    </form>
                </div>
            </div>

            <div id="resumoOperacaoCsv" class="row" style="margin: 10px; display: none;">
                <div class="col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Resumo da Operação</label>
                        <div class="alert alert-warning">
                            <p><strong>Registros:</strong> <span id="qtdRegistros">0</span></p>
                            <p><strong>Adicionados:</strong> <span id="qtdAdicionados">0</span></p>
                            <p><strong>Existentes:</strong> <span id="qtdExistente">0</span></p>
                            <p><strong>Cancelados:</strong> <span id="qtdCancelados">0</span></p>
                            <a data-toggle="modal" data-target="#meuModal" id="linkResumoOperacaoCsv">
                                Ver Detalhes
                            </a>
                        </div>
                    </div>
                </div>

                <form id="addUploadCsv">
                    <input type="hidden" name="dadosUploadCsv" id="dadosUploadCsv" value="" />
                    <div class="col-xs-12 text-right">
                        <button type="button" id="btnCancelar" class="btn btn-default col-xs-12" style="margin-bottom: 10px;">Cancelar</button>
                        <button type="submit" id="btnConfirmar" class="btn btn-success col-xs-12">Confirmar</button>
                    </div>
                </form>
            </div>

            <div class="progress" id="progressCsv" style="margin: 10px; display: none;">
                <div class="progress-bar progress-bar-striped active" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%">
                    <span class="sr-only">100% Complete</span>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-calendar52 position-left"></i> Lançamentos não importados</h6>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body" <?= ($this->session->userdata('usuario_padrao') == 0) ? 'style="display: none;"' : 'style="display: block;"'; ?>>

                <div class="card-body">
                    <div class="row" style="overflow-x: auto;">
                        <div class="form-group col-md-12" style="max-height: 500px;">
                            <table id="table_csv" class="table table-hover table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <!-- <th width="40%">Código Boleto</th> -->
                                        <th>Cliente</th>
                                        <th>Banco</th>
                                        <th>N. Parcelas</th>
                                        <th>Cartão / Bandeira</th>
                                        <th>Tipo</th>
                                        <th>Valor Bruto</th>
                                        <th>Código Autorização</th>
                                        <th>Data Operação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- <?php var_dump($lancamentos_importar[0]) ?> -->
                                    <?php
                                    $valor_bruto_total = 0;
                                    $valor_liquido_total = 0;
                                    foreach ($lancamentos_importar as $key => $value) :
                                        $valor_bruto_total += $value->operacao_csv_valor_bruto;
                                        $valor_liquido_total += $value->operacao_csv_valor_liquido;
                                    ?>
                                        <tr>
                                            <td>
                                                <?= empty($value->usuario_nome) ? 'Sem Cadastro' : $value->usuario_nome ?>
                                            </td>
                                            <td
                                                data-id="<?= $value->operacao_csv_id ?>"
                                                data-id-cli="<?= $value->operacao_csv_id_cliente ?>"
                                                data-id-user="<?= $value->operacao_csv_id_usuario ?>">
                                                <?= $value->operacao_csv_banco; ?>
                                            </td>
                                            <td>
                                                <?= $value->operacao_csv_parcelas; ?>
                                            </td>
                                            <td>
                                                <?= $value->operacao_csv_bandeira; ?>
                                            </td>
                                            <td>
                                                <?= $value->operacao_csv_tipo_operacao; ?>
                                            </td>
                                            <td data-valor_bruto="<?= $value->operacao_csv_valor_bruto ?>">
                                                <?= number_format($value->operacao_csv_valor_bruto, 2, ',', '.'); ?>
                                            </td>
                                            <td>
                                                <?= $value->operacao_csv_banco == "Stone" ? $value->operacao_csv_stone_id : $value->operacao_csv_codigo_nsu; ?>
                                            </td>
                                            <td data-data_operacao="<?= date('Y-m-d', strtotime($value->operacao_csv_data_transacao)) ?>">
                                                <?= date('d/m/Y', strtotime($value->operacao_csv_data_transacao)); ?>
                                            </td>
                                        </tr>
                                    <?php endforeach ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="total_valores" class="row center" style="<?= count($lancamentos_importar) > 0  ? 'display: block' : 'display: none' ?>">
                        <div class="form-group col-md-10 offset-md-4" style="text-align: left;">
                            <span>Total Bruto</span>
                            <br>
                            <span style="font-size: 20px; font-weight: bold;">R$ </span><span style="font-size: 35px; font-weight: 700;" id="total_bruto"><?= number_format($valor_bruto_total, 2, ',', '.') ?></span>
                        </div>
                        <div class="form-group col-md-2" style="text-align: right;">
                            <span>Total Líquido</span>
                            <br>
                            <span style="font-size: 20px; font-weight: bold;">R$ </span><span style="font-size: 35px; font-weight: 700;" id="total_liquido"><?= number_format($valor_liquido_total, 2, ',', '.') ?></span>
                        </div>
                    </div>
                </div>
                <div id="btns_csv" class="form-group col-md-12" style="<?= count($lancamentos_importar) > 0  ? 'display: block' : 'display: none' ?>">
                    <button type="button" id="btn_importar_csv" class="btn btn-sm btn-block btn-info" style="margin-top: 27px;">Importar Todos</button>
                    <button type="button" id="btn_cancelar_csv" class="btn btn-sm btn-block btn-danger" style="margin-top: 10px;">Cancelar</button>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title"><i class="icon-eye position-left"></i> Visualizar lançamentos</h6>

                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                        <!-- <li><a data-action="reload"></a></li>
                            <li><a data-action="close"></a></li> -->
                    </ul>
                </div>
            </div>

            <div class="panel-body" <?= ($this->session->userdata('usuario_padrao') == 0) ? 'style="display: none;"' : 'style="display: block;"'; ?>>
                <div class="card-body">

                    <div class="row">
                        <!-- Seletor para escolher entre data/dia ou mês -->
                        <div class="form-group col-md-4">
                            <label for="tipo-selecao">Buscar por</label>
                            <select id="tipo-selecao" class="form-control form-control-sm">
                                <option value="mes">Mês</option>
                                <option value="data">Data</option>
                            </select>
                        </div>

                        <!-- Input para selecionar a data ou mês, dependendo da escolha -->
                        <div class="form-group col-md-4" id="selecao-mes">
                            <label for="csv-mes-ano">Selecione o mês e o ano</label>
                            <div class="row">
                                <div class="col-md-6">
                                    <select id="selecao-mes-ano" class="form-control form-control-sm">
                                        <option value="">Selecione o mês</option>
                                        <option value="01">Janeiro</option>
                                        <option value="02">Fevereiro</option>
                                        <option value="03">Março</option>
                                        <option value="04">Abril</option>
                                        <option value="05">Maio</option>
                                        <option value="06">Junho</option>
                                        <option value="07">Julho</option>
                                        <option value="08">Agosto</option>
                                        <option value="09">Setembro</option>
                                        <option value="10">Outubro</option>
                                        <option value="11">Novembro</option>
                                        <option value="12">Dezembro</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="selecao-ano" id="selecao-ano" placeholder="Ano" class="form-control form-control-sm">
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-md-4" id="selecao-dia" style="display: none;">
                            <label for="csv-dia">Selecione a data</label>
                            <input type="date" name="csv-dia" id="csv-dia" class="form-control form-control-sm">
                        </div>

                        <!-- Botão para processar -->
                        <div class="form-group col-md-4">
                            <button type="button" id="btn_buscar" class="btn btn-sm btn-block btn-info" style="margin-top: 27px;">Buscar</button>
                        </div>
                    </div>

                    <div class="row" style="overflow-x: auto;">
                        <div class="form-group col-md-12">
                            <button id="delete_selected" class="btn btn-danger" disabled style="margin-bottom: 5px;">Excluir Selecionados</button>
                            <table id="table_csv_visualizar" class="table table-hover table-bordered table-sm">
                                <thead>
                                    <tr>
                                        <th>
                                            <input type="checkbox" id="select_all" title="Selecionar Todos">
                                            Todos
                                        </th>
                                        <th>N. Parcelas</th>
                                        <th>Cartão / Bandeira</th>
                                        <th>Tipo</th>
                                        <th width="15%">Valor</th>
                                        <th>Data Operação</th>
                                        <th>Código Autorização</th>
                                        <th>Banco</th>
                                        <th>Cliente</th>
                                    </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- <div class="row center">
                        <div class="form-group col-md-10 offset-md-4" style="text-align: left;">
                            <span class="lbl_tt_transacao">Total da Transação</span>
                            <br>
                            <span style="font-size: 20px; font-weight: bold;">R$ </span><span style="font-size: 35px; font-weight: 700;" id="total_transacao">0,00</span>
                        </div>
                        <div class="form-group col-md-2" style="text-align: right;">
                            <span style="font-size: 12px;">Total dos Cartão</span>
                            <br>
                            <span style="font-size: 10px;">R$ </span><span style="font-size: 20px;" id="total_boletos">0,00</span>
                        </div>
                    </div> -->
                </div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="meuModal" tabindex="-1" role="dialog" aria-labelledby="meuModalLabel">
    <div class="modal-dialog modal-lg" role="document"> <!-- modal-lg para largura maior -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="meuModalLabel">Operações</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="table_csv_resumo" class="table table-hover table-bordered table-striped" style="max-height: 500px;">
                        <thead class="thead-dark">
                            <tr>
                                <th>Cliente</th>
                                <th>Banco</th>
                                <th>N. Parcelas</th>
                                <th>Cartão / Bandeira</th>
                                <th>Tipo</th>
                                <th>Valor Bruto</th>
                                <th>Código Autorização</th>
                                <th>Data Operação</th>
                            </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </div>
        </div>
    </div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://plentz.github.io/jquery-maskmoney/javascripts/jquery.maskMoney.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>public/assets/js/jquery.mask.min.js"></script>

<script type="text/javascript">
    // funcoes //
    function formatarNumero(numero) {
        let numeroString = numero.replace('.', ',');
        let partes = numeroString.split(',');

        partes[0] = partes[0].replace(/\B(?=(\d{3})+(?!\d))/g, '.');

        return partes.join(',');
    }
    // fucoes //


    jQuery(document).ready(function($) {
        $("#clie_csv").autocomplete({
            source: "<?= base_url(); ?>/operacaocartao/autoCompleteCliente",
            minLength: 1,
            select: function(event, ui) {
                // console.log(ui.item.documento)
                $('#clie_csv').val(ui.item.label);
                $('#clie_csv_id').val(ui.item.id);
                $('#rg_cpf').val(ui.item.documento);
                return false;
            }
        });
    });

    $('#tipo-selecao').change(function() {
        if ($(this).val() == 'mes') {
            $('#selecao-dia').hide();
            $('#selecao-mes').show();

            $('#selecao-mes-ano').val('');
            $('#selecao-ano').val('');

        } else {
            $('#selecao-dia').show();
            $('#selecao-mes').hide();

            $('#csv-dia').val('');
        }
    });

    $('#select_all').on('change', function() {
        const isChecked = $(this).is(':checked');
        $('.select_row').prop('checked', isChecked);
        toggleDeleteButton();
    });

    function toggleDeleteButton() {
        const hasChecked = $('.select_row:checked').length > 0;
        $('#delete_selected').prop('disabled', !hasChecked);
    }

    $(document).on('change', '.select_row', function() {
        toggleDeleteButton();
    });

    $('#delete_selected').on('click', function() {
        const selectedIds = [];
        $('.select_row:checked').each(function() {
            selectedIds.push($(this).data('id-operacao'));
            // $(this).closest('tr').remove();
        });

        if (selectedIds.length > 0) {
            fetch(base_url + 'operacaocartao/excluirOperacoes', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        ids: selectedIds
                    })
                })
                .then(response => response.json())
                .then(data => {
                    // console.log(data.status); 
                    if (data.status == true) {
                        swal({
                            title: "Sucesso!",
                            text: "Registros excluídos com sucesso.",
                            type: "success",
                            showConfirmButton: true,
                            allowEscapeKey: false,
                            allowOutsideClick: false,
                        }, function() {
                            window.location.reload();
                        });
                    } else {
                        swal('Erro!', 'Ocorreu um erro ao excluir os registros, tente novamente mais tarde!', 'error');
                    }
                })
                .catch(error => {
                    console.error('Erro ao excluir os registros:', error); // Log do erro
                    swal('Erro!', 'Ocorreu um erro ao excluir os registros, tente novamente mais tarde!', 'error');
                });
        }

        $('#delete_selected').prop('disabled', true);
    });

    $('#btn_buscar').click(function() {
        let tipo = $('#tipo-selecao').val();
        let data = tipo == 'mes' ? $('#selecao-ano').val() + '-' + $('#selecao-mes-ano').val() : $('#csv-dia').val();
        let btn = $(this);

        if (($('#selecao-mes-ano').val() == '' || $('#selecao-ano').val() == '') && tipo == 'mes') {
            swal('Aviso!', 'Selecione uma data.', 'info');
            return false;
        } else if (!data) {
            swal('Aviso!', 'Selecione uma data.', 'info');
            return false;
        }

        btn.prop('disabled', true);
        btn.text('Carregando...');

        let dados = {
            data: data,
            tipo: tipo
        };

        let formData = new FormData();

        for (let chave in dados) {
            if (dados.hasOwnProperty(chave)) {
                formData.append(chave, dados[chave]);
            }
        }

        fetch(base_url + 'operacaocartao/visualizarOperacoes', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                btn.prop('disabled', false);
                btn.text('Buscar');
                // console.log(data);
                // return false;
                if (data.status == 'true') {
                    let tabela = $('#table_csv_visualizar');
                    tabela.find('tbody tr').remove();

                    data.dados.forEach(function(item) {
                        let dataString = item.operacao_csv_data_transacao;
                        let data = new Date(dataString);
                        let dia = data.getDate();
                        let mes = data.getMonth() + 1;
                        let ano = data.getFullYear();

                        let dataFormatada = (dia < 10 ? '0' : '') + dia + '/' + (mes < 10 ? '0' : '') + mes + '/' + ano;

                        let row =
                            `<tr>
                                <td>
                                    <input type="checkbox" class="select_row" data-id-operacao="${item.operacao_csv_id_operacao}">
                                </td>
                                <td>${item.operacao_csv_parcelas || ''}</td>
                                <td>${item.operacao_csv_bandeira || ''}</td>
                                <td>${item.operacao_csv_tipo_operacao || ''}</td>
                                <td>${item.operacao_csv_valor_bruto || ''}</td>
                                <td data-data_operacao="${item.operacao_csv_data_transacao}">${dataFormatada}</td>
                                <td>${item.operacao_csv_banco === 'Stone' 
                                        ? (item.operacao_csv_stone_id || '') 
                                        : (item.operacao_csv_codigo_nsu || '')}
                                </td>
                                <td data-id-operacao="${item.operacao_csv_id_operacao}" data-id="${item.operacao_csv_id}" data-id-cli="${item.operacao_csv_id_cliente}" data-id-user="${item.operacao_csv_id_usuario}">
                                    ${item.operacao_csv_banco}
                                </td>
                                <td>${item.usuario_nome === null ? 'Sem Registro' : item.usuario_nome}</td>
                            </tr>`;
                        tabela.find('tbody').append(row);
                    });
                } else {
                    swal('Erro!', 'Registro não encontrado.', 'error');
                }
            })
            .catch(error => {
                console.error(error);
                swal('Erro!', 'Ocorreu um erro ao buscar os registros.', 'error');
                btn.prop('disabled', false);
                btn.text('Buscar');
            });
    });

    $('#btn_cancelar_csv').click(function() {
        let btn = $(this);

        tabela = $('#table_csv').find('tbody tr');

        if (tabela.length == 0) {
            swal('Aviso!', 'Nenhum registro para cancelar.', 'info');
            return false;
        }

        btn.prop('disabled', true);
        btn.text('Carregando...');

        let dados = [];

        for (let i = 0; i < tabela.length; i++) {
            dados.push({
                id: tabela.eq(i).find('td').eq(1).data('id')
            });
        }

        // console.log(dados);
        // return false;
        let formData = new FormData();
        formData.append('dados', JSON.stringify(dados));

        fetch(base_url + 'operacaocartao/cancelarOperacoes', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                if (data.status == 'true') {
                    swal({
                        title: 'Sucesso!',
                        text: 'Registros cancelados com sucesso.',
                        type: 'success',
                        buttons: true,
                        dangerMode: true,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    }, function(isConfirm) {
                        window.location.reload();
                    });
                } else {
                    swal({
                        title: 'Erro!',
                        text: 'Não foi possível cancelar os registros.',
                        type: 'error',
                        buttons: true,
                        dangerMode: true,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    }, function(isConfirm) {
                        window.location.reload();
                    });
                }
            })
            .catch(error => {
                swal({
                    title: 'Erro!',
                    text: 'Ocorreu um erro ao cancelar os registros.',
                    type: 'error',
                    buttons: true,
                    dangerMode: true,
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                }, function(isConfirm) {
                    window.location.reload();
                });
            }).finally(() => {
                btn.prop('disabled', false);
                btn.text('Cancelar');
            });
    });


    $('#btn_importar_csv').click(function(e) {
        e.preventDefault();
        btn = $(this);
        tabela = $('#table_csv').find('tbody tr');

        if (tabela.length == 0) {
            swal('Aviso!', 'Nenhum registro para importar.', 'info');
            return false;
        }

        btn.prop('disabled', true);
        btn.text('Carregando...');

        let dados = [];
        for (let i = 0; i < tabela.length; i++) {
            dados.push({
                cliente: tabela.eq(i).find('td').eq(1).data('id-cli'), // ID do cliente
                usuario: tabela.eq(i).find('td').eq(1).data('id-user'), // ID do usuário
                id: tabela.eq(i).find('td').eq(1).data('id'), // ID da operação
                banco: tabela.eq(i).find('td').eq(1).text().trim(), // Banco
                parcelas: parseInt(tabela.eq(i).find('td').eq(2).text()), // Parcelas (convertido para número)
                bandeira: tabela.eq(i).find('td').eq(3).text().trim(), // Bandeira
                tipo: tabela.eq(i).find('td').eq(4).text().trim(), // Tipo de operação
                valor_bruto: tabela.eq(i).find('td').eq(5).data('valor_bruto'), // Valor bruto (convertido para número)
                codigo: tabela.eq(i).find('td').eq(6).text().trim(), // Código (NSU ou Stone ID)
                data_operacao: tabela.eq(i).find('td').eq(7).data('data_operacao'), // Data da operação
            });
        }

        // console.log(dados[0].valor_bruto);
        // return;
        let formData = new FormData();
        formData.append('dados', JSON.stringify(dados));

        fetch(base_url + 'operacaocartao/importarOperacoes', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                // console.log(data);
                // return false;
                if (data.status == 'true') {
                    swal({
                        title: 'Sucesso!',
                        text: 'Registros importados com sucesso.',
                        type: 'success',
                        buttons: true,
                        dangerMode: true,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    }, function(isConfirm) {
                        window.location.reload();
                    });
                } else {
                    swal({
                        title: 'Erro!',
                        text: 'Não foi possível importar os registros.',
                        type: 'error',
                        buttons: true,
                        dangerMode: true,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    }, function(isConfirm) {
                        window.location.reload();
                    });
                }
            })
            .catch(error => {
                console.error(error);
                swal({
                    title: 'Erro!',
                    text: 'Ocorreu um erro ao importar os registros.',
                    type: 'error',
                    buttons: true,
                    dangerMode: true,
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                }, function(isConfirm) {
                    window.location.reload();
                });
            }).finally(() => {
                btn.prop('disabled', false);
                btn.text('Importar Todos');
            });
    });

    $('#FormCsv').submit(function(e) {
        e.preventDefault();
        let btn = $('#btnFormCsv');
        let input = $('#csv')[0].files[0];

        if (!input) {
            swal('Aviso!', 'Selecione um arquivo CSV.', 'info');
            return false;
        }

        $('#progressCsv').show();
        $('#resumoOperacaoCsv').hide();
        $('#qtdRegistros').text('0');
        $('#qtdAdicionados').text('0');
        $('#qtdExistente').text('0');
        $('#qtdCancelados').text('0');

        $('body').css('cursor', 'progress');

        btn.prop('disabled', true);
        btn.text('Carregando...');
        let formData = new FormData();
        formData.append('csvFile', input);

        fetch(base_url + 'operacaocartao/uploadCsv2', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                console.log(data);

                $('body').css('cursor', 'auto');
                if (data.status === 'true') {

                    tabela = $('#table_csv_resumo');
                    tabela.find('tbody tr').remove();
                    data.dados.forEach(function(item) {
                        let dataString = item.operacao_csv_data_transacao;
                        let data = new Date(dataString);
                        let dia = data.getDate();
                        let mes = data.getMonth() + 1;
                        let ano = data.getFullYear();

                        let dataFormatada = (dia < 10 ? '0' : '') + dia + '/' + (mes < 10 ? '0' : '') + mes + '/' + ano;

                        let row =
                            `<tr>
                                <td>${item.usuario_nome === null ? 'Sem Registro' : item.usuario_nome}</td>
                                <td>${item.operacao_csv_banco}</td>
                                <td>${item.operacao_csv_parcelas || ''}</td>
                                <td>${item.operacao_csv_bandeira || ''}</td>
                                <td>${item.operacao_csv_tipo_operacao || ''}</td>
                                <td>${item.operacao_csv_valor_bruto || ''}</td>
                                <td>${item.operacao_csv_banco === 'Stone' ? (item.operacao_csv_stone_id || '') : (item.operacao_csv_codigo_nsu || '')}</td>
                                <td>${dataFormatada}</td>
                            </tr>`;
                        tabela.find('tbody').append(row);
                    });

                    $('#qtdRegistros').text(data.qtdRegistros);
                    $('#qtdAdicionados').text(data.qtdAdicionados);
                    $('#qtdExistente').text(data.qtdExistente);
                    $('#qtdCancelados').text(data.qtdCancelados);
                    data.qtdAdicionados <= 0 ? $('#linkResumoOperacaoCsv').hide() : $('#linkResumoOperacaoCsv').show();

                    $('#resumoOperacaoCsv').show();
                    $('#dadosUploadCsv').val(JSON.stringify(data.dados));
                } else {
                    swal('Erro!', data.message ? data.message : 'Não foi possível carregar o arquivo.', 'error');
                }
            })
            .catch(error => {
                console.error(error);
                swal('Erro!', 'Ocorreu um erro ao carregar o arquivo.', 'error');
                $('body').css('cursor', 'auto');
            }).finally(() => {
                btn.prop('disabled', false);
                btn.text('Carregar');
                $('#progressCsv').hide();
                $('#csv').val(null);
            });
    });

    $('#addUploadCsv').submit(function(e) {
        e.preventDefault();

        if ($('#dadosUploadCsv').val().trim() === '') {
            swal('Aviso!', 'Não foi possivel carregar as informações do arquivo, tente novamente mais tarde.', 'info');
            return false;
        }

        let btn = $('#btnConfirmar');
        btn.prop('disabled', true);
        btn.text('Carregando...');

        let formData = new FormData();
        formData.append('dados', $('#dadosUploadCsv').val());

        fetch(base_url + 'operacaocartao/addUploadCsv2', {
                method: 'POST',
                body: formData
            })
            .then(response => response.json())
            .then(data => {
                if (data.status == 'true') {
                    swal({
                        title: 'Sucesso!',
                        text: 'Registros importados com sucesso.',
                        type: 'success',
                        buttons: true,
                        dangerMode: true,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    }, function(isConfirm) {
                        window.location.reload();
                    });
                } else {
                    swal({
                        title: 'Erro!',
                        text: 'Não foi possível importar os registros.',
                        type: 'error',
                        buttons: true,
                        dangerMode: true,
                        allowEscapeKey: false,
                        allowOutsideClick: false,
                    }, function(isConfirm) {
                        window.location.reload();
                    });
                }
            })
            .catch(error => {
                console.error(error);
                swal({
                    title: 'Erro!',
                    text: 'Ocorreu um erro ao importar os registros.',
                    type: 'error',
                    buttons: true,
                    dangerMode: true,
                    allowEscapeKey: false,
                    allowOutsideClick: false,
                }, function(isConfirm) {
                    window.location.reload();
                });
            }).finally(() => {
                btn.prop('disabled', false);
                btn.text('Confirmar');
            });
    });

    $('#btnCancelar').click(function() {
        $('#resumoOperacaoCsv').hide();
        $('#csv').val(null);
    });
</script>