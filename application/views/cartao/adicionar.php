<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Cadastro de Cartões</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados do Cartões:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

			        <div class="form-group">
			          <label class="control-label col-lg-2">Cnpj Administradora:</label>
			          <div class="col-lg-5">
			            <input  type="text" class="form-control" placeholder="cnpj" name="cnpj_credenciadora" id="cnpj_credenciadora" value="<?php echo set_value('cnpj_credenciadora'); ?>" onkeypress="$(this).mask('00.000.000/0000-00')">
			          <?php echo form_error('cnpj_credenciadora'); ?>
			          </div>                    
			        </div> 

			        <div class="form-group">
			          <label class="control-label col-lg-2">Nome Administradora:</label>
			          <div class="col-lg-5">
			            <input  type="text" class="form-control" placeholder="Ex: PagSeguro, Rede, Cielo" name="nome_credenciadora" id="nome_credenciadora" value="<?php echo set_value('nome_credenciadora'); ?>">
			          <?php echo form_error('nome_credenciadora'); ?>
			          </div>                    
			        </div> 

			    <div class="card mb-4 mb-lg-0">
		          <div class="card-body">
		            <p><strong>Bandeiras</strong></p>
		          <!--   <img class="me-2" width="45px"
		              src="<?php echo base_url();?>public/assets/images/cart/visa.svg"
		              alt="Visa" /> -->
		            <img class="me-2" width="400px"
		              src="<?php echo base_url();?>public/assets/images/cart/aceitacao_cartao.png"
		              alt="Cartoes" />
		       
		          </div>
		        </div>                		

				</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
