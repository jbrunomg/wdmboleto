<!-- Form horizontal -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<h5 class="panel-title">Visualizar Cartão</h5>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<!-- <li><a data-action="close"></a></li> -->
        	</ul>
    	</div>
	</div>

	<div class="panel-body">
		<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe" method="post" enctype="multipart/form-data">
			<fieldset class="content-group">
				<legend class="text-bold">Dados Cartão:</legend>

				<input disabled type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

				<input disabled type="hidden" name="categoria_cart_id" value="<?php echo $dados[0]->categoria_cart_id; ?>" />

		

				<div class="form-group">
					<label class="control-label col-lg-2">Cnpj Administradora:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Cnpj Cartão" name="cnpj_credenciadora" id="cnpj_credenciadora" value="<?php echo $dados[0]->categoria_cart_cnpj_credenciadora; ?>">
					<?php echo form_error('categoria_cart_cnpj_credenciadora'); ?>
					</div>										
				</div>
				
				<div class="form-group">
					<label class="control-label col-lg-2">Nome Administradora:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Nome Cartão" name="nome_credenciadora" id="nome_credenciadora" value="<?php echo $dados[0]->categoria_cart_nome_credenciadora; ?>">
					<?php echo form_error('categoria_cart_nome_credenciadora'); ?>
					</div>										
				</div>

				<div class="form-group">
					<label class="control-label col-lg-2">Bandeira Cartão:</label>
					<div class="col-lg-5">
						<input disabled type="text" class="form-control" placeholder="Bandeira Cartão" name="cart_descricao" id="cart_descricao" value="<?php echo $dados[0]->categoria_cart_descricao; ?>">
					<?php echo form_error('categoria_cart_descricao'); ?>
					</div>										
				</div>

			
								
		
            </fieldset>  
		</form>
	</div>
</div>
