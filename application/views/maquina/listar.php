<div class="panel panel-flat">
    <div class="panel-heading">
        <?php if(checarPermissao('aMaquinetaCartao')){ ?>
        <div class="col-md-3 col-sm-6 row">
            <a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
        </div>
        <?php } ?>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!-- <li><a data-action="reload"></a></li> -->
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <br>
    <?php if(! empty($success)): ?>
        <div class="text-success">Maquina adicionada com sucesso!!!</div>
    <?php endif ?>
    <div class="text-danger"> <?php echo ! empty($atualizar) ? $atualizar : '' ?></div>
    <table class="table datatable-button-html5-columns">
        <thead>
            <tr>

                <th>Administradora</th>
                <th>N° da máquina do cartão</th>
                <th>Situação</th>
                <th>Data do cadastro</th>
                <th></th>

            </tr>
        </thead>
        <tbody>
            <?php foreach($dados as $valor): ?>
                <tr>
                    <td><?php echo $valor->administradora_cartao ?></td>
                    <td><?php echo $valor->maquina_numero ?></td>
                    <td><?php echo $valor->usuario_nome.'<br>'.$valor->maquina_situacao?></td>
                    <td><?php echo $valor->maquina_data_cadastro ?></td>
                    <td>
                        <ul class="icons-list">
                       
                            <li class="text-grey"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/listarClientes/<?php echo $valor->maquina_id.'/'.$valor->maquina_numero ?>" data-popup="tooltip" title="Cliente"><i class="icon-reading"></i></a></li>
                            <?php if(checarPermissao('eMaquinetaCartao')){ ?>    
                            <li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $valor->maquina_id; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
                            <?php } ?>
                            <?php if(checarPermissao('dMaquinetaCartao')){ ?>
                            <li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $valor->maquina_id; ?>"><i class="icon-trash"></i></a></li>
                            <?php } ?>
                            <?php if(checarPermissao('vMaquinetaCartao')){ ?>
                            <li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/visualizar/<?php echo $valor->maquina_id; ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>
                            <?php } ?>
                        </ul>
                    </td>

                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

</div>