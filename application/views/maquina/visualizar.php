<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Visualizar dados da máquina</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!-- <li><a data-action="reload"></a></li> -->
                <!-- <li><a data-action="close"></a></li> -->
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <fieldset class="content-group">

            <legend class="text-bold">Dados da máquina</legend>
            <div class="list-group d-flex">
                <div class="list-group-item">
                    <strong class="list-group-item-text">Número da máquina:</strong>
                    <span><?php echo $dados[0]->maquina_numero ?></span>
                </div>
                <div class="list-group-item">
                    <strong class="list-group-item-text">Observação:</strong>
                    <span><?php echo $dados[0]->maquina_observacao ?: 'Nenhuma observação registrada' ?></span>
                </div>
                <div class="list-group-item">
                    <strong class="list-group-item-text">Data de registro:</strong>
                    <span><?php echo date('d/m/Y',strtotime($dados[0]->maquina_data_cadastro)) ?></span>
                </div>
                <div class="list-group-item">
                    <strong class="list-group-item-text">Data de atualização do registro:</strong>
                    <span><?php echo $dados[0]->maquina_data_atualizacao ? date('d/m/Y',strtotime($dados[0]->maquina_data_atualizacao)) : 'Não houve atualização de dados para está maquina' ?></span>
                </div>
                <div class="list-group-item">
                    <strong class="list-group-item-text">Administradora do cartão:</strong>
                    <span><?php echo $dados[0]->administradora_cartao[0]->administradora_cartao ?></span>
                </div>
            </div>

        </fieldset>
        <div id="maquinas_clientes_listar"></div>
    </div>
</div>


<script>

    function alterarUrlApagandoUltimaRota(currentUrl) {
        let ultimaBarraDaUrl = currentUrl.lastIndexOf("/");
        return currentUrl.substring(0, ultimaBarraDaUrl);
    }
    const urlAtual = window.location.href;
    //Apagando ultimo endereçamento de rota, para restar apenas /maquinas/editar ao final da url
    const urlEditada = alterarUrlApagandoUltimaRota(urlAtual);
    //Apagando ultimo endereçamento de rota, para restar apenas /maquinas/ ao final da url
    const url = alterarUrlApagandoUltimaRota(urlEditada);

    const partirUrlAtual = urlAtual.split('/');
    const id = partirUrlAtual[partirUrlAtual.length - 1];

    function carregarMaquinasClientesListar() {
        fetch(`${url}/listarClientesEmVisualizarMaquinas/${id}`)
            .then(response => response.text())
            .then(html => {
                document.querySelector('#maquinas_clientes_listar').innerHTML = html
            })
            .catch(error => console.log(error));
    }
    carregarMaquinasClientesListar()
</script>