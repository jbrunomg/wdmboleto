<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Editar máquina</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!-- <li><a data-action="reload"></a></li> -->
                <!-- <li><a data-action="close"></a></li> -->
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <form  class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarExe/<?php echo $this->uri->segment(3) ?>" method="post" enctype="multipart/form-data">
            <fieldset class="content-group">

                <legend class="text-bold">Dados da máquina:</legend>

                <div class="form-group">
                    <label class="control-label col-lg-2">CNPJ do cartão</label>
                    <div class="col-lg-5">
                        <select  class="form-control" name="maquina_categoria_cartao_id" id="cartao" >
                            <option value="">Selecione</option>
                            <?php foreach ($cartao as $valor): ?>
                                <?php $selected = ($valor->categoria_cart_id == $dados[0]->maquina_categoria_cartao_id)?'SELECTED': ''; ?>
                                <option value="<?php echo $valor->categoria_cart_id; ?>" <?php echo $selected; ?>><?php echo $valor->administradora_cartao; ?></option>
                            <?php endforeach; ?>
                        </select>
                        <?php echo form_error('cartao'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Número da máquina</label>
                    <div class="col-lg-5">
                        <input type="text" class="form-control" placeholder="N° da máquina" name="maquina_numero" id="maquina_numero" value="<?php echo $dados[0]->maquina_numero ?>" title="Apenas números são permitidos." required>
                        <div class="form-text text-success" id="numeroTextoConfirmacao"></div>
                        <div class="form-text text-danger" id="numeroTextoError"></div>
                        <div class="form-text text-default">Apenas números são permitidos</div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-lg-2">Observação</label>
                    <div class="col-lg-5">
                        <textarea class="form-control" placeholder="Observação" name="maquina_observacao" id="maquina_observacao"><?php echo $dados[0]->maquina_observacao ?></textarea>
                    </div>
                </div>

                <legend class="text-bold">Dados do usuário</legend>
                <em>Em produção...</em>

            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn bg-teal">Atualizar <i class="icon-arrow-right14 position-right"></i></button>
            </div>

        </form>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('#cartaoCNPJ').select2();
    });
</script>

<script>
    function alterarUrlApagandoUltimaRota(currentUrl) {
        let ultimaBarraDaUrl = currentUrl.lastIndexOf("/");
        return currentUrl.substring(0, ultimaBarraDaUrl);
    }
    const urlAtual = window.location.href;
    //Apagando ultimo endereçamento de rota, para restar apenas /maquinas/editar ao final da url
    const urlEditada = alterarUrlApagandoUltimaRota(urlAtual);
    //Apagando ultimo endereçamento de rota, para restar apenas /maquinas/ ao final da url
    const url = alterarUrlApagandoUltimaRota(urlEditada);

    const maquinaNumeroInput = document.querySelector('#maquina_numero');
    const maquinaNumeroTextoConfirmacao = document.querySelector('#numeroTextoConfirmacao');
    const maquinaNumeroTextoError = document.querySelector('#numeroTextoError');

    async function validarNumero() {
        let response = false;

        //recuperando id
        const currentUrl = window.location.href;
        const dividirUrl = currentUrl.split("/");
        const id = dividirUrl[dividirUrl.length - 1];

        const data = new FormData();
        data.append('numero',maquinaNumeroInput.value);

        await fetch(`${url}verificandoNumeroDaMaquina/${id}`,{
            method: 'POST',
            body: data
        })
            .then(response => response.json())
            .then(data => {
                if(data.success) {
                    maquinaNumeroInput.classList.add('border-success');
                    maquinaNumeroTextoConfirmacao.textContent = 'Número validado com sucesso';
                    console.log('cheguei aqui');
                    response = true;
                } else {
                    maquinaNumeroInput.classList.add('border-danger');
                    maquinaNumeroTextoError.textContent = 'Número já cadastrado';
                }
            })
            .catch(error => console.log(error))
        return response;
    }

    maquinaNumeroInput.addEventListener('blur',() => {
        if(maquinaNumeroInput.value) {
            validarNumero();
        }
    });
    maquinaNumeroInput.addEventListener('input', () => {
        maquinaNumeroInput.classList.remove('border-success');
        maquinaNumeroInput.classList.remove('border-danger');
        maquinaNumeroTextoConfirmacao.textContent = '';
        maquinaNumeroTextoError.textContent = '';
    });

    const form = document.querySelector('#form');
    form.addEventListener('submit', (event) => {
        event.preventDefault();
        const submitForm = async () => {

            if( await validarNumero()) {
                form.submit();
            }
        }
        submitForm();
    });
</script>
