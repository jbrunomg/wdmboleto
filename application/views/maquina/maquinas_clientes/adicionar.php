<div class="panel panel-flat">

    <div class="panel-heading">        
        <h5 class="panel-title">Cadastro de clientes na máquina: <?php echo $this->uri->segment(4) ?> </h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!-- <li><a data-action="reload"></a></li> -->
                <!-- <li><a data-action="close"></a></li> -->
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <!-- tratando div como formulario, para registro de clientes -->
        <form action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarClienteExe" id="form" class="form-horizontal" method="post">
            <fieldset  class="content-group">

                <legend>Dados do cliente</legend>

                <input type="hidden" name="maquinas_clientes_maquina_id" value="<?php echo $this->uri->segment(3) ?>">

                <div class="form-group">
                    <label class="control-label col-lg-2">Nome do cliente</label>
                    <div class="col-lg-5">
                        <select type="text" class="form-control select2-container" name="maquinas_clientes_cliente_id" id="maquinaClienteId">
                            <?php foreach ($dados as $value): ?>
                                <option value="<?php echo $value->cliente_id ?>"><?php echo $value->cliente_nome ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Dia que o cliente recebeu a posse da máquina</label>
                    <div class="col-lg-5">
                        <input type="text" class="form-control classData" placeholder="Data Nascimento" name="maquinas_clientes_data_cadastro" id="maquinaData" value="<?php echo set_value('maquinas_clientes_data_cadastro'); ?>">
                    </div>
                </div>
            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn btn-primary">Cadastrar cliente</button>
            </div>
        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#maquinaClienteId').select2();
    });
</script>