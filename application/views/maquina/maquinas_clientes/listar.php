

<div class="panel panel-flat">
    <div class="panel-heading">
        <div class="col-md-3 col-sm-6 row">
            <a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionarCliente/<?php echo $this->uri->segment(3).'/'.$this->uri->segment(4)  ?>">Adicionar <i class="icon-plus2 position-right"></i></a>
        </div>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!-- <li><a data-action="reload"></a></li> -->
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>
    <div class="panel-body">
        <table class="table datatable-button-html5-columns">
            <thead>
                <tr>
                    <th>Nome</th>
                    <th>Cnpj</th>
                    <th>Contato</th>
                    <th>Email</th>
                    <th>Data de aquisição</th>
                    <th>Possui a maquina</th>
                    <th>Opções</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($dados as $valor): ?>

                <tr class="<?php echo $valor->maquinas_clientes_possui ? 'success text-blue' : '' ?>">
                    <td><?php echo $valor->usuario_nome; ?></td>
                    <td><?php echo $valor->usuario_cpf; ?></td>
                    <td><?php echo $valor->usuario_telefone; ?></td>
                    <td><?php echo $valor->usuario_email; ?></td>
                    <td><?php echo date('d-m-Y',strtotime($valor->maquinas_clientes_data_cadastro)); ?></td>
                    <td><?php echo $valor->maquinas_clientes_possui ? 'tem a maquina' : 'Não possui' ?></td>

                    <td>
                        <ul class="icons-list">
                            <?php if(true): ?>
                                <li class="text-primary-600"><a href="<?php echo base_url()?>maquinas/editarCliente/<?php echo $valor->maquinas_clientes_id ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
                            <?php endif ?>
                            <?php if(true): ?>
                                <li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1) ?>/excluirCliente/" registro="<?php echo $valor->maquinas_clientes_id;?>"><i class="icon-trash"></i></a></li>
                            <?php endif ?>
                            <?php if(true): ?>
                                <li class="text-teal-600"><a href="<?php echo base_url()?>maquinas/visualizarCliente/<?php echo  $valor->usuario_id; ?>/<?php echo $this->uri->segment(3) ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>
                            <?php endif ?>
                        </ul>
                    </td>
                </tr>
            <?php endforeach ?>
            </tbody>
        </table>
    </div>
</div>