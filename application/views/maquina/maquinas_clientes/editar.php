<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Edição do Cliente</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!-- <li><a data-action="reload"></a></li> -->
                <!-- <li><a data-action="close"></a></li> -->
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <form  class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarClienteExe/<?php echo $dados->maquinas_clientes_id ?>" method="post" enctype="multipart/form-data">
            <fieldset class="content-group">

                <input type="hidden" name="maquinas_clientes_maquina_id" value="<?php echo $dados->maquinas_clientes_maquina_id ?>">

                <legend>Dados do cliente</legend>

                <div class="form-group">
                    <label class="control-label col-lg-2">Nome do cliente</label>
                    <div class="col-lg-5">
                        <select type="text" class="form-control select2-container" name="maquinas_clientes_cliente_id" id="maquinaClienteId">
                            <?php foreach ($cliente as $value): ?>
                                    <?php $selecionado = ($dados->maquinas_clientes_cliente_id === $value->cliente_id) ? 'SELECTED' : '' ?>
                                <option value="<?php echo $value->cliente_id ?>" <?php echo $selecionado ?>><?php echo $value->cliente_nome ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Dia que o cliente recebeu a posse da máquina</label>
                    <div class="col-lg-5">
                        <input type="text" class="form-control classData" placeholder="Data Nascimento" name="maquinas_clientes_data_cadastro" id="maquinaData" value="<?php echo date('d-m-Y',strtotime($dados->maquinas_clientes_data_cadastro)) ?>">
                    </div>
                </div>

            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn bg-teal">Atualizar <i class="icon-arrow-right14 position-right"></i></button>
            </div>

        </form>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#maquinaClienteId').select2();
    });
</script>