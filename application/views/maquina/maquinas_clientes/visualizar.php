<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Visualizar maquina e cliente</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!-- <li><a data-action="reload"></a></li> -->
                <!-- <li><a data-action="close"></a></li> -->
            </ul>
        </div>
    </div>

    <div class="panel-body">
        <form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/editarClienteExe/<?php echo $this->uri->segment(3) ?>">
            <fieldset class="content-group">

                <legend>Dados do cliente</legend>
                <div class="form-group">
                    <label class="control-label col-lg-2">Nome</label>
                    <div class="col-lg-5">
                        <input type="text" disabled class="form-control" value="<?php echo  $dados[0]->usuario_nome ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Cnpj</label>
                    <div class="col-lg-5">
                        <input type="text" disabled class="form-control" value="<?php echo  $dados[0]->usuario_cpf ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Contato</label>
                    <div class="col-lg-5">
                        <input type="text" disabled class="form-control" value="<?php  echo $dados[0]->usuario_telefone ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Email</label>
                    <div class="col-lg-5">
                        <input type="text" disabled class="form-control" value="<?php echo  $dados[0]->usuario_email ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Nome</label>
                    <div class="col-lg-5">
                        <input type="text" disabled class="form-control" value="<?php echo  $dados[0]->usuario_nome ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">
                        <?php echo $dados[0]->maquinas_clientes_possui ? 'Possui a maquina desde' : 'Ultimo registro de pose da maquina' ?>
                    </label>
                    <div class="col-lg-5">
                        <input type="text" disabled class="form-control" value="<?php echo  date('d-m-Y', strtotime($dados[0]->maquinas_clientes_data_cadastro)) ?>">
                    </div>
                </div>

                <br>
                <legend>Dados da maquina</legend>
                <div class="form-group">
                    <label class="control-label col-lg-2">Número</label>
                    <div class="col-lg-5">
                        <input type="text" disabled class="form-control" value="<?php echo  $maquina[0]->maquina_numero ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Administradora</label>
                    <div class="col-lg-5">
                        <input type="text" disabled class="form-control" value="<?php echo  $maquina[0]->categoria_cart_nome_credenciadora ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Data de cadastro da maquina</label>
                    <div class="col-lg-5">
                        <input type="text" disabled class="form-control" value="<?php echo  date('d-m-Y', strtotime($maquina[0]->categoria_cart_data_cadastro)) ?>">
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Observação da maquina</label>
                    <div class="col-lg-5">
                        <textarea disabled class="form-control">
                            <?php echo  $maquina[0]->maquina_observacao ?>
                        </textarea>
                    </div>
                </div>

            </fieldset>

        </form>

    </div>

</div>