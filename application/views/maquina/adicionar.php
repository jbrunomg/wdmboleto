<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">Cadastro de máquinas</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <!-- <li><a data-action="reload"></a></li> -->
                <!-- <li><a data-action="close"></a></li> -->
            </ul>
        </div>
    </div>
    
    <div class="panel-body">
        <form class="form-horizontal" id="form" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
            <fieldset class="content-group">

                <legend class="text-bold">Dados da máquina:</legend>

                <div class="form-group">
                    <label class="control-label col-lg-2">Administradora do cartão:</label>
                    <div class="col-lg-5">
                        <select type="text" class="form-control select2-container" name="maquina_categoria_cartao_id" id="cartaoCNPJ" value="<?php echo set_value('maquina_categoria_cartao_id'); ?>" required>
                            <?php foreach ($dados as $value): ?>
                                <option value="<?php echo $value->categoria_cart_id ?>"><?php echo $value->administradora_cartao ?></option>
                            <?php endforeach ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Número da máquina</label>
                    <div class="col-lg-5">
                        <input type="text" class="form-control" placeholder="N° da máquina" name="maquina_numero" id="maquina_numero" value="<?php echo set_value('maquinaNumero') ?>" title="Apenas números são permitidos." required>
                        <div class="form-text text-success" id="numeroTextoConfirmacao"></div>
                        <div class="form-text text-danger" id="numeroTextoError"></div>
                        <div class="form-text text-default">Apenas números são permitidos</div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Observação</label>
                    <div class="col-lg-5">
                        <textarea class="form-control" placeholder="Observação" name="maquina_observacao" id="maquina_observacao" value="<?php echo set_value('maquina_observacao') ?>"></textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-lg-2">Situação</label>
                    <div class="col-lg-5">
                        <select class="form-control" data-placeholder="Situação da maquina" name="maquina_situacao" value="<?php echo set_value('maquina_situacao') ?>">
                            <option value="funcionando">funcionando</option>
                            <option value="quebrado">quebrado</option>
                        </select>
                    </div>
                </div>

                <legend class="text-bold">Dados do usuário</legend>
                    <input  type="text" class="form-control"  name="usuario_nome" id="usuario_nome" value="<?php echo $this->session->userdata('usuario_nome');?>" disabled >
                    <input id="maquina_usuario_id" class="span12" type="hidden" name="maquina_usuario_id" value="<?php echo $this->session->userdata('usuario_id');?>"  />
                    <?php  echo form_error('cliente_nome'); ?>

            </fieldset>

            <div class="text-right">
                <button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
            </div>

        </form>

    </div>

</div>

<script>
    $(document).ready(function () {
        $('#cartaoCNPJ').select2();
    });
</script>

<script>
    //apagando ultimo endereçamento de rota, para restar apenas /maquinas ao final da url
    const urlAtual = window.location.href;
    const ultimaBarraDaUrl = urlAtual.lastIndexOf("/");
    const url = urlAtual.substring(0, ultimaBarraDaUrl + 1);

    const maquinaNumeroInput = document.querySelector('#maquina_numero');
    const maquinaNumeroTextoConfirmacao = document.querySelector('#numeroTextoConfirmacao');
    const maquinaNumeroTextoError = document.querySelector('#numeroTextoError');

    async function validarNumero() {
        let response = false;

        const data = new FormData();
        data.append('numero',maquinaNumeroInput.value);

        await fetch(`${url}verificandoNumeroDaMaquina/`,{
            method: 'POST',
            body: data
        })
            .then(response => response.json())
            .then(data => {
                if(data.success) {
                    maquinaNumeroInput.classList.add('border-success');
                    maquinaNumeroTextoConfirmacao.textContent = 'Número validado com sucesso';
                    console.log('cheguei aqui');
                    response = true;
                } else {
                    maquinaNumeroInput.classList.add('border-danger');
                    maquinaNumeroTextoError.textContent = 'Número já cadastrado';
                }
            })
            .catch(error => console.log(error))
        return response;
    }

    maquinaNumeroInput.addEventListener('blur',() => {
        if(maquinaNumeroInput.value) {
            validarNumero();
        }
    });
    maquinaNumeroInput.addEventListener('input', () => {
        maquinaNumeroInput.classList.remove('border-success');
        maquinaNumeroInput.classList.remove('border-danger');
        maquinaNumeroTextoConfirmacao.textContent = '';
        maquinaNumeroTextoError.textContent = '';
    });

    const form = document.querySelector('#form');
    form.addEventListener('submit', (event) => {
        event.preventDefault();
        const submitForm = async () => {

            if( await validarNumero()) {
                form.submit();
            }
        }
        submitForm();
    });

</script>
