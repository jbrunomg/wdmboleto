<!-- Column selectors -->
<div class="panel panel-flat">
	<div class="panel-heading">
		<?php if(checarPermissao('aArquivo')){ ?>
		<div class="col-md-3 col-sm-6 row">
			<a class="btn bg-teal btn-block" href="<?php echo base_url(); ?><?php echo $this->uri->segment(1); ?>/adicionar">Adicionar <i class="icon-plus2 position-right"></i></a>
		</div>
		<?php } ?>
		<div class="heading-elements">
			<ul class="icons-list">
        		<li><a data-action="collapse"></a></li>
        		<!-- <li><a data-action="reload"></a></li> -->
        		<li><a data-action="close"></a></li>
        	</ul>
    	</div>
	</div>

	<br>
	<table class="table datatable-button-html5-columns">
		<thead>
			<tr>				
				<th>#</th>
                <th>Documento</th>
                <th>Data</th>
                <th>Tamanho/Extensão</th>
                <th>Categoria</th>								
				<th>Opções</th>
			</tr>
		</thead>
		<tbody>
            <?php foreach ($dados as $r) { ?>
                
                <tr>                                    
                    <td><?php echo $r->idDocumentos; ?></td>
                    <td><?php echo $r->documento; ?></td>
                    <td><?php echo date('d/m/Y',strtotime($r->cadastro)); ?></td>
                    <td><?php echo $r->tamanho.'/'.$r->tipo; ?></td>
                    <td><?php echo 'Contrato' ?></td>
                                                                        
                    <td>                                                                                
                            <ul class="icons-list">
                                    <?php if(checarPermissao('eArquivo')){ ?>
                                    <li class="text-primary-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/editar/<?php echo $r->idDocumentos; ?>" data-popup="tooltip" title="Editar"><i class="icon-pencil7"></i></a></li>
                                    <?php } ?>
                                    <?php if(checarPermissao('dArquivo')){ ?>
                                    <li class="text-danger-600"><a href="#" data-popup="tooltip" title="Excluir" class="sweet_loader_id" url="<?php echo $this->uri->segment(1); ?>/excluir" registro="<?php echo $r->idDocumentos; ?>"><i class="icon-trash"></i></a></li>
                                    <?php } ?>
                                    <?php if(checarPermissao('vArquivo')){ ?>
                                    <li class="text-teal-600"><a target="_blank" href="<?php echo $r->url ?>" data-popup="tooltip" title="Visualizar"><i class="icon-search4"></i></a></li>    
                                    <?php } ?>
                                    <?php if(checarPermissao('vArquivo')){ ?>
                                    <li class="text-teal-600"><a href="<?php echo base_url()?><?php echo $this->uri->segment(1); ?>/download/<?php echo $r->idDocumentos; ?>" data-popup="tooltip" title="download"><i class="icon-file-download"></i></a></li>    
                                    <?php } ?>                                                                                
                                
                            </ul>                                                                           
                    </td>                       
                </tr>
            <?php } ?>
		</tbody>
	</table>
</div>
<!-- /column selectors -->