<!-- Form horizontal -->
	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Arquivos</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			<form class="form-horizontal" action="<?php echo base_url();?><?php echo $this->uri->segment(1); ?>/adicionarExe" method="post" enctype="multipart/form-data">
				<fieldset class="content-group">
					<legend class="text-bold">Dados do Arquivo:</legend>

					<input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>" />

		<div class="form-group">
			<label class="control-label col-lg-2">Entrada de Arquivo</label>
			<div class="col-lg-5">
				<input id="arquivo" type="file" name="userfile" class="form-control"> (txt|pdf|gif|png|jpg|jpeg) 
			</div>
		</div>

        <div class="form-group">
          <label class="control-label col-lg-2">Nome da Arquivo:</label>
          <div class="col-lg-5">
            <input  type="text" class="form-control" placeholder="Nome Arquivo" name="documento" id="documento" value="<?php echo set_value('documento'); ?>">
          <?php echo form_error('documento'); ?>
          </div>                    
        </div> 

        <div class="form-group">
          <label class="control-label col-lg-2">Descrição:</label>
          <div class="col-lg-5">
          	<textarea rows="3" cols="63" name="descricao" id="descricao" value="<?php echo set_value('descricao'); ?>">
          	<?php echo form_error('descricao'); ?>	
          	</textarea>            
          
          </div>                    
        </div> 

        <div class="form-group">
			<label class="control-label col-md-2">Data:</label>
			<div class="col-md-5">
				<input class="form-control" type="date" name="cadastro" id="cadastro">				
			</div>
		</div>  
               		

		</fieldset>
				<div class="text-right">
					<button type="submit" class="btn bg-teal">Cadastrar <i class="icon-arrow-right14 position-right"></i></button>
				</div>
			</form>
		</div>
	</div>
